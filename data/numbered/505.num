EUROPEAN PARLIAMENT
1999
��� � � � � � � � ��

2004

Session document

C5-0080/2003
2001/0199(COD)

EN
12/03/2003

Common position
with a view to the adoption of a Directive of the European Parliament and of the Council amending Directive 2000/13/EC as regards indication of the ingredients present in foodstuffs

Doc. 15514/2/02 + 5757/1/03 Statements (13913/02) SEC(2003)0252

EN

EN



COUNCIL OF THE EUROPEAN UNION

Brussels, 20 February2003 (OR. en) 15514/2/02

Interinstitutional File: 2001/0199 (COD)

REV 2

DENLEG 88 CODEC 1636

LEGISLATIVE ACTS AND OTHER INSTRUMENTS Subject : Common Position adopted by the Council on 20 February 2003 with a view to the adoption of a Directive of the European Parliament and of the Council amending Directive 2000/13/EC as regards indication of the ingredients present in foodstuffs

15514/2/02 REV 2 DG I

SW/mmk

EN


DIRECTIVE 2003/

/EC OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL of amending Directive 2000/13/EC as regards indication of the ingredients present in foodstuffs (Text with EEA relevance)

THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION, Having regard to the Treaty establishing the European Community, and in particular Article 95 thereof, Having regard to the proposal from the Commission 1, Having regard to the Opinion of the European Economic and Social Committee 2, Acting in accordance with the procedure laid down in Article 251 of the Treaty 3,

1 2 3

OJ C 332 E, 27.11.2001, p. 257. OJ C 80, 3.4.2002, p. 35. Opinion of the European Parliament of 11 June 2002 (not yet published in the Official Journal), Council Common Position of (not yet published in the Official Journal) and Decision of the European Parliament of (not yet published in the Official Journal). SW/mmk DG I

15514/2/02 REV 2

EN

1


Whereas: (1) In order to achieve a high level of health protection for consumers and to guarantee their right to information, it must be ensured that consumers are appropriately informed as regards foodstuffs, inter alia through the listing of all ingredients on labels. (2) By virtue of Article 6 of Directive 2000/13/EC of the European Parliament and of the Council of 20 March 2000 on the approximation of the laws of the Member States relating to the labelling, presentation and advertising of foodstuffs 1, certain substances need not appear in the list of ingredients. (3) When used in the production of foodstuffs and still present, certain ingredients or other substances are the cause of allergies or intolerances in consumers, and some of those allergies or intolerances constitute a danger to the health of those concerned. (4) The Scientific Committee on Food set up by Article 1 of Commission Decision 97/579/EC 2 has stated that the incidence of food allergies is such as to affect the lives of many people, causing conditions ranging from very mild to potentially fatal. (5) The said Committee has acknowledged that common food allergens include cow's milk, fruits, legumes (especially peanuts and soybeans), eggs, crustaceans, tree nuts, fish, vegetables (celery and other foods of the Umbelliferae family), wheat and other cereals.

1 2

OJ L 109, 6.5.2000, p. 29. Directive as amended by Commission Directive 2001/101/EC (OJ L 310, 28.11.2001, p. 19). OJ L 237, 28.8.1997, p. 18. Decision as amended by Decision 2000/443/EC (OJ L 179, 18.7.2000, p. 13). SW/mmk DG I

15514/2/02 REV 2

EN

2


(6) (7)

The most common food allergens are found in a wide variety of processed foods. The said Committee has also noted that adverse reactions to food additives may occur and that the avoidance of food additives is often difficult since not all of them are invariably included on the labelling.

(8)

It is necessary to provide that additives, processing aids and other substances with allergenic effect covered by Article 6(4)(a) of Directive 2000/13/EC are subject to labelling rules, to give appropriate information to consumers suffering from food allergy.

(9)

Even if labelling, which is intended for consumers in general, is not to be regarded as the only medium of information acting as substitute for the medical establishment, it is nevertheless advisable to assist consumers who have allergies or intolerances as much as possible by providing them with more comprehensive information on the composition of foodstuffs.

(10)

The list of allergenic substances should include those foodstuffs, ingredients and other substances recognised as causing hypersensitivity

(11)

In order to provide all consumers with better information and to protect the health of certain consumers, it should be made obligatory to include in the list of ingredients all ingredients and other substances present in the foodstuff. In the case of alcoholic beverages, it should be mandatory to include in the labelling all ingredients with allergenic effect present in the beverage concerned.

15514/2/02 REV 2 DG I

SW/mmk

EN

3


(12)

In order to take account of the technical constraints involved in the manufacture of foodstuffs, it is necessary to authorise greater flexibility with regard to the listing of ingredients and other substances used in very small quantities.

(13)

In order to keep up with the development of scientific knowledge and progress as regards technological means of removing the allergenicity in ingredients and other substances and in order to protect consumers against new food allergens and avoid unnecessary obligations on labelling, it is important to be able to revise the list of ingredients rapidly, when necessary by including or deleting certain ingredients or substances. The revision should be based on scientific criteria determined by the European Food Safety Authority set up by Regulation (EC) No 178/2002 of the European Parliament and of the Council 1 and take the form of implementing measures of a technical nature, the adoption of which should be entrusted to the Commission in the interest of simplifying and accelerating the procedure. Furthermore, the Commission should, if necessary, draw up technical guidelines for the interpretation of Annex IIIa.

(14)

Directive 2000/13/EC should therefore be amended accordingly,

HAVE ADOPTED THIS DIRECTIVE:

1

OJ L 31, 1.2.2002, p. 1. SW/mmk DG I

15514/2/02 REV 2

EN

4


Article 1 Directive 2000/13/EC is hereby amended as follows: 1) Article 6 shall be amended as follows: (a) paragraph 1 shall be replaced by the following: "1. Ingredients shall be listed in accordance with this Article and Annexes I, II, III and IIIa."; (b) the following paragraph shall be inserted: "3a. Without prejudice to the rules for labelling to be established pursuant to paragraph 3, any ingredient, as defined in paragraph 4(a) and listed in Annex IIIa, shall be indicated on the labelling where it is present in beverages referred to in paragraph 3. This indication shall comprise the word "contains" followed by the name of the ingredient(s) concerned. However, an indication is not necessary when the ingredient is already included under its specific name in the list of ingredients or in the name under which the beverage is sold. Where necessary, detailed rules for the presentation of the indication referred to in the first subparagraph may be adopted in accordance with the following procedures:

15514/2/02 REV 2 DG I

SW/mmk

EN

5


(a)

as regards the products referred to in Article 1(2) of Council Regulation (EC) No 1493/99 of 17 May 1999 on the common organisation of the market in wine *, under the procedure laid down in Article 75 of that Regulation;

(b)

as regards the products referred to in Article 2(1) of Council Regulation (EEC) No 1601/91 of 10 June 1991 laying down general rules on the definition, description and presentation of aromatised wines, aromatised wine-based drinks and aromatised wine-product cocktails **, under the procedure laid down in Article 13 of that Regulation;

(c)

as regards the products referred to in Article 1(2) of Council Regulation (EEC) No 1576/89 of 29 May 1989 laying down general rules on the definition, description and presentation of spirit drinks ***, under the procedure laid down in Article 14 of that Regulation;

(d)

as regards other products, under the procedure laid down in Article 20(2) of this Directive.

����������������������� * OJ L 179, 14.7.1999, p. 1. Regulation as last amended by Regulation (EC) No 2585/2001 (OJ L 345, 29.12.2001, p. 10). ** OJ L 149, 14.6.1991, p. 1. Regulation as last amended by Regulation (EC) No 2061/96 of the European Parliament and of the Council (OJ L 277, 30.10.1996, p. 1). *** OJ L 160, 12.6.1989, p. 1. Regulation as last amended by Regulation (EC) No 3378/94 of the European Parliament and of the Council (OJ L 366, 31.12.1994, p. 1).";

15514/2/02 REV 2 DG I

SW/mmk

EN

6


(c)

the following point shall be added to paragraph 4(c): "(iv) substances which are not additives but are used in the same way and with the same purpose as processing aids and are still present in the finished product, even if in altered form.";

(d)

the second subparagraph of paragraph 5 shall be amended as follows: (i) the fourth indent shall be replaced by the following: "� where fruit, vegetables or mushrooms, none of which significantly predominates in terms of weight and which are used in proportions that are likely to vary, are used in a mixture as ingredients of a foodstuff, they may be grouped together in the list of ingredients under the designation "fruit", "vegetables" or "mushrooms" followed by the phrase "in varying proportions", immediately followed by a list of the fruit, vegetables or mushrooms present; in such cases, the mixture shall be included in the list of ingredients in accordance with the first subparagraph, on the basis of the total weight of the fruit, vegetables or mushrooms present,";

15514/2/02 REV 2 DG I

SW/mmk

EN

7


(ii)

the following indents shall be added: "� ingredients constituting less than 2% of the finished product may be listed in a different order after the other ingredients, � where ingredients which are similar or mutually substitutable are likely to be used in the manufacture or preparation of a foodstuff without altering its composition, its nature or its perceived value, and in so far as they constitute less than 2% of the finished product, they may be referred to in the list of ingredients by means of the phrase "contains ... and/or ...", where at least one of no more than two ingredients is present in the finished product. This provision shall not apply to additives or to ingredients listed in Annex IIIa.";

(e)

the second subparagraph of paragraph 8 shall be replaced by the following: "The list referred to in the first subparagraph shall not be compulsory: (a) where the composition of the compound ingredient is defined in current Community legislation, and in so far as the compound ingredient constitutes less than 2% of the finished product; however, this provision shall not apply to additives, subject to paragraph 4(c),

15514/2/02 REV 2 DG I

SW/mmk

EN

8


(b)

for compound ingredients consisting of mixtures of spices and/or herbs that constitute less than 2% of the finished product, with the exception of additives, subject to paragraph 4(c),

(c)

where the compound ingredient is a foodstuff for which a list of ingredients is not required under Community legislation.";

(f)

the following paragraphs shall be added: "10. Notwithstanding paragraph 2, the second subparagraph of paragraph 6 and the second subparagraph of paragraph 8, any ingredient used in production of a foodstuff and still present in the finished product, even if in altered form, and listed in Annex IIIa or originating from an ingredient listed in Annex IIIa shall be indicated on the label with a clear reference to the name of this ingredient. The indication referred to in the first subparagraph shall not be required if the name under which the foodstuff is sold clearly refers to the ingredient concerned. Notwithstanding paragraph 4(c)(ii), (iii) and (iv), any substance used in production of a foodstuff and still present in the finished product, even if in altered form, and originating from ingredients listed in Annex IIIa shall be considered as an ingredient and shall be indicated on the label with a clear reference to the name of the ingredient from which it originates.

15514/2/02 REV 2 DG I

SW/mmk

EN

9


11. The list in Annex IIIa shall be systematically re-examined and, where necessary, updated on the basis of the most recent scientific knowledge. The first re-examination shall take place at the latest on ..... . Updating could also be effected by the deletion from Annex IIIa of ingredients for which it has been scientifically established that it is not possible for them to cause adverse reactions. To this end, Annex IIIa may be amended, in compliance with the procedure referred to in Article 20(2), after an opinion has been obtained from the European Food Safety Authority issued on the basis of Article 29 of Regulation (EC) No 178/2002 *. Where necessary, technical guidelines may be issued for the interpretation of the list in Annex IIIa, in compliance with the procedure referred to in Article 20(2). ������������������� * OJ L 31, 1.2.2002, p. 1.";



Two years after the entry into force of this Directive.

15514/2/02 REV 2 DG I

SW/mmk

EN

10


2)

in the second subparagraph of Article 19, "Standing Committee on Foodstuffs" set up by Council Decision 69/414/EEC (1)" shall be replaced by "Standing Committee on the Food Chain and Animal Health" set up by Regulation (EC) No 178/2002;

3) 4)

the footnote, "OJ L 291, 29.11.1969, p. 9", shall be deleted; in Article 20(1) "Standing Committee on Foodstuffs" shall be replaced by "Standing Committee on the Food Chain and Animal Health";

5)

in Annex I, the designations "crystallised fruit" and "vegetables", and the corresponding definitions, shall be deleted;

6)

Annex IIIa, the text of which is set out in the Annex to this Directive, shall be inserted. Article 2

1. Member States shall bring into force, by ............... the laws, regulations and administrative provisions necessary to: - - permit, as from ......................... , the sale of products that comply with this Directive; prohibit, as from ......................... , the sale of products that do not comply with this Directive; any products which do not comply with this Directive but which have been placed on the market or labelled prior to this date may, however, be sold while stocks last.

 

12 months from the date of entry into force of this Directive. 24 months from the date of entry into force of this Directive. SW/mmk DG I

15514/2/02 REV 2

EN

11


They shall forthwith inform the Commission thereof. When Member States adopt those measures, they shall contain a reference to this Directive or be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made. 2. Member States shall communicate to the Commission the text of the main provisions of national law which they adopt in the field covered by this Directive. Article 3 This Directive shall enter into force on the day of its publication in the Official Journal of the European Communities. Article 4 This Directive is addressed to the Member States. Done at Brussels, For the European Parliament The President For the Council The President

������������������������

15514/2/02 REV 2 DG I

SW/mmk

EN

12


ANNEX "ANNEX IIIa Ingredients referred to in Article 6(3a), (10) and (11) Cereals containing gluten ( i.e. wheat, rye, barley, oats, spelt, kamut or their hybridised strains) and products thereof Crustaceans and products thereof Eggs and products thereof Fish and products thereof Peanuts and products thereof Soybeans and products thereof Milk and products thereof (including lactose) Nuts i. e. Almond (Amygdalus communis L.), Hazelnut (Corylus avellana), Walnut (Juglans regia), Cashew (Anacardium occidentale), Pecan nut (Carya illinoiesis (Wangenh.) K. Koch), Brazil nut (Bertholletia excelsa), Pistachio nut (Pistacia vera), Macadamia nut and Queensland nut (Macadamia ternifolia) and products thereof Celery and products thereof Mustard and products thereof Sesame seeds and products thereof Sulphur dioxide and sulphites at concentrations of more than 10 mg/kg or 10 mg/litre expressed as SO2."

____________

15514/2/02 REV 2 ANNEX

SW/mmk DG I

EN

1


COUNCIL OF THE EUROPEAN UNION

Brussels, 20 February 2003

Interinstitutional File: 2001/0199(COD)

15514/2/02 REV 2 ADD 1

DENLEG 88 CODEC 1636

STATEMENT OF THE COUNCIL'S REASONS Subject : Common Position adopted by the Council on 20 February 2003 with a view to the adoption of a Directive of the European Parliament and of the Council amending Directive 2000/13/EC as regards indication of the ingredients present in foodstuffs

STATEMENT OF THE COUNCIL'S REASONS

15514/2/02 REV 2 ADD 1

LL/am DG I

EN

1


I.

INTRODUCTION 1. On 7 September 2001 the Commission submitted to the Council a proposal for a Directive of the European Parliament and of the Council, based on Article 95 of the Treaty, Council amending Directive 2000/13/EC as regards indication of the ingredients present in foodstuffs 1. 2. 3. 4. The Economic and Social Committee delivered its Opinion on 16 January 2002 2. The European Parliament adopted a number of amendments on 11 June 2002 3. Following the European Parliament opinion, the Commission forwarded an amended proposal 4 to the Council on 4 September 2002. 5. On 20 February 2003, the Council adopted its Common Position in accordance with Article 251 of the Treaty.

II.

OBJECTIVE In the White Paper on Food Safety, the Commission had announced the intention to propose an amendment to the Directive 2000/13/EC, especially with regard to the possibility of not indicating the components of compound ingredients which form less than 25% of the final product. This possibility was introduced into Community legislation more than 20 years ago with the objective of avoiding inordinately long lists of ingredients. However, as food production has become more complex and the consumption of processed foods more widespread, consumers have repeatedly expressed the wish to be better informed about the foodstuffs they purchase, and specifically about their composition.

1 2 3 4

OJ C 332 E of 27.11.2001, p. 257. OJ C 80 of 03.04.2002, p. 35. Doc. 9840/02. OJ C 331 E of 31.12.2002, p. 188. LL/am DG I

15514/2/02 REV 2 ADD 1

EN

2


Moreover, the need was recognised to address the problem of food allergies, an issue which comes under food safety and health, by providing the consumers with information on the presence of allergenic ingredients. The proposal aims therefore at providing the consumer with more comprehensive information about the composition of food products and alcoholic beverage, particularly in order to make obligatory listing ingredients susceptible to cause allergies or intolerances. III. ANALYSIS OF THE COMMON POSITION A. General remarks related to the European Parliament's amendments 1. The Council has followed the Commission's amended proposal. It has accepted � either word by word or in substance � the four Parliament's amendments as set out by the Commission in its amended proposal. 2. The Council has also taken into account most of the other European Parliament amendments. a) Three other European Parliament amendments (i.e. amendments 8, 9 and 14), which were not incorporated into the Commission's amended proposal, have been accepted by the Council; b) As regards two other amendments (i.e. amendments 5 and 13), the Council, rather than deleting the relevant provision, has preferred to limit the derogation by lowering the level below which the derogation applies from 5 to 2%. 3. As regards amendment 10, the Council, like the Commission, considers that this question could be raised in the context of the future updating of the list.

15514/2/02 REV 2 ADD 1

LL/am DG I

EN

3


B.

Main innovations introduced by the Council 1. The main innovations introduced by the Council relate to: � coverage of certain substances which are not additives but are used in the same way and with the same purpose as processing aids and are still present in the finished product (cf. proposal for a new paragraph 4(c) (iv) of Article 6 in Directive 2000/13/EC), � obligation to indicate the ingredients originating from an ingredient listed in Annex IIIa with a clear reference to the name of this ingredient's, unless the name under which the foodstuff is sold clearly refers to the ingredient concerned (cf. proposal for a new paragraph 10 of Article 6 in Directive 2000/13/EC). 2. Other modifications have been made, of a purely technical nature and aiming at clarifying the text of the Directive.

IV.

CONCLUSIONS The Council considers that the Common Position responds to a large extent to the main wishes expressed by the European Parliament, while at the same time taking sufficient account of the concern expressed by Member States, inter alia with regard to public health and/or Community harmonisation. The Council considers that the Common Position achieves a good balance between the prerequisites for the proper functioning of the single market and consumer protection/information.

15514/2/02 REV 2 ADD 1

LL/am DG I

EN

4


COUNCIL OF THE EUROPEAN UNION

Brussels, 10 February 2003

5757/1/03 REV 1 Interinstitutional File: 2001/0199 (COD) DENLEG 3 CODEC 88 OC 2 "I/A" ITEM NOTE from : General Secretariat to : Coreper/Council No. prev. doc. : 15514/02 DENLEG 88 CODEC 1636 No. Cion prop. : 11745/02 DENLEG 55 CODEC 1030 - COM(2002) 464 final Subject : Common position adopted by the Council with a view to the adoption of a Directive of the European Parliament and of the Council amending Directive 2000/13/EC as regards indication of the ingredients present in foodstuffs COMMON GUIDELINES Consultation deadline: 11.02.2003

1.

At its meeting on 14 and 15 November 2002, the Council reached political agreement by qualified majority, the Austrian delegation voting against, with a view to the adoption of a common position on the above Directive.

2.

As the text agreed, including its preamble, has undergone legal/linguistic finalisation, the Permanent Representatives Committee could request the Council: to adopt a common position on the above Directive as set out in 15514/02 DENLEG 88 CODEC 1636 + REV 1 (nl) + COR 1 (fi) + ADD 2; to enter in its minutes the statements as contained in 13913/02 DENLEG 78 CODEC 1390 ADD 1.

5757/1/03 REV 1 DG I

LL/cm

EN

1


The statement of the Council's reasons is contained in 15514/02 DENLEG 88 CODEC 1636 ADD 1 + ADD 2.

5757/1/03 REV 1 DG I

LL/cm

EN

2


COMMISSION OF THE EUROPEAN COMMUNITIES

Brussels, 5.3.2003 SEC(2003) 252 final 2001/0199 (COD)

COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT pursuant to the second subparagraph of Article 251 (2) of the EC Treaty concerning the common position of the Council on the adoption of a directive of the European Parliament and the Council amending Directive 2000/13/EC as regards indication of the ingredients present in foodstuffs


2001/0199 (COD) COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT pursuant to the second subparagraph of Article 251 (2) of the EC Treaty concerning the common position of the Council on the adoption of a directive of the European Parliament and the Council amending Directive 2000/13/EC as regards indication of the ingredients present in foodstuffs

1-

BACKGROUND 6 September 2001 16 January 2002 11 June 2002 3 September 2002 20 February 2003

Date of forwarding of the proposal to the EP and Council (document COM(2001) 433 final � 2001/0199 (COD)): Date of the opinion of the European Economic and Social Committee: Date of the opinion of the European Parliament (first reading): Date of forwarding of the amended proposal (document COM(2002) 464 Final � 2001/0199 (COD)): Date of adoption of the common position: 2AIM OF THE COMMISSION'S PROPOSAL

The Commission's proposal is intended to make far more comprehensive labelling of foodstuff ingredients compulsory in order to ensure that consumers are better informed and, at the same time, to help solve the problem of food allergies and intolerances. To this end, it amends the rules for labelling ingredients laid down in Directive 2000/13/EC and mainly comprises the following points: � It does away with the exemption from labelling of components of compound ingredients accounting for less than 25% of the finished product, which, in turn, establishes the principle that all the ingredients should be identified. � It upholds a limited number of exemptions for some compound ingredients used in small quantities. � It draws up a list of ingredients causing the majority of food allergies or intolerances, for which no labelling exemption is permitted, even when these ingredients are present in alcoholic beverages. � It provides for a procedure for updating the above-mentioned list after an opinion has been obtained from the European Food Safety Authority.

2


� As regards labelling ingredients which are used in small quantities, it makes provisions for measures to enable manufacturers to comply more easily with the additional labelling requirements. 33.1. COMMENTS ON THE COMMON POSITION General remark

The common position adopted by the Council by a qualified majority represents a balanced compromise, including the majority of the measures proposed by the Commission whilst restricting the scope of the exemptions provided for, and is in keeping with the requests for amendments made by the European Parliament at first reading in order to increase the accuracy of the information which foodstuff labels must carry. 3.2. Amendments made by the European Parliament at first reading

Amendments incorporated in the Commission's amended proposal: The Commission has included in its amended proposal the amendments adopted at first reading by the European Parliament and accepted by the Commission. All these amendments appear in the common position in a form which should satisfy the European Parliament. Differences between the common position and the amended proposal: � Exemptions from the obligation to indicate the components of certain compound ingredients (ingredients whose composition is governed by Community legislation in force) and to comply with the presentation requirements for the list of ingredients are allowed only for ingredients accounting for less than 2% of the finished product, instead of the 5% proposed by the Commission. This compromise stems from the European Parliament's wish to dispense with the exemptions in question in order to provide consumers with more detailed information and affords a certain amount of flexibility, which the Commission advocates to enable manufacturers to meet the much more stringent foodstuff labelling requirements. � It is no longer possible not to repeat an ingredient as long as the label carries an explanatory note. This was in response to the European Parliament's request. The Commission initially upheld this measure but, given the practical difficulties in applying it, accepts its being deleted. � The list of allergenic ingredients should be reviewed and updated systematically to reflect scientific advances and not just every two years. Moreover, when the list is updated, ingredients may be taken off it if there is scientific evidence that they are no longer allergenic. The Commission also accepted the request of several Member States to make a declaration on this matter. This declaration is attached. The Commission endorses this approach which establishes the principle that the list should be revised whenever it is necessary and is also in keeping with that advocated by the European Parliament, whose amendment could not be accepted in its original form. � Celery and mustard and their derived products are added to the list of allergenic substances. This is also in line with the European Parliament's request.

3


The Commission takes the view that any addition to the list must be based on the opinion of the Scientific Committee on Food, which it has asked to recommend suitable criteria. However, it accepts the addition of the two ingredients mentioned above, since all the Member States report an increase in cases of allergies to these foodstuffs. 4CONCLUSIONS

For the above reasons, the Commission supports the common position adopted by the Council. 5COMMISSION DECLARATIONS

At the request of several delegations in the Council, the Commission has made the following declarations: � Declaration on the opinion requested from the Scientific Committee on Food. � Declaration on the definition of Community legislation under Article 1(1)(d) of the common position. These declarations are attached to this Communication.

4


ANNEX COMMISSION DECLARATIONS 1. The Commission declares that it has already asked for the opinion of the Scientific Committee on Food with a view to adapting Annex I (which will become Annex IIIa of Directive 2000/13/EC) as soon as possible, if this is justified from a scientific point of view. This request also covers all the ingredients listed in Annex IIIa, including products derived from cereals, and the Scientific Committee on Food is asked to say whether it is possible for certain derived ingredients to have lost their allergenic effect as a result of the production process or being incorporated in certain foods and whether these ingredients can be removed from Annex IIIa and hence be exempt from the labelling requirement. The Commission declares that the "Community legislation" referred to in Article 1(1)(d) is the legislation laying down harmonised standards for the composition of certain foodstuffs. The texts currently covered by this definition are the directives on : � � � cocoa and chocolate products, fruit juices and certain similar products, fruit preserves, jellies and marmalades.

2.

5


