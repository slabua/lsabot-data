EUROPEAN PARLIAMENT
DIRECTORATE-GENERAL FOR RESEARCH DIRECTORATE A DIVISION FOR INTERNATIONAL AND CONSTITUTIONAL AFFAIRS

FACTSHEET
MALTA 1. The acquis 1 The acquis covers the four broad areas of mutual recognition of professional qualifications, citizens' rights, free movement of workers and coordination of social security schemes: � In the politically sensitive area of free movement of workers the EU took the initiative of itself proposing a transitional measure for all the candidates except Cyprus and Malta. This involves the present system of work permits being maintained for a number of years after accession with a gradual transition towards free movement as was the case in previous enlargements in respect of certain other new Member States. As regards mutual recognition of professional qualifications, a particular problem in respect of some countries has been how to treat qualifications of citizens of candidate countries who hold qualifications from third countries because they completed their education at a time when certain candidate countries were part of the Soviet Union or Yugoslavia. The notion of a declaration of equivalence issued by the relevant bodies in the candidate countries together with an attestation in respect of the specific holder of a qualification is central to the approach adopted by the EU, as are tough monitoring provisions, in particular for the sectoral directives . In the area of citizens' rights, implementation of the directives on voting rights may require constitutional amendments in some countries and alignment with the acquis on residence rights will only be fully possible on accession. Coordination of social security is governed by regulations which will be directly applicable upon accession. The main challenge here is one of administrative structures and capacity. Candidate countries have acquired some experience in implementing existing bilateral agreements with Member States. Chapter 2 - Freedom of movement for persons

�

� �

1

Information largely drawn from the European Commission, DG Enlargement http://europa.eu.int/comm/enlargement/negotiations/index.htm A practical guide to free movement for persons in an enlarged Union has been published by the European Commission on its enlargement website at: http://europa.eu.int/comm/enlargement/negotiations/chapters/chap2/55260_practica_guide_including_comments. pdf

WIP/2002/11/0194

1


2. The negotiations The chapter has been closed with ten countries and provisionally closed with Bulgaria, while it remains open with Romania. Concerns about the risk of disturbances to the labour market in certain Member States and the potential impact on public opinion led the EU to request a transition period for free movement of workers in respect of all the negotiating countries except Cyprus and Malta. A seven-year safeguard clause has been agreed with Malta allowing it to have recourse to Community institutions if difficulties arise on its labour market following accession. Chapter opened: June 2001 Status: closed in December 2002 (provisionally closed in June 2001) Transitional arrangements: � safeguard in respect of free movement of workers . The EU has not requested a transition period in relation to Malta and Cyprus. However, Malta is concerned that its labour market could come under pressure following accession and so a safeguard clause has been agreed, which will run for 7 years. A joint declaration is also to be attached to the Act of Accession allowing for recourse by Malta to Community Institutions, should Malta's accession give rise to difficulties in relation to free movement of workers. 3. Position of the European Parliament In its resolution of 5 September 20011 Parliament notes that, from the EU's point of view, the question of free movement of persons and workers is not really a problem, given the limited number of inhabitants, but that, in view of its geographical position, Malta's reservations should be taken into account; notes, further, that Malta has expressed the need for a safeguard mechanism to be adopted on the freedom of movement of workers taking into consideration the disruption of the labour market in Malta in the event of a high inflow of workers following accession. 4. Latest assessment of the European Commission
2

In the 1999 update of its Opinion, the Commission concluded that the right of EU citizens to enter, reside and obtain employment in Malta was still partially restricted and that the necessary actions would have to be taken to ensure effective alignment with the acquis. It stated that some adaptation was still necessary in the area of mutual recognition of professional qualifications and that the formalities needed to practise regulated professions had to be made considerably lighter in the context of provision of services. Since the 1999 update of the Opinion, Malta has made progress in the area of mutual recognition of professional qualifications and little progress in other areas. Malta's alignment with the acquis on mutual recognition of professional qualifications is now rather advanced (the newly adopted legislation needs however further assessment) but the alignment with the
1
2

Resolution on the state of the negotiations with Malta, � 19: A5-0262/2001 European Commission, Regular Report on Malta 2002, p.43: http://www.europa.eu.int/comm/enlargement/report2002/ml_en.pdf

WIP/2002/11/0194

2


acquis on citizens' rights and free movement of workers is still low. While the overall administrative structure seems to be adequate in the area of mutual recognition of professional qualifications, further strengthening of the capacity to enforce the acquis is needed as regards co-ordination of social security systems and participation in EURES. Negotiations on this chapter have been provisionally closed. Malta has been granted a safeguard clause as regards the free movement of workers (until seven years after accession). Malta is generally meeting the commitments it has made in the accession negotiations in this field. However, delays have occurred in implementing commitments regarding in particular the citizens' rights and the free movement of workers. These issues must be addressed. In order to complete preparations for membership, Malta's efforts now need to focus on completing legislative alignment as regards mutual recognition of the healthcare professional qualifications, in the areas of citizens' rights and the free movement of workers, and on further strengthening its capacity to enforce the acquis as regards co-ordination of social security systems and participation in EURES.

January 2003

WIP/2002/11/0194

3


