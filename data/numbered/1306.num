ACP-EU JOINT PARLIAMENTARY ASSEMBLY
Committee on Social Affairs and the Environment

ACP-EU 3640/B/03

24 November 2003

DRAFT REPORT
on poverty diseases and reproductive health in ACP countries in the context of the 9th EDF Co-Rapporteurs: Karin Scheele Aime Fran�ois Betkou (Madagascar)

Explanatory Statement

PR\514370EN.doc

APP/3640/B


EXPLANATORY STATEMENT Poverty and Health Poor health diminishes personal capacity, lowers productivity and reduces earnings, hence contributing to poverty. A high prevalence of disease and poor health in a country harms economic performance while higher life expectancy, a key indicator of health status, stimulates economic growth. In this regard, assessment of poverty, require a combination of income-based, indicator-based and participatory�based information, which will lead to appropriate development policy formulation. Investment in basic health services in developing countries is only a fraction of what is needed. Low-income developing countries spend only $21 per capita per year on health care, much of it for expensive curative services rather than basic prevention and care. The World Health Organisation (WHO)/World Bank Commission on Macro-economics and Health estimated that an additional $30 billion per year is needed. Poverty-Related Diseases Poverty-related diseases (PRDs) are the major cause as well as the consequence of considerable poverty in Developing Countries such as ACP member Countries, in particular Sub-Saharan Africa. The burden of PRDs, especially HIV/AIDS Malaria, tuberculosis (TB) and infectious, diarrhoeal and skin diseases is mostly borne by the Least Developed Countries (LDCs). The annual global death toll of these three diseases approaches 6 million people and, in some African countries, HIV has infected 40 percent of the entire population. The fight against these diseases is one of the key strategies to eradicate poverty and promote economic growth in developing countries. HIV/AIDS posses a great threat to development in ACP Countries and the impact is hardest in the Least Developed ACP member Countries. According to the UNFPA 2002 report, one half of the new infections are among young people aged 15-24, many of whom have no information or prevention services and are still ignorant about the epidemic and how to protect themselves. HIV/AIDS is also contributing to the slowing of economic growth and activity in the worst affected countries, as the productive human resource fall prey to the infection. The needed effective strategies to combat the HIV/AIDS epidemic involve a combination of treatment, education, and prevention. Such strategies need to go beyond medicine and health care and reach the local communities. Above all, committed political leadership in this fight is a necessity. Combating Poverty-related Diseases (PRDs) The sixth Millennium Development Goal is devoted to combating HIV/AIDS, malaria and other diseases. It provides specific targets for HIV/AIDS and malaria incidence and it also refers PR\514370EN.doc 2/7 APP/3640/B

EN


prominently to tuberculosis. These three diseases are also the focus of a major new multi-agency initiative, the Global Fund for HIV/AIDS, Tuberculosis, and Malaria. At the Second Ordinary Session of their Assembly held in Maputo, Mozambique, 10-12 July 2003, the Africa Union Heads of State and Government devoted a special session to review and debate the current status of HIV/AIDS, Tuberculosis (TB), Malaria and Other Related Infectious Diseases (ORID) in Africa. They reaffirmed their commitment to achieving the goals they set concerning health sector financing in their States and recommitted themselves to meet the target of 15% of national budget to be allocated to health. For sustainability, initiatives to combat PRDs in ACP Countries need to be well integrated in the countries' National Indicative Programmes (NIPs) and Regional Indicative Programmes (RIPs) and social sector of intra-ACP Cooperation of the 9th EDF. The activities in these initiatives should be designed in such away that they target disadvantaged and vulnerable groups such as women, youth and children through: Information, education and communication programmes taking into account the social, health , economic and cultural dimensions of the populations; The development of services that will improve access to primary care; Policy formulation and identification of priorities giving political patronage in combating PRDs; Enhancing collaboration with development partners and international organisations dedicated to fight PRDs, to facilitate access to affordable needed drugs and health products as well as good drinking water; and Support to research and development addressing the relevant issues of PRDs in ACP countries.

International Initiatives The Global Health Sector Strategy for HIV/AIDS 2003 � 2007 of the World Health Organisation (WHO), underlines the wealth of knowledge and experience amassed over 20 years of global effort in responding to HIV/AIDS. That is, the existence of a great deal of knowledge about how to prevent HIV infection and about the factors that fuel its spread. From the lessons learned, the following are some of the effective interventions in the fight against HIV/AIDS: � � � � � Strong government leadership generates the most effective national responses to HIV/AIDS; Investing in prevention, treatment and care now avoids far heavier human and financial costs in the future; Wide-ranging public-information campaigns (which include frank discussion of sexual behaviour and drug use) help counter denial and lead to reduced levels of HIV infection; Making condoms, sterile injecting equipment and other commodities widely available reduces risk and results in lower infection rates; Strong control programmes for Sexually Transmitted Infections (STIs) result in fewer HIV infections; 3/7 APP/3640/B

PR\514370EN.doc

EN


� � � � �

Rational and effective use of antiretrovirals and other HIV-related treatments produces striking reductions in HIV/AIDS-related mortality and morbidity; National HIV/AIDS strategic plans help generate effective national and multisectoral responses and optimise the use of human and financial resources; Epidemiological and behavioural data are required to inform development and monitoring of national strategic plans for HIV/AIDS; Meaningful partnerships between governments, health professions, people living with HIV/AIDS, vulnerable groups, local communities and nongovernmental organisations result in strong national and local responses; and Laws and policies that counter stigma and discrimination against people living with HIV/AIDS and vulnerable populations reduce the negative impact of the pandemic and enhance prevention, health-promotion, treatment and care efforts.

To effect all these, it is important to recognise barriers such as lack of education, inaccessibility of treatments, gender inequality, negative cultural attitudes, stigma and discrimination, all which need to be strongly countered. A number of these issues are also key in combating other PRDs other than HIV/AIDS. Like other developing countries, many of the ACP Countries are in urgent need of access to affordable essential medicines for the treatment of communicable diseases, and are heavily dependent on imports of such medicines. In this connection, on 26 May 2003, the Council of the European Union (EU) adopted a Regulation (EC No. ) aimed at encouraging the pharmaceutical industry to offer essential medicines against HIV/AIDS, malaria and tuberculosis at reduced prices in favour of the poorest countries. The Regulation also provides for safeguard measures to avoid trade distortions that would be caused by the re-import of such medicines onto the EU market. Also, the ACP Group of States in June 2003, requested the European Commission to fund from intra-ACP resources, a programme entitled EC/ACP/WHO Partnership on Pharmaceutical Policies in ACP Countries, amounting to 25 million euros. The overall objective of the programme is to close the huge gap between the potential that essential medicines have to offer and the reality that for millions of people in ACP countries, medicines are unavailable, unaffordable, unsafe or improperly used. The Financing Agreement for this programme has jointly been signed. On 30 August 2003 the TRIPS Council of the WTO reached a decision that seemingly brings to an end a two-year stand-off on access to medicines in poor countries. It should be however noted, that while generic drugs have the effect of lowering prices, the majority of generic drug manufacturers will have to wait the exhaustion of patents (unless granted a compulsory license) before they can begin manufacturing such drugs and yielding lower prices for consumers. This will remain true for many drugs that are currently on market as well as any new drugs.

PR\514370EN.doc

4/7

APP/3640/B

EN


Reproductive Health and development Improving reproductive health is a mean to sustainable development as well as a human right. Investments in Reproductive Health save and improve lives, slow the spread of HIV/AIDS and encourage gender equality. Reproductive Health is the term to cover all areas relating to reproduction, from sexual health to couples choice in the timing and size of their family. International Conference on Population and Development (ICPD) held in 1994 in Cairo defined comprehensive Reproductive Health care as being made up of three closely interrelated components: � voluntary contraceptive and family planning services � antenatal, safe abortions, delivery, post-partum and post abortion services � services for the prevention, detection and treatment of sexually transmitted infections, including HIV/AIDS. The neglect of reproductive rights and reproductive and sexual health lies at the root of many problems the international community has identified as in need of urgent action. These include: � gender-based violence � HIV/AIDS � maternal mortality � teenage pregnancy � abandoned children � rapid population growth There are established links between poverty eradication and Reproductive Health which means that poverty reduction must be given pride of place among all development policy measures. Better basic health and education can lead to better individual and family health, more effective use of health services and improved family planning and reduction in sexually transmitted diseases. International policy The International Conference on Population and Development (ICPD) held in Cairo1994 was a milestone in the history of population and development, as well as the history of women's rights. The Programme of Action (PoA) that was adopted by 179 countries, by consensus, is a forward-looking 20-year plan. This PoA is essential in achieving the MDG (Millenium Development Goals; next page). The PoA also drew attention to other important aspects that play a part in attaining sustainable development: health in general, gender equity, empowerment of women, education, migration, and research. The outcomes of this Conference reflect the understanding that all these factors are linked and that they influence each other and the economy, environment, social structures and the quality of life in general. PR\514370EN.doc 5/7 APP/3640/B

EN


The PoA formulated a number of specific recommendations and goals that need to be achieved. Some of the more general ones are: Governments should strive to spend 0,7% of GNP to development aid and to increase the budget for population and development; NGO's and local actors who can make a contribution need to be involved in the process of policy-making;

Financial Agreement The PoA specified the financial resources, both domestic and donor funds, necessary to implement the population and reproductive health package over the following 20 years. It has been estimated that the implementation of these programmes in the world would necessitate USD 17 billion by 2000 (Art.13.15 PoA ICPD). Two third of the projected costs were expected to be provided by domestic sources, representing a total amount of 11.3 billion USD by 2000. One third of the total needs were to come from the international donors countries (Art 13.16 ICPD). These external resources should represent an amount of USD 5.7 billion by 2000 and USD 6.1 billion by 2005. (Art 14.11 PoA ICPD) The ICPD resource allocation goals Financial Resources required for 2000-2015 in billions of USD Years 2000 2005 2010 2015 Domestic Resources 11.3 12.4 13.7 14.5 External Resources 5.7 6.1 6.8 7.2 Total Resources 17.0 18.5 20.5 21.7

Source: Programme of Action of the ICPD, 5-13 September 1994 (Art 13.15 and 14.11) Unmet needs and funding shortfall The developed countries have shown efforts to achieve the goals and objectives of the ICPD after 1994. Indeed the total funding level from the developed countries increased from 1.37 billion USD in 1995 to 1.59 billion USD in 2000. Moreover, several donor countries positively revised their policies on sexual and reproductive health in line with the ICPD Goals. However, the general positive trends from after the ICPD seem to be fading and even reversing. The level of resource mobilisation in 2000 fell short of the agreed ICPD targets.

PR\514370EN.doc

6/7

APP/3640/B

EN


1. The contributions of the donor countries to international population activities represented only 28% of the USD 5.7 billion target for 2000. 2. The contributions from the UN system, the foundations, the development bank loans etc...represented 17.6% of the ICPD target for 2000. 3. In total, the total population spending (incl. bank loans and UN system) in 2000 did not even represent half of the ICPD target for that year (45.6%). Policy of the EU Given the funding shortfall the aim should be that EU and the member states reach 0,7% of the GDP to contribute to development assistance. The mainstay of EU Co-operation to the 77 ACP countries is provided through the European Development Fund (EDF). The Cotonou Agreement (2000) lays the foundation for the fiveyearly financing protocols for 9th EDF (2002-2006). It has to be emphasised that the EU and ACP countries, through the Commission and DG Development, have made a gesture both to UNFPA and IPPF to fill the decency gap created by the Mexico City Policy. This support to sexual and reproductive health and rights should be reinforced in the 9th EDF and future EDFs. Overall, reproductive health has not been identified as a priority area in the Country Strategy Papers (CSPs) of ACP countries. As a result, reproductive health receives little or no funds and ACP countries do not have one of the essential tools to tackle poverty. There is indeed a clear case for investing in sexual and reproductive health in terms of affordability and costefficiency: family planning services have been found to be one of the most cost-effective interventions available to reduce maternal and infant death1 and investments in the prevention of STIs, including HIV/AIDS, save thousands of lives. Health and reproductive health thus need to be made priorities within countries' programmes so that the appropriate funding can be allocated to these sectors. One way to address this problem would consist in actively involving Civil Society in the drafting and implementation of the CSPs to ensure that the reproductive health needs of the population are accurately reflected. A new instrument is the regulation on aid for policies and actions on reproductive and sexual health and rights in developing countries (COM(2002) 120).

1

It costs around 20 to 25 USD each year on average to bring basic family planning services to an individual in a developing country. This low-cost service addresses a wide range of reproductive concerns.

PR\514370EN.doc

7/7

APP/3640/B

EN


