EUROPEAN PARLIAMENT
DIRECTORATE-GENERAL FOR RESEARCH DIRECTORATE A DIVISION FOR INTERNATIONAL AND CONSTITUTIONAL AFFAIRS

FACTSHEET
SLOVAKIA 1. The acquis 1 The environmental acquis is very extensive and mainly takes the form of directives. It covers environmental quality protection, polluting and other activities, production processes, procedures and procedural rights, as well as products. Quality standards are set for air, waste management, water, nature protection, industrial pollution control, chemicals and geneticallymodified organisms, noise, nuclear safety and radiation protection. It also covers horizontal issues such as environmental impact assessments, access to information on the environment and combating climate change. The acquis is developing significantly: the new Environment Action Programme identifies four priorities - climate change, nature and bio-diversity, environment and health, natural resources and waste. Priority areas for the candidate countries in transposing the extensive acquis are: � Community framework legislation; � measures relating to international conventions to which the Community is party; � reduction of global and trans-boundary pollution; � nature protection legislation (to safeguard bio-diversity); � measures ensuring the functioning of the internal market (e.g. product standards). 2. The negotiations From the outset, the EU underlined that transitional measures would not be granted in respect of transposition, framework legislation, nature protection, essentials of the internal market and new installations (except where substantial adaptation of infrastructure spread over time is involved). Requests for transitional arrangements need to be supported by detailed implementation plans Chapter 22 - Environment

1

Information largely drawn from the European Commission, DG Enlargement http://europa.eu.int/comm/enlargement/negotiations/index.htm

WIP/2002/11/0281

1


ensuring that compliance will be achieved over a period of time. These plans allow the candidates to define intermediate targets which will be legally binding. All candidate countries requested transitional arrangements and technical adaptations but several requests have been withdrawn in the course of the negotiations. Limited transitional periods have been granted in respect of various aspects. The chapter has been closed with ten countries and remains open with Bulgaria and Romania. Chapter opened: March 2001 Status: closed in December 2002 (provisionally closed in December 2001) Transitional arrangements: � emissions of volatile organic compounds from storage of petrol until 2007 � recovery and recycling of packaging waste until 2007 � treatment of urban waste water until 2015 � discharges of dangerous substances into surface water until 2006 � integrated pollution prevention control until 2011 � air pollution from large combustion plants until 2007 � incineration of hazardous waste until 2006. 3. Position of the European Parliament In its resolution of 5 September 20011, Parliament: � recommends that Slovakia continue to endeavour to implement environmental protection measures in the area of rural development and forestry; � notes that negotiations with Slovakia on the environment chapter have only just been opened, but that proper justification of the requests for transition periods is now necessary; calls for any transition periods for full compliance with the acquis to be kept to a minimum, and for establishment of intermediate targets; calls further for careful monitoring of progress in meeting these targets, for the establishment of a practical programme of implementing measures and for details of the financing arrangements that are envisaged; � encourages Slovakia to speed up transposition of legislation in the area of environment, particularly in the fields water, waste, nature protection and industrial pollution and to develop financial strategies to ensure the full implementation of the acquis as soon as possible; reminds Slovakia that the European Parliament places a high premium on fulfilment of the acquis in this area, as confirmed in its resolution on enlargement of 4 October 2000; � welcomes the Slovak government's decision to join LIFE III and calls for the full utilisation of this programme in helping to preserve the outstanding biodiversity of Slovakia. In its resolution of 13 June 20022, Parliament calls on both the Slovak authorities and the Commission to ensure that EU-funded infrastructure policies are fully compatible with the provisions of the EC environmental legislation. In its resolution of 20 November 20023, Parliament asks the Slovak Government to pay particular attention to increasing administrative capacity in the area of the environment, in
1 2

Resolution on the state of negotiations with Slovakia, � 21, 24, 25, 56: A5-0256/2001 Resolution on the state of the enlargement negotiations, � 140: A5-0190/2002 3 Resolution on the progress of the candidate countries towards accession, � 118: A5-0371/2002

WIP/2002/11/0281

2


particular to transpose and enforce legislation in the area of Integrated Pollution Prevention and Control. 4. Latest Assessment by the European Commission
1

In its 1997 Opinion, the Commission concluded that a considerable effort had been made to establish environmental legislation compatible with Community law and that, if Slovakia continued in its legislative programme, full transposition of the acquis could be achieved in the medium term. On the other hand, effective compliance with a number of pieces of legislation requiring a sustained high level of investment and considerable administrative effort could be achieved only in the long to very long term. Since the Opinion, Slovakia has achieved considerable progress in aligning with the EC environmental acquis, and has progressed in developing the necessary administrative capacity to implement the acquis in this area. Slovakia has recently significantly increased environmental investments. Negotiations on the chapter have been provisionally closed. Slovakia has been granted transitional arrangements with regard to VOC emissions from storage of petrol (until 31 December 2007), packaging waste, (until 31 December 2007), urban waste water treatment (until 31 December 2015), discharge of certain dangerous substances (until 31 December 2006), large combustion plants (until 31 December 2010), incineration of waste (until 31 December 2006), and IPPC "existing" installations (until 31 December 2011). Slovakia is generally meeting the commitments it has made in the context of the accession negotiations. In order to complete preparations for membership, Slovakia's efforts now need to focus on finalising transposition, in particular as regards Integrated Pollution Prevention and Control (IPPC) and on implementation, notably with respect to the acquis on discharge of dangerous substances, on strengthening permitting capacity (IPPC, waste) and on the preparation of the pollution reduction programmes for waters.

January 2003

1

European Commission, Regular Report on Slovakia 2002, pp. 103 & 104: http://www.europa.eu.int/comm/enlargement/report2002/sk_en.pdf

WIP/2002/11/0281

3


