JF/bo Luxembourg, 23 March 1998

Briefing No 17

ENVIRONMENTAL POLICY AND ENLARGEMENT

* The views expressed in this document are not necessarily those held by the European Parliament as an institution.

PE 167.402 Or. DE


The briefings drafted by the European Parliament Secretariat's Task-Force on 'Enlargement' aim to present in a systematic, summary form the state of discussions on the various aspects of enlargement of the Union and the positions adopted by the Member States and European institutions. Briefings will be updated as the negotiations progress. The following papers have already appeared: - Cyprus and membership of the European Union - Hungary and the enlargement of the European Union - Romania and its accession to the European Union - The Czech Republic and the enlargement of the European Union - Malta and the relations with the European Union - Bulgaria and the enlargement of the European Union - Turkey and the relations with the European Union - The institutional aspects of enlargement of the European Union - Controlling and protecting European Union finances with a view to enlargement - Environmental policy and enlargement No 1 No 2 No 3 No 4 No 5 No 6 No 7 No 15 No 16 No 17

BRIEFING

2

PE 167.402


ON ENVIRONMENTAL POLICY AND ENLARGEMENT

CONTENTS

Page SUMMARY 4 I. II. INTRODUCTION 5 5 6

THE EUROPE AGREEMENTS

III. THE ENVIRONMENTAL ACQUIS COMMUNAUTAIRE 3.1 The Commission's White Paper on the internal market 6 3.2 The Guide to the Approximation of Environmental Legislation drawn up by the Commission's DG XI 6 IV. OPINIONS ON THE STATE OF AND PROSPECTS FOR ENLARGEMENT IN THE ENVIRONMENTAL SPHERE 4.1 The Commission's opinion 7 4.2 The European Parliament's opinion8 4.3 The Member States' opinions 9 V. EU INSTRUMENTS FOR ACCELERATING THE APPROXIMATION PROCESS 9 5.1 PHARE 9 5.2 DISAE 11 5 . 3 TAIEX 12 5.4 IMPEL 12 5.5. LIFE 13

7

VI. THE COST OF APPROXIMATING ENVIRONMENTAL LAW 13 VII. THE STATE OF THE APPROXIMATION OF ENVIRONMENTAL LAW IN CENTRAL AND EASTERN EUROPE 15 BIBLIOGRAPHY 17

3

PE 167.402


SUMMARY

In the view of the institutions concerned, environmental policy poses a major challenge to the enlargement process. The adoption of the Union's environmental legislation and standards is extremely important in this context. The Commission believes it would be unrealistic to expect full compliance with the acquis communautaire in the near future. Parliament has therefore urged the Commission to list the most important of the EU's provisions on the environment, in which it should be guided primarily by environmental rather than internal market criteria. Rules on the protection of the quality of air, water and soil should have priority in this context, with derogations on no account permitted and transitional arrangements allowed only in exceptional cases. The last of these demands is also reflected in the very few opinions so far adopted by the Member States. The EU has meanwhile created instruments designed to make it easier for the legislation of the candidate countries to be approximated to the EU environmental policy, examples being PHARE, DISAE and TAIEX. As much as possible of the acquis communautaire should be adopted before actual accession. The EP takes a particular interest in nuclear safety in this context. Linked to the adoption of EU legislation is the question of the costs involved, of which there have understandably so far been no more than estimates: between ECU 108 and 121 billion. As financing from the EU budget alone is not, in the EP's opinion, feasible, alternative forms of financing must be considered.

4

PE 167.402


I. INTRODUCTION A particularly important aspect of bringing the Central and Eastern European countries (CEECs)1 more closely into line with the EU is the approximation of their environmental law to European standards. The gap between the EU's and CEECs' legislation in this sphere is particularly wide. The Commission therefore notes: 'Environment is a major challenge for the enlargement.'2 The difference between the EU's and CEECs' standards of environmental legislation would, in the Commission's view, lead to a serious distortion of competition in the internal market and jeopardize any effective environmental policy pursued by an enlarged Community. This working paper will give a brief introduction to the history and state of the approximation process and is intended as a general introduction to country reports on the state of the harmonization of environmental legislation in the CEECs3. II. THE EUROPE AGREEMENTS The Europe Agreements are the first legal instruments of approximation4. The original intention of these bilateral agreements was to place the EU's economic relations with the CEECs on a new footing. Since the decision taken by the European Council in Copenhagen (1993) the agreements and the accession criteria defined in Copenhagen5 have acted as the legal framework for the accession process. They seek to ensure sustained satisfaction of the economic and political requirements for full
1

This briefing concerns only eastward enlargement, since it is in the East that the most serious environmental problems arise. 2 See Agenda 2000, Vol. II, Part II, Analysis, 6.2 Environment, p. 39. 3 The environmental situation in the candidate countries is considered in separate working papers, which report in detail on these countries' environmental policies (obtainable from DG IV of the EP, Environment, Energy and STOA Division). 4 In 1991 Poland and Hungary became the first of the Central and Eastern European countries to sign association agreements with the EU. In 1993 agreements with the Czech Republic, Slovakia, Romania and Bulgaria followed. The three Baltic republics signed in 1995, Slovenia in 1996. 5 The criteria include a stable democracy, a functioning market economy, the ability to accept rights and obligations arising from the acquis communautaire and compliance with the objectives of the Union and of Economic and Monetary Union.

5

PE 167.402


membership. The Europe Agreements are primarily economic agreements, but they also include accords on the environment, the political dialogue, vocational training, culture and agriculture. Article 70 of all Europe Agreements refers to the need for the approximation of environmental legislation. Article 81 then gives a more precise definition of cooperation on the environment. Cooperation extends to all aspects of the environment. The agreements provide for the establishment of joint strategies and the improvement of international and regional cooperation. Suitable measures are to be taken in support of economic cooperation to ensure compliance with the principle of sustainable development. Provision is also made for the exchange of information and experts and for joint training programmes and research activities. The Environment Ministers, who meet annually, have set two priorities: the approximation of environmental legislation and the establishment of environmental programmes. III. THE ENVIRONMENTAL ACQUIS COMMUNAUTAIRE Ever growing importance has been attached to the protection of the environment in the EU since the 1970s through the Single European Act (since when it has had its own legal basis in Articles 130r to 130t), Maastricht and, latterly, Amsterdam. The accession of Finland, Sweden and Austria has strengthened the environmental idea in the EU. The European Union's environmental legislation today consists of about 300 acts: regulations, directives, decisions and recommendations. They are joined by numerous communications and policy guidelines drawn up by the Commission. The Council's and Commission's official position is that full compliance with the acquis is essential for accession. However, the Commission also states in Agenda 2000 that, as none of the countries can achieve this objective in the near future, transitional arrangements must be made. 3.1 The Commission's White Paper on the internal market On 10 May 1995 the Commission published the White Paper on the preparation of the associated countries of Central and Eastern Europe for integration into the internal market of the European Union1. Where the environment is concerned, the White Paper refers only to those acts which relate to the free movement of goods, persons and capital. They comprise 36 environmental directives and 21 environmental regulations of the 70 environmental acts forming the core of the acquis of Community legislation2. The Commission divided the acts by their importance for the common market into Stage I measures - the basic acts in each sphere - and Stage II measures, which build on or are linked to Stage I acts.
1 2

COM(95)0163 final of 3 May They concern the radioactive contamination of foodstuffs, protection against irradiation, the putting into circulation and notification of dangerous chemical substances, monitoring the risks inherent in existing substances, the import and export of certain dangerous chemicals, the implications of releasing genetically modified organisms for the environment, waste management policy, noise emitted by building machinery and equipment, air conservation (lead content of petrol and sulphur content of certain liquid fuels) and air pollution caused by volatile organic compounds and the monitoring of substances that lead to depletion of the ozone layer.

6

PE 167.402


3.2 The Guide to the Approximation of Environmental Legislation drawn up by the Commission's DG XI The Community's environmental acts are described in detail in the Guide to the Approximation of European Union Environmental Legislation1. This follows on from the White Paper on the internal market. It divides the Union's environmental legislation into nine sectors2, subdividing each sector into non-White-Paper and White-Paper legislation. No account is taken of rules from other spheres - such as the agricultural policy - that may also have an impact on environmental interests. An accurate list of Community legislation on the environment can be found in Annex 2 to the guide. The Guide also contains guidelines for those responsible for implementation in the candidate countries. It thus takes the form of a map of the approximation of environmental law. It outlines the acquis and shows what practical steps need to be taken to transpose it into domestic legislation. Matters relating to application and enforcement, on the other hand, are not considered for the time being. IV. OPINIONS ON THE STATE OF AND PROSPECTS FOR ENLARGEMENT IN THE ENVIRONMENTAL SPHERE The Commission published Agenda 2000 on 16 July 19973 together with its opinions on the applications for accession received from CEECs. In Agenda 2000 it sets out an enlargement strategy for the Union and the associated countries. For the EU reforms of the agricultural policy and the Structural and Cohesion Funds and far-reaching institutional innovations are proposed. For the candidate countries satisfying the criteria is considered a requirement for accession. In the environmental sphere, however, there appears to have been a softening of the position of late. 4.1 The Commission's opinion As regards the transposition of environmental standards4 the Commission refers to the serious problems that persist in this area of policy in all the applicant countries. In all ten of these countries the adoption of the Community acquis would require massive investment, which would be beyond their national budgets. In the Commission's view full compliance with the acquis is therefore unlikely in the near future, this being especially true of the capital-intensive water, waste and air sectors. If this gap between the future new and old Member States persists, it is to be feared that protectionist measures will be taken, with the adverse effects they will have on the internal market and the future development of the environmental policy.

SEC(97)1608, 25 August 1997 Horizontal legislation, air, waste, water, nature conservation, industrial pollution and risk management, chemicals and genetically modified organisms, noise and nuclear safety 3 COM(97)2000 final, Vol. I, p. 4 COM(97)2000 final, Vol. I, p. 56
2

1

7

PE 167.402


The Commission believes that Hungary, Poland and the Czech Republic will be able to achieve this objective in the medium term if they do not flag in their efforts. The same can be said of Slovakia, Slovenia and the Baltic States if they step up their efforts. Romania and Bulgaria can achieve the objective only in the long term. The Commission refers to the tendency1 to leave the environmental sector till last.

Interview with Mrs Bjerregaard on Agenda 2000 in the newsletter Enlarging the Environment, No 5, July 1997

1

8

PE 167.402


As regards nuclear safety1 the Commission points out that nuclear energy accounts for an average of 30% of electricity generation in the applicant countries and for as much as 80% in some of them. However, most nuclear power stations do not meet international safety standards. Despite this, the Commission continues, the solution is not to close them down, since the cost of obtaining alternative energy supplies would be extremely high. If they are to meet the EU requirement that life and health be protected, the applicant countries should cooperate fully in efforts being made to bring their levels of nuclear safety up to the international standards. The timetables for closing plants that cannot be upgraded must be respected2. 4.2 The European Parliament's opinion The EP is critical of the fact that the accession negotiations are beginning with only some of the candidates3. This is particularly true of the environmental sector. The opinion of the Committee on the Environment, Public Health and Consumer Protection4 states that dividing the CEECs into two groups, one acceding in the first round, the other in the second, would have an adverse effect on the environment, since the former would gain the impression of being up-to-date environmentally and so slacken their efforts, while the latter would have less incentive to try, believing that the criteria for beginning the accession negotiations were primarily political and economic. Both factors would pose a threat to the environment. The EP notes that 'speedy and full accession of all candidate Member States will, in the long term, improve the environment throughout Europe, assuming a more intensive approach to important policy areas such as the environment, transport and energy'. As the EP believes that full compliance with the environmental acquis will be impossible in the near future, it requests the Commission to draw up a list of the most important elements of EU environmental legislation geared to environmental criteria (rather than, say, the internal market). Priority in this context should be given to rules safeguarding the quality of air, water and soil, food safety and health protection. Derogations must not, in the EP's opinion, be permitted in any circumstances, and transitional provisions should be allowed only in moderation and restricted to 'duly justified cases'. The resolution does not refer to the efforts being made by all the candidates in the environmental sphere, since it addresses the most important factors no more than briefly. However, where it does mention them (Bulgaria, Hungary, Poland), it says they are inadequate and should be increased significantly. As regards the application and public enforcement of environmental legislation, the EP believes priority should be given to the improvement of administration in the CEECs, since the existing administrative structures do not guarantee effective application and enforcement. The following comments are made on individual policies:

COM(97)2000 final, Vol. I, pp. 57 f. The applicant countries have entered into bilateral commitments concerning the closure of power stations. 3 Resolution of 4 December 1997 on the Commission communication 'Agenda 2000 - For a stronger and wider Union', pp. 4 ff. 4 PE 224.102.
2

1

9

PE 167.402


� The extent to which the environment is integrated into other policies is still unsatisfactory in both the EU and the CEECs. � Raising the safety of nuclear power stations to European standards must be guaranteed. Any power station not meeting these standards should be shut down. � There is an urgent need for energy generation from coal to be improved. � To increase the cohesion of an enlarged Union, the TENs1 need to be expanded, but the integration of the environment into the transport policy should be guaranteed. 4.3 The Member States' opinions The majority of Member States pay no further attention to the environmental sector. Only Finland, Sweden, Denmark and Austria mention the environmental sector in their opinions. However, they comment only on the EU's environmental policy, not on the enlargement aspect. Austria does at least welcome the account taken of environmental criteria in the assessment of the candidate countries. Finland and Sweden call for more thought to be given to the environment in the reform of the CAP, while Denmark takes the view that within strictly defined limits the EU should be able to levy environmental taxes2. The majority of Member States refer to the need for transitional periods for the adoption of the acquis, but favour strict selection. In its opinion on Agenda 2000 the General Affairs Council calls on the Commission to improve the information on the environmental sector in Agenda 2000 and its opinions on the accession applications3. V. EU INSTRUMENTS FOR ACCELERATING THE APPROXIMATION PROCESS Immediately after the collapse of the Communist regimes in the CEECs the EU launched the PHARE aid programme for Hungary and Poland (PHARE = Poland and Hungary: Assistance for the Restructuring of the Economy). This programme was expanded to include the whole region and complemented by such instruments as DISAE, TAIEX, IMPEL and LIFE. 5.1 PHARE The PHARE programme is and remains the most important instrument for the financial promotion of preparations for accession. It was originally designed to assist the former Communist countries with the restructuring of their economies and the development of democratic structures. From 1990 to 1996 ECU 600 million of PHARE's total budget of ECU 5 416.9 million was invested in the environmental sector. This is equivalent to 9% of PHARE resources4. However, expenditure has declined over the years. From 1995 to 1999 ECU 6.7 billion is to be allocated, from 2000 to 2006 ECU 1.5 billion p.a.5 A third of this is to be used to finance measures in the environmental sector.

Trans-European networks This proposal was, however, rejected in Amsterdam. 3 European Council - The Council, Report, 10 December 1997, 1324/97 4 PHARE Environmental Strategy: The Pre-Accession Phase, p. 15 5 From 2000 the pre-agricultural structural fund (ECU 0.5 bn) and the pre-structural fund (ECU 1.0 bn) must be added to this.
2

1

10

PE 167.402


However, according to PHARE officials, spending on the environment is in fact to account for some 8 to 9% under the current programme.

TABLE 1Environment and nuclear safety
Funds allocated by country 1990-1997 (ECU million) 1990-93 Albania Bosnia and Herzegovina Bulgaria Czech Republic Estonia FYROM Hungary Latvia Lithuania Poland Romania Slovakia Slovenia Multi-country programmes Other Czechoslovakia TOTAL 3.3 0 49.1 0 0 0 47 0 0 75 5 0 0 88.5 20 35 322.9 1994 0 0 5 0 2.5 0 15.5 5.5 1 12 0 0 0 13 23 0 77.5 1995 0 0 7 0 0 0 12 0 0 22 0 1 0 20 20 0 82 1996 1.5 0 6 5 1 0 0 1.1 2.5 5 8.4 0 0 10 15 0 55.5 1997 6.7 0 0 0 0 2 0 0 0 0 35 0 4 17 11.7 0 76.4 Total 11.5 0 67.1 5 3.5 2 74.5 6.6 3.5 114 48.4 1 4 148.5 89.7 35 614.3

Source: European Commission, DG IA, F6 (19.3.1998) PHARE aid is granted in the form of non-repayable grants. It is geared to the transfer of know-how and the provision of technical assistance, the programme availing itself of the help of various noncommercial, public and private organizations. It supports programmes and equips projects designed to bring the CEECs closer to the Community, but unable to raise resources on commercial terms. PHARE also invests directly in infrastructure projects. On the basis of an analysis of past PHARE projects and the experience gained by people in positions of responsibility in the CEECs a new PHARE strategy has been devised for the period 1995-19991. PHARE grants for environmental projects will then be allocated to four sectors: 1. Legislation and policy geared to implementation and enforcement, approximation to EU law and integration in general 2. Development of environmental financing:
1

See http://europa.eu.int/dg1a.home under the heading 'Phare programme'.
11 PE 167.402


(a) development of new financing instruments, with particular regard for the private sector (b) support for the development of key financial institutions, such as the eco-funds (c) financing of environmental projects that are too risky to attract private or government money alone 3. Support for institutions which are active, for example, in the approximation of legislation and are also assisted by the recipient country 4. Increase in environmental awareness From 1998 PHARE aid will be more closely linked to the honouring of certain commitments entered into in the past. Such commitments may include compliance with democratic principles, the fulfilment of obligations arising from Europe Agreements, the implementation of the most important provisions in the White Paper, the sensitive areas mentioned in the opinions, progress in certain PHARE projects and the first steps in the establishment of economic convergence programmes. As it was geared to applications received from the CEECs, the coherence of the old PHARE programme had waned. The new PHARE is therefore to be geared more closely to accession requirements. 5.2 DISAE The DISAE (Development of Implementation Strategies for Approximation in Environment) programme was set up by the Commission (DG XI and DG IA) with PHARE resources in September 1996 with a view to helping the associated countries to comply with environmental standards not covered by the White Paper. The programme is intended to help these countries to develop effective strategies for the application and enforcement of the requirements of Community law. Its implementation has been assigned to the London company Environment Resources Management (ERM), which maintains the DISAE office in Brussels for the implementation of the programme. Under the DISAE programme the associated countries are offered various services: technical assistance in the form of 'mini-projects' (ECU 7 million), seminars on approximation to Community standards and financial assistance for CEEC civil servants attending EU seminars and meetings. For the implementation of mini-projects in the sphere of technical assistance DISAE has a total budget of ECU 7 million. Individual projects are allocated ECU 40 000 to 250 000 depending on the nature of their task. The mini-projects are intended to provide tailor-made and rapid technical assistance for national measures. They are designed to help fill certain gaps and to enable specific problems to be solved, rather than covering general aspects of environmental protection. Miniprojects concern, for example, the development of approximation strategies for the environmental sphere and checks to ensure that national law complies with Community law. Furthermore, approximation programmes for specific EU directives are planned, the institutional framework is assessed and cost and financial studies are carried out. The influence of harmonization in various sectors is also examined. 5.3 TAIEX As early as May 1995 the Commission suggested in the White Paper that an instrument should be created for providing technical assistance with the harmonization of the CEECs' legislation. The

12

PE 167.402


structures needed for this were set up in DG XV and DG IA in the shape of TAIEX (Technical Assistance and Information Exchange Office). TAIEX was established to make matters more transparent for all those concerned with the provision and acceptance of technical assistance. Its services are geared specifically to the single market and especially to the application and enforcement of legislation. TAIEX collects and disseminates information on the state and nature of the implementation, application and enforcement of Community acts in the CEECs. The TAIEX office also makes available texts of EC acts on the single market and of the Member States' implementing laws. It also gives practical advice on the implementation and application of laws and, if requested, assigns experts to give support in the CEECs (May 1996 to August 1997: 134 assignments in response to 312 requests) and arranges seminars on focal aspects of the internal market in the CEECs (January to August 1997: 36 seminars attended by a total of 1800 people), brief study seminars (1997: 48 seminars attended by 1200 people from the CEECs) and brief visits (up to August 1997: 594 visits in response to 832 requests) to Brussels and the Member States. TAIEX services are primarily available on request from the CEECs themselves. They are open to the administrations of the candidate countries and Member States and to non-governmental public administrations, but not to individual citizens or private enterprises. Since June 1997 TAIEX has also been available to private economic entities. The TAIEX services are available to the candidate countries free of charge, the cost of visits by experts usually being financed from the annual PHARE budget for the approximation of legislation, with TAIEX paying only in emergencies. The Commission renewed the TAIEX mandate in Agenda 2000 (June 1997). It expanded its range of activities to include approximation to the whole acquis communautaire. This extension of the mandate beyond the sphere of the internal market covered in the White Paper may lead to overlapping with the tasks hitherto performed by DISAE with respect to the approximation of environmental legislation. 5.4 IMPEL IMPEL is the European Union's network for the 'implementation and enforcement of environmental law'. IMPEL is designed to promote the exchange of information, experience and staff among participating organizations to ensure more effective application of environmental law and so to facilitate the exchange of know-how on practical aspects of the legislation. It is meant to encourage the simplification of approaches to the implementation, application and enforcement of environmental law by local and regional entities as well as central government. The CEECs wanted to join IMPEL, but the Member States proposed the establishment of a separate network for them. 5.5 LIFE

13

PE 167.402


LIFE is the EU environmental policy financing instrument established in 1992 (volume until 2000: ECU 850 million). Its aim is to promote the development and implementation of legislation in the environmental sector. Until 1995 only actors in the EU Member States were able to apply for LIFE assistance. LIFE assists only major pilot projects that contribute to a measurable improvement in the environment through application on a major scale1. Its role is limited to cofinancing at varying levels and assistance in the form of the subsidization of interest rates and repayable aid. However, LIFE now provides ECU 10 million p.a. for environmental and nature conservancy projects in Central and Eastern Europe2. Project applications in this case must be submitted by the applicant third countries or EU enterprises. VI. THE COST OF APPROXIMATING ENVIRONMENTAL LAW The costs - as a percentage of GNP - incurred by the acceding countries in adopting the whole acquis communautaire will be very high in every sector. Various studies3 have estimated the cost of harmonizing environmental law. They differ widely in the conclusions they reach; the underlying assumptions often remain unclear. In September 1996 the Commission drew up guidelines for estimating these costs4 to persuade the applicant countries to adopt a uniform approach and to arrive at comparable results. The Commission calculated that the CEECs will have to invest between ECU 198.4 and 121.5 billion. For the main sectors - water, air and waste - this is equivalent to ECU 1 000 per capita or 3% of GNP in 1994, with investment spread over 20 years. For individual sectors the figures are as follows:

TABLE 2Total investments cost of approximation to CEECs (in ECU billions unless otherwise stateda) Country Supply Poland Hungary 4.4 3.5 Water Waste W. 13.7 3.1 Total 18.1 6.6 13.9 2.7 Air Min. 2.2 2.1 Waste Max. 3.3 4.4 Total Investment Min. b 34.1 11.5 Max. b 35.2 13.7 927 1306 Total (ECU per capita)

See DG XI's www site. GLOBE 'Global Europe Network Briefing', p. 2 3 See, for example, the studies by IFO-Institut, IIASA (1996) and Adler et al. (1994). 4 Background material for the DISAE seminar, Brussels, October 1997: 'Estimation of Compliance Costs for the Approximation of EU Legislation in CEE States - Guidelines for a Country-Study'
2

1

14

PE 167.402


Czech Rep. Slovak Rep. Bulgaria Romania Baltic (total) Estonia Latvia Lithuania Slovenia Total % of Total Max.

2.2 1.0 2.2 3.8

1.1 0.9 2.7 6.3

3.3 1.9 4.9 10.1

6.4 1.9 5.1 9.1 8.45

8.0 0.3 1.8 1.0 0.45

3.8 1.6 5.1 2.7 0.85

10.4 4.1 11.7 20.2 8.9 1.5 1.71 2.38

12.4 5.4 15.0 22.0 9.3 1.5 1.71 2.38 1.84 121.5 100%

1427 760 1668 943 1148 n.a. n.a. n.a. n.a. 1140

0.13 0.11 0.11 n.a. 17.5 14%

1.38 1.6 2.27 n.a. 33.1 27%

1.5 1.71 2.38 n.a. 50.5 42% 0.69 48.2 40% 1.15 9.7 1.15 22.7 19%

1.84 108.4

a) Figures for water supply, air and waste are based on the Ifo Study; for wastewater on Ifo and Wrc. b) Total Min. represents the minimum estimate for landfill; Total Max. represents the maximum estimate for waste management. Source: European Commission, DG IA - DG XI, Approximation Seminar, 26-28 October 1997 The Commission estimates total capital and operating costs at between ECU 8.303 and 12.353 million. The total annual cost of regional waste water treatment and the monitoring of air pollution and the operational cost of waste disposal will be ECU 8 to 12 billion, which is equivalent to ECU 80 to 120 per capita or 5.4% of the CEECs' GNP (EU15: 1.2% of GNP). However, fundamental doubts also hang over many of the studies quoted by the Commission. Thus the EP has questioned the Commission's calculations, calling them overly optimistic. Parliament has its doubts about the macroeconomic framework presented by the Commission. Above all, it believes that, at 2.5%, economic growth within the EU is set too high. It conveys the erroneous impression that eastward enlargement is possible without additional costs. As regards the financing of enlargement Parliament points out that the sum estimated by the Commission can be no more than a guide value and that the actual figure cannot be calculated until the accession negotiations have been completed. In the absence of additional resources enlargement could be accomplished only if cuts were made under existing policies, especially the agricultural policy, and the Structural Funds. Only when these policies had been reformed would it be possible to calculate the actual savings and costs, and only then could a decision be taken on the retention of the ceiling on financial resources. As regards the environment, the scale of the current problems and related rehabilitation and investment costs are such that advantage must be taken of a wide range of financing options1:

Resolution of 4 December 1997 on the communication from the Commission 'Agenda 2000: the 2000-2006 financial framework for the Union and the future financing system', paragraph 29
15 PE 167.402

1


�

making available appropriate funds for the most urgent environmental problems, especially those associated with drinking water supply, waste water treatment, waste management, transport and energy supply; solving structural problems, with priority given to aid to the energy sector and the urban environment; encouragement for the setting up of joint ventures between industries in the Member States and the applicant countries; it should be remembered in this context that the provisions of the EC Treaty require environmental and consumer protection considerations to be incorporated in all areas of EU activity; examining the possibility of establishing, in cooperation with the EBRD and EIB, guarantee funds for banks in the Central and Eastern European countries for credits granted to environmental projects, and especially to small and medium-sized enterprises and local and regional authorities.

�

�

�

Fundamental problems with the estimation of costs arise from the following deliberations. To calculate the cost of approximation, the costs arising as a result of efforts to comply with the EU's standards should ideally be compared with the costs arising if these requirements did not exist (the 'baseline scenario'). In practice, however, it is impossible to design a 'scenario without EU accession'. Rather than proposing that a 'with-without' comparison should be made, the Commission therefore recommends that the costs be quantified on the assumption that the total investment and operating costs needed to satisfy the requirements of a directive should be ascribed to it. Other effects should not be taken into account. It recommends a direct cost analysis, i.e. that the various options should be compared and the most efficient selected on the basis of rational criteria ('bottom-up approach'). VII. THE STATE OF THE APPROXIMATION OF ENVIRONMENTAL LAW IN CENTRAL AND EASTERN EUROPE1

Almost all the CEECs now have provisions in their constitutions relating specifically to the environment, separate framework environmental legislation and specific environmental programmes corresponding to the Union's environmental action plans. All CEECs have now set up a separate environment ministry, but not all have appropriate administrative infrastructure. This partly accounts for the poor application and enforcement of laws. Other reasons are the frequent failure to separate competence and power in the environmental sector and the communist 'tradition' of non-enforcement. The following table shows the state of the approximation of environmental law in the CEECs. For the methodology see the source referred to. One qualification is that the table essentially concerns the transposition of laws rather than their application and enforcement.

A more precise account of the progress made in integrating EU environmental legislation into the law of the CEECs can be found in the various country reports (see footnote 3).

1

16

PE 167.402


TABLE 3

State of the approximation of environmental law
General environmental policy Air Chemicals, industrial risks and biotechnology 27 Nature conservancy Noise Waste Water

Average % of EU directives transposed by the CEECs Range

57

46

65

32

33

61

22-87

27-60

19-35

33-100

0-50

14-78

44-78

Source: http//:www.rec.org/REC/Publications/EUlaw/intro.html

17

PE 167.402


BIBLIOGRAPHY Commission of the European Communities, AGENDA 2000, Vol. I: For a wider and stronger Union; Vol. II: The challenge of enlargement, Brussels, 15 July 1997, COM(97)2000 final Commission of the European Communities Briefing, Relations between the European Union and Central European Countries: Pre-Accession Strategy for the Associated Countries of Central Europe Commission of the European Communities, The Commission�s Work Programme for 1998, Brussels, 15 October 1997, COM(97)0517 final Commission of the European Communities/DG XI: Newsletter 'Enlarging the Environment', Nos 1-6, 1997; http://europa.eu.int/en/comm/dg11/dg11home.html Commission of the European Communities, Estimation of Compliance Costs for the Approximation of EU Legislation in CEE States - Guidelines for Country Studies, Brussels, September 1997; background material for the DISAE seminar Commission of the European Communities, PHARE Discussion Paper: PHARE Environmental Strategy: The Pre-Accession-Phase, Brussels, 25 June 1996 Commission of the European Communities, The Phare Programme, Annual Report 1995, Brussels, 23 July 1996, COM(96)0360 final Commission of the European Communities, Report on Implementation of the Commission�s Programme for 1997, Brussels, 15 October 1997, COM(97)1854 final DISAE - Status and Procedures, background material for the seminar held in Brussels from 26 to 28 October 1997 General Affairs Council, Presidency Report to the European Council on Enlargement of the Union and Agenda 2000, Brussels, 10 December 1997, 13241/97 GLOBE (Global Legislators Organisation for a Balanced Environment), Globe Europe Network Briefing: Adapting Environmental Legislation in Central and Eastern Europe to EU Standards Ifo-Institut, Der Bedarf an Umweltschutzinvestitionen in Mittel- und Ost-Europa, Ifo-Schnelldienst 29/95 Mayhook-Walker, Alex (DISAE), The Role of Parliaments in the Approximation Process, Proposition for discussions at the GLOBE EU Conference on Approximation in the Environmental Sphere in Central and Eastern Europe, The Hague, 5/6 June 1997 M�ller, Edith, MEP/Rainer Emschermann, A Green Agenda 2000 - Financing the EU�s Eastern Enlargement, Brussels, November 1997 European Parliament, Fiche Th�matique N� 1, Le Trait� d'Amsterdam et l'Elargissement de l'Union Europ�enne, Luxembourg, 16 September 1997 Special Report No 3/97 concerning the decentralized system for the implementation of the PHARE programme (period 1990 - 1995) together with the Commission's replies, OJ C 175, 9.6.1997, p. 1 Tebbe, Gerd, Die Politik der EU zur Assoziierung und Integration der MOEL, in: Mayer, Otto G. et al., Osterweiterung der EU, pp. 63-79, Baden-Baden 1997 Sz�mler, Tam�s, The Budgetary Costs of EU Enlargement, in: Oppenl�nder, Karl Heinrich, Westund Osteuropa auf dem Weg in die EU, Cologne 1997 *** Further information can be obtained from: Hans-Hermann KRAUS, European Parliament, DG IV, Luxembourg, Environment, Energy and STOA Division

18

PE 167.402


Tel. (32) 2 284 3721 / Fax: (32) 2 284 4980 / E-mail: hkraus@europarl.eu.int

19

PE 167.402


