29.2.2000

EN

Official Journal of the European Communities

C 57/1

II
(Preparatory Acts)

COMMITTEE OF THE REGIONS

Resolution of the Committee of the Regions on `The ongoing EU enlargement process' (2000/C 57/01)

THE COMMITTEE OF THE REGIONS,

having regard to the decisions of the Copenhagen European Council (21 and 22 June 1993) and the Cologne European Council (3 and 4 June 1999); having regard to the applications for accession to the European Union submitted by Cyprus, Malta and the ten Central and Eastern European countries; having regard to the opinions published by the European Commission on these applications; having regard to the Commission's second regular report on the progress of the applicant countries towards accession; having regard to the report of the European Parliament on the Communication from the Commission `Agenda 2000: for a stronger and wider Union' (A4-0368/97); having regard to the opinion of the Committee of the Regions on the institutional aspects of enlargement (CdR 52/99 fin) (1); having regard to its opinion on the implementation of EU law by the regions and local authorities (CdR 51/99 fin) (2); having regard to its opinions and resolutions concerning the principle of subsidiarity (CdR 302/98 fin (3); CdR 305/97 fin (4); CdR 136/95) (5); having regard to its opinion on cross-border cooperation with the CEEC (CdR 207/96 fin (6));
(1) (2) (3) (4) (5) (6) OJ C 374 of 23.12.1999, p. 15. OJ C 374 of 23.12.1999, p. 25. OJ 198 of 14.7.1999, p. 73. OJ 64 of 27.2.1998, p. 98. OJ 100 of 2.4.1996, p. 1. OJ 34 of 3.2.1997, p. 18.


C 57/2

EN

Official Journal of the European Communities

29.2.2000

having regard to its opinion on the effects on the Union's policies of enlargement (CdR 280/97 fin (1)); having regard to its opinion on an instrument for structural policies for pre-accession -- ISPA (CdR 241/98 fin (2)); having regard to its opinion on pre-accession measures for agriculture -- SAPARD (CdR 273/98 fin (3)); having regard to its Bureau's decision on 20 October 1999 to appoint Mr Roger Kaliff (SV, PES) as rapporteur-general, a) whereas the European Union's efforts to bring its institutions closer to the European citizen have steadily increased over the past decade. The importance of regional and local government in building Europe has been acknowledged and has consequently been taken increasingly into account in the EU decision-making process. Indeed, the inclusion of the subsidiarity principle in the Treaty on European Union, the establishment of the Committee of the Regions and the development of the partnership principle in the implementation of the Structural Funds policy of the Union are proof of the progress made in this direction; b) whereas further efforts must be made in order to strengthen the position of regional and local authorities in the European Union as these entities often have both legislative and executive powers and thus dispose of considerable competencies in implementing EU legislation. In its resolution to the European Council in Cologne (3 and 4 June 1999) the Committee of the Regions therefore submitted proposals for the amendment of the Treaties during the next IGC (CdR 54/99 fin) (4); c) whereas, as regards the ongoing process of EU enlargement, the Committee of the Regions underlines its commitment to this process which it regards as an investment in peace, political stability, social cohesion and prosperity for the people of Europe; d) whereas, in the light of the conclusions of the Commission's second regular report on the progress of the applicant countries towards accession and the conclusions of the special meeting of the European Council held in Tampere, the Commission and the Council must steadily intensify the dialogue with Bulgaria, Latvia, Lithuania, Malta, Romania and Slovakia within the framework of the EU's enlargement strategy, with a view to launching accession negotiations with these countries as soon as possible; e) whereas enlargement must not, however, be allowed to jeopardise the level of European integration already achieved with respect to the Internal Market and other Community policies; f) whereas the basic principles of the European Union, such as the principle of proximity to citizens (Article 1 of the EU Treaty), the subsidiarity principle and the principle of proportionality (Article 5 of the EC Treaty and Protocol appended to the Amsterdam Treaty on the application of the principles of subsidiarity and proportionality) formally relate primarily to the relations between the Union and the Member States. However, in practice they are also relevant to the national decision-making process and to the transposition of European law in the Member States and the application of that law at national, regional and local level; g) whereas these principles must, therefore, also guide the accession negotiations with the applicant countries. The European Union, whilst respecting the way applicant countries' powers are organised internally, must work towards ensuring that EU law will be implemented in the future Member States as efficiently as possible but also as closely as possible to the citizen;
(1) (2) (3) (4) OJ 64 of 27.2.1998, p. 48. OJ 51 of 22.2.1999, p. 7. OJ 93 of 6.4.1999, p. 1. OJ C 293 of 13.10.1999, p. 74.


29.2.2000

EN

Official Journal of the European Communities

C 57/3

h) whereas since 1997 the Committee of the Regions has taken an active role in the EU enlargement process by organising a dialogue between its members and political representatives of the regional and local levels in the applicant countries. In fact, after having invited local and regional elected representatives from all the applicant countries to Brussels in June 1997 and then organised bilateral meetings with each of these first six countries, a general conference was held in Brussels on 16 November 1999 with the representatives of the local and regional authorities of these countries. The conference took stock of the discussions and debated this resolution; i) whereas in the course of these six bilateral conferences, it has become clear that the local and regional levels in all applicant countries have a vital role to play in the further development of a modern democracy in these countries. Moreover, the part of local and regional levels in implementing EU legislation is extremely significant and will increase due to the ongoing devolution processes currently under way in these countries. However, the information provided for local and regional authorities on the current negotiations between their national governments and the European Union, as well as their participation in these negotiations, are not sufficiently developed and must therefore be enhanced; j) whereas the conferences initiated a process of mutual information and consultation having regard also to the experiences of regional and local authorities of all 15 EU Member States during the previous EU enlargement processes (in 1973, 1981, 1986 and 1995); k) whereas the conclusions of the Commission report on the progress made by all the countries applying to join the European Union must be respected, unanimously adopted the following resolution at its 31st plenary session of 17 November 1999.

The Committee of the Regions calls upon

-- the Council and the European Commission

-- the local and regional authorities in the European Union

5. to press forward without delay with the EU enlargement negotiations with Cyprus, the Czech Republic, Estonia, Hungary, Poland and Slovenia according to the principle of proximity to citizens, the subsidiarity principle and the principle of proportionality enshrined in the Treaties;

1. to incorporate the enlargement dimension into the their bilateral cooperation arrangements, e.g. twinning or bilateral cooperation agreements, with the regions and local authorities in the applicant countries, if this has not already been done;

6. to launch accession negotiations with all applicant countries which have made sufficient progress towards meeting the criteria for accession established by the Copenhagen European Council;

2. to promote cross-border and transnational cooperation at devolved government level through their own bilateral contacts or through their Member States' bilateral contacts with the applicant countries;

3. as far as their resources and remit allow, to exchange officials with or to host trainees from devolved administrations of the applicant countries in order to familiarise them with European institutions, policies and procedures;

7. to ensure that, in matters affecting their remit or vital interests, local and regional authorities receive all relevant information and are consulted sufficiently, and in good time, before negotiations with the applicant countries enter the crucial phase;

4. to help the local and regional authorities in the applicant countries to open representative offices in Brussels, for instance, in liaison with existing regional offices;

8. to consider and respect the rights, powers and responsibilities of local or regional authorities laid down in the legal framework of the applicant countries and to push for the involvement of these levels in the process of negotiation, e.g. through increased contacts with the Commission delegations in each of the countries concerned;


C 57/4

EN

Official Journal of the European Communities

29.2.2000

9. to act swiftly in launching the institution-building programme for local and regional authorities, as announced in the new Phare guidelines, so that these tiers of administration, which are also affected to a decisive extent by the application of the acquis communautaire, can be widely involved in the preparations for accession; 10. to develop innovative awareness-raising instruments for people in the applicant countries with the aim of briefing them about European integration, not just its economic implications, but also the way in which it affects ordinary people; 11. to ensure that the EU programmes to assist cross-border, interregional and transnational cooperation are compatible with the pre-accession strategy measures, including the Phare programme; 12. to take into account the opinions issued by the Committee of the Regions in negotiating the conditions for EU accession in those policy areas where the Committee has rights of mandatory consultation; 13. to work towards the speedy establishment of joint committees composed of representatives of the Committee of the Regions and of local and regional levels in the applicant countries at the request of the applicant countries' governments and on the proposal of the representatives of the regional and local authorities; 14. to foresee in the forthcoming IGC on institutional reform of the European Union the inclusion of a provision in the Treaties that allows the national governments of the applicant countries to nominate observers to the Committee of the Regions for the period between the signing and the effective date of their Treaty of EU Accession; 15. to ensure that account is taken of the justifiable concerns of existing and future border regions, where the impact of enlargement will be felt much more keenly than in other regions; as well as economic, structural and environmental issues, such effects in particular include nuclear safety; 16. to undertake to implement the institutional reforms required to improve the functioning of the Community institutions, without slowing down the enlargement process;

18. to ensure that the vital role of local and regional levels for the further development of democracy prosperity, social cohesion and political stability in the applicant countries is acknowledged and supported by the European Union;

19. to ensure sufficient resources so that local and regional elected representatives in the applicant countries preparing their country for EU accession at their administrative level can receive appropriate information and support as soon as possible and, in particular, can follow the work of the Committee of the Regions;

-- the national governments of the applicant countries

20. to guarantee that representatives of their local and regional levels receive all relevant information regarding their own areas of competence during the EU accession negotiations;

21. to ensure sufficient financial resources for local and regional authorities and their associations, thereby enabling them to participate more actively in the EU accession process;

22. to guarantee that their local and regional authorities are consulted on a mandatory basis in these areas well before the negotiations with the European Union enter into the crucial phase and that they may present their views directly to the negotiators of the government;

23. to ensure that their local and regional authorities are consulted in those areas which will have financial and administrative consequences for local and regional authorities;

24. to ensure that where negotiations on areas concerning the competencies of their local and regional authorities have already been finalised, these authorities are immediately informed about the consequences of the negotiated solutions;

-- the European Parliament and the Member States' parliaments 17. to ensure without delay that the EU enlargement negotiations are guided according to the principle of proximity to citizens, the subsidiarity principle and the principle of proportionality enshrined in the Treaties;

25. to ensure and facilitate that their local and regional authorities may establish official contacts across national boundaries in order to prepare their country as well as possible for EU accession at their level of administration and responsibility;

26. to work towards the speedy establishment of joint committees composed of representatives of the Committee of the Regions and of local and regional levels in the applicant countries;


29.2.2000

EN

Official Journal of the European Communities

C 57/5

27. to ensure that their local and regional governments will be involved in fixing the priorities of Phare national programmes, in particular the institutions-building programme, so that they can prepare themselves for integration with the European Union;

28. The Committee of the Regions instructs its President to forward the present resolution to the European Commission, the EU Council, the European Parliament, the Helsinki European Council and the parliaments of the Member States, and to the governments and the parliaments of the applicant countries.

Brussels, 17 November 1999. The President of the Committee of the Regions
Manfred DAMMEYER

Opinion of the Committee of the Regions on the `Communication from the Commission to the Council, the European Parliament, the Economic and Social Committee and the Committee of the Regions on The Convergence of the Telecommunications, Media and Information Technology Sectors and the Implications for Regulation' (2000/C 57/02)
THE COMMITTEE OF THE REGIONS,

having regard to the Communication from the Commission to the Council, the European Parliament, the Economic and Social Committee and the Committee of the Regions on The Convergence of the Telecommunications, Media and Information Technology Sectors and the Implications for Regulation [COM(97) 623 final] [COM(1999) 108 final]; having regard to the decision of the Commission of 17 March 1999 to consult the Committee on the subject in accordance with the first paragraph of Article 198c of the Treaty establishing the European Community; having regard to the decision of its Bureau of 2 June 1999 to instruct Commission 3 (Trans-European networks, transport, information society) to prepare the opinion; having regard to the draft opinion (CdR 191/99 rev. 1) adopted by Commission 3 on 24 September 1999 (rapporteur: Mr Schiffmann) (D, PSE), at its 31st plenary session of 17 and 18 November 1999 (meeting of 17 November) unanimously adopted the following opinion. 1. Introduction 1.2. This was followed by a public consultation on the content of the green paper, the results of which were published by the Commission on 29 July 1998 in a working document (1). In this working document the Commission also posed questions on three problem areas which had proved to be of particular importance during the first stage of the consultation (access to networks and gateways; investment, innovation and content production; balance between the public interest and competition considerations in regulation).

Consultation on the Green Paper on Convergence

1.1. The subject of this opinion is the results of the public consultation on the green paper on The Convergence of the Telecommunications, Media and Information Technology Sectors and the Implications for Regulation. The green paper was submitted by the Commission on 3 December 1997.

(1) SEC(1998) 1284 final.


