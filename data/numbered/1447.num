EUROPEAN PARLIAMENT
DIRECTORATE-GENERAL FOR RESEARCH DIRECTORATE A DIVISION FOR INTERNATIONAL AND CONSTITUTIONAL AFFAIRS

FACTSHEET
Slovakia 1. The acquis 1 The acquis in this chapter mainly covers indirect taxation, particularly VAT and excise duties, while on direct taxation it is limited to legislation on corporate taxation. On the whole, the candidate countries have an indirect taxation system close to that of the EC. VAT was introduced in 1970 by the first and second VAT Directives. The decision on own resources for the EC led to the harmonisation of VAT. The sixth VAT directive (77/388/EEC) harmonised the basis and the assessment basis and is still the main body of law for VAT, laying down all Community definitions and principles, while leaving a number of options to Member States. The acquis in the area of excise duties covers harmonised legislation on mineral oils, tobacco products and alcoholic beverages. The legislation lays down the structure of the duty and minimum rates. Duty is payable to the Member State in which the product is consumed. All fiscal controls at the Community's internal frontiers were abolished in 1993. Legislation in the area of administrative cooperation and mutual assistance provides instruments to circumvent tax evasion and avoidance across the frontiers of Member States by information gathering and exchange. The direct taxation acquis concerns some aspects of corporate taxation and capital duty such as administrative cooperation between tax authorities and removing obstacles to cross-frontier business activities. There is also a code of conduct for business taxation. Chapter 10 - Taxation

1

Information largely drawn from the European Commission, DG Enlargement http://europa.eu.int/comm/enlargement/negotiations/index.htm

WIP/2002/11/0265

1


2. The negotiations The chapter has been closed with ten countries and provisionally closed with Bulgaria, while it remains open with Romania. Most countries have provided a timetable for full alignment for VAT and excise duties and have declared during the negotiations that they accept and will apply the principles of the code of conduct for business taxation. The Commission is analysing the candidate countries' legislation in order to identify potentially harmful practices not in line with the code. All candidate countries have requested transitional measures and a limited number of derogations, mostly as regards VAT and excise duties. One country has requested a transitional period in the area of business taxation. The level of rates of tax or duty has been the most contentious issue as excise duty in particular tends to be much lower in the candidate countries. Governments fear the economic and social implications and, hence, the political consequences of sharp tax rises on socially-sensitive goods such as cigarettes, and all have therefore asked for transitional periods on particular goods or services to allow for a longer period of adjustment. For its part, the EU has considered both the need to safeguard the proper functioning of the internal market and the political, social and economic impact in the candidate countries. Thus, some transitional measures of relatively short duration have been accepted. Chapter opened June 2001 Status closed in December 2002 (provisionally closed in March 2002) Transitional arrangements: � Reduced VAT rate on heating; � Reduced VAT rate on construction; � Reduced VAT rate on electricity, gas; � Level of VAT turnover threshold for SMEs; � Lower excise duty rates on cigarettes; � Special excise regime for fruit growers' distillation for personal consumption � VAT exemption on international passenger transport. 3. Latest Assessment of the European Commission
1

In its 1997 Opinion, the Commission concluded that the acquis concerning direct taxation should present no significant difficulties, and that where indirect taxation is concerned, Slovakia should be able to comply with the acquis on VAT and excise duties in the medium term, provided that a sustained effort was made. The Commission added that it should be possible for Slovakia to start participating in mutual assistance as the tax administration developed its expertise in this respect. Since the Opinion, and especially over the last two years, Slovakia has made good progress in aligning with the acquis, although a number of aspects remain to be addressed. Slovakia has also made progress with developing the necessary administrative capacity to implement the acquis in this area, and the organisational reshuffle implemented since 2000 represents a major development.

1

European Commission, Regular Report on Slovakia 2002, pp.75 & 76: http://www.europa.eu.int/comm/enlargement/report2002/sk_en.pdf

WIP/2002/11/0265

2


Negotiations on this chapter have been provisionally closed. Slovakia was granted transitional periods as regards the continued application of the reduced VAT rate on the supply of construction work for residential housing (until 31 December 2007) and on the supply of heat energy for private households (until 31 December 2008). Slovakia was also allowed to apply a VAT exemption and registration threshold of  35.000 for small and medium sized enterprises. Furthermore, Slovakia obtained a transitional period of one year from accession in order to apply for the reduced rate on the supply of natural gas and electricity upon accession. In the field of excise duties Slovakia was granted a transitional period regarding a delayed implementation of the excise duty rates on cigarettes until 31 December 2008 and a permanent derogation to continue applying its excise duty scheme for small fruit grower's distillation, provided that the quantity does not exceed 30 litres of fruit spirit per year per household, and that the reduced excise rate is not less than 50% of the standard national duty rate for alcohol. Slovakia is generally meeting the commitments it has made in the context of the accession negotiations. In order to be ready for membership, Slovakia will need to focus further efforts on pursuing the measures undertaken to modernise and strengthen the tax administration. Furthermore additional efforts are needed to complete transposition - except for areas where transitional arrangements were agreed - in the areas of VAT and excise duties, including intra-Community transactions. Slovakia should fully implement the comprehensive reform agenda it has established to address the identified shortcomings, including by strengthening its administrative capacity.

January 2003

WIP/2002/11/0265

3


