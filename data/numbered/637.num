EUROPEAN PARLIAMENT
1999
Session document
2

004

C5-0328/2000 03/07/2000

***II COMMON POSITION
Subject : Common position adopted by the Council on 26 June 2000 with a view to the adoption of a Regulation of the European Parliament and of the Council modifying the common principles of the European System of national and regional accounts in the Community (ESA 95) as concerns taxes and social contributions and amending Council Regulation (EC) No 2223/96 (COD 1999/0200)

EN

EN







COUNCIL OF THE EUROPEAN UNION

Brussels, 27 June 2000 (OR. en) 8276/2/00 REV 2

Interinstitutional File: 1999/0200 (COD) LIMITE ECOFIN 113 CODEC 338

LEGISLATIVE ACTS AND OTHER INSTRUMENTS Subject : Common position adopted by the Council on 26 June 2000 with a view to the adoption of a Regulation of the European Parliament and of the Council modifying the common principles of the European System of national and regional accounts in the Community (ESA 95) as concerns taxes and social contributions and amending Council Regulation (EC) No 2223/96

8276/2/00 REV 2 DG G

S W / vmc

EN


REGULATION (EC) No

/2000 OF THE EUROPEAN PARLIAMENT of

AND OF THE COUNCIL

modifying the common principles of the European System of national and regional accounts in the Community (ESA) 95 as concerns taxes and social contributions and amending Council Regulation (EC) No 2223/96

THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION, Having regard to the Treaty establishing the European Community, and in particular Article 285 thereof, Having regard to the proposal from the Commission 1, Having regard to the Opinion of the Economic and Social Committee 2, Acting in accordance with the procedure laid down in Article 251 of the Treaty 3,

1 2 3

OJ C 21 E, 25.1.2000, p. 68. OJ C 75, 15.3.2000, p. 19. Opinion of the European Parliament of 13 April 2000 (not yet published in the Official Journal), Council Common Position of (not yet published in the Official Journal) and Decision of the European Parliament of (not yet published in the Official Journal). S W / vmc DG G

8276/2/00 REV 2

EN

1


Whereas: (1) Council Regulation (EC) No 2223/96 of 25 June 1996 on the European System of national and regional accounts in the Community 1 (ESA 95) contains the reference framework of common standards, definitions, classifications and accounting rules for drawing up the accounts of the Member States for the statistical requirements of the Community, in order to obtain comparable results between Member States. (2) Article 2 of Regulation (EC) No 2223/96 sets out the conditions under which the Commission may adopt amendments to the ESA 95 methodology which are intended to clarify and improve its content. (3) It is therefore necessary to refer the clarifications concerning the recording of taxes and social contributions in ESA 95 to the European Parliament and to the Council as these clarifications modify basic concepts. (4) Article 2 of the protocol on the excessive deficit procedure relating to Article 104 of the Treaty states that the government deficit means net borrowing of the general government sector as defined in the European System of Integrated Economic Accounts (ESA).

1

OJ L 310, 30.11.1996, p. 1. Regulation as amended by Regulation (EC) No 448/98 (OJ L 58, 27.2.1998, p. 1). S W / vmc DG G

8276/2/00 REV 2

EN

2


(5)

The Statistical Programme Committee (SPC), set up by Council Decision 89/382/EEC, Euratom 1, the Committee on Monetary, Financial and Balance of Payments Statistics (CMFB), set up by Council Decision 91/115/EEC 2, and the Gross National Product Committee (GNP Committee) can state their opinion on the country-specific accounting treatment of taxes and social contributions whenever they consider it relevant.

(6) (7)

The SPC and the CMFB have been consulted. The measures necessary for the implementation of Regulation (EC) No 2223/96 should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission 3,

HAVE ADOPTED THIS REGULATION: Article 1 Purpose The purpose of this Regulation is to modify the common principles of ESA 95 as concerns taxes and social contributions so as to ensure comparability and transparency among the Member States.

1 2 3

OJ L 181, 28.6.1989, p. 47. OJ L 59, 6.3.1991, p. 19. Decision as amended by Decision 96/174/EC (OJ L 51, 1.3.1996, p. 48). OJ L 184, 17.7.1999, p. 23. S W / vmc DG G

8276/2/00 REV 2

EN

3


Article 2 General principles The impact on the net lending/borrowing of general government of taxes and social contributions recorded in the system shall not include amounts unlikely to be collected. Accordingly, the impact on general government net lending/borrowing of taxes and social contributions recorded in the system on an accrual basis shall be equivalent over a reasonable amount of time to the corresponding amounts actually received. Article 3 Treatment of taxes and social contributions in the accounts Taxes and social contributions recorded in the accounts may be derived from two sources: amounts evidenced by assessments and declarations or cash receipts. (a) If assessments and declarations are used, the amounts shall be adjusted by a coefficient reflecting assessed and declared amounts never collected. As an alternative treatment, a capital transfer to the relevant sectors could be recorded equal to the same adjustment. The coefficients shall be estimated on the basis of past experience and current expectations in respect of assessed and declared amounts never collected. They shall be specific to different types of taxes and social contributions. The determination of these coefficients shall be country-specific, the method being cleared with the Commission (Eurostat) beforehand.

8276/2/00 REV 2 DG G

S W / vmc

EN

4


(b)

If cash receipts are used, they shall be time-adjusted so that the cash is attributed when the activity took place to generate the tax liability (or when the amount of tax was determined, in the case of some income taxes). This adjustment may be based on the average time difference between the activity (or the determination of the amount of tax) and cash tax receipt. Article 4 Verification

1. The Commission (Eurostat) shall verify the implementation by Member States of the principles laid down in this Regulation. 2. From 2000 onwards, Member States shall provide the Commission (Eurostat) before the end of each year with a detailed description of the methods they plan to use for the different categories of taxes and social contributions in order to implement this Regulation. 3. The methods applied and the possible revisions shall be subject to agreement between each Member State concerned and the Commission (Eurostat). 4. The Commission (Eurostat) shall keep the SPC, the CMFB and the GNP Committee informed of the methods and the calculation of the aforementioned coefficients.

8276/2/00 REV 2 DG G

S W / vmc

EN

5


Article 5 Implementation Within 6 months of the adoption of this Regulation, the Commission shall introduce in the text of Annex A to Regulation (EC) No 2223/96, pursuant to the procedure in Article 4 thereof, the changes needed for the application of this Regulation. Article 6 Committee procedure Article 4 of Regulation (EC) No 2223/96 shall be replaced by the following: "Article 4 1. The Commission shall be assisted by the Statistical Programme Committee (hereinafter referred to as "the Committee"). 2. Where reference is made to this Article, Articles 4 and 7 of Decision 1999/468/EC shall apply, having regard to the provisions of Article 8 thereof. The period laid down in Article 4(3) of Decision 1999/468/EC shall be set at three months. 3. The Committee shall adopt its rules of procedure.".

8276/2/00 REV 2 DG G

S W / vmc

EN

6


Article 7 Entry into force 1. This Regulation shall enter into force on the twentieth day following that of its publication in the Official Journal of the European Communities. 2. Member States may ask the Commission for a transitional period of no more than two years in which to bring their accounting systems into line with this Regulation.

This Regulation shall be binding in its entirety and directly applicable in all Member States. Done at Brussels, For the European Parliament The President For the Council The President

8276/2/00 REV 2 DG G

S W / vmc

EN

7


COUNCIL OF THE EUROPEAN UNION

Brussels, 27 June 2000

Interinstitutional File: 1999/0200 (COD)

8276/2/00 REV 2 ADD 1 LIMITE ECOFIN 113 CODEC 338

COMMON POSITION ADOPTED BY THE COUNCIL ON 26 June 2000 WITH A VIEW TO THE ADOPTION OF REGULATION .../.../EC OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL MODIFYING THE COMMON PRINCIPLES OF THE EUROPEAN SYSTEM OF NATIONAL AND REGIONAL ACCOUNTS IN THE COMMUNITY (ESA 95) AS CONCERNS TAXES AND SOCIAL CONTRIBUTIONS AND AMENDING COUNCIL REGULATION (EC) No 2223/96

STATEMENT OF THE COUNCIL'S REASONS

8276/2/00 REV 2 ADD 1 DG G

cm

EN

1


Statement of the Council's reasons for the common position adopted by the Council on 26 June 2000 with a view to the adoption of a Regulation ..../..../EC of the European Parliament and of the Council modifying the common principles of the European System of national and regional accounts in the Community (ESA 95) as concerns taxes and social contributions and amending Council Regulation (EC) No 2223/96 I. 1. INTRODUCTION On 19 October 1999, the Commission forwarded to the Council a proposal for a European Parliament and Council Regulation modifying the common principles of the ESA 95 for recording taxes and social contributions and amending Council Regulation (EC) No 2223/96. 2. The above proposal is based on Article 285 of the Treaty according to which the procedure of co-decision with the European Parliament prescribed in Article 251 of the Treaty applies. 3. 4. 5. The European Parliament delivered its opinion in first reading on 13 April 2000. The Economic and Social Committee delivered its opinion on 26 January 2000. On 26 June 2000, the Council adopted its common position pursuant to Article 251 of the Treaty. II. OBJECTIVE OF THE PROPOSAL The main purpose of the proposal is to modify the common principles relating to the ESA 95 with regard to the recording of taxes and social contributions. Furthermore it aims to amend Council Regulation (EC) No 2223/96 by replacing the committee procedure of Article 4 in order to take account of Council Decision 1999/468/EC of 28 June 19991. Concerning the main purpose it aims to ensure that Community's statistical needs are met by recording taxes and social contributions in the ESA 95 in a comparable and transparent manner. The major modification to be introduced is that taxes and social contributions recorded in the system exclude amounts which are not expected to be collected. This aims to ensure that within a reasonable period, taxes and social contributions recorded on the basis of the operative event
1

OJ L 184, 17.7.1999, p. 23 cm DG G

8276/2/00 REV 2 ADD 1

EN

2


are equivalent to the corresponding amounts actually collected.

II.

ANALYSIS OF THE COMMON POSITION The common position follows the main approach of the Commission proposal. The changes in the common position as compared to the Commission proposal are set out below. Amendment 1 of the European Parliament, changing the title of the Regulation by replacing the term 'clarifying' with 'amending', is included in the common position in substance but with slightly modified drafting taking into account the Interinstitutional agreement of 22 December 1998 on common guidelines for the quality of drafting of community legislation1, in particular point 18 thereof. Article 1 In the common position, Article 1 has been redrafted in order to take into account the change made to the title of this Regulation. In substance, this change follows the Amendment 1 of the European Parliament. However, since this proposal aims to modify the existing regulation relating to the ESA 95 only as far as the principles for recording taxes and social contributions are concerned, the Council did not find it appropriate to include in this context provisions creating a legal basis for the determination of the VAT own resource using ESA 95. Therefore the Amendment 6 of the European Parliament has not been included in the common position. Article 2 The modifications to the text of Article 2 do not change the substance of the Commission proposal, but aim to improve the text by using a more precise wording.

1

OJ C073, 17.3.1999, p.1 cm DG G

8276/2/00 REV 2 ADD 1

EN

3


Article 3 In Article 3 a) of the common position, a provision for an alternative treatment has been added in order to increase flexibility. Hence the Amendment 7 of the European Parliament has been included, although with a slightly altered wording. Article 4 of the Commission proposal The Council considered that the provisions in Article 4 of the original Commission proposal were not sufficiently clear and decided to delete the Article as unnecessary. Article 4 (Article 5 of the Commission proposal) No changes have been introduced to the Commission proposal. Article 5 (Article 6 of the Commission proposal) The wording of this Article has been amended in order to improve the clarity of the text. The Amendment 8 of the European Parliament has been included. Article 6 (new) In order to take account of Council Decision 1999/468/EC of 28 June 19991, which replaces the committee procedure of Council Decision 87/373/EEC, a new Article 6 has been introduced to the common position. This provision replaces Article 4 of Regulation (EC) No 2223/96 by the new committee procedure. This amendment does not change the substance of Article 5 of the common position. Article 7 of the Commission proposal The date of entry into force in the common position is set for the 20th day following its publication in the Official Journal.

1

OJ L 184, 17.7.1999, p.23 cm DG G

8276/2/00 REV 2 ADD 1

EN

4


In Article 7 (2) of the common position the Council has introduced a provision allowing Member States to ask the Commission for a transitional period of no more than two years. The common position thus includes the Amendment 9 of the European Parliament.

Recitals The recitals have been adapted following the modifications to the Commission proposal. In order to clarify the text, the Council has deleted recital 3 of the Commission proposal and amended recital 4 (new 3) by making clear that it is necessary to refer the clarifications concerning the recording of taxes and social contributions in ESA 95 to the European Parliament and to the Council as these clarifications modify basic concepts. The Council does not disagree with the principle that there is a need to establish clear criteria for accounts in the various Member States to be homogeneous. However, the Council did not include the Amendments 2 and 3 of the European Parliament, because their purpose is not to explain the enacting terms of the Regulation (see point 10 of the Interinstitutional agreement on common guidelines for the quality of drafting of Community legislation 1) Furthermore for the reasons explained under Article 1, the Council could not accept the Amendments 4 and 5 of the European Parliament. In relation to Article 5, a new recital 5 has been included in the common position in order to clarify the role of the various committees.

1

OJ C 73, 17.3.1999, p.1 cm DG G

8276/2/00 REV 2 ADD 1

EN

5


III. CONCLUSION The Council considers that all modifications introduced by the Council in its common position are fully in line with the objectives of the proposed Regulation. This aims to ensure that, whichever statistical sources are used, the Member States are placed on an equal footing. The common position includes the substance of most of the amendments to the enacting terms as proposed by the European Parliament.

8276/2/00 REV 2 ADD 1 DG G

cm

EN

6


COMMISSION OF THE EUROPEAN COMMUNITIES

Brussels, 30.06.2000 SEC(2000) 1135 final 1999/0200 (COD)

COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT pursuant to the second subparagraph of Article 251 (2) of the EC-Treaty concerning the common position of the Council on the proposal for a European Parliament and Council Regulation modifying the principles for recording taxes and social contributions in the European System of National and Regional Accounts in the Community (ESA 95) and amending Council Regulation (EC) No 2223/96


1999/0200 (COD) COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT pursuant to the second subparagraph of Article 251 (2) of the EC-Treaty concerning the common position of the Council on the proposal for a European Parliament and Council Regulation modifying the principles for recording taxes and social contributions in the European System of National and Regional Accounts in the Community (ESA 95) and amending Council Regulation (EC) No 2223/96

1.

BACKGROUND Proposal submitted to the European Parliament and the Council (COM(1999)488 final, 1999/0200(COD)): 18 October 1999. Opinio n of Economic and Social Co mmittee: European Parliament's Opinio n (first reading): Adoption of co mmon posit ion: 26 January 2000. 13 April 2000. 26 June 2000.

2.

PURPOSE OF COMMISSION PROPOSAL To define the principles for recording taxes and social contribut ions in the European System of Nat ional and Regio nal Accounts in the Co mmunit y (ESA 95) so as to ensure comparabilit y between the accounts of different Member States and, in particular, a realist ic and transparent figure for government deficit.

3. 3.1

COMMENTS ON THE COMMON POSITION General The substant ive amendments to the Commissio n's original proposal set out in the commo n posit ion are very similar to those put forward by the European Parliament: � the tit le of the proposal describes the Regulat ion as "clarifying" the principles for recording taxes and social contribut ions in the ESA 95, while according to the commo n posit ion it actually changes them; the commo n posit ion provides for a possible alternat ive treatment of taxes and social contributions assessed but unlikely to be collected as transfers of capital. The Commissio n regrets this change, since the amended draft Regulation ensures comparabilit y of government deficits, but no longer direct comparabilit y o f tax burdens. The Commissio n can nevertheless accept the amend ment since it intends to 2

�


specify in the provisio ns to be included in the text of the ESA after the Regulation has been adopted that Member States applying this alternat ive treatment must show the amounts of capital transfers invo lved in a separate line in their accounts. This will guarantee transparency; � the commo n posit ion introduces a provisio n under which Member States may ask the Commissio n for a transit ional period of not more than two years in which to bring their account ing systems into line w ith the Regulat ion. The Commissio n is concerned that this amendment would delay the co mparabilit y of public deficit s and does not, moreover, think it is necessary fro m a technical point of view. The Commissio n can accept the amendment on the understanding that it would imply a right on its part to ask Member States to just ify such requests and possibly to reject them or limit the transit ional period to one year.

Apart fro m to these three substant ive changes co mmo n to the European Parliament and the Council, there are differences of opinio n between the two inst itutions on certain details - particularly the wording of the recitals. There are also formal differences. In particular: � the commo n posit ion takes account of the new "comitology" procedure (Council Decisio n of 28 June 1999 laying down the procedures for the exercise of implement ing powers conferred on the Commiss io n), which necessitates an amendment to the ESA Regulat ion; the commo n posit ion does not incorporate the European Parliament's amendments Nos 4, 5 and 6, which concern the use of ESA 95 for determining the VAT own resource. The Commissio n would point out that, at the first reading of the draft Regulation, it undertook to submit a separate legal act to deal wit h this specific quest ion. Work has already begun on the adoption of a Commissio n proposal on this matter for submission to the European Parliament and the Council.

�

3.2 3. 2. 1

Decisions on the European Parliament's amendments following the first reading. Accepted by the Commission and incorporated into the common position The European Parliament proposed nine amendments fo llowing its first reading o f the Commissio n's original proposal. The fo llowing four were accepted by the Commissio n and incorporated into the commo n posit ion, somet imes wit h minor changes in wording that did not alter them substant ively: � Amendment No 1 on the tit le of the proposal, which now specifies that the Regulation modifies the principles for recording taxes and social contribut ions in ESA 95 rather than merely clarifying them; Amendment No 7 (concerning Article 3 of the Regulat io n) provides for an alternat ive treatment of taxes and social contribut io ns assessed but unlikely to be co llected as transfers of capital;

�

3


� �

Amendment No 8 makes Article 6, on the implementation of the Regulat ion, more specific; Amendment No 9 adds a new paragraph to Article 7 on the entry into force of the Regulat ion, under which the Member States ma y ask the Co mmissio n for a transit ional period of not more than two years. The Co mmissio n accepts this on the understanding that it would have the right to reject such a request by a Member State.

3. 2. 2

Accepted by the Commission but not incorporated into the common position (the Commission's position) The European Parliament's Amendments Nos 2 and 3, concerning the third and fourth recitals in the Co mmissio n's original proposal, have not been incorporated into the commo n posit ion, in which the recitals in question have been reworded and combined into a single recital. The Commissio n believes this is an improvement on both its original proposal and the European Parliament's Amendments Nos 2 and 3.

3.3

New provisions introduced by the Council and the Commission's position. The Council has introduced six new provisio ns into the proposal for a Regulat ion, all of which have the Commissio n's approval: � with the new "co mito logy" procedure (Council Dec isio n 1999/468/EC of 28 June 1999) the ESA Regulat ion needs to be amended, as acknowledged in the tit le of the Regulat ion, the new seventh recital and the new Article 6; the new third recital co mbines the former third and fourth recitals and is a stylist ic improvement; the former sixth recital has been deleted, since the various countries do not interpret the relevant paragraphs of SNA 93 in the same way. It is simpler, therefore, to delete this reference; a new fift h recital states that the SPC, the CMFB and the GNP Committee may express their opinio ns on treatment of taxes and social contribut ions in the nat ional accounts of the various Member States; Art icle 2, which deals wit h general principles, has been reworded to take account of the implications of the amendment to Art icle 3 introducing the possibilit y of alternat ive treatment as transfers of capital. It is no longer possible to state in the general principles that taxes and social contribut ions do not include amounts that are unlikely to be collected, but it is still vital to confirm that government deficit does not include these amounts. the former Art icle 4 (balancing the calculat ion of GDP) has been deleted, since it was thought that the Regulat ion should contain only the general recording principles. The Co mmissio n approves of this simplificat ion.

� �

�

�

�

4


3. 4

Problems of comitology encountered during the adoption of the common position (and the position adopted by the Commission). None.

4.

CONCLUSIONS OR GENERAL REMARKS The Commissio n takes a posit ive view o f the ent ire commo n position, subject to its interpretation of the provisio n regarding the transitio nal period. The Commissio n thinks that the amendments contained in the Council's co mmon posit ion take account of the prime object ive of the proposal for a Regulat ion, which is to ensure comparabilit y and transparency in the calculat ion of government deficit in all the Member States. It should be borne in mind that ESA 95 is the tool used for calculat ing and comparing the accounts and economic aggregates of the Member States and that the Protocol on the excessive deficit procedure refers to it explic it ly as the means of determining government deficit. Even if the co mmo n position departs fro m the Commissio n's original proposal, it nevertheless retains the basic idea o f putting the various Member States on an equal footing, regardless of the statist ical sources used, i.e. the collect ion or notificat ion of taxes and social contributions. Taxes and social contribut ions unlikely to be collected should not lead to under-evaluation o f government deficit.

5.

COMMISSION STATEMENTS The Commissio n has made a unilateral statement for the Council minutes on its interpretation of the provisio ns regarding the transit ional period (see Annex).

5


