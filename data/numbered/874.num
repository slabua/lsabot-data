EUROPEAN PARLIAMENT
DIRECTORATE-GENERAL FOR RESEARCH DIRECTORATE A DIVISION FOR INTERNATIONAL AND CONSTITUTIONAL AFFAIRS

FACTSHEET
CYPRUS 1. The acquis 1 The acquis in this chapter consists of regulations and decisions covering the organisation, establishment and implementation of the EU budget. These do not require transposition into national legislation as they will be directly applicable by the candidate countries on accession. However, the effective application of these rules will also depend largely on measures covered by other chapters such as agriculture, customs, statistics, taxation and financial control. Effective administrative capacity will be necessary in relation to own resources, and this is the main area of preparatory action under this chapter for the candidate countries. There is no set model for the organisation and operation of the Member States' public finances but appropriate action must be taken to ensure the sound financial management of Community budget resources. In general terms, the candidate countries are in the process of establishing and developing the necessary central co-ordinating 'Own Resources Units' and are bringing budgetary legislation, systems and procedures in line with EU norms. As concerns the countries which will join the EU in May 2004, the Commission will continue to monitor in the course of 2003 and up to accession the level of preparedness of these countries to apply the EU system of Own Resources and to provide technical assistance where necessary. 2. The negotiations The chapter has been closed with ten countries, including Cyprus, and remains open with Bulgaria and Romania. Chapter opened: First half of 2000 Status : Closed December 2002.
1

Chapter 29 - Finance and Budgetary Provisions

Information largely drawn from the European Commission, DG Enlargement http://www.europa.eu.int/comm/enlargement/negotiations/chapters/index.htm

WIP/2003/01/0071

1


Transitional arrangements: All Candidate countries have requested transitional arrangements in relation to their payment of Own Resources contributions to the EU budget. Such requests are generally based on the expectation that while contributions to the EU budget are payable immediately on accession, there is often a time-lag between EU budgetary commitments in favour of new Member States and actual payments. For Romania and Bulgaria the current EU position is that it is premature to decide on any transitional arrangements that may be agreed in relation to the EU Budget because it is difficult at this stage to assess what the budgetary position will be on accession. For the ten countries joining the EU in May 2004, it was agreed at the conclusion of negotiations in Copenhagen that new Member States would pay fully their contributions to the EU budget from the first day of accession. However, as part of the overall financial package in their favour, it was also agreed that temporary budgetary compensation would be payable to the Czech Republic, Cyprus, Malta and Slovenia in the period 2004-2006 with the aim of ensuring these countries would not experience a deterioration in their net budgetary balance (i.e. the difference between what a country pays into the EU budget and what it receives from it) vis-�vis the EU budget compared to the positive position they were in 2003 as recipients of preaccession aid. For the other six acceding countries, estimates show that, based on the overall agreement of Copenhagen, these countries would be financially better off in net terms already in 2004 compared to their position in 2003. Nevertheless, it was also agreed in Copenhagen to provide all ten acceding countries with funds from a special temporary cash-flow facility (in total 2.4 billion) in the period 2004-2006 to further improve their budgetary positions. 1 3. Position of the European Parliament In its resolution of 13 June 20022, Parliament welcomes the proposal made in January 2001, as part of the Commission's common financial framework (2004-2006) for accession negotiations, to adjust the framework of funds to help the northern part of Cyprus catch up in its development after accession, in the context of a political settlement. In its resolution of 20 November 20023, Parliament: � Reiterates its previous positions on the financial impact and budgetary implications of the enlargement of the European Union, especially as put forward in its resolution of 13 June 2002 on the financial impact of the enlargement of the European Union4; stresses that as a result of the accession of the new Member States, the financial perspective needs to be adjusted to cater for enlargement in agreement between the two arms of the budgetary authority, Parliament and the Council, according to the procedure laid down in paragraph 25 of the Interinstitutional Agreement of 6 May 1999; recalls that at that time, the financial perspective was agreed under the assumption that six new Member States would join the Union in 2002; � Considers that no new Member State should become a net contributor to the Community budget during the first few years after accession; notes the Commission proposals on a temporary budgetary compensation; � Expects an arrangement which will all cases preclude a country being in a worse financial position after accession than it was immediately prior to accession;
1 2

See Annexes for tables showing the financial framework agreed at Copenhagen. Resolution on the state of enlargement negotiations. � 41: A5-0190/2002 3 Resolution on the progress of the candidate countries towards accession, � 27, 28, 29 & 47: A5-0371/2002 4 Resolution on financing enlargement: A5-0178/2002

WIP/2003/01/0071

2


�

is convinced that, in the event of a political solution, a supplementary budgetary effort will be required in order to facilitate the 'catching-up' of the north of the island with the acquis communautaire and to contribute to the reconstruction of the 'buffer zone' as well as those areas in most need.
1

4. Latest Assessment by the European Commission

In its 2000 Regular Report, the Commission concluded that, overall, Cyprus had established the necessary rules and procedures concerning its budget for medium term programming of expenditure as well as for evaluation and follow-up. However, these procedures needed to be strengthened. Since then, Cyprus has proceeded with preparatory work. The budgetary system enabling it to meet Community requirements with regard to co-financing and multi-annual programming has been put in place. The administrative capacity of the services involved has been increased substantially, including the co-ordination aspects. Overall preparations for implementation of the acquis in the field of the national budget and EC co-financed measures are progressing well after the establishment of a single budget in 2000, but the effort needs to be sustained. Negotiations on this chapter continue. Cyprus has not requested any transitional arrangements. Cyprus is generally meeting the commitments it has made in the accession negotiations. In order to complete preparations for membership Cyprus's efforts need to focus on setting up effective instruments to combat VAT and customs duty fraud, and improving the management of the GNP resource and the traditional own resources. It should also transform the current temporary co-ordinating unit in the Ministry of Finance, into a permanent resources unit for the co-ordination of calculation, control, payment of own resources, and reporting to the EU.

January 2003 Annexes

1

European Commission, Regular Report on Cyprus 2002, pp. 116: http://www.europa.eu.int/comm/enlargement/report2002/cy_en.pdf

WIP/2003/01/0071

3


Annex 1

WIP/2003/01/0071

4


WIP/2003/01/0071

5


Annex 2

WIP/2003/01/0071

6


