EUROPEAN PARLIAMENT
1999
��� � � � � � � � ��

2004

Session document

C5-0640/2001
2000/0080(COD)

EN
10/12/2001

Common position
adopted by the Council on 3 December 2001 with a view to the adoption of a Directive of the European Parliament and of the Council on the approximation of the laws of the Member States relating to food supplements

Doc.12394/2/01 + ADD1 SEC(2001)1975

EN

EN



COUNCIL OF THE EUROPEAN UNION

Brussels, 4 December 2001 (OR. fr) 12394/2/01 REV 2

Interinstitutional File: 2000/0080 (COD) DENLEG 46 CODEC 960

LEGISLATIVE ACTS AND OTHER INSTRUMENTS Subject : Common Position adopted by the Council on 3 December 2001 with a view to the adoption of a Directive of the European Parliament and of the Council on the approximation of the laws of the Member States relating to food supplements

12394/2/01 REV 2 DG I

S W /jrb

EN


DIRECTIVE 2001/ /EC OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL of on the approximation of the laws of the Member States relating to food supplements (Text with EEA relevance)

THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION, Having regard to the Treaty establishing the European Community, and in particular Article 95 thereof, Having regard to the proposal from the Commission 1, Having regard to the Opinion of the Economic and Social Committee 2, Acting in accordance with the procedure laid down in Article 251 of the Treaty 3,

1 2 3

OJ C 311 E, 31.10.2000, p. 207 and C 180 E, 26.6.2001, p. 248. OJ C 14, 16.1.2001, p. 42. Opinion of the European Parliament of 14 February 2001 (not yet published in the Official Journal), Council Common Position of (not yet published in the Official Journal) and Decision of the European Parliament of (not yet published in the Official Journal). S W /jrb DG I

12394/2/01 REV 2

EN

1


Whereas: (1) There is an increasing number of products marketed in the Community as foods containing concentrated sources of nutrients and presented for supplementing the intake of those nutrients from the normal diet. (2) Those products are regulated in Member States by differing national rules that may impede their free movement, create unequal conditions of competition, and thus have a direct impact on the functioning of the internal market. It is therefore necessary to adopt Community rules on those products marketed as foodstuffs. (3) An adequate and varied diet could, under normal circumstances, provide all necessary nutrients for normal development and maintenance of a healthy life in quantities which meet those established and recommended by generally acceptable scientific data. However, surveys show that this ideal situation is not being achieved for all nutrients and by all groups of the population across the Community. (4) Consumers, because of their particular lifestyles or for other reasons, may choose to supplement their intake of some nutrients through food supplements. (5) In order to ensure a high level of protection for consumers and facilitate their choice, the products that will be put on to the market must be safe and bear adequate and appropriate labelling.

12394/2/01 REV 2 DG I

S W /jrb

EN

2


(6)

There is a wide range of nutrients and other ingredients that might be present in food supplements including, but not limited to, vitamins, minerals, amino acids, essential fatty acids, fibre and various plants and herbal extracts.

(7)

As a first stage, this Directive should lay down specific rules for vitamins and minerals used as ingredients of food supplements. Food supplements containing vitamins or minerals as well as other ingredients should also be in conformity with the specific rules on vitamins and minerals laid down in this Directive.

(8)

Specific rules concerning nutrients, other than vitamins and minerals, or other substances with a nutritional or physiological effect used as ingredients of food supplements should be laid down at a later stage, provided that adequate and appropriate scientific data about them become available. Until such specific Community rules are adopted and without prejudice to the provisions of the Treaty, national rules concerning nutrients or other substances with nutritional or physiological effect used as ingredients of food supplements, for which no Community specific rules have been adopted, may be applicable.

(9)

Only vitamins and minerals normally found in, and consumed as part of, the diet should be allowed to be present in food supplements although this does not mean that their presence therein is necessary. Controversy as to the identity of those nutrients that could potentially arise should be avoided. Therefore it is appropriate to establish a positive list of those vitamins and minerals.

12394/2/01 REV 2 DG I

S W /jrb

EN

3


(10) There is a wide range of vitamin preparations and mineral substances used in the manufacture of food supplements currently marketed in some Member States that have not been evaluated by the Scientific Committee for Food and consequently are not included in the positive lists. These should be submitted to the Scientific Committee for Food for urgent evaluation, as soon as appropriate files are presented by the interested parties. (11) The chemical substances used as sources of vitamins and minerals in the manufacture of food supplements should be safe and also be available to be used by the body. For this reason, a positive list of those substances should also be established. Such substances as have been approved by the Scientific Committee for Food, on the basis of the said criteria, for use in the manufacture of foods intended for infants and young children and other foods for particular nutritional uses can also be used in the manufacture of food supplements. (12) In order to keep up with scientific and technological developments it is important to revise the lists promptly, when necessary. Such revisions would be implementing measures of a technical nature and their adoption should be entrusted to the Commission in order to simplify and expedite the procedure. (13) Excessive intake of vitamins and minerals may result in adverse effects and therefore necessitate the setting of maximum safe levels for them in food supplements, as appropriate. Those levels must ensure that the normal use of the products under the instructions of use provided by the manufacturer will be safe for the consumer.

12394/2/01 REV 2 DG I

S W /jrb

EN

4


(14) When maximum levels are set, therefore, account should be taken of the upper safe levels of the vitamins and minerals, as established by scientific risk assessment based on generally acceptable scientific data, and of intakes of those nutrients from the normal diet. Due account should also be taken of reference intake amounts when setting maximum levels. (15) Food supplements are purchased by consumers for supplementing intakes from the diet. In order to ensure that this aim is achieved, if vitamins and minerals are declared on the label of food supplements, they should be present in the product in a significant amount. (16) The adoption of the specific values for maximum and minimum levels for vitamins and minerals present in food supplements, based on the criteria set out in this Directive and appropriate scientific advice, would be an implementing measure and should be entrusted to the Commission. (17) General labelling provisions and definitions are contained in Directive 2000/13/EC of the European Parliament and of the Council of 20 March 2000 on the approximation of the laws of the Member States relating to the labelling, presentation and advertising of foodstuffs 1, and do not need to be repeated. This Directive should therefore be confined to the necessary additional provisions.

1

OJ L 109, 6.5.2000, p. 29. S W /jrb DG I

12394/2/01 REV 2

EN

5


(18) Council Directive 90/496/EEC of 24 September 1990 on nutrition labelling for foodstuffs 1 does not apply to food supplements. Information relating to nutrient content in food supplements is essential for allowing the consumer who purchases them to make an informed choice and use them properly and safely. That information should, in view of the nature of those products, be confined to the nutrients actually present and be compulsory. (19) Given the particular nature of food supplements, additional means to those usually available to monitoring bodies should be available in order to facilitate efficient monitoring of those products. (20) The measures necessary for the implementation of this Directive should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission 2, HAVE ADOPTED THIS DIRECTIVE: Article 1 1. This Directive concerns food supplements marketed as foodstuffs and presented as such. These products shall be delivered to the ultimate consumer only in a pre-packaged form.

1 2

OJ L 276, 6.10.1990, p. 40. OJ L 184, 17.7.1999, p. 23. S W /jrb DG I

12394/2/01 REV 2

EN

6


2. This Directive shall not apply to medicinal products as defined by Council Directives 65/65/EEC of 26 January 1965 on medicinal products 1 and 92/73/EEC of 22 September 1992 widening the scope of Directives 65/65/EEC and 75/319/EEC on the approximation of provisions laid down by law, regulation or administrative action relating to medicinal products and laying down additional provisions on homeopathic medicinal products 2. Article 2 For the purposes of this Directive: (a) "food supplements" means foodstuffs the purpose of which is to supplement the normal diet and which are concentrated sources of nutrients or other substances with a nutritional or physiological effect, alone or in combination, marketed in dose form, namely forms such as capsules, pastilles, tablets, pills and other similar forms, sachets of powder, ampoules of liquids, drop dispensing bottles, and other similar forms of liquids and powders designed to be taken in measured small unit quantities; (b) "nutrients" means the following substances: (i) (ii) vitamins, minerals.

1 2

OJ L 22, 9.2.1965, p. 369. Directive as last amended by Directive 93/39/EC (OJ L 214, 24.8.1993, p. 22). OJ L 297, 13.10.1992, p. 8. S W /jrb DG I

12394/2/01 REV 2

EN

7


Article 3 Member States shall ensure that food supplements may be marketed within the Community only if they comply with the rules laid down in this Directive. Article 4 1. Only vitamins and minerals listed in Annex I, in the forms listed in Annex II, may be used for the manufacture of food supplements, subject to paragraph 6. 2. The purity criteria for substances listed in Annex II shall be adopted in accordance with the procedure referred to in Article 13(2), except where they apply pursuant to paragraph 3. 3. Purity criteria for substances listed in Annex II, specified by Community legislation for their use in the manufacture of foodstuffs for purposes other than those covered by this Directive, shall apply. 4. For those substances listed in Annex II for which purity criteria are not specified by Community legislation, and until such specifications are adopted, generally acceptable purity criteria recommended by international bodies shall be applicable and national rules setting stricter purity criteria may be maintained. 5. Modifications to the lists referred to in paragraph 1 shall be adopted in accordance with the procedure referred to in Article 13(2).

12394/2/01 REV 2 DG I

S W /jrb

EN

8


6. By way of derogation from paragraph 1 and until ................. , Member States may allow in their territory the use of vitamins and minerals not listed in Annex I, or in forms not listed in Annex II, provided that: (a) the substance in question is used in one or more food supplements marketed in the Community on the date of entry into force of this Directive, (b) the Scientific Committee for Food has not given an unfavourable opinion in respect of the use of that substance, or its use in that form, in the manufacture of food supplements, on the basis of a dossier supporting use of the substance in question to be submitted to the Commission by the Member State not later than ................ . 7. Notwithstanding paragraph 6, Member States may, in compliance with the rules of the Treaty, continue to apply existing national restrictions or bans on trade in food supplements containing vitamins and minerals not included in the list in Annex I or in the forms not listed in Annex II. 8. Not later than ................ , the Commission shall submit to the European Parliament and the Council a report on the advisability of establishing specific rules, including, where appropriate, positive lists, on categories of nutrients or of substances with a nutritional or physiological effect other than those referred to in paragraph 1, accompanied by any proposals for amendment to this Directive which the Commission deems necessary.

  

The end of the seventh year as from the date of entry into force of this Directive. 18 months after the date of entry into force of this Directive. Five years after the date of entry into force of this Directive. S W /jrb DG I

12394/2/01 REV 2

EN

9


Article 5 1. Maximum amounts of vitamins and minerals present in food supplements per daily portion of consumption as recommended by the manufacturer shall be set, taking the following into account: (a) upper safe levels of vitamins and minerals established by scientific risk assessment based on generally accepted scientific data, taking into account, as appropriate, the varying degrees of sensitivity of different consumer groups; (b) intake of vitamins and minerals from other dietary sources.

2. When the maximum levels referred to in paragraph 1 are set, due account should also be taken of reference intakes of vitamins and minerals for the population. 3. To ensure that significant amounts of vitamins and minerals are present in food supplements, minimum amounts per daily portion of consumption as recommended by the manufacturer shall be set, as appropriate. 4. The maximum and minimum amounts of vitamins and minerals referred to in paragraphs 1, 2 and 3 shall be adopted in accordance with the procedure referred to in Article 13(2).

12394/2/01 REV 2 DG I

S W /jrb

EN

10


Article 6 1. For the purposes of Article 5(1) of Directive 2000/13/EC, the name under which products covered by this Directive are sold shall be "food supplement". 2. The labelling, presentation and advertising must not attribute to food supplements the property of preventing, treating or curing a human disease, or refer to such properties. 3. Without prejudice to Directive 2000/13/EC, the labelling shall bear the following particulars: (a) the names of the categories of nutrients or substances that characterise the product or an indication of the nature of those nutrients or substances; (b) (c) (d) the portion of the product recommended for daily consumption; a warning not to exceed the stated recommended daily dose; a statement to the effect that food supplements should not be used as a substitute for a varied diet; (e) a statement to the effect that the products should be stored out of the reach of young children.

12394/2/01 REV 2 DG I

S W /jrb

EN

11


Article 7 The labelling, presentation and advertising of food supplements shall not include any mention stating or implying that a balanced and varied diet cannot provide appropriate quantities of nutrients in general. Rules for implementing this Article may be specified in accordance with the procedure referred to in Article 13(2). Article 8 1. The amount of the nutrients or substances with a nutritional or physiological effect present in the product shall be declared on the labelling in numerical form. The units to be used for vitamins and minerals shall be those specified in Annex I. Rules for implementing this paragraph may be specified in accordance with the procedure referred to in Article 13(2). 2. The amounts of the nutrients or other substances declared shall be those per portion of the product as recommended for daily consumption on the labelling. 3. Information on vitamins and minerals shall also be expressed as a percentage of the reference values mentioned, as the case may be, in the Annex to Directive 90/496/EEC.

12394/2/01 REV 2 DG I

S W /jrb

EN

12


Article 9 1. The declared values mentioned in Article 8(1) and (2) shall be average values based on the manufacturer's analysis of the product. Further rules for implementing this paragraph with regard in particular to the differences between the declared values and those established in the course of official checks shall be decided upon in accordance with the procedure referred to in Article 13(2). 2. The percentage of the reference values for vitamins and minerals mentioned in Article 8(3) may also be given in graphical form. Rules for implementing this paragraph may be adopted in accordance with the procedure referred to in Article 13(2). Article 10 To facilitate efficient monitoring of food supplements, Member States may require the manufacturer or the person placing the product on the market in their territory to notify the competent authority of that placing on the market by forwarding it a model of the label used for the product.

12394/2/01 REV 2 DG I

S W /jrb

EN

13


Article 11 1. Without prejudice to Article 4(7), Member States shall not, for reasons related to their composition, manufacturing specifications, presentation or labelling, prohibit or restrict trade in products referred to in Article 1 which comply with this Directive and, where appropriate, with Community acts adopted in implementation of this Directive. 2. Without prejudice to the Treaty, in particular Articles 28 and 30 thereof, paragraph 1 shall not affect national provisions which are applicable in the absence of Community acts adopted under this Directive. Article 12 1. Where a Member State, as a result of new information or of a reassessment of existing information made since this Directive or one of the implementing Community acts was adopted, has detailed grounds for establishing that a product referred to in Article 1 endangers human health though it complies with the said Directive or said acts, that Member State may temporarily suspend or restrict application of the provisions in question within its territory. It shall immediately inform the other Member States and the Commission thereof and give reasons for its decision. 2. The Commission shall examine as soon as possible the grounds adduced by the Member State concerned and shall consult the Member States within the Standing Committee for Foodstuffs, and shall then deliver its opinion without delay and take appropriate measures.

12394/2/01 REV 2 DG I

S W /jrb

EN

14


3. If the Commission considers that amendments to this Directive or to the implementing Community acts are necessary in order to remedy the difficulties mentioned in paragraph 1 and to ensure the protection of human health, it shall initiate the procedure referred to in Article 13(2) with a view to adopting those amendments. The Member State that has adopted safeguard measures may in that event retain them until the amendments have been adopted. Article 13 1. The Commission shall be assisted by the Standing Committee for Foodstuffs instituted by Decision 69/414/EEC 1 (hereinafter referred to as "the Committee"). 2. Where reference is made to this paragraph, Articles 5 and 7 of Decision 1999/468/EC shall apply, having regard to the provisions of Article 8 thereof. The period laid down in Article 5(6) of Decision 1999/468/EC shall be set at three months. 3. The Committee shall adopt its rules of procedure. Article 14 Provisions that may have an effect upon public health shall be adopted after consultation with the Scientific Committee for Food.

1

OJ L 291, 19.11.1969, p. 9. S W /jrb DG I

12394/2/01 REV 2

EN

15


Article 15 Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive by ............ . They shall forthwith inform the Commission thereof. Those laws, regulations and administrative provisions shall be applied in such a way as to: (a) (b) permit trade in products complying with this Directive, from .........  at the latest; prohibit trade in products which do not comply with the Directive, from .........  at the latest.

When Member States adopt these measures, they shall contain a reference to this Directive or be accompanied by such a reference on the occasion of their official publication. The methods of making such reference shall be adopted by the Member States. Article 16 This Directive shall enter into force on the day of its publication in the Official Journal of the European Communities.

  

The last day of the month, one year after the entry into force of this Directive. The first day of the month following the end of the first year to have expired after the entry into force of this Directive. First day of the month following the end of the third year to have expired after the entry into force of this Directive. S W /jrb DG I

12394/2/01 REV 2

EN

16


Article 17 This Directive is addressed to the Member States. Done at,

For the European Parliament The President

For the Council The President

_______________

12394/2/01 REV 2 DG I

S W /jrb

EN

17


ANNEX I Vitamins and minerals which may be used in the manufacture of food supplements 1. Vitamins Vitamin A (�g RE) Vitamin D (�g) Vitamin E (mg -TE) Vitamin K (�g) Vitamin B1 (mg) Vitamin B2 (mg) Niacin (mg NE) Pantothenic acid (mg) Vitamin B6 (mg) Folic acid (�g) Vitamin B12 (�g) Biotin (�g) Vitamin C (mg)

12394/2/01 REV 2 ANNEX I

SW/mjm DG I

EN

1


2. Minerals Calcium (mg) Magnesium (mg) Iron (mg) Copper (�g) Iodine (�g) Zinc (mg) Manganese (mg) Sodium (mg) Potassium (mg) Selenium (�g) Chromium (�g) Molybdenum (�g) Fluoride (mg) Chloride (mg) Phosphorus (mg)

_____________

12394/2/01 REV 2 ANNEX I

SW/mjm DG I

EN

2


ANNEX II Vitamin and mineral substances which may be used in the manufacture of food supplements

A. 1.

Vitamins VITAMIN A a) b) c) d) retinol retinyl acetate retinyl palmitate beta-carotene

2.

VITAMIN D a) b) cholecalciferol ergocalciferol

3.

VITAMIN E a) b) c) d) e) D-alpha-tocopherol DL-alpha-tocopherol D-alpha-tocopheryl acetate DL-alpha-tocopheryl acetate D-alpha-tocopheryl acid succinate

12394/2/01 REV 2 ANNEX II

S W /jrb DG I

EN

1


4.

VITAMIN K a) phylloquinone (phytomenadione)

5.

VITAMIN B1 a) b) thiamin hydrochloride thiamin mononitrate

6.

VITAMIN B2 a) b) riboflavin riboflavin 5'-phosphate, sodium

7.

NIACIN a) b) nicotinic acid nicotinamide

8.

PANTOTHENIC ACID a) b) c) D-pantothenate, calcium D-pantothenate, sodium dexpanthenol

9.

VITAMIN B6 a) b) pyridoxine hydrochloride pyridoxine 5'-phosphate

10.

FOLIC ACID a) pteroylmonoglutamic acid

12394/2/01 REV 2 ANNEX II

S W /jrb DG I

EN

2


11.

VITAMIN B12 a) b) cyanocobalamin hydroxocobalamin

12.

BIOTIN a) D-biotin

13.

VITAMIN C a) b) c) d) e) L-ascorbic acid sodium-L-ascorbate calcium-L-ascorbate potassium-L-ascorbate L-ascorbyl 6-palmitate

B.

Minerals calcium carbonate calcium chloride calcium salts of citric acid calcium gluconate calcium glycerophosphate calcium lactate calcium salts of orthophosphoric acid calcium hydroxide calcium oxide magnesium acetate magnesium carbonate

12394/2/01 REV 2 ANNEX II

S W /jrb DG I

EN

3


magnesium chloride magnesium salts of citric acid magnesium gluconate magnesium glycerophosphate magnesium salts of orthophosphoric acid magnesium lactate magnesium hydroxide magnesium oxide magnesium sulphate ferrous carbonate ferrous citrate ferric ammonium citrate ferrous gluconate ferrous fumarate ferric sodium diphosphate ferrous lactate ferrous sulphate ferric diphosphate (ferric pyrophosphate) ferric saccharate elemental iron (carbonyl+electrolytic+hydrogen reduced) cupric carbonate cupric citrate cupric gluconate cupric sulphate copper lysine complex

12394/2/01 REV 2 ANNEX II

S W /jrb DG I

EN

4


sodium iodide sodium iodate potassium iodide potassium iodate zinc acetate zinc chloride zinc citrate zinc gluconate zinc lactate zinc oxide zinc carbonate zinc sulphate manganese carbonate manganese chloride manganese citrate manganese gluconate manganese glycerophosphate manganese sulphate sodium bicarbonate sodium carbonate sodium chloride sodium citrate sodium gluconate sodium lactate sodium hydroxide sodium salts of orthophosphoric acid

12394/2/01 REV 2 ANNEX II

S W /jrb DG I

EN

5


potassium bicarbonate potassium carbonate potassium chloride potassium citrate potassium gluconate potassium glycerophosphate potassium lactate potassium hydroxide potassium salts of orthophosphoric acid sodium selenate sodium hydrogen selenite sodium selenite chromium (III) chloride chromium (III) sulphate ammonium molybdate (molybdenum (VI)) sodium molybdate (molybdenum (VI)) potassium fluoride sodium fluoride

12394/2/01 REV 2 ANNEX II

S W /jrb DG I

EN

6


COUNCIL OF THE EUROPEAN UNION

Brussels, 4 December 2001 (OR. fr) 12394/2/01

Interinstitutional File: 2000/0080 (COD)

REV 2 ADD 1

DENLEG 46 CODEC 960

Subject :

Common position adopted by the Council on 3 December 2001 with a view to the adoption of a Directive of the European Parliament and of the Council on the approximation of the laws of the Member States relating to food supplements

STATEMENT OF THE COUNCIL'S REASONS

12394/2/01 REV 2 ADD 1 DG I

man/MM/vmd

EN

1


I.

INTRODUCTION 1. On 11 May 2000 the Commission submitted to the Council a proposal for a Directive of the European Parliament and of the Council, based on Article 95 of the Treaty, on the approximation of the laws of the Member States relating to food supplements 1. 2. 3. 4. The Economic and Social Committee delivered its Opinion on 19 October 2000 2. The European Parliament delivered its Opinion on 14 February 2001 3. On the occasion of the European Parliament vote, the Commission accepted 16 of the 38 amendments adopted, and forwarded an amended proposal 4 to the Council on 21 March 2001, following the above European Parliament Opinion. 5. On 3 December 2001 the Council adopted its Common Position in accordance with Article 251 of the Treaty.

II.

OBJECTIVE The marketing of numerous products in Community territory over recent years, in particular as "food supplements", has led to an increase in the number of national rules governing such products, and in the Commission's opinion this has created barriers to free movement. After consulting widely with the parties concerned, the Commission concluded that it was necessary to adopt Community rules in that area, inter alia in connection with its food safety activities.

1 2 3 4

OJ C 311 E, 31.10.2000, p. 207. OJ C 14, 16.1.2001, p. 42. 6096/01 CODEC 119 DENLEG 5. OJ C 180 E, 26.6.2001, p. 248. man/MM/vmd DG I

12394/2/01 REV 2 ADD 1

EN

2


The text of the common position aims to harmonise Member States' national legislations on the basis of the following elements: � � definition of the concept of food supplement; establishment of a list of vitamins and minerals which may be used in the manufacture of a food supplement in the forms determined in a second list; � determination of the procedures for derogation from and amendment of the aforementioned lists; � � establishment of quantities of vitamins and mineral salts present in food supplements; establishment of appropriate standards as regards labelling, presentation and advertising; � introduction of safeguard measures in the event of a risk to public health.

III. ANALYSIS OF THE COMMON POSITION A. General remarks In general, the Council has followed the Commission's amended proposal. It has accepted � either in whole or in substance � the sixteen amendments adopted at first reading by the European Parliament as set out by the Commission in its amended proposal.

12394/2/01 REV 2 ADD 1 DG I

man/MM/vmd

EN

3


The other European Parliament amendments which were not incorporated into the Commission's amended proposal have also been omitted by the Council. However, the idea expressed in amendments 39, 49 and 32 rev., according to which some vitamins and substances currently marketed but not covered by the Annexes should undergo scientific evaluation, has been taken into account to some extent in Article 4(6) (cf. section B(b)). B. Main innovations introduced by the Council 1. The main innovations introduced by the Council relate to: � � � (a) the setting of quantities of food supplements, authorisation of the placing on the market of existing food supplements, the revision of the Directive. setting of quantities of food supplements: Article 5, in conjunction with recital 14, ensures a balanced approach guaranteeing both the safety of such supplements and due regard for reference intakes for the population. (b) authorisation of existing food supplements not listed in Annexes I or II: Article 4(6) and (7), in conjunction with Article 15 on the text's entry into force, aims to allay the concern of participants in this market by laying down clear procedures and time limits for all current supplements not listed in Annexes I or II to the text, thereby reassuring both industry and the consumer.

12394/2/01 REV 2 ADD 1 DG I

man/MM/vmd

EN

4


(c)

Directive revision clause: Article 4(8) establishes an amendment procedure after a period of five years, which will enable the Commission usefully to take stock of the implementation of the Directive, particularly with regard to its scope, and to propose amendments if necessary.

2.

Furthermore, the Council has considered that the obligation to provide information when a product is placed on the market, as provided for in Article 10, is ultimately the responsibility of the Member States. Finally, it has introduced a clause providing for the possibility of the participation of the Standing Committee on Foodstuffs in order to specify the procedures for implementing Articles 7 and 8 on the labelling, presentation and advertising of food supplements.

IV.

CONCLUSIONS The Council considers that the Common Position responds to a large extent to the basic wishes expressed by the European Parliament, while at the same time taking sufficient account of the concern expressed by Member States, inter alia with regard to public health and/or Community harmonisation. The Council considers that the Common Position achieves a good balance between the prerequisites for the proper functioning of the single market and consumer protection/information.

12394/2/01 REV 2 ADD 1 DG I

man/MM/vmd

EN

5



COMMISSION OF THE EUROPEAN COMMUNITIES

Brussels, 7.12.2001 SEC(2001) 1975 final 2000/0080 (COD)

COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT pursuant to the second subparagraph of Article 251 (2) of the EC Treaty concerning the common position of the Council on the adoption of a proposal for a European Parliament and Council Directive on the approximation of the laws of the Member States relating to food supplements


COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT pursuant to the second subparagraph of Article 251 (2) of the EC Treaty concerning the common position of the Council on the adoption of a proposal for a European Parliament and Council Directive on the approximation of the laws of the Member States relating to food supplements

1-

BACKGROUND 10 May 2000. 19 October 2000. 14 February 2001. 19 March 2001. 3 December 2001.

Date of transmission of the proposal to the EP and the Council (document COM(2000)222 final � 2000/0080COD): Date of the opinion of the Economic and Social Committee: Date of the opinion of the European Parliament, first reading: Date of transmission of the amended proposal: Date of adoption of the common position: 2OBJECTIVE OF THE COMMISSION PROPOSAL

The proposal aims to ensure both a high level of public health protection and the free circulation of products concerned by ensuring that food supplements are safe and bear adequate and appropriate labelling. The most important points of the present proposal are : � Defines food supplements as concentrated sources of nutrients and other ingredients with a nutritional or physiological effect and, as a first stage, lays down specific provisions for vitamins and minerals. Specific rules to cover other ingredients may be laid down when scientific knowledge allows. � Sets a positive list of vitamins and minerals and their specific forms to be used in their manufacture. � Lays down scientifically based risk analysis (safety) as the basic principle for establishing maximum amounts of vitamins and minerals in the products, through the Regulatory Committee procedure, taking into account also intakes from normal foods and fortified foods. The reference intakes for the population should also be taken into account in the case of some vitamins and minerals that have upper safe levels close (1-5 times) to these reference intakes. � Provides for appropriate specific labelling rules.

2


� Enables Member States to require the notification of the marketing of these products in order to facilitate their monitoring. 3COMMENTS ON THE COMMON POSITION

3.1. The Common Position is a carefully balanced compromise, in particular on the issue of the criteria to be used for setting maximum levels for vitamins and minerals in these products. It is in line with the basic principles of food law, namely that food should be safe and properly labelled so that consumers can make an informed choice from a wide range of safe products. 3.2. The Commission included in the modified proposal those amendments adopted in first reading by the European Parliament that were acceptable. Those amendments remain in the Common Position in a way that should be satisfactory to the EP. There are three important differences between the modified proposal and the Common Position : (1) In the Common Position, the issue of setting maximum limits for vitamins and minerals in food supplements is resolved by providing that scientific risk assessment and intakes from other sources will be primary factors to be taken into account for the safety of these products, and that in the overall exercise the reference intakes for the population will be given due consideration (Article 5). While this differs from the Commission proposal, supported by the Parliament, that the reference intakes be taken into account only in the case of some nutrients, the Commission can accept the Common Position because it strengthens the process of establishing safe maximum levels for vitamins and minerals in food supplements. Article 4.8 of the Common Position requires the Commission to submit to the European Parliament and the Council, within five years from the entry into force of the Directive, a report on the advisability of establishing specific rules on other nutrients or substances with a nutritional or physiological effect, accompanied by any proposals for appropriate legal measures. The Commission can accept the request to submit a report on the subject within a specified time limit while maintaining the right of initiative to propose appropriate measures. Article 4.6 of the Common Position is new. It foresees a transitional period during which vitamins and minerals and certain of their forms that are not listed in the Annexes but are currently in food supplements marketed in some Member States, may continue to be used until their evaluation by the Scientific Committee for Food and eventual insertion in the Annexes. It was introduced to give effect to an amendment of the European Parliament that was not acceptable in its original form. The Commission supports this new position. CONCLUSION

(2)

(3)

4-

For the above reasons the Commission supports the Common Position adopted by the Council on 3.12.2001.

3


