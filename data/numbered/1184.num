EUROPEAN PARLIAMENT
DIRECTORATE-GENERAL FOR RESEARCH DIRECTORATE A DIVISION FOR INTERNATIONAL AND CONSTITUTIONAL AFFAIRS

FACTSHEET
MALTA 1. The acquis 1 The energy acquis represents the body of all energy related EU law, regulations and policies. Implementing the acquis requires not only adequate legislation but also properly functioning institutions (for example, a regulatory body, as required in the electricity and gas directives, a nuclear safety authority etc.). In view of the energy acquis, candidate countries need notably to: � decide on an overall energy policy with clear timetables for restructuring the sector; � prepare for the internal energy market (the Gas and Electricity directives; the Directive on electricity produced from renewable energy sources); � improve energy networks in order to create a real European market; � prepare for crisis situations, particularly through the constitution of 90 days of oil stocks; � address the social, regional and environmental consequences of the restructuring of mines; � waste less energy and increase the use of renewable energies such as wind, hydro, solar and biomass in their energy balance; � improve the safety of nuclear power plants in order to ensure that electricity is produced according to a high level of nuclear safety; � ensure that nuclear waste is handled in a responsible manner; and prepare for the implementation of Euratom Safeguards on nuclear materials. Candidate countries have made considerable progress in the last few years and the abovementioned issues are applicable to them in varying degrees. However, more is necessary, and this will evidently require large amounts of investment funding. Although the EU will continue to assist with pre-accession aid, the bulk will have to be financed by candidate countries themselves. Private investments have an important role to play in this context and require a stable investment climate.
1

Chapter 14 - Energy

Information largely drawn from the European Commission, DG Enlargement http://europa.eu.int/comm/enlargement/negotiations/index.htm

WIP/2002/11/0206

1


As regards the issue of nuclear energy, the European Union has repeatedly emphasised the importance of a high level of nuclear safety in candidate countries. In June 2001, the Council of the European Union took note of a Report on Nuclear Safety in the Context of Enlargement. This Report contains recommendations to all candidate countries to continue their national safety improvement programmes, including the safe management of spent fuel and radioactive waste, and regarding the safety of their research reactors. All candidate countries have responded to these recommendations. During the first half of 2002, a special Peer Review on nuclear safety assessed the progress made by candidate countries in implementing all recommendations. This exercise under the auspices of the Council resulted in a Status Report, which was published in June 2002. It comes to the general conclusion that all candidate countries are clearly committed to fulfil the recommendations. The EU has insisted on the early closure of certain types of nuclear power units. 2. The negotiations The chapter has been closed with ten countries and provisionally closed with Bulgaria, while it remains open with Romania. Generally, in the energy chapter, negotiations concentrate, depending on the country concerned, on the constitution of emergency oil stocks, the internal energy market (gas and electricity directives) and nuclear safety. Transitional arrangements have been granted to all the countries which have closed the chapter so far, except Hungary, allowing them longer periods in which to build oil stocks up to the required level. The Czech Republic has been granted a transitional period up to the end of 2004 to implement the Gas Directive and Estonia has been allowed until the end of 2008 to implement the Electricity Directive. Chapter opened: first half of 2001 Status: closed in December 2002 (provisionally closed in first half of 2001) Transitional periods: � build up of oil stocks to required level, until the end of 2006 3. Latest Assessment by the European Commission
1

In the 1999 update of its Opinion, the Commission concluded that the vast majority of the acquis still needed to be transposed, especially the acquis covering crude oil, oil products trade, oil stocks, crises measures and energy efficiency. It was noted that particular attention should be paid to the restructuring of Malta's institutional structures to separate policy formulation and regulation (creation of an independent Regulator for the development and monitoring of energy policy) and to the reinforcement of its capacity to implement the acquis. Since the 1999 update of the Opinion, Malta has made progress in setting up an independent Regulator for the energy sector and transposing partly the acquis on crude oil, crisis measures and energy efficiency. It has started the restructuring of Enemalta, its Energy Corporation to put in line with the principles of the internal energy market. Overall, partial legislative alignment
1

European Commission, Regular Report on Malta 2002, pp. 71 & 72: http://www.europa.eu.int/comm/enlargement/report2002/ml_en.pdf

WIP/2002/11/0206

2


with the EC requirements has been achieved. The development of administrative capacities is on track to meet the requirements of the acquis by accession. Negotiations on this chapter have been provisionally closed. Malta has been granted a transitional period until the end of 2006 to build up oil stocks to the required level. Malta is generally meeting the commitments it has made in the accession negotiations in this field. In order to complete preparations for membership, Malta's efforts now need to focus on the full and timely transposition of the Electricity Directive and the remaining acquis related to energy efficiency, as well as on the gradual building up of its oil stocks and finalising the restructuring of the Enemalta Corporation.

January 2003

WIP/2002/11/0206

3


