EUROPEAN PARLIAMENT
DIRECTORATE-GENERAL FOR RESEARCH DIRECTORATE A DIVISION FOR INTERNATIONAL AND CONSTITUTIONAL AFFAIRS

FACTSHEET
CYPRUS Chapter 2 - Freedom of movement for persons

1. The acquis 1 The acquis covers the four broad areas of mutual recognition of professional qualifications, citizens's rights, free movement of workers and coordination of social security schemes: � In the politically sensitive area of free movement of workers the EU took the initiative of itself proposing a transitional measure for all the candidates except Cyprus and Malta. This involves the present system of work permits being maintained for a number of years after accession with a gradual transition towards free movement as was the case in previous enlargements in respect of certain other new Member States. As regards mutual recognition of professional qualifications, a particular problem in respect of some countries has been how to treat qualifications of citizens of candidate countries who hold qualifications from third countries because they completed their education at a time when certain candidate countries were part of the Soviet Union or Yugoslavia. The notion of a declaration of equivalence issued by the relevant bodies in the candidate countries together with an attestation in respect of the specific holder of a qualification is central to the approach adopted by the EU, as are tough monitoring provisions, in particular for the sectoral directives . In the area of citizens' rights, implementation of the directives on voting rights may require constitutional amendments in some countries and alignment with the acquis on residence rights will only be fully possible on accession. Coordination of social security is governed by regulations which will be directly applicable upon accession. The main challenge here is one of administrative structures and

�

�

�

Information largely drawn from the European Commission, DG Enlargement http://www.europa.eu.int/comm/enlargement/negotiations/chapters/index.htm For a guide to free movement for persons in an enlarged Union see the European Commission's enlargement website.

1

WIP/2002/11/0016

1


capacity. Candidate countries have acquired some experience in implementing existing bilateral agreements with Member States. 2. The negotiations

The chapter has been closed with ten countries and provisionally closed with Bulgaria, while it remains open with Romania. Concerns about the risk of disturbances to the labour market in certain Member States and the potential impact on public opinion led the EU to request a transition period for free movement of workers in respect of all the negotiating countries except Cyprus and Malta. A seven-year safeguard clause has been agreed with Malta allowing it to have recourse to Community institutions if difficulties arise on its labour market following accession. Chapter opened May 2000 Status Closed December 2002 (Provisionally closed in June 2001) Transitional arrangements: none.

3.

Position of the European Parliament

In its resolution of 5 September 20011, Parliament: � notes that while the acquis communautaire as a whole is being adopted quickly, certain areas nevertheless require special attention:...the free movement of persons..must all be matters of priority for Cyprus; stresses that if the negotiations proceed at their current pace, these problems should not be unsurmountable; � urges the Cyprus legislature to drop forms of discrimination not permitted under Community law from the rules adopted on access to the labour market and working life, in particular requirements relating tonationality, place of residence, membership of occupational associations and certificates of good character. 4. Latest Assessment by the European Commission 2

In its 1998 Regular Report, the Commission concluded that further efforts to transpose the acquis would be needed and that special attention would have to be paid to implementation and enforcement structures. Since the 1998 Regular Report, Cyprus has continued to undertake measures for further harmonisation with the acquis in the field of free movement of persons. It has abolished inconsistencies as regards the co-ordination of social security systems and has approved legislation granting the mutual recognition of qualifications. The alignment with the acquis is at an advanced stage. Cyprus has reached a good level of administrative capacity to implement the acquis in this field.

1 2

Resolution on the state of negotiations with Cyprus, � 13 & 21: A5-0261/2001 European Commission, Regular report on Cyprus 2002, p. 54: http://www.europa.eu.int/comm/enlargement/report2002/cy_en.pdf

WIP/2002/11/0016

2


Negotiations on this chapter have been provisionally closed. Cyprus has not requested any transitional arrangement. Cyprus is generally meeting the commitments it has made in the accession negotiations in this field. In order to complete preparations for membership, Cyprus' efforts now need to focus on the adoption of outstanding legislation on the mutual recognition of qualifications (second General System Directive and sectoral legislation on architects, doctors, dentists, pharmacists, nurses, and midwives), on the right of free movement of workers and residence of the nationals of the Member States and the members of their families, and on voting rights.

January 2003

WIP/2002/11/0016

3


