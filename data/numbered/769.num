DRAFT CHARTER OF FUNDAMENTAL RIGHTS OF THE EUROPEAN UNION
fundamental.rights@consilium.eu.int Brussels, 22 May 2000

CHARTE 4266/00

CONTRIB 139

COVER NOTE Subject : Draft Charter of Fundamental Rights of the European Union

Please find hereafter the submission by the Society for Threatened Peoples International in occasion of the hearing on 27 April 2000. 1 ____________________

1

This text has been submitted in German and English languages. cb JUR 1

CHARTE 4266/00

EN


12 3 45 67 8 90 1 23 4 5 12 3 45 67 8 90 1 23 4 5 12 3 45 67 8 90 1 23 4 5 12 3 45 67 8 90 1 23 4 5 12 3 45 67 8 90 1 23 4 5 12 3 45 67 8 90 1 23 4 5 12 3 45 67 8 90 1 23 4 5 12 3 45 67 8 90 1 23 4 5 12 3 45 67 8 90 1 23 4 5 12 3 45 67 8 90 1 23 4 5 12 3 45 67 8 90 1 23 4 5 12 3 45 67 8 90 1 23 4 5

society for
threatened peoples

international

Towards the effective protection of minorities in the EU 's future Charter on Fundamental Rights
Written submission by Society for Threatened Peoples International to the hearing of the European Parliament in Brussels on 27 April 2000 Mr President, Representatives of the European Parliament and the EU Member States, Ladies and Gentlemen, On behalf of the Society for Threatened Peoples (Gesellschaft fur bedrohte Voelker/GfbV)International we salute you and thank you for the invitation to attend this hearing. Our human rights organisation,which has been working for more than 30 years on behalf of linguistic,ethnic and religious minorities subjected to discrimination and persecution in Europe and throughout the world, welcomes the advent of a European Union Charter on Fundamental Rights.It is high time for the growing might of the EU institutions to be balanced by the establishment of legal rights of a binding and individually accessible nature for the people living in Europe.We note with particular concern,however,that the draft texts of the Convention on Fundamental Rights to date have made no provision for the necessary protection of linguistic, ethnic and religious minorities in Europe. Minorities are an integral part of every society.They contribute to the essential diversity of cultures.They are therefore entitled to recognition and protection. Nevertheless time after time minorities in Europe have become the victims of persecution,climaxing in the crimes of genocide,class murder and mass deportation committed by the National Socialistm,Fascist and Communist dictatorships.Such crimes are still continuing in the Balkans today.Even in the stable democracies of western Europe people are discriminated against on account of their skin colour,language,culture or religion. The victims of discrimination also include members of linguistic minorities and ethnic groups longestablished in Europe.Although citizens of their own national states and of the EU,they are frequently deprived of the resources their educational and cultural institutions require.This has resulted in a process of progressive cultural impoverishment:the ,,euromosaic "study published by the European Commission in 1996 indicated that almost half the 46 minority languages of Europe had either only ,,limited "or ,,no prospects of survival ".

CHARTE 4266/00 JUR

cb

2

EN


It is the opinion of international lawyers of repute that the only effective source of protection for linguistic,ethnic, religious and similar communities is the guarantee of their collective rights.However it appears to have already been decided that,following the tradition of international agreements on human rights already in force and the constitutions of the European democracies,the Charter on Fundamental Rights will embody individual rights.It appears all the more important,then,that the Charter should incorporate an Article which guarantees minimum rights for the members of ethnic or national,linguistic and religious minorities .Based on Article 27 of the International Covenant on Political and Civil Rights of 1966 we propose the following form of words:
Persons belonging to ethnic or national,linguistic or religious minorities shall have the right,in community with other members of their group,to practise their religion,to promote their own culture and to use their own language in private or public.

The outlawing of discrimination on the grounds of race,origin,nationality,language,gender,sexual orientation,religion,ideology or political opinion is likewise imperative if minorities are to be protected.It is almost certain that the Charter on Fundamental Rights will in fact contain an Article dealing with this subject and it is essential therefore that the prohibition of discriminationshould apply not only to the citizens of the Union but explicitly to all peoples within the area of the EU. As we are aware from discussions relating,for example, to the equal status of men and women or the problems of disabled persons,discrimination applied in practice against entire groups can often only be remedied with difficulty.In order to ensure that equal opportunities for such groups is a reality,we call for ,,affirmative action " along the lines of the successful American model:groups that have suffered discrimination should qualify for specialsupport. Such support should moreover be targeted in particular at members of long-established linguistic, ethnic or national minorities.The discussion concerning whether the Charter should include political targets inaddition to individual rights has been a source of controversy.Nevertheless we are now proposing that the following paragraph be added to the Article dealing with the protection of minorities:
The EU and the Member States call for specific measures to be taken to ensure the genuine equality of members of national majorities and members of linguistic,ethnic or national minorities.The EU calls for trans-border co-operation in minority regions.

We also urge the EU to give a lead regarding the prevention of genocide,mass expulsion and other grave crimes against humanity by adopting the Charter on Fundamental Rights.In addition to making this a target for political action,we would also urge the inclusion of an Article on the right to a homeland that would offer individual protection from expulsion :
1. 2. Every person shall have the right to remain in their place of residence,their home region and their own country. Every person shall have the right likewise to return of their own free decision to a place of their choice within their country of origin.

The prohibition of individual or collective expulsion,the right to a return home in safety and the freedom to choose a place of residence are already enshrined in different Articles of the International Covenant on Civil and Political Rights and in the European Convention on Human Rights and the Supplementary Protocols thereto.The right to a homeland was introduced in the Draft Declaration on Population Transfers and the Implantation of Settlers unanimously adopted by the UN Human Rights Commission on 17 April 1998.

CHARTE 4266/00 JUR

cb

3

EN


Finally we would counsel against any attempt to limit the scope of the Fundamental Rights concerned or to restrict the Charter to a non-binding declaration. Not only would this result in the destruction of the concept of a European ,,Bill of Rights ",the international evolution of human rights would also experience a severe setback. In the final declaration issued by the Council of Europe meeting in Copenhagen in 1993 the Members of what was then the EC stated that their willingness to agree to the admission of additional states was conditional on the enactment of legislation incorporating the protection of minorities into the constitutions of the countries concerned.The adoption of such a right in the Charter on Fundamental Rights has therefore become a test of credibility. On behalf of GfbV International we wish the members of the Convention every sagacity,courage and success in dealing with the task in front of them. Luxembourg/Goettingen,April 2000, Tilman Zulch,President of GfbV International, Andre Rollinger,Vice-president

The Society for Threatened Peoples (Gesellschaft f�r bedrohte V�lker/GfbV)International,human rights organization for discriminated and persecuted ethnic,religious and national minorities,has offices in Germany, Luxembourg,Austria,Switzerland,South Tyrol/Italy,Bosnia-Hercegovina and in France.Since 1993,she has Consultative Status (Cat.Spec.)with the Economic and Social Council (ECOSOC)of the United Nations. To get more information about our human rights work,to support this submission for a better protection of minorities in the EU 's future Charter on Fundamental Rights or in case of any questions,suggestions or new ideas concerning this campaign,please contact Dr.Andreas Selmeci at our main office,P.O.Box 20 24,D-37010 G�ttingen,Tel.+49 551 49906-22,Fax +49 551 58028,email pogrom@gfbv.de.Information about this campaign is also available on our homepage www.gfbv.de (click on the British banner and look at ,,Documents and Statements about Threatened Peoples and Minorities ").

____________________

CHARTE 4266/00 JUR

cb

4

EN


