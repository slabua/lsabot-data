AUSTRIA
Verband Österreichischer Tierschutzvereine

BELGIUM
GAIA

DENMARK
Dyrenes Beskyttelse

In view of the public hearing on Wednesday 21 January

FINLAND
Finnish Group for Animal Welfare

Ref: PPEurogroup/TRA/2/2004

FRANCE
Eurogroupe-France

General comments on the Commission proposal
COM/2003/0425
In July 2003, the European Commission published a proposal for a new draft regulation amending the EU transport rules for live animals. Eurogroup welcomes the efforts made to improve the welfare conditions of animals during transport. However, there remain a number of shortcomings which should be overcome. This position paper highlights the improvements contained in the Commission proposal as well as its failings and proposes amendments which should be introduced in order to achieve a legislative text which truly improves and safeguards the welfare of animals during transport. The most controversial issue ­ a maximum journey time limit - has not been addressed. Instead the proposal includes a system which is linked to drivers working hours with sequences of 9 hours of travel and 12 hours of rest on the vehicle. Eurogroup considers that the basis of this Regulation should be the physiological needs of the animals. Furthermore, there have been no complaints that existing rules are conflicting with drivers' social rules.

GERMANY
Deutscher Tierschutzbund eV

GREECE
Hellenic Animal Welfare Society

IRELAND
Irish Society for the Prevention of Cruelty to Animals

ITALY
Lega Anti Vivisezione

LUXEMBOURG
Ligue Nationale pour la Protection des Animaux

PORTUGAL
Liga Portuguesa dos Direitos do Animal

Negative proposals for animal welfare
No maximum journey time limit; Journeys will become longer due to 12 hrs rest; Staging points will no longer be permitted. Unclear where the vehicles will stop to allow for the 12 hrs rest, who will verify if the rest period is taken; Definition of journey times does not include time taken from loading the first animal to unloading the last animal; Excessively steep loading ramps are permitted ­ not in line with the SCAHAW recommendations; In multi-deck vehicles the height between tiers is not sufficient - not in line with the SCAHAW recommendations; Time on roll-on-roll- off vessels is considered as rest period; No review is foreseen. The important requirements in the Annex can be reviewed by a Commission's Committee procedure giving the European Parliament no input in future amendments.

SPAIN
Asociación Nacional para la Defensa de los Animales

SWEDEN
Svenska EU-gruppen för djurskydd

THE NETHERLANDS
Nederlandse Vereniging tot Bescherming van Dieren

UNITED KINGDOM
Royal Society for the Prevention of Cruelty to Animals

6 rue des Patriotes ­ B-1000 Bruxelles ˇ Tel.: 32 (0)2 740 08 20 ­ Fax: 32 (0)2 740 08 29 ˇ E-mail: info@eurogroupanimalwelfare.org Director : David B. Wilkins MBE MA MRCVS


Good points
Stricter rules on registration and licensing of transport companies; Driver's training mandatory and independent examination; Electric prods or goads prohibited; Fitness to travel: conditions for assessing fitness are stricter, especially relating to young animals; Assembly centres and markets must meet certain conditions; Transport of horses: very strict rules ­ only to be transported in individual compartments; Ventilation and humidity standards require some form of forced ventilation with a power source independent of the engine.

Main areas of concern
No limit on journey times
The Commission's proposal that journey cycles for animals travelling by road of 9hrs ­ 12hrs rest ­ 9 hrs can be repeated without end, is unacceptable especially as animals will remain on the vehicle constantly. This is a step back from the existing situation where at least the animals could rest for 24hrs after a journey. Furthermore, the proposed journey times and rest period will extend journey times unnecessarily. A single maximum journey time limit of 9 hours for slaughter and further fattening animals transported by road should be introduced.

Justification:
As long distance transport of living animals can be replaced by that of carcasses, sperm and embryos there is little justification to continue these transports; Recent outbreaks of infectious diseases such as FMD calls for a reduction in the length and frequency of live animal transports for animal health reasons; The European Parliament has repeatedly asked the Commission and Member States to introduce a maximum journey time for animals travelling by road. The Scientific Committee on Animal Health and Animal Welfare (SCAHAW) recommends that "welfare of animals unaccustomed to loading and transport is significantly poorer than normal during the first hours after loading...Hence such animals should not be transported if this can be avoided and journeys should be as short as possible. It further states that "as with increasing journey duration, the welfare of animals generally gets worse" ­ it would be better for slaughter animals to avoid journeys longer than the period after which food and water provision is recommended for the various species and ages in question.

2

1/19/2004


-

The Federation of Veterinarians of Europe (FVE) support the principle of limiting journey times overall, and of minimizing travel times for slaughter and further fattening animals. The FVE has stated that fattening of animals should take place "within or near the place of birth", that animals should be slaughtered "as near the point of production as possible", and that: "Journey time for slaughter animals should never exceed the physiological needs of the animal for food and water". The problems of geographically remote areas should not be an obstacle to implementing short journey times. Exemptions can be granted where there is a demonstrable need. These exceptions should not dictate the overall rules.

-

Rest periods of 12hrs on the vehicle
The Commission suggests that animals are kept on the vehicle during the 12 hours rest and staging points are no longer to be used. Whilst Eurogroup recognises that unloading and loading is stressful, it is no solution to keep the animals on the vehicle for the rest period during the whole journey which can sometimes last for several days. It is also unclear where the vehicles must stop in order for the animals to be rested, fed or watered, making enforcement of this procedure very difficult when compared with a rest period at a staging post. Leaving the animals on the vehicle during stops will also make inspection and recognition of sick or injured animals much more difficult, if not impossible. Even if ailing animals are noticed, there is also the problem of what to do with them if the vehicle is not at a staging point. A particular problem will arise with lactating cows, sheep and goats, as they need to be milked every 12 hours. The hygiene problem and spread of diseases might be increased if the stops and rest periods are not in a controlled environment. Most of the stakeholders, including the FVE and some Member States, agree that staging points need to be maintained.

Resting feeding and watering on the vehicles requires more space per animal
The transport of breeding animals over long distances is only acceptable if they are given sufficient space to lie down altogether at the same time and for each individual to feed and drink on board. These activities will only be possible if the Commission's proposals regarding stocking density are improved, especially for goats and sheep, in particular unshorn sheep for which the space is not sufficient. Animals should be given enough headroom to stand up naturally and to allow proper ventilation. Both Eurogroup and the FVE propose to improve the Commission's proposal on headroom, supported by the scientifically-based recommendations of the SCAHAW report. Suggestions that giving more space will cause problems is contrary to research which shows that it is better to give more space since this allows animals to maintain their balance more easily. Experience shows that if animals do fall and space allowance is low, the animals have difficulty getting up again so they are trampled and even killed.

3

1/19/2004


Only fit animals should be transported
The clarification of Fitness for travel is to be welcomed. As in the present Directive, the new proposal states that - New born or very young animals ­ whose navels have not completely healed, should not be considered fit for transport. Research of the Justus- Liebig University in Germany indicates that the external part of the navel of a calf is healed in two weeks and the internal part in four weeks. The navel of lambs and kids are not always completely healed in two weeks, therefore Eurogroup suggests increasing the age at which they can be transported to four weeks. In Denmark, Germany and Poland, transport of calves younger then 2 weeks is banned. This age limit is supported by research showing that the effects on health and welfare of marketing/transporting calves at younger ages are too severe to be acceptable.

Piglets of three weeks are much too young to be transported. This would also be in contradiction
with what is prescribed on weaning in Directive 2001/93/EC laying down minimum standards for the protection of pigs, which states that:
"No piglets shall be weaned from the sow at less than 28 days of age unless the welfare or health of the dam or the piglet would otherwise be adversely affected. However, piglets may be weaned up to seven days earlier if they are moved into specialised housings which are emptied and thoroughly cleaned and disinfected before the introduction of a new group and which are separated from housings where sows are kept, in order to minimise the transmission of diseases to the piglets."

Transport vehicles cannot be considered as `specialised housings' for young piglets, and therefore, to avoid a clear breach of this Directive, the minimum legal age for removal of piglets from their mothers for the purposes of transport must be four weeks. For long distance journeys Eurogroup is calling for an official veterinarian to be present during the loading of the animals to check fitness to travel.

Improved quality of transport vehicles
Loading ramps:
The proposal allows the use of loading ramps of 33 ° and even 50° for sheep and cattle. This is much higher then the 20° which is recommended in the SCAHAW report and much higher than what is used by the vast majority of transporters. Pigs have particular difficulties with moving up or down slopes and SCAHAW suggests loading them on the level, either with a lift at the back of the vehicle or a moveable floor or ramp. This system is already in use by many hauliers.

Temperature and humidity and ventilation:
Eurogroup welcomes the proposal to introduce a system which requires the measurement and control of temperature and humidity and ventilation. This requires a system of forced ventilation with a power source separate from the engine. Many of the serious welfare incidents seen during long distance journeys have been associated with high temperatures and inadequate ventilation. However, the proposed maximum temperatures for the different species are too high and not in accordance with the thermal capacity of the species and with the SCAHAW report which recommends a maximum of 25°Celsius.

4

1/19/2004


