EUROPEAN PARLIAMENT
DIRECTORATE-GENERAL FOR RESEARCH DIRECTORATE A DIVISION FOR INTERNATIONAL AND CONSTITUTIONAL AFFAIRS

FACTSHEET
MALTA 1. The acquis 1 The acquis covers areas where there is already a substantial body of secondary legislation such as health and safety, labour law and equality of treatment, as well as other areas such as social dialogue, employment and social protection, where convergent policies are being developed on the basis of the EC Treaty. � Labour law: Directives exist in the field of collective redundancies, safeguarding employment rights in case of transfer of an undertaking, obligation on employers to inform workers of the conditions applicable to employment contracts, guarantee for employees in the even of the insolvency of the employer, posting of workers and organisation of working time. Equal treatment: The Amsterdam Treaty added equality between men and women to the list of EC objectives. New Article 141 EC is particularly important in this context. The practical implementation of gender mainstreaming is spelt out in the Community Framework Strategy on Gender Equality 2001-2005. Legislation is also used to achieve equality, in particular in the fields of equal treatment in employment and occupation, social security, occupational social security schemes, parental leave, protection of pregnant women, women who have recently given birth and women who are breastfeeding. Anti-discrimination: Under new Article 13 EC, the Community has the power to combat discrimination on a wider range of grounds in the employment context and in other areas. Two recent directives prohibit discrimination on grounds of race, ethnicity, religion, disability, age and sexual orientation. Health and safety: The Single European Act gave impetus to social policy in this field. The acquis includes directives fixing minimum standards for working conditions. Timely and complete transposition of the legislation must be backed up by the effective operation of the relevant inspectorates. Chapter 13 - Employment and social policy

�

�

�

1

Information largely drawn from the European Commission, DG Enlargement http://europa.eu.int/comm/enlargement/negotiations/index.htm

WIP/2002/11/0205

1


�

�

� � �

Social protection: The funding and organisation of social protection systems is a matter for the Member States, but the EC requires that these systems are able to develop and operate sustainable and universally applicable social protection standards in line with the Treaty objectives. Social dialogue: The Treaty requires that social dialogue be promoted and gives the social partners additional powers. The candidates are invited to confirm that they accord the importance required to social dialogue and that social partners are sufficiently developed to discharge their responsibilities, and to indicate that they are consulted on the relevant legislation. Employment: The candidate countries are required to cooperate with the EU on the followup to the employment policy review. Public health: Article 152 EC stipulates that a high level of health protection shall be ensured in all Community policies and activities. There are also several specific directives in the area of tobacco production and advertising. Institutions: The candidate countries are requested to effectively enforce the acquis through national judicial and administrative systems similar to those of the existing Member States.

The acquis also covers the European Social Fund, public health programmes, ECSC measures, the Council regulation on the European Monitoring Centre on racism and xenophobia and measures concerning the European Foundation for the improvement of living and working conditions. 2. The negotiations The chapter has been closed with ten countries and provisionally closed with Bulgaria and Romania. Four countries (Latvia, Malta, Poland and Slovenia) asked for transitional periods in the field of health and safety, one in the area of working time (Malta) and one in respect of maximum tar yield of cigarettes (Bulgaria), which were accepted. Some legislative work remains to be done by all countries in the areas of labour law, equality of treatment and major efforts are needed to comply with health and safety rules. Candidate countries have been asked to provide detailed timetables for adoption and implementation of all measures. In general this has been done. They have also been asked to provide more details on enforcement and implementation, particularly regarding the role of the various inspectorates and systems of redress for aggrieved persons. All candidates also participate in employment reviews with the Commission. Chapter opened: November 2001 Status: closed in December 2002 (provisionally closed in November 2001) Transitional arrangements: � Directives 93/104/EC (working time) until 31/7/04; However, the existing collective agreements providing for a working time which exceed the terms of the Directive would remain in force until 31/12/04; � Directive 89/655/EEC (work equipment) until 31/12/05.

WIP/2002/11/0205

2


3. Position of the European Parliament In its resolution of 5 September 20011, Parliament: � calls on the Maltese government to adopt strategies for integrating minorities, to recognise their legitimate rights and, in particular, to introduce legislation against discrimination pursuant to Article 13 of the EC Treaty and the Charter of Fundamental Rights; � welcomes the progress made by Malta with the Community acquis in social matters and the proposed measures on health and safety and social protection; � urges the Maltese legislature to commit itself, with a view to accession, both to transposing Community social and employment law and to expanding the government departments responsible; � considers that harmonisation with the Community acquis concerning equal opportunities for men and women is an essential condition of Malta's accession to the EU since it is effectively a human rights issue, and that full implementation of the Community acquis requires the establishment and consolidation of institutions and administrative structures together with the formulation of specific policies in that area. In its resolution of 13 June 20022, Parliament recommends that an appropriate system be developed to monitor (State aids) and Community law implemented, specifically as regards labour law and health and safety at work. 4. Latest Assessment by the European Commission
3

In the 1999 update of its Opinion, the Commission concluded that Malta had transposed parts of the acquis, but further progress was necessary in the main legislative areas. The main labour laws still had to be reviewed to achieve full compliance with the EC Directives. A detailed review of the legislation covering equal treatment of men andwomen still had to be carried out, covering issues of enforcement. Since the 1999 update of the Opinion, Malta has made progress in the area of health and safety at the workplace and, to a lesser extent, on equal treatment of women and men. The process of bringing Malta's legislation and structures in relation to occupational health and safety and social dialogue into line with the acquis has developed satisfactorily, but this has not been the case in the areas of equal treatment and labour law where important parts of the acquis still need to be transposed. The administrative capacity needed to implement the acquis is in place but needs to be further strengthened, particularly in the field of occupational health and safety and public health. Negotiations on this chapter have been provisionally closed. Malta has been granted transitional arrangements covering the implementation of the Directive on noise at work until 31/12/04, the Directive on work equipment until 31/12/05, the Directive on temporary mobile construction until 31/12/04, and the Directive on working time until 31/7/04 (however, the existing collective agreements providing for a working time which exceed the terms of the Directive would remain in force until 31/12/04). Malta is meeting the majority of the commitments it has made in the
1
2

Resolution on the state of negotiations with Malta, � 8 & 33 to 35: A5-0262/2001 Resolution on the state of the enlargement negotiations, � 111: A5-0190/2002 3 European Commission, Regular Report on Malta 2002, p. 70: http://www.europa.eu.int/comm/enlargement/report2002/ml_en.pdf

WIP/2002/11/0205

3


accession negotiations in this field. However, important delays have occurred on the transposition of the Directives in the field of labour law and equal treatment. This needs to be urgently addressed. In order to be ready for membership, Malta should focus further efforts on stepping up the pace of legislative work to ensure full alignment of its legislation, particularly in the areas of equal treatment and labour law, in accordance with its commitments. It must also continue to strengthen its administrative capacity in the areas of occupational health and safety and public health. Ongoing preparations must be pursued.

January 2003

WIP/2002/11/0205

4


