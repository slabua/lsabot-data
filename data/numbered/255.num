EUROPEAN PARLIAMENT
Directorate General for Research-Directorate A STOA - Scientific and Technological Options Assessment

Briefing Note N� STOA 504EN
PE nr. 303.124 July 2001

Support policies for electricity produced from renewable energy sources in the EU
1.- INTRODUCTION:

For reasons that are both strategic (reduce external dependency and increase security of supply) and environmental (reduce CO2 emissions) the EU has set the target of doubling the consumption of energy from renewable sources, from the current 6% to 12% of the total by 2010. Also, the EU has committed itself to produce 22,1% of electricity produced by the year 2010 using renewable energy sources (RES-E). The current figure is 13,9% and 4/5 of it comes from large hydro plants, a technology that, even if fairly competitive in the electricity market, is already fully exploited in the EU, the development of new usable sites meeting with strong local resistance. Apart from large hydro, renewable sources of energy still have a position of comparative disadvantage compared with conventional sources, notably fossil fuels and nuclear. Therefore support policies are needed to help RES-E to enter the market and fill the competitive gap. The obstacles to the development of renewables are not of a technical but a financial nature. Some renewables need significant initial investment, as was the case with other energy sources, such as coal, oil and nuclear energy. Also, before they can achieve a profitability threshold, several renewable technologies may need aid for relatively long periods. It is evident that the renewable energy market in the EU cannot be expected to develop regularly without a supporting policy in the medium term on the part of the public authorities. The Commission affirms that, if nothing is done, the total energy picture in 2030 will see a share for

renewables of a bare 8%. Moreover, the energy market is changing fast, as the EU moves toward a liberalisation of the market. As a result, competition between energy companies in Europe is increasing, thereby putting downward pressure on prices. Under such circumstances there is a danger that the development of more expensive technologies will be held back. Therefore, a policy of promotion of renewable sources of energy is needed. It could fall within a whole set of decisions, stretching from fiscal measures in favour of renewable energy sources to the obligation on the part of electricity producers and distributors to purchase a minimum percentage of electricity produced from renewables. Aid to further research in the field would be another important means, as well as financing mechanisms (interest subsidies, guarantee funds, parafiscal tax on other sources of energy).

2.- CURRENT FIGURES FOR RENEWABLE ENERGY SOURCES IN THE EU

Between 1985 and 1998 the increase in energy production from renewables was significant in relative terms (+30%), but still fairly insignificant in absolute terms (from 65 to 85 million TOE, including hydroelectric power). This is because progress made in the renewables sector has been offset by an increase in total energy consumption. The share for RES has stagnated at around 6% of global consumption (11% of production) despite consistent annual growth of around 3% in the sector.
Briefing Note N� STOA 504EN


PE no 303.124, July 2001

Support policies for electricity produced from renewable energy sources in the EU

1998 Gross Inland Consumption (Ktoe) for renenewable energy sources in the EU Hydro Austria Belgium Denmark Finland France Germany Greece Ireland Italy Luxembourg Netherlands Portugal Spain Sweden UK European Union 3192 33 2 1294 5388 1511 320 79 3544 10 9 1116 2924 6391 449 26263 30% Wind 4 1 239 2 4 395 6 15 20 1 55 8 185 27 76 1037 1.2% Solar 55 1 7 0 17 80 119 0 9 0 6 16 26 5 7 348 0.4% Geoth 7 2 1 0 117 10 3 0 2801 0 0 45 7 0 1 2993 3.5% Biomass 3508 623 1519 5951 11364 6506 908 166 6904 40 1384 2406 3860 7311 1730 54176 63.6% Other 0 119 0 164 0 0 0 0 55 0 0 0 0 0 0 338 0.3% Total 6765 779 1768 7411 16890 8501 1355 259 13334 50 1454 3591 7001 13734 2263 85155 RES contribution 23,7% 1,4% 8,2% 22,8% 6,9% 2,5% 5,3% 2,1% 7,9% 1,5% 1,9% 16,9% 6,6% 27,3% 1.0% 6,0%

source: European Commission, DG TREN, 2000 - Annual energy review

Hydro and wind power together increased their output by 2.6% per year on average in the 199098 period to generate 13.6% of the total EU electricity in 1998. In particular, recent growth of wind generated electricity has been remarkable: 19% in 1996, 51% in 1997 and 64% in 1998. However, the major growth in the 90's, among renewable sources, concerned biomass (3.5% per year on average over the period 90-98), the two main markets for which (power generation and direct use in tertiary-domestic sector) are increasing, and which reached 7.3% of total primary production in '98. Moreover, the situation varies appreciably from one country to another. There are four countries that use renewables to a significant extent: Portugal (16.9%), Finland (20.7%), Austria (23.3%) and Sweden (26.7%) drawing on their forestry and water potential, while for others it is only a bare 1-2% of the total consumption. In detail, due to the small starting base, the contribution of `high-profile' renewables (wind and solar) to the energy supply is marginal. However, there has been significant growth in many member countries. Growth in the use of wind power and solar heat has been strong, in relative terms, in Germany, Denmark, Spain and

Greece as a result of public and private interest in developing wind power in Germany, Spain and Denmark, and solar water heaters in Greece. Finally, it has to be said that supply-side efforts will only succeed in the future if they are accompanied by policies to rationalise the demand for energy. 2.1 EU campaign for take-off of renewable energy To ensure an EU coordinated approach to a real take-off of renewables with the objective of doubling the RES share by 2010, the Commission has proposed a campaign with the following key actions: � Installation of 1.000.000 photovoltaic systems, in order to provide a sufficiently large market base to enable the prices to fall substantially. � Installation of 10.000 MW of large wind farms, representing 25% of feasible overall wind energy for 2010 and EU help would be given to less favourable or unconventional applications. � 10.000 MW of biomass installations to promote decentralised biopower throughout Europe. Such installations could range from a few hundred KW to multi-MW and
Briefing Note N� STOA 504EN

2


PE no 303.124, July 2001

Support policies for electricity produced from renewable energy sources in the EU

combine different technologies appropriate to local circumstances. � Integration of renewable energies in 100 pilot communities which can reasonably aim at 100% power supply from renewables. 3. ELECTRICITY COMING FROM RENEWABLE SOURCES OF ENERGY (RES-E) IN THE EU In its White paper on RES policy the Commission set an indicative target of raising the contribution of RES to gross inland energy consumption to 12% in 2010. For the electricity sector this translates as 22.1% for electricity from RES in 2010, compared to 13.9% in 19971. The Commission calls this target "ambitious but realistic". The major part of the targeted increase in RES use is expected to come from biomass, with biogas exploitation (livestock production, sewage treatment, and landfills), agricultural and forest residues and energy crop plantations. Moreover, recent developments, for example in the field of wind power, indicate that the target could even be exceeded, if incentives for renewables remain in force.
Projected electricity production by RES (twh) for 2010

Finally, doubling the share of RES by 2010 corresponds to an emission-reduction potential of 200-300 million tons of CO2, which is a significant contribution towards fulfilment of the EU's Kyoto commitment, as well as to other long-term sustainability goals. 4. EXISTING OPTIONS SUPPORT POLICY FOR A EU

There are various possible options for financial aid to the sector, with different degrees of public intervention. The main existing support schemes can be divided into three major models: � The direct feed-in scheme, which provides direct monetary support for every unit of electricity produced from a renewable source � The quota system, based on the issuing and trading of so-called 'green certificates', a quota of which all the utilities are obliged to buy � The tendering model, in which the compensatory levy to be paid by each regional energy supplier is centrally decided. Tenders are laid down for plants to produce electricity from renewable energies. 4.1 EU guidelines on state aid for green electricity Support policies for electricity from renewable energy sources are detailed in the Commission's guidelines for Member States' support policies, which aims to provide a fair competition framework in the internal EU market as well as inside single countries among different energy sources. According to the EU guidelines Member States may grant aid in the following ways: � Renewable energy is accompanied by particularly high unit investment costs. Therefore, Member States may grant aid to compensate for the difference in production cost of renewable energy and the market price, until the investment has been written down. � Member States may grant support using market mechanisms such as green certificates or tenders. The price of these green certificates is not fixed in advance but depends on supply and demand. One of the conditions is that it does not culminate in an overcompensation of renewable
Briefing Note N� STOA 504EN

PROJECTED 2010 TYPE OF ENERGY Total Wind Total Hydro Large hydro Small hydro Photovoltaics Biomass Geothermal Total Renewable Energies Twh 2,870 80 355 300 55 3 230 7 675

FOR

%of total 2.8 12.4 0.1 8.0 0.2 23.5

Source: White Paper on RES (1997)

1

In its White Paper the Commission has set a target of 675 TWh/a for electricity from RES, which was translated into a share of 23.5% of projected gross electricity consumption in 2010. In the draft "Directive on the promotion of electricity from renewable energy sources in the internal electricity market" consumption is assumed to be higher; the generation target from RES therefore translates into a lower share of 22.1%

3


PE no 303.124, July 2001

Support policies for electricity produced from renewable energy sources in the EU

energy and/or dissuade renewable energy producers from becoming more competitive. � Member States may grant operating aid to new plants producing renewable energy, calculated on the basis of avoided external costs. This means that public funding is used to pay the difference in environmental cost compared to conventional power plants. � The amount of aid may not exceed 0,05  per kWh and must be part of a scheme which ensures unbiased treatment of firms in the renewable energy sector. � Finally, Member States may grant operating aid that is to be reduced over time (degressive aid) and which may not be granted for more than five years. It may cover as much as 100% of the extra costs in the first year but must fall in a linear fashion to zero by the end of the fifth year. A non-degressive aid that amounts to 50% during a maximum of 5 years will also be allowed. 4.2 The direct feed-in scheme

The German electricity feed-in law has been much criticised, but on 15 March 2001 the European Court of Justice ruled that it does not constitute state aid under the EU Treaty nor contravene the internal market rules. This law, like the other feed-in laws in place in Denmark or Spain, has achieved a very large increase in RES-E and big investments in energy plants (notably wind farms, these being the most competitive renewable energy technologies).
Type of incentive and wind power added in 1998

Type of incentive Fixed Tariiffs Other systems

Country Denmark Germany Spain Total France* Ireland UK Total

MW added 310 793 368 1.471 8 11 10 29

Percentag e increase 28 38 72 40 62 21 3 7

The feed-in model is one in which any operator may feed-in power produced from renewable sources of energy into the grid against payment. This system is currently in use in countries like Denmark and Germany, which are among the biggest producer of RES-E and it has been in place in other reference countries like the Netherlands for years. This policy of direct subsidy is often criticised for violating market competition rules, but it is the one that actually attracts the biggest quantity of investment in renewable energy. The German 'Renewable Energy Law' is definitely the most significant example of a direct feed-in system. In this particular national support scheme the utilities are obliged to pay to the producers of RES-E a tariff fixed by the central government at 17pf/kWh (around  0,09), a very high subsidy considering that the production price for wind energy is around 0,05 /kWh. Moreover, according to this law, German utilities must give priority for RES-E being fed into the distribution grid, and are responsible for payment and feed-in of this kind of electricity into the grid.

Source: BTM Consult, 1999
*note that France has announced in Dec.2000 its will to create a system of fixed price support for RES-E

The German association of energy producers (VDEW), reveals that the share of renewable generators in the market increased in 2000: together, they delivered 12 (1999: 8.1) billion kWh of electricity produced from renewable energy sources to German energy suppliers � this represents an increase of 48 percent in comparison with 1999. The feed-in system has worked well in Denmark too, where wind energy production has grown from 2.197 TJ in 1990 to 10.005 TJ in 1998. But the most remarkable transformation is certainly in Spain, where, thanks to a feed-in law (Plan de fomento de las energias renovables), the total wind capacity installed has growth from 880 Mw in 1998 to 1.477Mw in 1999 to 2.270 Mw in 2000. These figures have made Spain the second country in Europe for wind energy produced. In Spain the central government fixes a price for wind energy (currently around 0.06 /Kwh) and the electricity distributor is obliged to buy all the electricity produced by the renewable energy plant. In Spain, as in Germany, the company that builds the park pays for the connection to the main distribution grid.
Briefing Note N� STOA 504EN

4


PE no 303.124, July 2001

Support policies for electricity produced from renewable energy sources in the EU

Installed wind power, 1997 and 1998 Country Installed MW 1997 Denmark 285 Finland 5 France 8 Germany 533 Greece 0 Ireland 42 Italy 33 Netherlands 44 Portugal 20 Spain 262 Sweden 19 United Kingdom 55 Other Europe 13 Total Europe 1.318
Source: BTM Consult, 1999 *in Spain the huge increase has occurred in 99/00

Cumulative MW 1997 1.116 12 13 2.081 29 53 103 329 39 512 122 328 57 4.793

Installed MW 1998 310 6 8 793 26 11 94 50 13 368 54 10 23 1.766

Cumulative MW 1998 1.420 18 21 2.874 55 64 197 379 51 880* 176 338 80 6.553

Despite considerations of market competition, there is no doubt that the direct, cost-oriented support scheme helps RES development far more than any other model. Moreover, it is very important to consider the time period for which these incentives last: considerations relating to competition law are generally subordinated to environmental policy aspects, in the sensitive initial stage of increased market penetration. This seems to be necessary if the final aim is the actual development of renewable energy technologies and their replacing of other less desirable means of electricity production. This consideration is even more important if one refers to solar power. Other important points to consider in the application of a fixed-price scheme are the differences deriving from the geographical location of the RES-E plant. An enlightening example can be provided in the German case, where the same price is fixed for the whole country, making it far more convenient to install a plant along the northern coast (where the wind speed is higher) than in other regions. Furthermore, one should consider the different price of production of electricity for every technology. Solar thermal electricity, for instance, is now far from competitive (0,12-0,15 /Kwh), but the technology is only at the early

stage of maturity and this price can be calculated to fall to less than half over the next 20 years (UNDP). In southern regions solar thermal technologies, using direct irradiation to generate electricity, have a potential that many estimates consider enormous, in the light of the progress seen in terms of cost and efficiency in the last few years. In fact STE technologies have the advantage of integrating perfectly into conventional thermal plants, in parallel with a fossil-fuelled boiler. They can thus, with minimal fossil back-up and thermal energy storage, be transformed into firm capacity without the need for separate back-up power plants and without stochastic perturbations of the grid. Also Greece and Italy have, in this field, a huge potential. An eventual EU-wide support scheme should thus consider the actual geographic differences between member states that could make preferable some technologies instead of others. 4.3 The quota system (trading in green certificates) Very different from the previously analysed support scheme, is the one recently put in place in the Netherlands, which is based on a system of green labels for the electricity produced from renewable sources of energy.
Briefing Note N� STOA 504EN

5


PE no 303.124, July 2001

Support policies for electricity produced from renewable energy sources in the EU

In this model renewable energy producers receive "green labels" for the power they supply. Every kWh of power from a renewable source will earn the producer one green label. Each block of ten thousand labels will be given a unique serial number, to ensure that it can only be issued once. This number will be recorded at a registration centre. These labels can be sold on a free market on the basis of supply and demand, nevertheless, nowadays energy companies are obliged by the government to buy a certain amount of green labels. The amount of green energy that utilities have to purchase can vary every year on the basis of national environmental and energy strategy and on the basis of agreements made with the industry and energy sector. The registration centre serves also as a monitoring institute: when an energy company buys a block of labels, it will report the serial number to the registration centre. To prevent fraud, the centre will check that the block has been legitimately issued. This independent body has access to production data and information from all the generation units. Since green labels are financially significant, reliability is very important. This is the reason why a kind of compliance regime is necessary to implement the system. In the Dutch model, for instance, those who fail to meet the commitments are obliged to make good the shortfall by buying labels at a settlement price, which will be higher than the prevailing market price. The energy company will therefore have a financial incentive to buy labels during the course of the year. The green labels market is seen by many as a market-based solution in the variety of support systems for renewable energy. Among the advantages provided by this system is the fact of generating cash flows for renewable energy technologies, notably on the futures market. RES-E plants (especially wind farms) are built on the request of energy companies that look for a long-term contract to secure their supply of green labels for the future. Moreover, in the future, developments in the demand of green electricity could come from other sources like consumers who are sensitive to environmental problems, or from the

commercial sector that could then sell products labelled 'made with renewable energy'. Local governments, also, can set good examples for the public committing themselves to buy green certified electricity. It is too early to say if the quota system is a successful one, nevertheless it is much liked by the business sector and many countries are planning to move towards a system providing 'green certificates' in the near future. Over 70 European energy companies with an interest in trading Renewable Energy Certificates have founded an umbrella group (RECS) working to create a European market for green certificates which, if transparent and harmonised, would become an EU-wide system. Another attractive feature of this scheme is that it allows energy companies to compete on equal terms for renewable energy labels. In the promotion of renewable energy, transparent and reliable certification should be considered a priority. Even in this model, government action is necessary, at least to set the quotas the utilities are obliged to buy. Trading in green certificates can also be seen as supplementary, or complementary to the direct feed-in scheme, or other state support. One should not forget that even in the Netherlands the quota system works in parallel with an eco-tax system that provides direct subsidy to RES-E. 4.4 The tendering scheme

In this model the Member State, via a national authority or an arbitration body, decides the price to be paid for RES-E in a certain region and sets down a tender for plants. This scheme has the advantage of effectively compelling those who wish to obtain a subsidy to offer a price as close to the cost price as possible, but the disadvantage of being created to suit a centralised electricity supply system. In the tendering procedure only operators who win tenders get access to the main distribution grid. Independent producers have by far less opportunity to gain access to the grid and the market for electricity coming from renewable sources is restricted and limited to central authority initiatives.
Briefing Note N� STOA 504EN

6


PE no 303.124, July 2001

Support policies for electricity produced from renewable energy sources in the EU

Nevertheless, where a plan is needed for the development and growth of energy consumption in a certain area, the tendering system appears to be a good solution. Not surprisingly Italy, Ireland and Greece have adopted this model: in these countries a particular geographic and economic situation requires central efforts to provide electricity to all areas and concessions and calls for tenders appear interesting for attracting project developers in regions remote from major electricity markets. Also France, whose electricity market is characterised by strong centralisation, has kept this tendering scheme in use up to now. Nonetheless, the tendering system is gradually being abandoned by Member States in the light of progressive liberalisation in the electricity market. Moreover, systems based on calls for tenders only once in two or three years may lead to extreme fluctuations in market growth. 5. ADDITIONAL POLICIES RENEWABLE ENERGY DEVELOPMENT FOR

The introduction of and adjustments to carbon taxes can be greatly facilitated by starting with a small levy along with the announcement that the tax will be gradually increased by specified increments at specified intervals. This approach gives energy users time to adjust their patterns of investment and use of energy-using devices to less carbon-intensive and more efficient ones, minimising economic disruption. Carbon taxes are an interesting policy option, and their main negative impacts can be compensated through the design of the tax and the use of the generated fiscal revenues. Consideration can also be given to shifting the imposition or collection of such taxes from downstream (consumers or importing governments) to upstream (producers or exporting governments) to offset the implied diversion of income that would otherwise result. 5.2 Reducing energy subsidies to conventional

5.1 Carbon taxes A carbon tax is a charge on each fossil fuel proportional to the quantity of CO2 emitted when the fuel is burned. Carbon taxes have often been advocated as a cost-effective instrument for reducing emissions, in order to comply with the Kyoto commitments to fight global climate change. What has to be remembered here is that renewable energies do not emit CO2 and will, with the introduction of such a levy, obtain a sort of competitive advantage over fossil fuels. Emission taxes are market-based instruments because, once the administrative authority has set the tax rates, emission-intensive goods will have higher market prices, lower profits, or both. As a result, market forces will spontaneously work in a cost-effective way to reduce emissions. Carbon taxes could have a direct and an indirect positive effect on renewables: the direct one, by pushing towards a wide fuel-switching towards RES; the indirect effect, by recycling the fiscal revenues collected in the promotion of cleaner technologies like renewables.

Many European governments as well as the EU itself provide financial support to various sources of energy. Such subsidies have been introduced for a variety of reasons, including economic, social, political, technological and environmental ones. However, energy subsidies also imply market distortion and may have side effects such as reducing the attractiveness of energy conservation and renewable energy. When looking only at money transfer and tax relief, it can be concluded that the total amount of subsidy going to renewable energy is substantially lower than the amount going to fossil fuels and probably of the same order of magnitude as the subsidies to nuclear energy alone (even if these last figures are difficult to quantify). In addition to these direct and indirect subsidies, there are important factors favouring certain types of energy over others. Firstly, energy producers and users often do not pay for the full external costs and damage (such as pollution, accidents and risks). In some cases (oil spills, nuclear accidents), international conventions provide for limited liability of the perpetrators. Secondly, "traditional" energy continues to benefit from support that it has received in the
Briefing Note N� STOA 504EN

7


PE no 303.124, July 2001

Support policies for electricity produced from renewable energy sources in the EU

past, e.g. in the form of below-commercial rates of return on investments. A comparison with the amounts involved in money transfers and tax reliefs shows that the bias in favour of traditional sources of energy is even stronger if these factors are taken into account. Conventional sources of energy have received strong subsidy in the past, regardless of their external costs. The internalisation of this cost should be provided and all kinds of subsidy to technologies that already have reached the point of commercialisation should be phased out to provide fair competition among different energy sources and different technologies. Tentative estimates of subsidies and other kinds of support in the EU (  m per year) Fossil fuels >18,000 Nuclear Renewables

Money >1,500 >1,800 transfer and tax relief Uninternali � Up to � 500 sed 50,000 20,000 external costs inheritanc � 8,500 e of past subsidies preferential 1,600 - 5,000 treatment total >68,000 >10,000 >4,000 Source: IVM; Vrije Universiteit, Amsterdam 5.3 Special loans and financial arrangements for investors in RES There are many possible financial arrangements that can improve the situation for RES market and facilitate a wider diffusion of renewable technologies. The Commission White Paper indicates the following: * Favourable depreciation of renewable energy investments * Favourable tax treatment for third party financing of renewable energies * Start up subsidies for new production plants, SME's and new job creation

* Financial incentives for consumers to purchase RE equipment and services * So called "golden" or "green" funds addressed to capital markets. Such funds are financed from private bank accounts which in this case attract lower interest rates. The margin created by the lower interest rate paid to the account holder is passed on by the bank to the renewable energies investor in the form of discount rates. * Public renewable energy funds, managed by regulated agencies. The facilities offered could include revolving funds as well as credit guarantees (renewable energies bonds) and should in any case conform to the Treaty provisions * Soft loan and special facilities from institutional banks: International financial institutions such as the EIB (incl. EIF etc.) and the EBRD and their national counterparts have already become involved in the financing of renewable energies, in particular hydro and wind plants. Their role can be strengthened considerably by: � providing soft loans and credit guarantees; �creating special facilities for renewable energies; � developing schemes facilitating loans for small renewable energy projects. Specific action focused on commercial banks will be promoted: � guidelines and risk evaluation schemes to help banks to audit RES businesses applying for loans; � EU support to packaged projects in order to facilitate soft loan access. Member States' experience indicates that a common problem slowing down the affirmation of RES could be far more significant: the administrative and bureaucratic proceedings in the energy sector. In most cases, installing new power plants or connecting a decentralised producer to the grid is expensive and takes too much time as a result of the heavy burden imposed by obsolete and conservative legislation in this field. 6. CONCLUSIONS

It is clear that, without a "fast track" process for market access for RES, the ambitious EU targets will not be reached.
Briefing Note N� STOA 504EN

8


PE no 303.124, July 2001

Support policies for electricity produced from renewable energy sources in the EU

Although other possible options exist to reduce both greenhouse gas emissions and external dependence and to increase the security of supply in the EU, it is clear that RES have the main role to play in any long-term strategy. In order to make these technologies available at reasonable cost at the appropriate time, it is necessary to develop them progressively, drawing them closer to the market. The necessity of government support for these technologies is self-evident, especially in the case of the least mature and economically competitive ones (notably solar and offshore wind). Past experience in this field indicates that fixed price mechanisms and preferential treatments applied to RES have been successful in creating a remarkable increase of energy production from renewables. However, too much interference in the energy market by central authorities can create an unwanted distortion. The priority should be to allow new technologies easy access to the market thereby reducing the administrative burden that still makes proceedings for decentralised electricity production difficult and expensive. BIBLIOGRAPHY GREEN PAPER "Towards a European strategy for the security of energy supply" - COM (2000) 769 Energy for the future - renewable sources of energy: White paper for a Community Strategy and Action Plan COM (97) 599 final Annual energy review 2000 DG TREN; January 2001 Campaign for Take Off (CTO) DG TREN - Commission services paper October 1999 World Energy Assessment (UNDP; UNDESA; WEC) "Energy and the challenge of sustainability", Sept.2000

"Energy subsidies in the European Union" IVM; Vrije Universiteit, Amsterdam - executed for DG4 of the European Parliament, 2001 (unpublished). ENER 103 "Innovations for the promotion of renewable energies" European Parliament - DG IV April 1998 WORLD WIDE WEB Sources: www.iea.org www.europarl.ep.ec www.europa.eu.int www.agores.org www.greenprices.com Author: Vincenzo COLLARINO, STOA-scholar Editor: Peter PALINKAS, STOA
This and other briefings can be downloaded from: http://www.europarl.eu.int/stoa/publi/default_en.htm Statements made in this Briefing do not necessarily reflect the views of the European Parliament.

Directorate A Division for Industry, Research, Energy, Environment and STOA European Parliament L-2929 LUXEMBOURG Fax: (352) 4300 27718

or: Rue Wiertz 60 B-1047 BRUSSELS Fax: (32) 2 2844980

Briefing Note N� STOA 504EN

9


