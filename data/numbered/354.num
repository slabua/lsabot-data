EUROPEAN PARLIAMENT

DIRECTORATE-GENERAL FOR RESEARCH

BRIEFING Budgetary Affairs No.1

Fraud and the EU Budget

The views expressed are the sole responsibility of the authors and do not necessarily reflect the official position of the European Parliament

10.02.1998

PE 167.114

DA

DE

EL

EN

ES

FR

FI

IT

NL

PT

SV


Summary This note places fraud against the EU budget in its political and legal context. It explains the nature of frauds frequently undertaken and discusses their level. It outlines actions to combat fraud and describes the role of the European Parliament.

-----

AUTHOR:

Anthony COMFORT Principal Administrator Schuman Building 6/60

PUBLISHER:

EUROPEAN PARLIAMENT DIRECTORATE B Division for Budgetary Affairs, Civil Liberties and Internal Affairs, Rules of Procedure, Petitions and Comparative Law EUROPEAN PARLIAMENT L-2929 LUXEMBOURG Phone: (00352) 4300-22167 Fax: (00352) 4300-27723 e-mail: acomfort@europarl.eu.int

Original: English - also available in French and German Manuscript finished in November 1997


Introduction The question of fraud against the interests of EU taxpayers has been pushed to a key position in the political priorities of the EU institutions. The European Council has adopted various conclusions demanding greater attention to the fight against fraud, in particular at its meeting in Essen in December 1994. The Commission has also given increasing attention to this area and has introduced legislative proposals (see below). However, the fight against fraud needs to be seen in the broader context of efforts to improve financial management in the European Union and also of the existing framework of controls on the Union's budget. The latter include the annual and special reports of the European Court of Auditors, which are given careful attention by the other EU institutions and which form part of the basis for Parliament's discharge decision granted annually to the Commission for its management of EU monies in the financial year concerned. Furthermore, a Financial Regulation imposes strict procedures in regard to the handling of all expenditure by EU institutions. In the context of the EP, fraud against the EU budget is the responsibility in the first instance of the Committee on Budgetary Control1.

Legal base Article 209a of the EC Treaty provides that: "Member States shall take the same measures to counter fraud affecting the financial interests of the Community as they take to counter fraud affecting their own financial interests. Without prejudice to other provisions of this Treaty, Member States shall coordinate their action aimed at protecting the financial interests of the Community against fraud. To this end they shall organize, with the help of the Commission, close and regular cooperation between the competent departments of their administration." The provisions for discharge of the EU budget are contained in EC Treaty, Article 206. For a discussion of the impact of the Treaty of Amsterdam on these articles see the related briefing on institutional aspects of budgetary control. The Financial Regulation, which governs the actions of all EU institutions in the budgetary sphere, was last amended by Council Regulation No 2333/95 of 18.9.952. There are also a number of implementing regulations of the Commission3.

1

Point 10 of the powers and responsibilities of standing committees, XV - Committee on Budgetary Control, Annex VI of EP Rules of Procedure, 12th edition, Nov 1996. OJ L240 of 7.10.95, p.1. Especially Comm Reg 3418/93; OJ L315 of 16.12.93.

2

3

%

PE 167.114


In regard to specific policies involving expenditure, the regulations concerned establish procedures which are intended to help control fraud, for example, by requiring detailed monitoring of programmes receiving assistance from the EU structural funds. However, it should be noted that responsibility for implementing such policies and for carrying out the necessary controls rests for the most part with national authorities and not with EU institutions. Level of fraud In its most recent Statement of Assurance4 referring to the year 1996 the European Court of Auditors stated in its conclusions that 5.4% of the total budget was the object of substantive errors. Most of these were in regard to the eligibility of operations for Community financing and 90% occurred at the level of Member States' administrations, with a lower rate than average in the area of the EAGGF-Guarantee section and a higher rate in that of the structural funds. These "errors" were formal and did not necessarily involve fraud. The term "irregularities" was not used here, but has been employed in the past by the ECA to encompass both administrative shortcomings and fraud. In many cases, it is difficult to disentangle these concepts, but it is widely acknowledged that intentional operations to defraud the European taxpayer form only a small minority of cases discovered by the European Court of Auditors. However, the Court also stated at point 19.4 of its conclusions that "Certain cases of deliberate irregularity to the detriment of the Community finances including, for example, the failure to declare dutiable imports, cannot, by their very nature, be detected by the usual auditing procedures implemented in accordance with generally accepted auditing standards. The Court is not therefore in a position to give any assurance of the absence of cases of this type." Chapter 1 of the European Court of Auditors' Annual Report looks in particular at the area of own resources and draws attention to a number of problems involving both maladministration and fraud in regard to the EU Customs regime. The cases mentioned concern, in particular, improperly issued certificates of origin which entitle exporters in countries benefiting from preferential tariffs to send goods to the EU at reduced or zero rates of duty. The absolute level of fraud is impossible to determine with any precision. The European Parliament recently established a Committee of Inquiry to examine the Community transit regime, which was known to be particularly prone to fraud, following two reports of the Court of Auditors on this sector (see below). After a detailed examination of the problem, involving the hearing of many witnesses, the Committee's rapporteur was able to conclude that at least ECU 3.5 billion per annum of customs duties and excise which should have been paid to European Union and to national budgets were being evaded. However, this figure is only an estimate and reliable information is impossible to obtain.

4

OJ C348 of 18 November 1997, Court of Auditors Annual Report covering 1996, Volume 2; the "Statement of Assurance as to the reliability of the accounts and the regularity of the underlying transactions" (known as the 'DAS') is a new requirement introduced by the Maastricht Treaty and included at Article 188c.1 of the EC Treaty.

&

PE 167.114


The estimates of the European Commission - as revealed in the last annual report of UCLAF on the fight against fraud5 - indicate that reported and detected fraud is to be found mostly in the area of traditional own resources, approximately twice as much as in the main expenditure policy, agriculture. Much lower amounts of fraud are detected in the structural funds and other policies involving EU expenditure. Fraud has been discovered in a few specific cases involving EU institutions themselves. Recently, the Court of Auditors published a special report on tourism policy6. Following problems in the management of the European Your of Tourism in 1990, the Court had already drawn up an earlier report which had not been able to find specific cases of fraud, although it did draw attention to administrative failings. However, further information came to light subsequently which resulted in the arrest by Belgian authorities of two officials of the Commission. The information resulting in these arrests came from investigations carried out by the Commission's own Directorate General for Financial Control, which then alerted the anti-fraud unit in the Commission, UCLAF, but the spur to such investigations was a complaint lodged with the Belgian authorities by a Member of the European Parliament7. The immunity which protects EU officials from prosecution by national police authorities has now been lifted at the request of the Belgian authorities investigating this case for 6 Commission officials . The lifting of immunity is necessary to permit investigations; it does not imply any degree of guilt. Although serious and with important implications for relations between EU institutions, the case of the European Commission's tourism unit is of less financial importance to the budget than other areas which are the object of fraud outside the EU institutions. In the area of agricultural spending, some products are known to be especially subject to fraudulent claims by beneficiaries. Increasingly, it has come to light that programmes and projects financed under the EU's structural funds are also subject to fraud. Recent cases are discussed below. Nature of Fraud The investigations of UCLAF reveal that the frauds most damaging to the EU budget occur as a result of organised criminal acts involving exploitation of the Community's legislation in the field of customs. Major fraud cases often have a transfrontier dimension and exploit the loopholes in complex EU legislation, which is sometimes vulnerable to fraud because of the need to reach a compromise between differing national interests. Lack of joint investigative bodies and difficulties in judicial collaboration mean that the penalties risked by the criminals concerned are slight. The resources available at central level are few and coordination between national police and customs services is handicapped both by language problems and by the varied nature of powers and organisations in fifteen Member States.

5

See note 13 below. European Court of Auditors, Special Report No 3/96 on Tourism Policy and the Promotion of Tourism. Two reports of the Committee on Budgetary Control have been adopted. The first concerns the Court of Auditors' special report No 3/96 on tourism policy and the promotion of tourism (rapporteur: Mr De Luca- EP 222.172/def-A4/0040-98), while the second is on the Commission's conduct in respect of alleged fraud and irregularities in the tourism sector (rapporteur: Mrs Wemheuer-EP 222.170/rev2-A4/0049-98).

6

7

'

PE 167.114


Furthermore, large-scale frauds often make use of sophisticated technology, complicated logistics, carefully forged documentation and the proceeds are then laundered through complex transfrontier operations. The bodies responsible for countering such frauds are often poorly equipped as well as divided by national boundaries.

Actions to combat fraud
Recent developments There has been a rising level of activity to combat fraud against the EU budget at least since 1991. The European Commission, the Court of Auditors and Parliament's Committee on Budgetary Control have always been concerned, but since the Treaty of Maastricht the Council of Ministers and the European Council have also paid increasing attention. As the Copenhagen Council stated in June 1993: "The European Council underlined the importance of continuing to combat fraud and irregularities in connection with the Community budget, both in view of the sums involved and in order to promote confidence in the construction of Europe...." The issue was discussed in particular at the Corfu European Council in June 1994 and work began on establishing a European Convention for the protection of the Communities' financial interests. A serious problem has been incompatibility of national criminal legislation and the lack of adequate deterrents for fraud against the EU budget. These questions were the subject of a Council Resolution of 6.12.94 8 at a meeting of the Justice and Home Affairs ministers, while the European Council meeting at Essen in the same month called on the institutions and the Member States to take concerted action against fraud and asked Member States to report on the domestic measures implemented by them to combat wastefulness and the misuse of Community resources. Because of reluctance on the part of some Member States to see greater powers being passed to EU institutions, the means chosen to promote collaboration was that of an inter-governmental agreement or convention, which must be ratified by the signatories' parliaments, together with a Council Regulation establishing a legal framework for further EU actions. This approach was not accepted by the European Parliament which would have preferred measures based on the 'first pillar' i.e. under the control of European institutions rather than relying on inter-governmental cooperation. During 1995 this Convention on the protection of the European Communities'financial interests was nevertheless signed in July (but will not enter into force until ratified)9. The Convention seeks to define fraud against the EU budget and to settle the criminal liability of fraudsters, the penalties applicable and questions of jurisdiction. It aims to provide a uniform level of protection throughout the Community. The national reports on action to combat wastefulness and misuse of Community resources, together with a synthesis of the Commission, were discussed in December at the Madrid European Council.

8

OJ C355, 14.12.94 OJ C316 of 27.11.95; EP Resolution of 19.9.96, OJ C320, 28.10.96

9

(

PE 167.114


Council Regulation No 2988/9510 establishing the legal framework was then adopted (also on protection of the European Communities' financial interests) - but without the EP's amendments. This regulation applies to all EU expenditure and own resources and establishes Community administrative penalties and the concept of 'abuse of rights'as key elements in the fight against fraud. The relationship of national criminal procedures and Community administrative law is made explicit. In 1996 a further Council Regulation was passed, in the legal framework established by Regulation 2988/95, concerning on-the-spot checks and inspections carried out by the Commission to protect the Communities' financial interests against fraud11. This lays down rules for the powers and duties of Commission inspectors and places partnership between the Member States and the Commission on a more official basis. A first Protocol to the Convention on the protection of financial interests, directed against corruption involving Community or national officials likely to damage the Communities'financial interests, was signed on 27.9.9612. It requires all Member States to make such corruption a criminal offence, extraditable in serious cases and attracting penalties including imprisonment. A second Protocol relates to the liability of legal persons, confiscation of the proceeds of crime, money-laundering and cooperation between Commission and Member States; it was adopted by the Council on 19 June 199713 (following adoption on 26.5.97 of a new Convention establishing the commitment by Member States to take the measures necessary to ensure that active and passive corruption is made a criminal offence at national level14). The lengthy delays in ratification by national parliaments of the Convention and its protocols unfortunately place in doubt the degree of national commitment to eradicating fraud in EU policies. To date not a single Member State's Parliament has ratified the Convention signed in July 1995.Nevertheless, it is still hoped that the Convention and its protocols will enter into force in the course of 1998; the Treaty of Amsterdam includes a provision which will allow such conventions to enter into force for the Member States concerned once a majority of signatories have ratified (Article K6).

10

OJ L312 of 23.12.95. CReg No 2185/96, OJ L292, 15.11.96. OJ C313, 23.10.96. See also the protocol on interpretation by the European Court of Justice of the convention on the protection of the EU's financial interests, 29.11.96, which gives the ECJ jurisdiction for preliminary rulings. OJ C221, 19.7.97; EP Resolution of 24.10.96, OJ C347, 18.11.96 OJ C195, 25.6.97; EP Resolution of 15.11.96, OJ C362, 2.12.96

11

12

13

14

)

PE 167.114


Action by the Commission Within this difficult legislative context, the Commission has nevertheless been very active in pursuing cases of fraud, insofar as it was able to uncover these. To this end it established a unit for the co-ordination of fraud prevention (UCLAF) in its secretariat-general in 1988, which, following a reorganisation in 1993, has specific units dealing with policy, intelligence, common market organisations in agriculture, trade in agricultural products, the structural funds and own resources . The personnel attached to the unit has been substantially increased since 1993, following action by Parliament to make available additional posts, and now includes the special anti-fraud units formerly attached to other DGs. Commissioner Gradin is responsible. The Commission produces an annual work programme in advance of each year's anti-fraud activities and has produced annual reports since 199015. A large number of seminars are held to coordinate the work of UCLAF with that of national departments and to acquaint them with transnational Community fraud. Special training courses are also organised for national officials responsible for preventing fraud against the EU budget, especially those concerned in tax inspectorates, customs, audit offices, police and judicial authorities. Increasingly, such seminars and courses are also organised for officials from the EU's partners in Central and Eastern Europe. Many bodies bringing together Commission and national officials meet on a regular basis, in particular the Advisory Committee on Fraud Prevention. - Investigations Frauds which have been discovered by UCLAF and the services of the Member States are of many different types. Where they involve economic operators, frauds may include illegal exploitation of economic position to increase market share, but more frequently involve exploiting the EU's rules and subsidies to obtain direct economic advantage. Where such fraud is transnational in nature, the Member States' investigation services run into obvious difficulties, often of a legal nature. Yet the organisations responsible for fraud have no such problems, with sophisticated networks of suppliers and operators, frequently making use of accomplices in many different countries. The major organised fraud cases investigated by the Commission in 1996 concerned smuggling of cigarettes and alcohol. Other cases included VAT on computer components; export refunds on olive oil; replacement of high quality beef by offal; removal of various products en routs from consignments in transit; and fictitious exports of wine. There were also various major frauds in connection with the rules of origin and trade under Community preferential agreements and a wide range of other frauds in connection with the CAP, the structural funds, research funds, energy and various operations involving humanitarian aid and the EDF.

15

The Annual Report on the Fight Against Fraud for 1995 was published by the Office for Official Publications of the EC with the reference ISBN 92-827-6560-1; that for 1996 is ISBN 92-828-04852.

*

PE 167.114


- Legislative proposals Much of the Commission's legislative programme in this area has now been completed, although of course ratification by national parliaments of the Convention and implementation of the new rules by Member States are another matter. The second protocol to the Convention on the protection of financial interests was originally intended by the Commission to have a broader scope, laying down guidelines and rules for judicial cooperation and for joint investigation into transnational cases of fraud. It has put into practice those aspects of the Council Resolutions of November 1991 and of 1.12.9416 which concern money laundering and the liability of legal persons, but may need to be extended later. - Transit The Common Transit Procedure which permits goods to enter the territory of the Union and its EFTA partners (and that of Poland, Hungary and the Czech Republic) without paying customs duties or excise, provided that they are then re-exported, has been the subject of serious frauds which have had a serious impact on public budgets. Despite various attempts to tighten up the procedures 17 the loss of revenue involved has continued to be very substantial and for some sensitive goods, such as tobacco, a 100% guarantee of the value of the taxes and duties concerned has been required. See Role of the European Parliament below. A substantial revision of the transit regime is being carried out, involving a large degree of computerisation but there are delays in completing this project. - Other actions The Commission is developing its actions in partnership with Member States. Training schemes and conferences are organised on a regular basis with a view to improving international cooperation and more effective action in cases of transfrontier fraud. Training missions are also organized through the PHARE and TACIS programmes for partner countries.

Role of the EP Parliament's role is a political one. It draws public attention to the problem; it discusses the issue in its committees and public hearings; it reviews possible solutions and makes recommendations. It can therefore act only indirectly - except in regard to its own budget - by demanding stronger measures from the Commission and Council. The issue is also of especial interest to Parliament and to the EU in general because of the potential clash between the concepts of "subsidiarity" and effective management of EU policies. The measures required to ensure proper protection of EU financial interests may sometimes involve joint actions by the EU and its Member States which will seem to some as involving an unacceptable transfer of power to the Union's institutions.

16

OJ C328, 17.12.91. For example, proposals outlined in the Commission Communication of 29.3.95, COM(95)108.

17

+

PE 167.114


In general, the role of the Parliament has been to push for tougher measures to combat fraud. It has done so through the reports and resolutions initiated by the Committee on Budgetary Control18 and through the latter's monitoring of financial implementation of EU policies; by its questions to the Commission and Council; by ensuring that more resources are made available for this purpose through the budget; and through the organisation of public hearings. Recently, the Parliament also made use for the first time of a new power accorded by the Maastricht Treaty to establish a Committee of Inquiry to examine fraud in a specific area - that of Community Transit. The major power of the Parliament is however that of discharge. Its Budgetary Control Committee examines the implementation of the EU budget on an annual basis and has the power to refuse discharge to the Commission if it believes that there has been serious mismanagement. It also makes recommendations in this context to which the Commission is obliged to respond. However, it remains true that - since they are responsible for managing about 80% of EU expenditure - it must be in the Member States that the bulk of the action is needed to combat fraud. The Community's institutions are now well-prepared and are steadily improving their financial management; but the controls exercised by these institutions cannot extend very far into the administration of the Member States. For this reason, the emphasis of the Commission in its inspections has been to ensure that appropriate systems of financial control are in place in the Member States. Parliament has also sought to ensure that penalties which currently apply to Member States failing to exercise adequate controls in the agricultural sector be extended to other areas of EU policy, but this proposal has not yet met with the approval of the Member States. The efforts of Parliament and the Commission, to secure a treaty on legal protection of EU financial interests, which began in 1976, have now seen partial success. The Treaty of Maastricht obliges Member States to treat money from the Community budget as if it were national resources, when it comes to fraud. The new Convention - discussed above - will, when ratified, considerably extend the concept of common interest in this area. This common interest was the reason for the organisation by Parliament of joint assizes with delegations from the parliaments of Member States in 1996 at which the issue of fraud against the EU budget was discussed in depth. At another level Parliament has sought to extend the involvement of the individual citizen in the fight against fraud. In particular, it initiated the fraud free-phone system which became operational in 1994 and permits anyone to ring the appropriate departments of the Commission free of charge to provide information on fraud and the EU budget. One related area of special concern to the Parliament has been judicial cooperation between Member States. It has provided funds for a Community-wide network of national associations of lawyers working to protect EC financial interests and has held a public hearing entitled "Protecting the European taxpayer: Towards a common criminal and judicial area".

18

See especially its report on that of the Commission for 1996 and on the Commission's Work Programme for 1997/8 on the protection of financial interests and the fight against fraud :A40287/97 (rapporteur: Mr.B�SCH); EP resolution of 22 October 1997.

 #" 

PE 167.114


Other recent activities of the EP in the field of fraud are as follows: * Public hearing on action to combat fraud against the budget of the European Union; 29/30 May 1995 in Brussels (synthesis of proceedings available from Committee on Budgetary Control) Interparliamentary Conference on Combatting Fraud against the Community budget, 23/24 April 1996 in Brussels (documents published as PE 217.468) Committee of Inquiry into the Community Transit System, (report published in 4 volumes, 19 February 1997, PE 220.895).

* *

A further hearing on fraud in regard to rules of origin and preferential trading agreements is planned for April 1998.

***

 ## 

PE 167.114


