EUROPEAN PARLIAMENT
DIRECTORATE-GENERAL FOR RESEARCH DIRECTORATE A DIVISION FOR INTERNATIONAL AND CONSTITUTIONAL AFFAIRS

FACTSHEET
Latvia 1. The acquis 1 The acquis in this chapter mainly covers indirect taxation, particularly VAT and excise duties, while on direct taxation it is limited to legislation on corporate taxation. On the whole, the candidate countries have an indirect taxation system close to that of the EC. VAT was introduced in 1970 by the first and second VAT Directives. The decision on own resources for the EC led to the harmonisation of VAT. The sixth VAT directive (77/388/EEC) harmonised the basis and the assessment basis and is still the main body of law for VAT, laying down all Community definitions and principles, while leaving a number of options to Member States. The acquis in the area of excise duties covers harmonised legislation on mineral oils, tobacco products and alcoholic beverages. The legislation lays down the structure of the duty and minimum rates. Duty is payable to the Member State in which the product is consumed. All fiscal controls at the Community's internal frontiers were abolished in 1993. Legislation in the area of administrative cooperation and mutual assistance provides instruments to circumvent tax evasion and avoidance across the frontiers of Member States by information gathering and exchange. The direct taxation acquis concerns some aspects of corporate taxation and capital duty such as administrative cooperation between tax authorities and removing obstacles to cross-frontier business activities. There is also a code of conduct for business taxation. Chapter 10 - Taxation

1

Information largely drawn from the European Commission, DG Enlargement http://europa.eu.int/comm/enlargement/negotiations/index.htm

WIP/2002/11/0147

1


2. The negotiations The chapter has been closed with ten countries and provisionally closed with Bulgaria, while it remains open with Romania. Most countries have provided a timetable for full alignment for VAT and excise duties and have declared during the negotiations that they accept and will apply the principles of the code of conduct for business taxation. The Commission is analysing the candidate countries' legislation in order to identify potentially harmful practices not in line with the code. All candidate countries have requested transitional measures and a limited number of derogations, mostly as regards VAT and excise duties. One country has requested a transitional period in the area of business taxation. The level of rates of tax or duty has been the most contentious issue as excise duty in particular tends to be much lower in the candidate countries. Governments fear the economic and social implications and, hence, the political consequences of sharp tax rises on socially-sensitive goods such as cigarettes, and all have therefore asked for transitional periods on particular goods or services to allow for a longer period of adjustment. For its part, the EU has considered both the need to safeguard the proper functioning of the internal market and the political, social and economic impact in the candidate countries. Thus, some transitional measures of relatively short duration have been accepted. Chapter opened: May 2001 Status: closed in December 2002 (provisionally closed in June 2002) Transitional arrangements: � Level of VAT turnover threshold for SMEs � Lower excise duty rates on cigarettes � VAT exemption for international passenger transport, royalties � Reduced VAT rate on heating 3. Latest assessment of the European Commission
1

In its 1997 Opinion, the Commission concluded that the acquis concerning direct taxation should present no significant difficulties, and that where indirect taxation was concerned, a considerable effort would be required if Latvia was to comply with the acquis on VAT and excise duties in the medium term. The Commission added that it should be possible for Latvia to start participating in mutual assistance as the tax administration developed its expertise in this respect. Since the Opinion, and especially over the last two years, Latvia has made significant progress in aligning with the EC acquis on taxation. Latvia has also made progress with developing the necessary administrative capacity to implement the acquis in this area, but the organisational restructuring which is underway represents a major challenge. Latvia's level of alignment with the taxation acquis has reached a reasonable degree although a number of weaknesses remain to be addressed. Its implementation capacity is partly hampered by the delays in information technology and interconnectivity. This needs to be urgently addressed.

1

European Commission, Regular Report on Latvia 2002, p. 79: http://www.europa.eu.int/comm/enlargement/report2002/lv_en.pdf

WIP/2002/11/0147

2


Negotiations on this chapter have been provisionally closed. Latvia has been granted, for an indefinite period of time, the right to apply a VAT registration and exemption threshold of LVL 10 000 ( 17 857) for small and medium-sized enterprises; a technical transitional period of one year for the purpose of applying, once a Member State, for a derogation for simplified procedures on VAT on timber transactions; and a transitional period until 31 December 2009 in order to reach the EC minimum excise duty levels on cigarettes. In addition, Latvia has been granted specific arrangements to continue VAT exemption with credit for input tax on international passenger transport and without credit for input tax on services supplied by authors, artists and performers. Latvia is generally meeting the commitments it has made in the context of the accession negotiations. In order to be ready for membership, Latvia should focus further efforts on completing transposition - except for areas where transitional arrangements have been agreed - in the areas of indirect and direct taxation, including intra-Community transactions, and on further pursuing the measures taken to modernise and reinforce the tax administration. Latvia should step up its ongoing legislative work, and urgently speed up its preparations for electronic interconnectivity.

January 2003

WIP/2002/11/0147

3


