EUROPEAN PARLIAMENT
DIRECTORATE-GENERAL FOR RESEARCH DIRECTORATE A DIVISION FOR INTERNATIONAL AND CONSTITUTIONAL AFFAIRS

FACTSHEET
POLAND 1. The acquis 1 The energy acquis represents the body of all energy related EU law, regulations and policies. Implementing the acquis requires not only adequate legislation but also properly functioning institutions (for example a regulatory body, as required in the electricity and gas directives, a nuclear safety authority etc.). In view of the energy acquis, candidate countries need notably to: � decide on an overall energy policy with clear timetables for restructuring the sector; � prepare for the internal energy market (the Gas and Electricity directives; the Directive on electricity produced from renewable energy sources); � improve energy networks in order to create a real European market; � prepare for crisis situations, particularly through the constitution of 90 days of oil stocks; � address the social, regional and environmental consequences of the restructuring of mines; � waste less energy and increase the use of renewable energies such as wind, hydro, solar and biomass in their energy balance; � improve the safety of nuclear power plants in order to ensure that electricity is produced according to a high level of nuclear safety; � ensure that nuclear waste is handled in a responsible manner; and prepare for the implementation of Euratom Safeguards on nuclear materials. Candidate countries have made considerable progress in the last few years and the abovementioned issues are applicable to them in varying degrees. However, more is necessary, and this will evidently require large amounts of investment funding. Although the EU will continue to assist with pre-accession aid, the bulk will have to be financed by candidate countries Chapter 14 - Energy

1

Information largely drawn from the European Commission, DG Enlargement http://europa.eu.int/comm/enlargement/negotiations/index.htm

WIP/2002/11/0241

1


themselves. Private investments have an important role to play in this context and require a stable investment climate. As regards the issue of nuclear energy, the European Union has repeatedly emphasised the importance of a high level of nuclear safety in candidate countries. In June 2001, the Council of the European Union took note of a Report on Nuclear Safety in the Context of Enlargement. This Report contains recommendations to all candidate countries to continue their national safety improvement programmes, including the safe management of spent fuel and radioactive waste, and regarding the safety of their research reactors. All candidate countries have responded to these recommendations. During the first half of 2002, a special Peer Review on nuclear safety assessed the progress made by candidate countries in implementing all recommendations. This exercise under the auspices of the Council resulted in a Status Report, which was published in June 2002. It comes to the general conclusion that all candidate countries are clearly committed to fulfil the recommendations. The EU has insisted on the early closure of certain types of nuclear power units. 2. The negotiations The chapter has been closed with ten countries and provisionally closed with Bulgaria, while it remains open with Romania. Generally, in the energy chapter, negotiations concentrate, depending on the country concerned, on the constitution of emergency oil stocks, the internal energy market (gas and electricity directives) and nuclear safety. Transitional arrangements have been granted to all the countries which have closed the chapter so far, except Hungary, allowing them longer periods in which to build oil stocks up to the required level. The Czech Republic has been granted a transitional period up to the end of 2004 to implement the Gas Directive and Estonia has been allowed until the end of 2008 to implement the Electricity Directive. Chapter opened second half of 1999 Status closed in December 2002 (provisionally closed in second half of 2001) Transitional periods: � build up of oil stocks to required level, until the end of 2008 3. Latest Assessment by the European Commission 1 In its 1997 Opinion, the Commission concluded that, provided efforts were maintained, Poland should be in a position to comply with most of the then existing EC energy legislation in the medium term, adding that matters such as the adjustment of monopolies including import and export issues, access to networks, import barriers for oil products, energy pricing, emergency preparedness (including the building up of mandatory oil stocks), state interventions in the coal sector, and the development of energy efficiency needed to be closely followed. No major difficulties were foreseen for compliance with Euratom provisions.

1

European Commission, Regular Report on Poland 2002, pp. 91 & 92: http://www.,europa.eu.int/comm/enlargement/report2002/pl_en.pdf

WIP/2002/11/0241

2


Since the Opinion, progress has been made, although not consistently. Most notable has been the advances with regard to oil stocks, some improvement of energy efficiency, nuclear issues, and the gradual development of administrative capacity. Progress in implementation, in particular as far as transformation of the gas and coal sub-sectors is concerned, has significantly slowed down. Overall, Poland has achieved a good level of alignment with the EC energy acquis. Negotiations on this chapter have been provisionally closed. One transition period has been granted to Poland in relation to the build up of oil stocks (end of 2008). Poland is generally meeting the commitments it has made in the accession negotiations in this field. In order to complete preparations for membership, Poland's efforts now need to focus on ensuring full and timely adoption and implementation of legislation in this area, in particular with regard to the internal energy market (notably gas) and on strengthening the administrative capacity of the newly established bodies. The restructuring of the sector also warrants due attention, as does the progressive building up of oil stocks.

January 2003

WIP/2002/11/0241

3


