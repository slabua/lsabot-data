APPENDIX 1 The euro as an anchor for CEECs currencies
Bayoumi and Eichengreen (1997) operationalise the theory of optimum currency areas (OCA) by estimating an equation which explains the instability of each bilateral exchange rate by variables representing the degree of shock asymmetry and the intensity of bilateral trade. Here, we use the same technique, but we focus on the variability against the three main currencies (USD, DM, yen) only, instead of studying all the combinations of bilateral exchange rate. This choice aims at highlighting the role of these three currencies as potential anchors, whereas observing stable, bilateral exchange rates in a region does not reveal what the anchor is. The equation estimated is:
S D e ij

( )=

a 0 + a 1 S D y ij + a 2 T R A D E ij + a 3 S I M

+

()

-

-

ij

+ u ij

SD eij is the standard deviation of the log-variations of the bilateral exchange rate between i and j (j = $, DM, yen).
It measures the variability of the bilateral exchange rate.

()

SD yij is the standard deviation of growth differentials between i and j (j = US, EMS core (G, F, B, NL, Dk), Japan). It measures the asymmetry of the real shocks in i and j countries.
TRADE ij is the average of the share of exports and of imports in i's GDP (j = US, EMS core (G, F, B, NL, Dk), Japan). It measures the role of j as a trade partner of i.

()

SIMij = 100


k

 X ik X jk  Min ,  is a measure of the similarity of the structure of exports    Xi X j 

between i and j. k stands for 70 products (source Cepii-Chelem). This Finger index varies from zero (complete dissimilarity) to 100 (complete similarity). When the Finger index is high, a shock to a specific sector (say, the automobile sector) has a symmetric impact in i and in j. Cross-section estimations are run on a sample containing 36 currencies against 3 potential anchors. All coefficients, but the Finger index, are significant and correctly signed. The fact that the Finger index is not significant may be related to the importance of intra-industry trade: sectoral shocks may have a differentiated impact in the various countries according to their intra-industry specialisation. The results without the Finger index are reported in the Table below.


Table: results of the estimations All countries OECD Non-OECD Emergent countries (1)

Without dummie s
N� observ. Constant SD(yij) TRADEij DUMWij DUMAij DUMEij Adj. R� 108 -0.28** 2.60** -0.22** / / / 0.49

With dummie s
108 -0.40 2.71** -0.24* 1.79 -4.40* 1.31 0.52

Without dummie s
48 2.76 1.41** -0.11* / / / 0.75

With dummie s
48 2.89** 1.40** -1.12* -1.11 / / 0.76

Without dummie s
60 -4.40* 3.81** -0.36* / / / 0.55

With dummie s
60 -3.93* 3.73** -0.35* / -3.30 2.26 0.21

Without dummie s
36 2.73# 1.18** -0.23# / / / 0.21

With dummie s
36 3.44** 1.08** -0.22# / -3.49** 2.76# 0.44

(1) CEECs + emergent countries of Asia. ** significant at the 99% level. * significant at the 95% level. # significant at the 90% level. Source: authors' calculations
In a second step, dummies were added to the equation in order to catch regional behaviours: DUMWij = 1 if i is a West-European country and j is the DM, 0 if not. DUMAij = 1 if i is an Asian country and j is the USD, 0 if not. DUMEij = 1 if i is a CEEC and j is the DM, 0 if not. The coefficient for the West-European dummy is never significant, meaning that the anchor strategy of these countries is accounted for by the OCA theory. The coefficient for the Asian dummy is negative and generally significant: the variability of Asian currencies against the USD is too low, by 3-4 percentage points compared to what would be required by the OCA theory. Finally, the coefficient on the CEECs dummy is positive. It is significant in the emerging countries sample: the variability of CEECs currencies against the DM is too high, by about 2 percentage points compared to what would be required by the OCA theory. According to this analysis, the CEECs would be better-off in stabilising their currencies more against the DM, and subsequently against the euro.


