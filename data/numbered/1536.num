EUROPEAN PARLIAMENT
1999

       

2004

Committee on Legal Affairs and the Internal Market

13 June 2002

WORKING DOCUMENT
on the meeting with chairmen of committees responsible in national Parliaments of 20 March 2002 at the European Parliament in Brussels - Part 1 'Transposition of Community law by Member State parliaments' 9.10 a.m. to 10.50 a.m.

DT\472023EN.doc

PE 316.181/A/fin

EN

EN


The chairman of the Committee on Legal Affairs and the Internal Market, Mr Gargani, welcomed the participants. Mrs Wallis introduced the subject of the meeting. She noted that the transposition of directives pursuant to Community law was absolutely vital for the completion of the internal market. Similarly, appropriate access to justice for all citizens of the Union was of utmost importance. Essentially, she saw 3 problem areas as regards transposition: a) where there was no transposition for political reasons; b) where transposition was technically flawed; c) where the administration and judiciary were not adequately informed as to the new legal situation thus leading to shortcomings in the application of legislation. She noted that a flawed transposition of legislation reflected badly on the system as a whole and set a bad example for accession countries. Measures to counteract this were required both at European level and at Member State level. In this connection, it was worth highlighting procedures such as those practised by Denmark and Finland. At Member State level, a different attitude towards European law and better information of lawyers was required since the average lawyer was frequently of the opinion that Community law was of no relevance to him or her. Information already available was not really getting through. Greater account also needed to be taken of the regions. Problems also arose in connection with the increasingly common phenomenon of "soft law". Mr Mormino from the Italian Camera dei Deputati considered that society as a whole needed to be made more aware of the European issues and that this would require a great deal of publicity. Mr Koukiadis, vice-chairman, stressed the importance of and the need for cooperation between the European and national parliaments. National parliaments needed to keep a check on their respective states. The fact was that at national level, people were totally unaware of what went on at European level and that cooperation was often unsatisfactory. Mr Lax from the Finnish Eduskunta considered that the procedures in Finland were sound and that this was due to the fact that parliament was properly involved. Mr MacCormick considered it very important for the public to be informed properly. He was of the opinion that the Council should hold its meetings in public whenever engaged in enacting legislation. All institutions should endeavour to provide information in good time and with the maximum of publicity prior to adopting legal acts. Hearings in the European parliament were one useful took to this end. Representatives of national and regional parliaments should regularly participate by invitation at relevant sittings of European parliament committees. The UK government provided regular briefings to UK MEPs so that he found himself normally aware of the UK position in relation to forthcoming decisions. For the sake of parliamentarians, draft reports should be made available to national parliaments, and obstacles to the flow of information between national parliaments and the European parliament should be eliminated.
PE 316.181/A/fin 2/4 DT\472023EN.doc

EN


Mr Medina Ortega stressed that while what was needed was harmonisation, the transposition of directives appeared to have the opposite effect. Thus, the directive on "time-sharing" had been transposed almost word for word in Germany whilst the Spanish transposition of the directive had rendered it almost unrecognisable. Moreover, important issues were considered by the Council independently of Parliament. Mr Harbour considered that directives were mostly of a highly technical nature and therefore quite complicated so that specialists were needed. The latter should be consulted at an early stage. Links and closer contact between the national parliaments and European Parliament should be institutionalised. Mr Miller, vice-chairman, highlighted the distrust born of ignorance which was often exhibited vis-�-vis the Union and its institutions and also between the various parliaments. In addition, he was not of the opinion that all documents should be forwarded to the national parliaments since it was not desirable that the latter be flooded with information. Instead, they should only be informed with regard to matters where the European Parliament really exerted some power, e.g. with regard to codecision procedures. As far as he knew, the Danish Members of the European Parliament and the Members of the national Parliament met once a month at the Danish Parliament in order to discuss European issues. Mrs Moerman from the Belgian Chambre de R�presentants explained the complexities of the Belgian system of government. In addition to a bicameral national Parliament, there were several regional parliaments. This caused delays in the transposition of legislation. If only one of these parliaments failed to transpose a directive, Belgium was deemed not to have been transposed the directive. Directives frequently went into excessive detail, thus running counter to the principle of subsidiarity. Mr Cointat from the French Senate noted that one needed to participate in something in order to feel bound by it. This was far from being the case for the various national public authorities with acts of the European Parliament. The transposition of directives in France was such a lengthy process because there were so many laws there - more than in any other Member State. Directives which were not particularly urgent were therefore put off. In addition, it was the government that set the order of business and not Parliament so that Parliament was almost never able to place a directive on the agenda. He also stressed the need for a good level of cooperation between the various actors in the legislative process at European and national level. Mr Dobbin from the British House of Commons stressed the need for better contacts between the European Parliament and national parliaments. He drew attention to similar problems between the Scottish Parliament and the United Kingdom Parliament. Mr Stucchi from the Italian Camera dei Deputati stressed the need for quality rather than quantity. Directives should by nature and in keeping with the principle of subsidiarity concentrate on principles rather than details. Mr Kaalund from the Danish Folketing noted that the Danish Parliament was always well informed on European issues. Before travelling to Brussels to take a decision in the Council, a
DT\472023EN.doc 3/4 PE 316.181/A/fin

EN


minister would normally visit the Folketing's relevant committee in order to hear its views on the matter. Members of the Folketing's relevant committee were therefore acquainted with the subject matter early on so that any resulting directive could be transposed more easily. Mr Ofner from the Austrian Bundesrat explained that there was a tendency to blame Brussels whenever there were problems. Nevertheless, Austria was very successful in transposing directives both as regards speed and substance. Austria similarly endeavoured to bring adjoining areas of Austrian legislation into line with Community law. Mrs Berger noted that while Austria's track record was very good in the area of justice, this was not the case in with regard to the internal market. She asked the guests from the national parliaments whether they felt responsible where the transposition of directives was excessively slow or flawed or whether they considered the fault to lie with their governments. She also asked what, in the opinion of the national parliaments, such an improved cooperation between them and the European Parliament, for instance by means of more joint committees in the European Parliament, might look like. Mr Lax from the Finnish Eduskunta said that expert committees were closely involved in the work of the Finnish Parliament. Mr MacCormick also considered that it would be excessive to submit all documents to the national parliaments. This did not apply to draft legislative documents. Mr Koukiadis, vice-chairman, thought that it would be advisable to make provision for the national parliaments to report on the transposition of legislation. Mrs Wallis welcomed the useful contributions made and stressed the need to strengthen the sense of community and to overcome the prevailing sense of mistrust.

PE 316.181/A/fin

4/4

DT\472023EN.doc

EN


