EUROPEAN PARLIAMENT
1999
��� � � � � � � � ��

2004

Session document

C5-0299/2003
2002/00152(COD)

EN
01/07/2003

Common position
with a view to the adoption of a Directive of the European Parliament and of the Council amending Directive 94/35/EC on sweeteners for use in foodstuffs

Doc. 9714/2/03 Statements 10422/03 SEC(2003)0783

EN

EN



COUNCIL OF THE EUROPEAN UNION

Brussels, 25 June 2003 (OR. en) 9714/1/03 REV 1

Interinstitutional File: 2002/0152 (COD) DENLEG 26 CODEC 709 OC 307

LEGISLATIVE ACTS AND OTHER INSTRUMENTS Subject : Common position adopted by the Council on 25 June 2003 with a view to the adoption of a Directive of the European Parliament and of the Council amending Directive 94/35/EC on sweeteners for use in foodstuffs COMMON GUIDELINES Consultation deadline: 24.06.2003

9714/1/03 REV 1 DG I

HV/kjf

EN


DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL 2003/ of amending Directive 94/35/EC on sweeteners for use in foodstuffs

/EC

THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION, Having regard to the Treaty establishing the European Community, and in particular Article 95 thereof, Having regard to the proposal from the Commission 1, Having regard to the Opinion of the European Economic and Social Committee 2, After consultation of the Scientific Committee on Food, pursuant to Article 6 of Council Directive 89/107/EEC of 21 December 1988 on the approximation of the laws of the Member States concerning food additives authorised for use in foodstuffs intended for human consumption 3 , Acting in accordance with the procedure laid down in Article 251 of the Treaty 4,

1 2 3 4

OJ C 262 E of 29.10.2002, p. 429. OJ C 85, 8.4.2003, p. 34. OJ L 40, 11.02.1989, p. 27. Directive as amended by Directive 94/34/EC of the European Parliament and of the Council (OJ L 237, 10.09.1994, p. 1). Opinion of the European Parliament of 10 April 2003 (not yet published in the Official Journal), Council Common Position of (not yet published in the Official Journal) and Decision of the European Parliament of (not yet published in the Official Journal). HV/kjf DG I

9714/1/03 REV 1

EN

1


Whereas: (1) Directive 94/35/EC of the European Parliament and of the Council of 30 June 1994 on sweeteners for use in foodstuffs 1 lays down a list of sweeteners that may be used in the Community and their conditions of use. (2) Since 1996, two new sweeteners, sucralose and the salt of aspartame and acesulfame, have been found acceptable for use in food by the Scientific Committee on Food. (3) The opinion of the Scientific Committee on Food on cyclamic acid and its sodium and calcium salts (which led to the establishment of a new Acceptable Daily Intake (ADI)) and recent studies on the intake of cyclamates lead to a reduction of the maximum usable doses of cyclamic acid and its sodium and calcium salts. (4) The designation of certain food categories in Directive 94/35/EC should be adapted to take into account Directive 2002/46/EC of the European Parliament and of the Council of 10 June 2002 on the approximation of the laws of the Member States relating to food supplements 2 and of specific Directives adopted for some groups of foodstuffs listed in Annex I of Council Directive 89/398/EEC 3. (5) The use of food additives concerned complies with the general criteria laid down in Annex II to Directive 89/107/EEC.

1 2 3

OJ L 237, 10.9.1994, p. 3. Directive as amended by Directive 96/83/EC (OJ L 48, 19.2.1997, p. 16). OJ L 183, 12.7.2002, p. 51. OJ L 186, 30.6.1989, p. 27. Directive as last amended by Directive 1999/41/EC of the European Parliament and of the Council (OJ L 172, 8.7.1999, p. 38). HV/kjf DG I

9714/1/03 REV 1

EN

2


(6)

Articles 53 and 54 of Regulation (EC) No 178/2002 of the European Parliament and of the Council of 28 January 2002 laying down the general principles and requirements of food law, establishing the European Food Safety Authority and laying down procedures in matters of food safety 1 establish procedures for taking emergency measures in relation to food of Community origin or imported from a third country. They allow the Commission to adopt such measures in situations where food is likely to constitute a serious risk to human health, animal health or the environment and where such risk cannot be contained satisfactorily by measures taken by the Member State(s) concerned.

(7)

The measures necessary for implementation of Directive 94/35/EC should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission 2.

(8)

Directive 94/35/EC should therefore be amended accordingly,

HAVE ADOPTED THIS DIRECTIVE:

1 2

OJ L 31, 1.2.2002, p. 1. OJ L 184, 17.7.1999, p. 23. HV/kjf DG I

9714/1/03 REV 1

EN

3


Article 1 Directive 94/35/EC is hereby amended as follows: 1) Article 4 shall be replaced by the following: "Article 4 It may be decided in accordance with the procedure laid down in Article 7: � where there are differences of opinion as to whether sweeteners can be used in a given foodstuff under the terms of this Directive, whether that foodstuff is to be considered as belonging to one of the categories listed in the third column of the Annex, � whether a food additive listed in the Annex and authorised at "quantum satis" is used in accordance with the criteria referred to in Article 2 and � 2) whether a substance is a sweetener within the meaning of Article 1(2)."

A third indent shall be added to Article 5(2): "� salt of aspartame and acesulfame: "contains a source of phenylalanine""

9714/1/03 REV 1 DG I

HV/kjf

EN

4


3)

Article 7 shall be replaced by the following: "Article 7 1. The Commission shall be assisted by the Standing Committee on the Food Chain and Animal Health set up pursuant to Article 58 of Regulation (EC) No 178/2002 *, hereinafter referred to as "the Committee". 2. Where reference is made to this Article, Articles 5 and 7 of Decision 1999/468/EC ** shall apply, having regard of the provisions of Article 8 thereof. The period laid down in Article 5(6) of Decision 1999/468/EC shall be set at three months. 3. The Committee shall adopt its rules of procedure.

* **

OJ L 31, 1.2.2002, p. 1. Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission (OJ L 184, 17.7.1999, p. 23).".

4) The Annex shall be amended in accordance with the Annex to this Directive.

9714/1/03 REV 1 DG I

HV/kjf

EN

5


Article 2 Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive in order to: � authorise trade in and use of products conforming with this Directive by at the latest, � prohibit trade in and use of products not conforming with this Directive by comply with this Directive may be marketed until stocks are exhausted. They shall forthwith inform the Commission thereof. When Member States adopt these measures, they shall contain a reference to this Directive or shall be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made. Article 3 This Directive shall enter into force on the day of its publication in the Official Journal of the European Union.
** *

at the latest; however, products placed on the market or labelled before that date which do not

* **

12 months after the entry into force of this Directive. 18 months after the entry into force of this Directive. HV/kjf DG I

9714/1/03 REV 1

EN

6


Article 4 This Directive is addressed to the Member States. Done at Brussels,

For the European Parliament The President

For the Council The President

_______________

9714/1/03 REV 1 DG I

HV/kjf

EN

7


ANNEX

The Annex to Directive 94/35/EC is hereby amended as follows: (1) In the third column of the tables the following categories of foodstuffs shall be renamed:

(a)

Instead of "Complete formulae for weight control intended to replace total daily food intake or an individual meal" to read "Foods intended for use in energy-restricted diets for weight reduction as referred to in Directive 96/8/EC *";

(b)

Instead of "Complete formulae and nutritional supplements for use under medical supervision" to read "Dietary foods for special medical purposes as defined in Directive 1999/21/EC **";

(c)

Instead of "Liquid food supplements/dietary integrators" to read "Food supplements as defined in Directive 2002/46/EC *** supplied in a liquid form";

(d)

Instead of "Solid food supplements/dietary integrators" to read "Food supplements as defined in Directive 2002/46/EC supplied in a solid form";

(e)

Instead of "Food supplements/dietary integrators based on vitamins and/or mineral elements, syrup-type or chewable" to read "Food supplements as defined in Directive 2002/46/EC, based on vitamins and/or mineral elements and supplied in a syrup-type or chewable form";

9714/1/03 REV 1 ANNEX

HV/kjf DG I

EN

1


(2) "

The following footnotes shall be added after the tables:

*

Commission Directive 96/8/EC of 26 February 1996 on foods intended for use in energy-restricted diets for weight reduction (OJ L 55, 6.3.1996, p. 22). ** Commission Directive 1999/21/EC of 25 March 1999 on dietary foods for special medical purposes (OJ L 91, 7.4.1999, p. 29). *** Directive 2002/46/EC of the European Parliament and of the Council of 10 June 2002 on the approximation of the laws of the Member States relating to food supplements (OJ L 183, 12.7.2002, p. 51)."; (3) For E 951 "Aspartame" the following category shall be added under "Confectionery":
"� Essoblaten 1000 mg/kg"

(4)

For E 952 cyclamic acid and its sodium and calcium salts: (a) for the following categories of foodstuffs, the maximum usable dose of "400 mg/l" shall be replaced by "250 mg/l": � � Water-based flavoured drinks, energy-reduced or with no added sugar; Milk- and milk derivative-based or fruit-juice-based drinks, energy-reduced or with no added sugar. (b) the following categories of foodstuffs and maximum usable doses shall be deleted: "
� Confectionery with no added sugar � Cocoa- or dried-fruit-based confectionery, energy-reduced or with no added sugar � Starch-based confectionery, energy-reduced or with no added sugar � Chewing gum with no added sugar � Breath-freshening micro-sweets, with no added sugar � Edible ices, energy-reduced or with no added sugar 1500 mg/kg 2500 mg/kg 250 mg/kg 500 mg/kg 500 mg/kg 500 mg/kg

";

9714/1/03 REV 1 ANNEX

HV/kjf DG I

EN

2


(5) "

The following tables shall be added:
Name Sucralose Foodstuffs Non-alcoholic drinks � Water-based flavoured drinks, energy-reduced or with no added sugar � Milk- and milk-derivative-based or fruit-juice-based drinks, energy-reduced or with no added sugar Desserts and similar products � Water-based flavoured desserts, energy-reduced or with no added sugar � Milk- and milk-derivative-based preparations, energy-reduced or with no added sugar � Fruit- and vegetable-based desserts, energy-reduced or with no added sugar � Egg-based desserts, energy-reduced or with no added sugar � Cereal-based desserts, energy-reduced or with no added sugar � Fat-based desserts, energy-reduced or with no added sugar � "Snacks": certain flavours of ready to eat, pre-packed, dry, savoury starch products and coated nuts Confectionery � Confectionery with no added sugar � Cocoa- or dried-fruit-based confectionery, energy-reduced or with no added sugar � Starch-based confectionery, energy-reduced or with no added sugar � Cornets and wafers, for ice cream, with no added sugar � Essoblaten � Cocoa-, milk-, dried-fruit- or fat-based sandwich spreads, energy-reduced or with no added sugar � Breakfast cereals with a fibre content of more than 15%, and containing at least 20% bran, energy-reduced or with no added sugar � Breath-freshening micro-sweets with no added sugar � Strongly flavoured freshening throat pastilles with no added sugar � Chewing gum with no added sugar � Energy-reduced tablet form confectionery � Cider and Perry � Drinks consisting of a mixture of a non-alcoholic drink and beer, cider, perry, spirits or wine � Spirit drinks containing less than 15% alcohol by volume � Alcohol-free beer or with an alcohol content not exceeding 1,2% vol � "Bi�re de table/Tafelbier/Table beer" (original wort content less than 6%) except for "Oberg�riges Einfachbier" � Beers with a minimum acidity of 30 milli-equivalents expressed as NaOH � Brown beers of the "oud bruin" type � Energy-reduced beer � Edible ices, energy-reduced or with no added sugar � Canned or bottled fruit, energy-reduced or with no added sugar � Energy-reduced jams, jellies and marmalades � Energy-reduced fruit and vegetable preparations � Sweet-sour preserves of fruit and vegetables � Feinkostsalat � Sweet-sour preserves and semi-preserves of fish and Maximum usable dose 300 mg/l 300 mg/l 400 mg/kg 400 mg/kg 400 mg/kg 400 mg/kg 400 mg/kg 400 mg/kg 200 mg/kg 1000 mg/kg 800 mg/kg 1000 mg/kg 800 mg/kg 800 mg/kg 400 mg/kg 400 mg/kg 2400 mg/kg 1000 mg/kg 3000 mg/kg 200 mg/kg 50 mg/l 250 mg/l 250 mg/l 250 mg/l 250 mg/l 250 mg/l 250 mg/l 10 mg/l 320 mg/kg 400 mg/kg 400 mg/kg 400 mg/kg 180 mg/kg 140 mg/kg 120 mg/kg

EC No E 955

9714/1/03 REV 1 ANNEX

HV/kjf DG I

EN

3


� � � � � � � � �

marinades of fish, crustaceans and molluscs Energy-reduced soups Sauces Mustard Fine bakery products for special nutritional uses Foods intended for use in energy-restricted diets for weight reduction as referred to in Directive 1996/8/EC Dietary foods for special medical purposes as defined in Directive 1999/21/EC Food supplements as defined in Directive 2002/46/EC supplied in a liquid form Food supplements as defined in Directive 2002/46/EC supplied in a solid form Food supplements as defined in Directive 2002/46/EC, based on vitamins and/or mineral elements and supplied in a syrup-type or chewable form

45 mg/l 450 mg/kg 140 mg/kg 700 mg/kg 320 mg/kg 400 mg/kg 240 mg/l 800 mg/kg 2400 mg/kg

9714/1/03 REV 1 ANNEX

HV/kjf DG I

EN

4


EC No E 962

Name Salt of aspartameacesulfame

Foodstuffs

Maximum usable dose*

Non-alcoholic drinks � Water-based flavoured drinks, energy-reduced or with no added sugar � Milk- and milk-derivative-based or fruit-juice-based drinks, energy-reduced or with no added sugar Desserts and similar products � Water-based flavoured desserts, energy-reduced or with no added sugar � Milk- and milk-derivative-based preparations, energyreduced or with no added sugar � Fruit- and vegetable-based desserts, energy-reduced or with no added sugar � Egg-based desserts, energy-reduced or with no added sugar � Cereal-based desserts, energy-reduced or with no added sugar � Fat-based desserts, energy-reduced or with no added sugar � "Snacks": certain flavours of ready to eat, prepacked, dry, savoury starch products and coated nuts Confectionery � Confectionery with no added sugar � Cocoa- or dried-fruit-based confectionery, energy-reduced or with no added sugar � Starch-based confectionery, energy-reduced or with no added sugar � Essoblaten � Cocoa-, milk-, dried-fruit or fat-based sandwich spreads, energy-reduced or with no added sugar � Breakfast cereals with a fibre content of more than 15%, and containing at least 20% bran, energy-reduced or with no added sugar � Breath-freshening micro-sweets with no added sugar � Chewing gum with no added sugar � Cider and perry � Drinks consisting of a mixture of a non-alcoholic drink and beer, cider, perry, spirits or wine � Spirit drinks containing less than 15% alcohol by volume � Alcohol-free beer or with an alcohol content not exceeding 1,2% vol � "Bi�re de table/Tafelbier/Table beer"' (original wort content less than 6%) except for "Oberg�riges Einfachbier" � Beers with a minimum acidity of 30 milli-equivalents expressed as NaOH � Brown beers of the 'oud bruin' type � Energy-reduced beer � Edible ices, energy-reduced or with no added sugar � Canned or bottled fruit, energy-reduced or with no added sugar
*

350 mg/l (a) 350 mg/l (a) 350 mg/kg (a) 350 mg/kg (a) 350 mg/kg (a) 350 mg/kg (a) 350 mg/kg (a) 350 mg/kg (a) 500 mg/kg (b) 500 mg/kg (a) 500 mg/kg (a) 1000 mg/kg (a) 1000 mg/kg (b) 1000 mg/kg (b) 1000 mg/kg (b) 2500 mg/kg (a) 2000 mg/kg (a) 350 mg/l (a) 350 mg/l (a) 350 mg/l (a) 350 mg/l (a) 350 mg/l (a) 350 mg/l (a) 350 mg/l (a) 25 mg/l (b) 800 mg/kg (b) 350 mg/kg (a)

Maximum usable doses for the salt of aspartame-acesulfame are derived from the maximum usable doses for its constituent parts, aspartame (E951) and acesulfame-K (E950). The maximum usable doses for both aspartame (E951) and acesulfame-K (E950) are not to be exceeded by use of the salt of aspartame-acesulfame, either alone or in combination with E950 or E951. Limits in this column are expressed either as (a) acesulfame-K equivalents or (b) aspartame equivalents. HV/kjf DG I

9714/1/03 REV 1 ANNEX

EN

5


� � � � � � � � � � � � � �

Energy-reduced jams, jellies and marmalades Energy-reduced fruit and vegetable preparations Sweet-sour preserves of fruit and vegetables Feinkostsalat Sweet-sour preserves and semi-preserves of fish and marinades of fish, crustaceans and molluscs Energy-reduced soups Sauces Mustard Fine bakery products for special nutritional uses Foods intended for use in energy-restricted diets for weight reduction as referred to in Directive 1996/8/EC Dietary foods for special medical purposes as defined in Directive 1999/21/EC Food supplements as defined in Directive 2002/46/EC supplied in a liquid form Food supplements as defined in Directive 2002/46/EC supplied in a solid form Food supplements as defined in Directive 2002/46/EC, based on vitamins and/or mineral elements and supplied in a syrup-type or chewable form

1000 mg/kg (b) 350 mg/kg (a) 200 mg/kg (a) 350 mg/kg (b) 200 mg/kg (a) 110 mg/l (b) 350 mg/kg (b) 350 mg/kg (b) 1000 mg/kg (a) 450 mg/kg (a) 450 mg/kg (a) 350 mg/l (a) 500 mg/kg (a) 2000 mg/kg (a)

"

9714/1/03 REV 1 ANNEX

HV/kjf DG I

EN

6


COUNCIL OF THE EUROPEAN UNION

Brussels, 25 June 2003

9714/1/03 REV 1 ADD 1 Interinstitutional File: 2002/0152(COD) DENLEG 26 CODEC 709 OC 307 STATEMENT OF THE COUNCIL'S REASONS Subject : Common Position adopted by the Council on 25 June 2003 with a view to the adoption of a Directive of the European Parliament and of the Council amending Directive 94/35/EC on sweeteners for use in foodstuffs

STATEMENT OF THE COUNCIL'S REASONS

9714/1/03 REV 1 ADD 1 DG I

LL/am

EN

1


I.

INTRODUCTION 1. On 11 July 2002 the Commission submitted to the Council a proposal for a Directive of the European Parliament and of the Council, based on Article 95 of the Treaty, amending Directive 94/35/EC on sweeteners for use in foodstuffs 1. 2. The Economic and Social Committee and the European Parliament gave their opinion on this proposal respectively on 11 December 20022 and 10 April 2003 3. 3. Following the European Parliament opinion, the Commission forwarded an amended proposal 4 to the Council on 19 May 2003. 4. On 25 June 2003, the Council adopted its Common Position in accordance with Article 251 of the Treaty.

II.

OBJECTIVE In the White Paper on Food Safety, the Commission had announced the intention to propose an amendment to the Directive 94/35/EC, with a view to update and revise the list of sweeteners for use in foodstuffs. Therefore, in the light of the latest technical and scientifical developments, the proposal mainly aims at authorising two new sweeteners (sucralose and salt of aspartame and acesulfame) and monitoring authorised sweeteners (reduction of the intake of cyclamates).

1 2 3 4

OJ C 262 E of 29.10.2002, p. 429. OJ C 85 of 08.04.2003, p. 34. Doc. 8306/03. Doc. 9753/03. LL/am DG I

9714/1/03 REV 1 ADD 1

EN

2


III. ANALYSIS OF THE COMMON POSITION A. General remarks related to the European Parliament's amendments 1. The Council has accepted � either word by word or in substance � the two European Parliament's amendments (i.e. amendments 1 and 6) as set out in the Commission's amended proposal. It has however changed the deadline for the prohibition of use of products not conforming with the Directive to 18 months after the entry into force; 2. Furthermore, the Council has followed the Commission's amended proposal in reducing the authorised levels for cyclamates for water-based drinks as well as drinks based on milk or fruit-juice as proposed by the European Parliament, to 250 mg/l. While this level is greater than the 100 mg/l proposed by the European Parliament, the Commission has undertaken in a minutes statement to keep under review the maximum usable doses of these substances, taking account inter-alia of information on intakes from Member States; 3. As regards the other amendments, the Council, like the Commission, considers that some of the questions addressed by these amendments could be raised in the context of the future updating of the legislation.5

5

See draft minutes statement by the Commission: "The Commission undertakes to examine within a period of four years the sucralose consumption study results provided by the Member States in accordance with the procedures followed for the report on food additive consumption submitted in October 2001." LL/am DG I

9714/1/03 REV 1 ADD 1

EN

3


B.

Main innovations introduced by the Council 1. The main innovations introduced by the Council with regard to the Commission's amended proposal relate to: clarification of the wording of Article 4 of Directive 94/35/EC, notably as regards the use of a food additive listed in the Annex and authorised at 'quantum satis' (Article 1(1)), labelling obligation as regards salt of aspartame and acesulfame (Article 1(2)), simplification of the layout of the table in the Annex related to the maximum usable dose for salt of aspartame-acesulfame, in order to indicate the limits either as acesulfame-K equivalents or as aspartame equivalents. 2. Other modifications have been made, of a purely technical nature and aiming at clarifying the text of the Directive (notably concerning the renaming of certain categories and foodstuffs in the Annex, following the adoption of a certain number of Directives since 1994).

IV.

CONCLUSIONS The Council considers that the Common Position ensures further harmonisation while taking into account the main concerns expressed by the European Parliament, notably with a view to protecting public health and providing better consumer information.

9714/1/03 REV 1 ADD 1 DG I

LL/am

EN

4


COUNCIL OF THE EUROPEAN UNION

Brussels, 20 June 2003

10422/03 Interinstitutional File: 2002/0152 (COD) DENLEG 31 CODEC 825

I/A ITEM NOTE from : General Secretariat to : Coreper/Council No. prev. doc. : 9237/03 DENLEG 22 CODEC 624 No. Cion prop. : 9753/03 DENLEG 27 CODEC 716 Subject : Adoption by the Council of a common position with a view to the adoption of a Directive of the European Parliament and of the Council amending Directive 94/35/EC on sweeteners for use in foodstuffs

1.

At its meeting on 19 May 2003, the Council reached political agreement with a view to the adoption of a common position on the above Directive.

2.

As the text agreed, including its preamble, has undergone legal/linguistic finalisation, the Permanent Representatives Committee could request the Council: � adopt, within the meaning of the third indent of Article 251(1) of the Treaty, a common position on the above Directive as set out in 9714/03 DENLEG 26 CODEC 709 OC 307 + COR 1(de) + COR 2 (fr), and to approve the draft statement of the Council's reasons as set out in 9714/03 DENLEG 26 CODEC 709 OC 307 ADD 1; � authorise entry in the Council minutes of the statement set out in the Addendum to this note.

10422/03 DG I

LL/am

EN

1


COUNCIL OF THE EUROPEAN UNION

Brussels, 25 June 2003

9714/1/03 REV 1 ADD 1 Interinstitutional File: 2002/0152(COD) DENLEG 26 CODEC 709 OC 307 STATEMENT OF THE COUNCIL'S REASONS Subject : Common Position adopted by the Council on 25 June 2003 with a view to the adoption of a Directive of the European Parliament and of the Council amending Directive 94/35/EC on sweeteners for use in foodstuffs

STATEMENT OF THE COUNCIL'S REASONS

9714/1/03 REV 1 ADD 1 DG I

LL/am

EN

1


I.

INTRODUCTION 1. On 11 July 2002 the Commission submitted to the Council a proposal for a Directive of the European Parliament and of the Council, based on Article 95 of the Treaty, amending Directive 94/35/EC on sweeteners for use in foodstuffs 1. 2. The Economic and Social Committee and the European Parliament gave their opinion on this proposal respectively on 11 December 20022 and 10 April 2003 3. 3. Following the European Parliament opinion, the Commission forwarded an amended proposal 4 to the Council on 19 May 2003. 4. On 25 June 2003, the Council adopted its Common Position in accordance with Article 251 of the Treaty.

II.

OBJECTIVE In the White Paper on Food Safety, the Commission had announced the intention to propose an amendment to the Directive 94/35/EC, with a view to update and revise the list of sweeteners for use in foodstuffs. Therefore, in the light of the latest technical and scientifical developments, the proposal mainly aims at authorising two new sweeteners (sucralose and salt of aspartame and acesulfame) and monitoring authorised sweeteners (reduction of the intake of cyclamates).

1 2 3 4

OJ C 262 E of 29.10.2002, p. 429. OJ C 85 of 08.04.2003, p. 34. Doc. 8306/03. Doc. 9753/03. LL/am DG I

9714/1/03 REV 1 ADD 1

EN

2


III. ANALYSIS OF THE COMMON POSITION A. General remarks related to the European Parliament's amendments 1. The Council has accepted � either word by word or in substance � the two European Parliament's amendments (i.e. amendments 1 and 6) as set out in the Commission's amended proposal. It has however changed the deadline for the prohibition of use of products not conforming with the Directive to 18 months after the entry into force; 2. Furthermore, the Council has followed the Commission's amended proposal in reducing the authorised levels for cyclamates for water-based drinks as well as drinks based on milk or fruit-juice as proposed by the European Parliament, to 250 mg/l. While this level is greater than the 100 mg/l proposed by the European Parliament, the Commission has undertaken in a minutes statement to keep under review the maximum usable doses of these substances, taking account inter-alia of information on intakes from Member States; 3. As regards the other amendments, the Council, like the Commission, considers that some of the questions addressed by these amendments could be raised in the context of the future updating of the legislation.5

5

See draft minutes statement by the Commission: "The Commission undertakes to examine within a period of four years the sucralose consumption study results provided by the Member States in accordance with the procedures followed for the report on food additive consumption submitted in October 2001." LL/am DG I

9714/1/03 REV 1 ADD 1

EN

3


B.

Main innovations introduced by the Council 1. The main innovations introduced by the Council with regard to the Commission's amended proposal relate to: clarification of the wording of Article 4 of Directive 94/35/EC, notably as regards the use of a food additive listed in the Annex and authorised at 'quantum satis' (Article 1(1)), labelling obligation as regards salt of aspartame and acesulfame (Article 1(2)), simplification of the layout of the table in the Annex related to the maximum usable dose for salt of aspartame-acesulfame, in order to indicate the limits either as acesulfame-K equivalents or as aspartame equivalents. 2. Other modifications have been made, of a purely technical nature and aiming at clarifying the text of the Directive (notably concerning the renaming of certain categories and foodstuffs in the Annex, following the adoption of a certain number of Directives since 1994).

IV.

CONCLUSIONS The Council considers that the Common Position ensures further harmonisation while taking into account the main concerns expressed by the European Parliament, notably with a view to protecting public health and providing better consumer information.

9714/1/03 REV 1 ADD 1 DG I

LL/am

EN

4



COUNCIL OF THE EUROPEAN UNION

Brussels, 23 June 2003

Interinstitutional File: 2002/0152 (COD)

10422/03 COR 1(en)

DENLEG 31 CODEC 825

CORRIGENDUM TO THE I/A ITEM NOTE from : General Secretariat to : Coreper/Council No. prev. doc. : 9237/03 DENLEG 22 CODEC 624 No. Cion prop. : 9753/03 DENLEG 27 CODEC 716 Subject : Adoption by the Council of a common position with a view to the adoption of a Directive of the European Parliament and of the Council amending Directive 94/35/EC on sweeteners for use in foodstuffs

Point 2, second indent should read as follows: "authorise entry in the Council minutes of the statements set out in ADD 1 REV 1 to this note."

10422/03 COR 1(en) DG I

LL/am

EN

1



COUNCIL OF THE EUROPEAN UNION

Brussels, 23 June 2003

Interinstitutional File: 2002/0152 (COD)

10422/03 COR 2 (en,sv)

DENLEG 31 CODEC 825

CORRIGENDUM TO THE I/A ITEM NOTE from : General Secretariat to : Coreper/Council No. prev. doc. : 9237/03 DENLEG 22 CODEC 624 No. Cion prop. : 9753/03 DENLEG 27 CODEC 716 Subject : Adoption by the Council of a common position with a view to the adoption of a Directive of the European Parliament and of the Council amending Directive 94/35/EC on sweeteners for use in foodstuffs

Point 2, first indent should read as follows: "adopt, within the meaning of the third indent of Article 251(2) of the Treaty, ......."

10422/03 COR 2 (en,sv) DG I

LL/am

EN

1



COMMISSION OF THE EUROPEAN COMMUNITIES

Brussels, 2.7.2003 SEC(2003) 783 final 2002/0152 (COD)

COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT pursuant to the second subparagraph of Article 251 (2) of the EC Treaty concerning the common position of the Council on the adoption of a Directive of the European Parliament and of the Council amending Directive 94/35/EC on sweeteners for use in foodstuffs


2002/0152 (COD) COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT pursuant to the second subparagraph of Article 251 (2) of the EC Treaty concerning the common position of the Council on the adoption of a Directive of the European Parliament and of the Council amending Directive 94/35/EC on sweeteners for use in foodstuffs

1.

BACKGROUND 11 July 2002 11 December 2002 10 April 2003 16 May 2003 19 May 2003 25 June 2003

Date of transmission of the proposal to the EP and the Council (document COM(2002)375 final � 2002/0152 (COD)): Date of the opinion of the European Economic and Social Committee: Date of the opinion of the European Parliament, first reading: Date of transmission of the amended proposal: Date of political agreement (unanimity): Date of adoption of the common position: 2. OBJECTIVE OF THE COMMISSION PROPOSAL

The Commission proposal aims at adapting Directive 94/35/EC on sweeteners for use in foodstuffs to recent technical and scientific developments. To this end it foresees: � To authorise two new sweeteners, sucralose and the salt of aspartame and acesulfame, after favourable opinions by the Scientific Committee on Food (SCF). To reduce the intake of an already authorised sweetener, cyclamate, following an opinion of the SCF lowering the ADI (Acceptable Daily Intake) for this substance. To confer on the Commission the power to decide whether a substance should be considered as a sweetener according to the Directive. To clarify the meaning of the food category `fine bakery wares for special nutritional uses' that is not interpreted in a homogeneous manner by Member States.

�

� �

2


3. 3.1

COMMENTS ON THE COMMON POSITION General remarks The common position is based on the amended Commission proposal that took several amendments proposed by the European Parliament into account. It has been adopted by the Council by unanimity. The Commission accepted 2 amendments adopted by the European Parliament at its first reading. Of these 2 amendments, the common position takes one into account. The common position does not include European Parliament amendments that were rejected by the Commission. Additional changes have been included in the common position as a result of discussions in the Council. Furthermore, as explained below, the Commission and the Council have moved towards the European Parliament concerning the maximum permitted level for cyclamates in soft drinks and milk and juice based drinks.

3.2

Amendments of the European Parliament in first reading The Commission accepted two amendments of the European Parliament without changes: One concerning the recital on cyclamates and the other concerning the delays for the transposition of the Directive. The common position takes over the amendment concerning the recital on cyclamates. The Parliament adopted a further amendment lowering the maximum permitted dose for cyclamate for soft drinks to 100 mg/l and extending this reduction to milk and juice based drinks. The Commission could not accept the value voted by the Parliament. However, the Commission proposed in its amended proposal a further reduction and extended this reduction to milk and juice based drinks (250 mg/l). The common position follows the Commission amended proposal in this matter.

3.3

Additional changes included in the common position Comitology The Commission proposal foresees to confer on the Commission the power to decide whether a substance should be considered as a sweetener according to the Directive. The common position proposes to confer on the Commission additionally the power to decide if a sweetener is correctly used according to the quantum satis principle (i.e. in accordance with good manufacturing practice). The Commission can support this change. Labelling of the salt of aspartame and acesulfame The sweetener Directive stipulates that the labelling of a table-top sweetener containing aspartame must bear the warning `contains a source of phenylalanine'. The common position proposes to make explicit that this warning must also appear on the labelling of a table-top sweetener containing the salt of aspartame and acesulfame. Although the Commission considers that the provisions in the Directive already cater for this, the Commission can accept this clarification.

3


Transposition period The common position deviates from the Commission amended proposal in foreseeing a delay of 18 months instead of 12 months for the prohibition of trade in and use of products not conforming to this Directive. The Commission can accept this. Adaptation of the designation of certain food categories The designations of certain food categories in Directive 94/35/EC have been adapted in the common position to take account of Directive 2002/46/EC relating to food supplements and of specific Directives adopted for some groups of foodstuffs listed in Annex I of Council Directive 89/398/EC on foods for special nutritional purposes. The Commission agrees with these adaptations. Concerning the renaming of the food category `fine bakery wares for special nutritional uses' the Council could not agree on the Commission proposal nor on an alternative wording. Therefore, the common position does not take up the Commission proposal to clarify the wording of this food category. The Commission can accept this. 4. CONCLUSION In the light of the above comments, the Commission agrees with the common position by the Council with a view of the adoption of a Directive of the European Parliament and of the Council amending Directive 94/35/EC on sweeteners for use in foodstuffs. 5. COMMISSION DECLARATIONS The Commission's declarations to the minutes of the Council are attached in Annex to this communication.

4


ANNEX COMMISSION DECLARATIONS Ad Annex point 4(a) "The Commission undertakes to keep under review the maximum usable doses of E 952 cyclamic acid and its sodium and calcium salts taking account inter-alia of information on intakes from Member States." Ad Annex point 5 "The Commission undertakes to examine within a period of four years the sucralose consumption study results provided by the Member States in accordance with the procedures followed for the report on food additive consumption submitted in October 2001."

5


