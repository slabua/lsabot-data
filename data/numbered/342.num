B5-0327, 0353, 0354 and 0357/1999 - 16.12.1999 European Parliament resolution on the Helsinki European Council The European Parliament, having regard to the conclusions of the Luxembourg, Berlin and Cologne European Councils and having heard the statements by the President of the Council and the President of the Commission on the conclusions of the Helsinki European Council, having regard to its resolution of 19 November 1997 on the Treaty of Amsterdam1, having regard to its resolution of 18 November 1999 on the preparation of the reform of the Treaties and the next Intergovernmental Conference2, having regard to its resolution of 2 December 1999 on the preparation of the meeting of the European Council in Helsinki on 10 and 11 December 19993, having regard to its resolution of 16 September 1999 on the establishment of the Charter of Fundamental Rights4, having regard to the conclusions of the Helsinki European Council,

-

On enlargement 1. Welcomes the Council's decision to begin negotiations on accession with Romania, Slovakia, Latvia, Lithuania, Bulgaria and Malta, thus creating a fully flexible, multispeed accession process, in which these countries are given the possibility to catch up within a reasonable period of time with those candidate countries already in negotiations, if they have made sufficient progress in their preparations; Urges the Commission and the Council to pay particular attention in the implementation of the pre-accession strategies and during the negotiations to strengthening the rule of law, social progress, protection of the environment, energy safety and security, protection of minorities, gender discrimination, and asylum and migration policy in the applicant countries; Welcomes the Council's decision that the peaceful settlement of all border conflicts, if necessary through bringing outstanding disputes before the International Court of Justice in The Hague, is considered as part of fulfilling the Copenhagen criteria for accession to the Union; Takes note of the decision to consider Turkey a candidate country for membership of the European Union; reiterates its position, however, that negotiations cannot be opened because Turkey is a long way from meeting the political criteria of Copenhagen;
OJ C 371, 8.12.1997, p. 99. Texts adopted at that sitting, point 4. Texts adopted at that sitting, point 13. Minutes of that sitting, Part II, Item 10(a).

2.

3.

4.

1 2 3 4


5.

Expects that the granting of candidate status to Turkey will bring about in this country the necessary reforms in the fields of democracy, rule of law and the respect for human and minority rights with regard in particular to the solution of the Kurdish question; calls on the Ankara parliament to abolish the death penalty immediately; Welcomes the fact that the European Council recalled the importance of high standards of nuclear safety in Central, Eastern and South-Eastern Europe and explicitly shares its view that the Council should consider how to address the issue of nuclear safety in the framework of the enlargement process in accordance with the relevant Council conclusions; Regrets that no concrete decisions have been taken with regard to the future of the European Conference;

6.

7.

Intergovernmental Conference 8. Deplores the lack of political vision displayed by the European Council, which has restricted institutional reform at the next ICG to the size and composition of the Commission, the weighting of votes in the Council and the possible extension of qualified majority voting in the Council; Considers that this reform is inadequate to ensure that an enlarged Europe can function effectively; Calls for its position on the IGC agenda to be taken fully into account and asks the Portuguese Presidency to make use as soon as possible of the possibility of adding further items to the agenda in accordance with its proposals; Regrets that the European Council chose not to apply the Community method to the next IGC by fully involving Parliament and the Commission; expresses its concern at the institutional trend confirmed by the conclusions of the Helsinki European Council, which places the Council and the intergovernmental approach at the heart of the Union's institutional system; Notes that the European Council clearly supports the reform of the Commission's administration and recognises the need to make substantial changes to the Council's working methods, measures that are essential if these institutions are to become more effective and prepare themselves for enlargement; is concerned about the obvious lack of ambition of the Commission in its forthcoming proposal for a Regulation on public access to documents; urges yet greater openness on behalf of both the Commission and the Council, including publication of the records of meetings when the Council acts in its legislative capacity; calls on the Council to participate more fully in dialogue with the Parliament on all political matters; Deplores the fact that Parliament's two representatives are not being fully associated with all stages and at all levels of the IGC, as it had requested; Calls for the final text produced by the IGC to be submitted to the European Parliament under the same procedure as is used for obtaining its assent;

9. 10.

11.

12.

13. 14.


15.

Reserves the right to give its opinion on the convening of the IGC pursuant to Article 48 of the EU Treaty;

A Common European Policy on Defence and Security 16. Takes note of the Council's decisions regarding a Common European Policy on Security and Defence; urges the Council to involve the European Parliament fully in the further development of the instruments and mechanisms of this Common Policy; Urges the Council in this framework to strengthen further its capacity for active conflict prevention and non-military crisis management; Urges the Commission to take steps to improve its capability for more timely and effective implementation of civil programmes relevant to conflict prevention and crisis management and to report to Parliament within six months on the measures it intends to take; Proposes that further civilian (non-military) forms of crisis management be developed as a key means of managing and resolving crises and therefore invites the Council and the Commission to play a more proactive role in conflict prevention, strengthening democracy, respect for human rights and progress in economic and social development through encouragement of good governance in third countries in accordance with the full range of European values;

17. 18.

19.

A competitive, job-generating, sustainable economy 20. Welcomes the Council's recognition of the Commission's Internal Market Strategy, but regrets that this is not endorsed more strongly as the overarching strategy for the Union's economic and industrial policies; still believes that the link between economic and employment policies as well as policies for social cohesion must be seen as the three sides of an equilateral triangle forming a balanced policy mix and that they must respect each other; Equally, welcomes the recognition of e-commerce initiatives but believes that the importance of e-commerce in stimulating the internal market has been understated; Notes the Council's endorsement of the e-Europe initiative but regrets that the supporting documents for this initiative have not yet been sent to Members of the European Parliament; Notes that the European Council has come round to recognising the uncoordinated nature of the successive 'processes, 'dialogues' and 'guidelines', within the so-called European Employment Pact agreed at Cologne; therefore insists on the need to introduce more streamlined and coherent arrangements, in which the European Parliament must have a full role; Reminds the European Council that the way forward for a sustainable reduction in unemployment is also by encouraging entrepreneurship and business start-ups, and therefore welcomes the timely recognition which the European Council has given to the

21. 22.

23.

24.


role which electronic commerce can play in increasing the dynamism and openness of the European economy; 25. Also welcomes the attention given to competitiveness and asks to be fully involved alongside the Commission and Council in the debate which the European Council has launched on competitiveness policy and economic co-ordination; Expects that in Lisbon the heads of state and government will finally deal intensively with the topics of promotion of employment and social protection, as the Helsinki summit did not give any binding declarations; Notes that there will be a report on the e-Europe Action Plan in Lisbon; intends to provide an opinion ahead of this discussion; Deplores the poor management of the WTO Ministerial Conference in Seattle and the predominance of US domestic politics which led to its collapse; reaffirms its belief in the need for an open, rules-based multilateral trading system which respects social and environmental standards and the interests of the developing countries; requests the Commission to bring forward proposals for the revival of a comprehensive round of trade negotiations;

26.

27. 28.

On the tax package 29. Regrets the failure to reach agreement on the tax coordination package, believing it essential that no resident citizen of the EU should escape taxation of savings income; urges a speedy resolution of that problem;

Environment and sustainable development 30. Notes with satisfaction the clear commitment of the European Council to regular evaluation, follow-up and monitoring as well as the urgent development of adequate instruments and applicable data for these purposes; notes, however, that further progress will depend on concrete policy measures in the various policy fields concerned; Looks forward to the conclusions of the Internal Market, Development and Industry Councils as well as those of the General Affairs, ECOFIN and Fisheries Councils on their strategies for integrating the environmental dimension into their work and welcomes the European Council's determination to introduce a new phase in this process by calling for comprehensive strategies with the possibility of including a timetable for further measures and a set of indicators for all of these sectors to be presented to the European Council in June 2001;

31.

Other internal policies Public Health and Food Safety 32. Shares the European Council's opinion that a high level of human health protection in the definition of all Community policies must be ensured; welcomes in this context the call for particular attention to all efforts to ensure healthy and high-quality food for all


citizens by improving quality standards and enhancing monitoring systems covering the whole of the food chain; stresses that proper and adequate monitoring falls within the competence of Member States and is therefore an obligation and responsibility Member States have to take care of; looks forward to the Commission's forthcoming proposals on the possible establishment of a EU food and public health authority; Fight against organised crime and drugs 33. Notes that the European Council has endorsed the European Union Action Plan to Combat Drugs 2000-2004 and called on the institutions and bodies concerned by the strategy to report on the initial results by the end of 2002, but very much regrets that the recommendations made by Parliament in its resolution of 19 November 19991 on this matter have not been examined by the Council, in particular the principles of an extraordinary inter-pillar Council to address the fight against drugs; Is concerned that the request for a new EU strategy for preventing and combating organised crime displays a complete lack of urgency, with no firm instructions or deadlines given to the Council; also notes that the European Council was silent on the EU-Russia action plan on organised crime, and urges the Council to consult the Parliament before it is adopted by the EU;

34.

External relations 35. 36. Regrets the lack of progress towards resolving the dispute over Gibraltar; Welcomes the adoption of a Common Strategy of the European Union on Ukraine, but regrets that the European Parliament has not been involved in the drafting of this strategy and calls on the Council to take into account in its further steps the mutual interest of the EU and the Ukraine in strengthening the country's European aspirations and its ability to contribute to a just international order; Regrets that no decisions were taken by the European Council on further continuation and intensification of the Barcelona process for peace, stability and economic cooperation for development in the Mediterranean area; Welcomes the decisions taken by the European Council on a further continuation and intensification of the work on the Northern Dimension and its invitation to the Commission to prepare, in cooperation with the Council and in consultation with the partner countries, an Action Plan for the Northern Dimension in the external and crossborder policies of the EU; Welcomes the European Council's commitment to the full and speedy implementation of the Stability Pact for South-Eastern Europe; stresses, however, that this programme has to be financed in a proper way and based on proper assessment of the needs; Regrets the lack of evident progress in Kosovo; condemns the serious violation of the rights of the Serb, Roma and other minorities and supports the Council in its determination to fulfil the UN mandate; regrets the fact that no action has been

37.

38.

39.

40.

1

Texts adopted at that sitting, item 1.


undertaken with regard to the 2000 Albanian war prisoners still held in Serbia; 41. Welcomes the latest events concerning the peace process in the Middle East and invites the Council to exert the necessary pressure so that the opening of talks between Israel and Syria leads to a quick solution of the conflict, satisfactory for all parties involved in the Middle East peace process;

Northern Ireland 42. Subscribes to the European Council's observations on, and welcomes, the significant developments in Northern Ireland; o oo 43. Instructs its President to forward this resolution to the European Council, the Council, the Commission and the governments and parliaments of the Member States.


