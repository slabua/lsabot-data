EUROPEAN PARLIAMENT
1999
Session document
2

004

C5-0023/1999 19/07/1999

***II COMMON POSITION
Subject :
COMMON POSITION (EC) No /99 ADOPTED BY THE COUNCIL ON 28 JUNE 1999 WITH A VIEW TO THE ADOPTION OF DECISION No /99/EC OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL ESTABLISHING A SINGLE FINANCING AND PROGRAMMING INSTRUMENT FOR CULTURAL COOPERATION (CULTURE 2000 PROGRAMME) (COD 1998/0169)

EN

EN







EUROPEAN UNION THE COUNCIL Interinstitutional File No 98/0169 (COD)

Brussels, 16 July 1999 (OR.en)

13328/2/98 REV 2 LIMITE CULTURE 88 CODEC 646

COMMON POSITION (EC) No

/99

ADOPTED BY THE COUNCIL ON 28 JUNE 1999 WITH A VIEW TO THE ADOPTION OF DECISION No /99/EC OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL ESTABLISHING A SINGLE FINANCING AND PROGRAMMING INSTRUMENT FOR CULTURAL COOPERATION (CULTURE 2000 PROGRAMME)

13328/2/98 REV 2 DG J

CR/ct

EN


DECISION No of

/99/EC

OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL

establishing a single financing and programming instrument for cultural cooperation (Culture 2000 programme)

THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION, Having regard to the Treaty establishing the European Community, and in particular the first indent of Article 151(5) thereof, Having regard to the proposal from the Commission (1), Having regard to the Opinion of the Committee of the Regions (2), Acting in accordance with the procedure laid down in Article 251 of the Treaty (3),

(1) (2) (3)

OJ C 211, 7.7.1998, p. 18. OJ C European Parliament Opinion of 5 November 1998 (not yet published in the Official Journal), Council Common Position of (not yet published in the Official Journal), European Parliament Decision of (not yet published in the Official Journal). CR/ct EN 1

13328/2/98 REV 2 DG J


(1)

Whereas culture has an important intrinsic value to all people in Europe, is an essential element of European integration and contributes to the affirmation and vitality of the European model of society and to the Community's influence on the international scene;

(2)

Whereas culture is both an economic factor and a factor in social integration and citizenship; whereas, for that reason, it has an important role to play in meeting the new challenges facing the Community, such as globalisation, the information society, social cohesion and the creation of employment;

(3)

Whereas, in view of the growing importance of culture for European society and the challenges facing the Community at the dawn of the 21st century, it is important to increase the effectiveness and consistency of Community measures in the cultural field by proposing a single guidance and programming framework for the period 2000-2004, bearing in mind the need for the Community policies concerned to take greater account of culture; whereas, in this respect, the Council Decision of 22 September 1997 regarding the future of European cultural action (1) calls upon the Commission to make proposals with a view to establishing a single instrument for programming and financing aimed at the implementation of Article 151 of the Treaty;

(4)

Whereas, if citizens give their full support to, and participate fully in, European integration, greater emphasis should be placed on their common cultural values and roots as a key element of their identity and their membership of a society founded on freedom, democracy, tolerance and solidarity; whereas a better balance should be achieved between the economic and cultural aspects of the Community, so that these aspects can complement and sustain each other;

(1)

OJ C 305, 7.10.1997, p. 1. CR/ct EN 2

13328/2/98 REV 2 DG J


(5)

Whereas the Treaty confers responsibility on the European Union for creating an ever-closer union among the peoples of Europe and for contributing to the flowering of the cultures of the Member States, while respecting their national and regional diversity and at the same time bringing the common cultural heritage to the fore; whereas special attention should be devoted to safeguarding the position of Europe's small cultures and less widely-spoken languages;

(6)

Whereas the Community is consequently committed to working towards the development of a cultural area common to the European people, which is open, varied and founded on the principle of subsidiarity, cooperation between all those involved in the cultural sector, the promotion of a legislative framework conducive to cultural activities and ensuring respect for cultural diversity, and the integration of the cultural dimension into Community policies as provided for in Article 151(4) of the Treaty;

(7)

Whereas, to bring to life the cultural area common to the European people, it is essential to encourage creative activities, promote cultural heritage with a European dimension, encourage mutual awareness of the culture and history of the peoples of Europe and support cultural exchanges with a view to improving the dissemination of knowledge and stimulating cooperation and creative activities;

(8)

Whereas there is a need, in this context, to promote greater cooperation with those engaged in cultural activities by encouraging them to enter into cooperation agreements for the implementation of joint projects, to support more closely targeted measures having a high European profile, to provide support for specific and innovative measures and to encourage exchanges and dialogue on selected topics of European interest;

13328/2/98 REV 2 DG J

CR/ct

EN 3


(9)

Whereas the Kaleidoscope, Ariane and Raphael cultural programmes set out, respectively, in Decision No 719/96/EC (1), in Decision No 2085/97/EC (2) and in Decision No 2228/97/EC (3) marked the first positive stage in the implementation of Community action on culture; whereas, however, the Community's cultural endeavours should be simplified and reinforced on the basis of the results of the evaluation and achievements of the abovementioned programmes;

(10) Whereas, in accordance with the Commission's communication "Agenda 2000", the effectiveness of measures at Community level should be increased, notably by concentrating the resources available for internal policies � including cultural action; (11) Whereas considerable experience has been acquired, particularly through the evaluation of the first cultural programmes, the wide-ranging consultation of all interested parties and the results of the Cultural Forum of the European Union held on 29 and 30 January 1998; (12) Whereas the Community's cultural activities should take account of the specific nature, and hence the specific needs, of each cultural area;

(1) (2) (3)

OJ L 99, 20. 4.1996, p. 20. OJ L 291, 24.10.1997, p. 26. OJ L 305, 8.11.1997, p. 31. CR/ct EN 4

13328/2/98 REV 2 DG J


(13) Whereas the conclusions of the European Council at Copenhagen on 21 and 23 June 1993 called for the opening of Community programmes to the countries of Central and Eastern Europe which have signed association agreements; whereas the Community has signed, with some third countries, cooperation agreements which contain a cultural clause; (14) Whereas this Decision therefore establishes a single financing and programming instrument for cultural cooperation, entitled the "Culture 2000 programme", for the period from 1 January 2000 to 31 December 2004; (15) Whereas this Decision lays down, for the entire duration of the Culture 2000 programme, a financial framework constituting the principal point of reference, within the meaning of point 1 of the Declaration by the European Parliament, the Council and Commission of 6 March 1995 (1), for the budgetary authority during the annual budgetary procedure; (16) Whereas, in accordance with the subsidiarity and proportionality principles established by Article 5b of the Treaty, as the objectives of this action cannot be sufficiently achieved by the Member States, they can therefore, by reason of the scale or effects of the proposed action, be better attained by the Community; whereas this Decision is limited to the minimum required for the attainment of those objectives and does not go beyond what is necessary to that end; (17) Whereas the Culture 2000 programme should be the only programme operating from the year 2000 in the field of culture; whereas therefore Decision No 2228/97/EC should be repealed,

(1)

OJ C 102, 4.4.1996, p. 1. CR/ct EN 5

13328/2/98 REV 2 DG J


HAVE DECIDED AS FOLLOWS: Article 1 Duration and objectives A single financing and programming instrument for cultural cooperation, hereinafter referred to as the "Culture 2000 programme", is hereby established for the period from 1 January 2000 to 31 December 2004. The Culture 2000 programme shall contribute to the promotion of a cultural area common to the European peoples. In this context, it shall support cooperation between creative artists, cultural operators, private and public promoters, the activities of the cultural networks, and other partners as well as the cultural institutions of the Member States and of the other participant States in order to attain the following objectives: (a) promotion of cultural dialogue and of mutual knowledge of the culture and history of the European peoples; (b) promotion of creativity and the transnational dissemination of culture and the movement of artists, creators and other cultural operators and professionals and their works, with a strong emphasis on young and socially disadvantaged people and on cultural diversity; (c) the highlighting of cultural diversity and the development of new forms of cultural expression;

13328/2/98 REV 2 DG J

CR/ct

EN 6


(d)

sharing and highlighting, at the European level, the common cultural heritage of European significance; disseminating know-how and promoting good practices concerning its conservation and safeguarding;

(e) (f)

taking into account the role of culture in socio-economic development; the fostering of intercultural dialogue and mutual exchange between European and non-European cultures;

(g)

explicit recognition of culture as an economic factor and as a factor in social integration and citizenship.

The Culture 2000 programme shall further an effective linkage with measures implemented under other Community policies which have cultural implications. Article 2 Types of cultural actions and events The objectives listed in Article 1 shall be achieved by the following means: � specific innovative and/or experimental actions; � integrated actions covered by structured, multiannual cultural cooperation agreements; � special cultural events with a European and/or international dimension. The actions and their implementing measures are described in Annex I. They are either vertical (concerning one cultural field) or horizontal (associating several cultural fields).

13328/2/98 REV 2 DG J

CR/ct

EN 7


Article 3 Budget The financial framework for the implementation of the Culture 2000 programme for the period referred to in Article 1 is hereby set at EUR 167 million. The annual appropriations shall be authorised by the budgetary authority within the limits of the financial perspective. Article 4 Implementation 1. The Commission shall implement the Culture 2000 programme in accordance with this Decision. 2. The Commission shall be assisted by a Committee composed of two representatives per Member State and chaired by the representative of the Commission. Members of the Committee may be assisted by experts or advisers. 3. The representative of the Commission shall submit to the Committee draft measures concerning: � the priorities and general guidelines for all the measures described in Annex I and the annual programme resulting therefrom;

13328/2/98 REV 2 DG J

CR/ct

EN 8


� the general balance between all the Actions; � the selection rules and criteria for the various types of project described in Annex I (Actions I.1, I.2 and I.3); � the financial support to be provided by the Community (amounts, duration, distribution and beneficiaries); � the detailed procedures for monitoring and evaluating this programme, together with the conclusions of the assessment report provided for in Article 7 and any other measure readjusting the Culture 2000 programme arising from the assessment report. The Committee shall deliver its opinion on the draft measures referred to in the first subparagraph within a time limit which the Chairman may lay down according to the urgency of the matter. The opinion shall be delivered by the majority laid down in Article 205(2) of the Treaty in the case of decisions which the Council is required to adopt on a proposal from the Commission. The votes of the representatives of the Member States within the Committee shall be weighted in the manner set out in that Article. The Chairman shall not vote. The Commission shall adopt the measures which shall apply immediately. However, if these measures are not in accordance with the opinion of the Committee, they shall be communicated by the Commission to the Council forthwith. In that event: (a) the Commission shall defer application of the measures which it has decided for a period of two months from the date of such communication;

13328/2/98 REV 2 DG J

CR/ct

EN 9


(b) the Council, acting by a qualified majority, may take a different decision within the time limit referred to in point (a). 4. The Commission may consult the Committee on any other matter concerning the implementation of the Culture 2000 programme not covered by paragraph 3. The representative of the Commission shall submit to the Committee a draft of the measures to be taken. The Committee shall deliver its opinion on the draft within a time limit which the Chairman may lay down according to the urgency of the matter, if necessary by taking a vote. The opinion shall be recorded in the minutes; in addition each Member State shall have the right to ask to have its position recorded in the minutes. The Commission shall take the utmost account of the opinion delivered by the Committee. It shall inform the Committee of the manner in which its opinion has been taken into account. Article 5 Consistency and complementarity In the implementation of the Culture 2000 programme, the Commission shall, in cooperation with the Member States, ensure the overall consistency and complementarity with relevant Community policies and actions having an impact on the field of culture. This could involve the possibility of including complementary projects financed through other Community programmes.

13328/2/98 REV 2 DG J

CR/ct

EN 10


Article 6 Third countries and international organisations The Culture 2000 programme shall be open to participation by the countries of the European Economic Area and also to participation by Cyprus and the associated countries of Central and Eastern Europe in accordance with the conditions laid down in the Association Agreements or in the Additional Protocols to the Association Agreements relating to participation in Community programmes concluded or to be concluded with those countries. The Culture 2000 programme shall also permit cooperation with other third countries which have concluded association or cooperation agreements containing cultural clauses, on the basis of additional funds made available in accordance with procedures to be agreed with the countries in question. The Culture 2000 programme shall permit joint action with international organisations competent in the field of culture, such as UNESCO or the Council of Europe, on the basis of joint contributions and in accordance with the various rules prevailing in each institution or organisation for the realisation of the cultural actions and events listed in Article 2. Article 7 Evaluation and monitoring During 2002 the Commission shall present to the European Parliament, the Council and the Committee of the Regions a detailed assessment report on the results of the Culture 2000 programme, having regard to its objectives, and accompanied if necessary by a proposal for the amendment of this Decision.

13328/2/98 REV 2 DG J

CR/ct

EN 11


On completion of the Culture 2000 programme, the Commission shall present a report on its implementation to the European Parliament, the Council and the Committee of the Regions. Moreover, the Commission shall present annually a short report monitoring the situation of the implementation of the Culture 2000 programme to the European Parliament, the Council and the Committee of the Regions. Article 8 Repeal Decision No 2228/97/EC shall be repealed with effect from 1 January 2000. Article 9 Entry into force This Decision shall enter into force on 1 January 2000. Done at Brussels, For the European Parliament The President For the Council The President

13328/2/98 REV 2 DG J

CR/ct

EN 12


ANNEX I ACTIVITIES AND IMPLEMENTING MEASURES FOR THE CULTURE 2000 PROGRAMME

I. I.1.

Description of actions and events Specific innovative and/or experimental actions Each year the Community will support (particularly through grants, seminars and congresses, studies and measures to increase awareness) cooperation projects which originate with cultural operators from several Member States and other participating States on the basis of priorities defined after consultation of the Committee referred to in Article 4 of the Decision. These actions will cover in principle a period of one year which may be extended to two supplementary years. These vertical actions (concerning one cultural field) or horizontal actions (associating several cultural fields) should be innovative and/or experimental and aim primarily to do the following:

(i)

place the main emphasis on facilitating access to culture and wider cultural participation by the people in Europe, in all their social, regional and cultural diversity, in particular young people and the most underprivileged;

(ii)

encourage the emergence and spread of new forms of expression, within and alongside traditional cultural fields (such as music, the performing arts, the plastic and visual arts, photography, architecture, literature, books, reading, the cultural heritage including the cultural landscape and children's culture);

13228/2/98 REV 2 (ANNEX I) DG J

CR/ct

EN 1


(iii)

support projects aimed at improving access to books and reading, as well as training professionals working in the field;

(iv)

support projects of cooperation aimed at conserving, sharing, highlighting and safeguarding, at the European level, the common cultural heritage of European significance;

(v)

support the creation of multimedia products, tailored to meet the needs of different publics, and thus make European artistic creation and heritage more visible and more accessible to all;

(vi)

encourage initiatives, discussions and cooperation between cultural and socio-cultural operators working in the field of social integration, especially integration of young people;

(vii)

foster an intercultural dialogue and mutual exchange between European and other cultures, in particular by encouraging cooperation on subjects of common interest between cultural institutions and/or other operators in the Member States and those in third countries;

(viii) promote the dissemination of live cultural events using the new technologies of the information society. Community support may not exceed 60% of the budget for a specific action. In most cases this support may not be less than EUR 50 000 or more than EUR 150 000 a year.

13228/2/98 REV 2 (ANNEX I) DG J

CR/ct

EN 2


I.2.

Integrated actions covered by structured, multiannual transnational cultural cooperation agreements With a view to European added value, the Culture 2000 programme will encourage closer cooperation through cultural cooperation agreements. The programme will contribute in particular to the emergence of broad and structured areas of cooperation through the participation of networks of cultural operators, as well as organisations and research institutes in the cultural field, in the various Member States and in the other participant States. The cultural cooperation agreements will be aimed at carrying out structured, multiannual cultural actions, between operators of several Member States and those of other States participating in the Culture 2000 programme. These agreements will concern transnational actions concerning one cultural field, (vertical actions) such as music, the performing arts, the plastic and visual arts, literature, books and reading including translation and cultural heritage. They will moreover promote, also by using new media, the achievement of trans-sectoral integrated actions (horizontal actions based on synergy), i.e. associating several cultural fields. The proposed cooperation agreements, which cover a maximum period of three years, will include an annual report on the activities concerned and will involve some or all of the following measures: (i) co-production and distribution of works and creations, making them accessible to the public; (ii) (iii) movement of artists, creators and other cultural operators; further training for professionals in the cultural field and exchange of experience;

13228/2/98 REV 2 (ANNEX I) DG J

CR/ct

EN 3


(iv) (v)

use of new technologies; research, dissemination of knowledge, highlighting of cultural diversity and of multilingualism;

(vi)

promoting mutual awareness of the history, roots, common cultural values of the European peoples and their common cultural heritage.

Following consultation of the Committee referred to in Article 4 of the Decision, the Community will grant support for the implementation of cultural cooperation agreements. Community support may not exceed 60% of the cultural cooperation agreement's budget. It may not be less than EUR 200 000 or more than EUR 350 000 a year. This support may be raised by a maximum of 20% in order to cover the relevant costs incurred in the management of the cultural cooperation agreements. I.3. Special cultural events with a European or international dimension These events, substantial in scale and scope, should strike a significant chord with the people of Europe and help to increase their sense of belonging to the same community as well as making them aware of the cultural diversity of the Member States, as well as intercultural and international dialogue.

13228/2/98 REV 2 (ANNEX I) DG J

CR/ct

EN 4


These event include in particular: (i) (ii) the European Capital of Culture and the European Cultural Month; organising symposia to study questions of common cultural interest in order to foster cultural dialogue both inside and outside the Community; (iii) (iv) recognising and highlighting European artistic talent, particularly among young people; organising innovative cultural events which have a strong appeal and are accessible to citizens in general, particularly in the field of cultural heritage, and which in particular provide a link between education, the arts and culture. The priorities relating to these events will be established after consultation of the Committee referred to in Article 4 of the Decision. Community support may not exceed 60% of the budget for a special cultural event. It may not be less than EUR 200 000 or more than EUR 1 million a year for the events referred to in item (i). For the events referred to in items (ii) to (iv), the corresponding limits will in most cases not be less than EUR 150 000 a year and in all cases not be more than EUR 300 000 a year. * * *

13228/2/98 REV 2 (ANNEX I) DG J

CR/ct

EN 5


The three types of actions and events described in I.1, I.2 and I.3 follow either a vertical (concerning one cultural field) or horizontal (associating several cultural fields) approach. An indicative description of these approaches is provided in Annex II. II. Coordination with the other Community instruments in the field of culture The Commission will ensure coordination with other Community instruments active in the cultural sphere through specific actions, cultural cooperation agreements and special cultural events, mainly with a view to promoting and arranging for collaboration between sectors with common and converging interests, such as for example: � � culture and tourism (through cultural tourism); culture, education and youth (in particular, presentations to schools and colleges of audiovisual and multimedia products on European culture, with commentaries by creative or performing artists); � culture and employment (encouraging the creation of jobs in the cultural sector, especially in the new cultural areas); � � culture and external relations; cultural statistics resulting from an exchange of comparative statistical information at Community level; � culture and the internal market;

13228/2/98 REV 2 (ANNEX I) DG J

CR/ct

EN 6


� � III.

culture and research; culture and the export of cultural goods.

Communication Recipients of Community support must mention this support explicitly, and as prominently as possible, in all information or communications relating to the project.

IV.

Technical assistance and accompanying actions When executing the Culture 2000 programme, the Commission may have recourse to technical assistance organisations for which the financing is planned within the total funding of the programme. It may also, under the same conditions, make use of experts or networks of experts. In addition, the Commission may arrange evaluative studies as well as organise seminars, colloquia or other experts' meetings which might assist with the implementation of the Culture 2000 programme. The Commission may also organise actions related to information, publication and dissemination.

V.

Contact points The Commission and the Member States will organise on a voluntary basis, and step up, the mutual exchange of information for use in the implementation of the Culture 2000 programme, by means of cultural contact points which will be responsible for: � promoting the programme;

13228/2/98 REV 2 (ANNEX I) DG J

CR/ct

EN 7


� �

encouraging as many professionals as possible to take part in its projects; providing an efficient link with the various institutions providing aid to the cultural sector in the Member States, thus contributing to the complementarity between the measures taken under the Culture 2000 programme and national support measures.

VI.

Overall budget breakdown

VI.1. At the beginning of the operation, and no later than 1 March every year, the Commission will submit to the Committee an ex ante breakdown of budget resources by type of action, taking into account, to this end, the objectives set out in Article 1 of the Decision. VI.2. The funds available will be broken down internally subject to the following indicative guidelines: (a) the funds allocated to specific innovative and/or experimental actions should be around 40% of the annual budget for the Culture 2000 programme; (b) the funds allocated to integrated actions covered by structured, multiannual cultural cooperation agreements should be around 40% of the annual budget for the Culture 2000 programme; (c) the funds allocated to special cultural events with a European and/or international dimension should be around 10% of the annual budget for the Culture 2000 programme;

13228/2/98 REV 2 (ANNEX I) DG J

CR/ct

EN 8


(d)

the remaining expenditure, including the costs related to the Contact Points, should be around 10% of the annual budget for the Culture 2000 programme.

VI.3. All the percentages given above are indicative and may be adapted by the Committee according to the procedure laid down in Article 4 of the Decision. ________________

13228/2/98 REV 2 (ANNEX I) DG J

CR/ct

EN 9


ANNEX II

INDICATIVE DESCRIPTION OF THE VERTICAL AND HORIZONTAL APPROACHES

The three actions of the Culture 2000 programme represent either a vertical approach (concerning one cultural field) or a horizontal approach (associating several cultural fields). As an indication, these may be considered in the following manner: I. A vertical approach This implies a sectoral approach which seeks to take into account the specific needs of each cultural field, in particular: (a) in the following fields: music, the performing arts, the plastic and visual arts, photography, architecture and children's culture as well as regards other forms of artistic expression (for example, multimedia and street art). This approach, according to the individual aspects of each cultural field, should: � promote exchanges and cooperation between cultural operators; � aid the movement of artists and their works around Europe; � improve the possibilities of training and further training, in particular when combined with the improved mobility of those working in the cultural field;

13328/2/98 REV 2 (ANNEX II) DG J

CR/mc

EN 1


� encourage creativity, while supporting the implementation of activities promoting European artists and their works in the abovementioned fields within Europe and favouring a policy of dialogue and exchanges with other world cultures; � support initiatives which would use creativity as a means of social integration; (b) as regards books, reading and translation, this approach aims to: � encourage exchanges and cooperation between institutions and/or individuals from the different Member States and other countries participating in the programme as well as third countries; � improve awareness and the distribution of literary creation and the history of the European people through supporting the translation of literary, dramatic and reference works; � encourage the mobility and further training of those working in the books and reading field; � promote books and reading, in particular in young people and less favoured sectors of society; (c) as regards cultural heritage of European importance, in particular intellectual and nonintellectual, movable and non-movable heritage (museums and collections, libraries, archives, including photographic archives, audiovisual archives covering cultural works), archaeological and sub-aquatic heritage, architectural heritage, all of the cultural sites and landscapes (cultural and natural goods), this approach seeks to: � encourage projects of cooperation aimed at the conservation and restoration of the European cultural heritage;

13328/2/98 REV 2 (ANNEX II) DG J

CR/mc

EN 2


� encourage the development of international co-operation between institutions and/or individuals, in order to contribute to exchanges of know-how and the development of best practice as regards conservation and safeguarding the cultural heritage; � improve access to the cultural heritage, where there is a European dimension, and encourage the active participation of the general public, in particular children, young people, the culturally deprived and inhabitants from rural or peripheral regions of the Community; � encourage mobility and training on cultural heritage for those working in the cultural sector; � encourage international cooperation for the development of new technologies and innovation in the different heritage sectors and as regards the conservation of traditional crafts and methods; � take heritage into consideration in other Community policies and programmes; � encourage cooperation with third countries and the relevant international organisations. II. A horizontal approach This approach seeks to promote synergy and develop cultural creation, as much through the promotion of trans-sectoral activities involving a number of cultural sectors, as through supporting joint activities involving different Community programmes and policies (in particular those concerning education, youth, professional training, employment etc).

13328/2/98 REV 2 (ANNEX II) DG J

CR/mc

EN 3



COUNCIL OF THE EUROPEAN UNION

Brussels, 16 July 1999 (OR. f) 13328/2/98 REV 2 ADD 1 LIMITE CULTURE 88 CODEC 646

Interinstitutional File No 98/0169 (COD)

COMMON POSITION (EC) No

/98

ADOPTED BY THE COUNCIL ON 28 JUNE 1999 WITH A VIEW TO THE ADOPTION OF DECISION No /98/EC OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL ESTABLISHING A SINGLE FINANCING AND PROGRAMMING INSTRUMENT FOR CULTURAL COOPERATION (CULTURE 2000 PROGRAMME)

STATEMENT OF THE COUNCIL'S REASONS

13328/2/98 REV 2 ADD 1

kis/PT/at

EN


I.

INTRODUCTION 1. On 28 May 1998, the Commission submitted to the European Parliament and to the Council a proposal for a Decision establishing a single financing and programming instrument for cultural cooperation (8348/98 CULTURE 23 CODEC 255 � COM(98) 266 final). 2. 3. 4. The European Parliament delivered its Opinion on 5 November 1998. The Committee of the Regions delivered its Opinion on 19 November 1998. On 17 November 1998, the Commission submitted to the European Parliament and the Council an amended proposal (13150/98 CULTURE 87 CODEC 630 � COM(98) 673 final).

II.

AIM OF THE PROPOSAL The purpose of the proposal is to define the framework for cultural action by the Community for the 2000-2004 period, through the creation of a single financing and programming instrument for cultural cooperation. 1. General remarks In its common position, the Council approved the main part of the Commission proposal, although it did make certain changes which it considered desirable.

13328/2/98 REV 2 ADD 1

kis/PT/at

EN


2. 2.1 2.1.1

Specific remarks Major amendments made by the Council to the Commission's proposal Objectives and priorities The Council amended part of Article 1 of the Decision (Duration and objectives) in order to place more emphasis on young and socially disadvantaged people and on cultural diversity. It also stressed the importance of fostering an intercultural dialogue and mutual exchange between European and non-European cultures.

2.1.2

Budget distribution The Council felt it necessary to amend the Commission proposal so as to give a specific indication of the distribution of the budget between the various actions planned. It therefore indicated its wish to see specific actions, integrated actions, special cultural events and other expenditure being allocated about 40%, 40%, 10% and 10% respectively of the programme's annual budget. Furthermore, it considered it necessary to limit Community support for special cultural events (other than the European Capital of Culture and the European Cultural Month), by specifying that Community support for those projects could not, in most cases, be less than EUR 150 000 per year and, in all cases, more than EUR 300 000 per year.

2.1.3

Committee procedure The Council thought it necessary to change the type of committee proposed by the Commission in Article 4 (Implementation) to a type IIb "management" committee, according to the classification laid down in the "Comitology" Decision of 13 July 1987.

13328/2/98 REV 2 ADD 1

kis/PT/at

EN


2.2 2.2.1

European Parliament amendments European Parliament amendments adopted by the Commission The Commission accepted, in part or in their essence, fourteen of the European Parliament's 27 amendments. The amendments are as follows: Nos 2, 4, 5, 6, 7, 9, 10, 12, 13, 14, 15, 19, 21 and 23.

2.2.2

European Parliament amendments adopted by the Council The Council adopted, in part or in their essence, nine of the amendments proposed by the European Parliament. The amendments are Nos 4, 5, 7, 8, 9, 12, 14, 19 and 21.

2.2.3

European Parliament amendments not adopted by the Council The Council did not accept the following amendments: � Amendment 1 Title of the decision The Council believes that the expression "cultural cooperation" is more appropriate than "cultural policy". � Amendment 2 Recital 1a (new) The Council does not share the European Parliament's viewpoint as expressed in this amendment.

13328/2/98 REV 2 ADD 1

kis/PT/at

EN


�

Amendment 3 Recital 1b (new) The Council does not share the European Parliament's viewpoint as expressed in this amendment.

�

Amendment 6 Recital 7 The Council does not consider it advisable to refer to mutual awareness of languages in this recital.

�

Amendment 10 Recital 13a (new) The Council feels that the issues addressed by the European Parliament in this amendment overstep the framework of this draft Decision.

�

Amendment 11 Recital 14 The Council takes the view that the expression "cultural cooperation" is more appropriate than "cultural policy".

13328/2/98 REV 2 ADD 1

kis/PT/at

EN


�

Amendment 13 Article 1(f)a (new) The Council considers that public access and participation are already implicitly taken into account in the other paragraphs in this Article.

�

Amendment 15 Article 2 While it partly agrees with the European Parliament's view on the respective importance of the vertical and horizontal approaches, the Council considers that Annex II to the common position expresses that concern more clearly.

�

Amendment 16 Article 3 The Council considers that the budget allocation proposed by the Commission (EUR 167 million) is sufficient.

�

Amendments 17 and 18 Articles 4 and 4a (new) The Council feels that Article 4 should contain provisions on the committee procedure, and should comply with the Council Decision of 13 July 1987 on the matter.

13328/2/98 REV 2 ADD 1

kis/PT/at

EN


�

Amendment 20 Article 6 The Council considers that the provisions of Article 7 of the common position are adequate to ensure proper evaluation and monitoring of the "Culture 2000" programme.

�

Amendment 22 Annex I (introductory part) The Council considers that the introductory part of the Commission proposal should be deleted.

�

Amendment 23 Annex I, Section I While it partly agrees with the European Parliament's view on the respective importance of the vertical and horizontal approaches, the Council feels that Annex II to the common position expresses that concern more clearly. It also feels that Section I of Annex I to the common position contains all the necessary information.

�

Amendment 24 Annex I, Section II The Council considers that Section I of Annex I to the common position already contains a number of the points in the European Parliament's amendment.

13328/2/98 REV 2 ADD 1

kis/PT/at

EN


�

Amendments 25, 26 and 27 Annex I, Sections III, IV and V The Council wishes to retain the provisions in Sections III, IV and V of Annex I to the common position.

III. CONCLUSIONS The Council considers that its common position constitutes a balanced text which defines a clear framework for the Community's cultural action over the 2000-2004 period.

13328/2/98 REV 2 ADD 1

kis/PT/at

EN






