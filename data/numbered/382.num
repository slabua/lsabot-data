PRESIDENT'S SPEECH

3

SPEECH
delivered by the President of the European Parliament, Nicole FONTAINE to the Extraordinary European Council on 21 September 2001 in BRUSSELS

As delivered

Bulletin 21.09.2001

- EN -

PE 310.210


PRESIDENT'S SPEECH

5

President of the Council, Heads of State and Government, President of the Commission, High Representative for the CFSP, Ministers, May I thank you, Mr Prime Minister, President-in-office of the Union, for agreeing to the European Parliament's participation in this extraordinary meeting of the European Council, which I welcome, having myself expressed a particular wish for it to be held. Today the eyes not only of Europe, but of much of the world, are turned towards this Council. It is an event of historic importance as the world seeks the most appropriate way to fight the terrorism which last week manifested itself in the most appalling form imaginable. When it met here in Brussels on Wednesday the European Parliament asked me to convey three messages to you in these exceptional circumstances : solidarity, determination and effectiveness.

All of us in our various ways, with deep emotion and sadness, have expressed solidarity with the American people, and honoured the victims of the tragedy, among whom there are many Europeans. We have shared in America's grief. We have all felt personally affected, perceiving the events as an assault on our values of freedom and the right to life. And unfortunately, we are also aware that the European Union is not immune from danger. It too has pockets of terrorism within its borders and could at any time be the target of this kind of large-scale attack. In the response which must be given, it is the fervent hope of the European Parliament that our fifteen Member States will act unanimously, not only in declarations of principle, which goes without saying and has already been done, but also, and most importantly, in the choice and implementation of the means to be deployed. Parliament also hopes that the United States, notwithstanding its right as the primary victim to defend itself, will be careful to avoid a response which is excessively unilateral, or which is seen as such. The response must be a genuine expression of the broadest possible alliance of all the States that have pledged their participation, and I have noted with interest the latest developments within the US Administration, which seem to be moving in this direction. The European Parliament has repeatedly stressed how crucial it is to do everything within our power to ensure that Islamic fundamentalism is not equated with Islam as a whole. I can tell you that this risk is taken very seriously by the ambassadors of the League of Arab States to the European Union, who already last Tuesday asked for a meeting with me to express their concerns. Despite the timely and unequivocal statements made by numerous senior statesmen in the EU Member States over the past few days, the fear remains, and it is a great fear.

Bulletin 21.09.2001

- EN -

PE 310.210


6

PRESIDENT'S SPEECH

We have large Muslim communities in our countries, and special links with the Arab world. The testing situation we are currently experiencing must serve to reactivate these links, inter alia by relaunching the Euro-Mediterranean dialogue and, more generally, the dialogue between Europe and the Muslim world. The European Parliament plans to take initiatives along these lines. We all know, furthermore, that unresolved regional conflict is the soil in which terrorism flourishes. This is true of the Middle East. The pernicious downward spiral developing there must be halted. I would like to take this opportunity to pay the warmest possible tribute to the High Representative for the Common Foreign and Security Policy, Mr Javier Solana, to the Belgian Presidency and to those of the Ministers who maintain constant contact with the Israelis and Palestinians. This commitment, coupled with the spirit of reason prevailing among the chief protagonists, seems to offer some glimmers of hope. Despite a small number of incidents in the area, the tension has significantly decreased, and I hope that Israel's Prime Minister will withdraw the conditions he has set for a meeting between Shimon Peres and Yasser Arafat. There will be no progress in the Middle East if dialogue does not resume. And if peace should prove unattainable, the whole world would be threatened by the continuation of this conflict. I am convinced that your meeting here today will help the European Union to identify the tools it needs in order to fight shoulder-to-shoulder against terrorism. Within the European Union this solidarity has been expressed towards all the democratic forces in Spain fighting against ETA terrorism. And, in the same spirit, a few months ago we welcomed in Strasbourg Ahmed Shah Masood, leader of the struggle against the Taliban in Afghanistan. As you know, this is a political step which has long been called for by the European Parliament. Here its thinking is echoed by the Commission, which on Wednesday presented proposals many of which are based on past or recent recommendations made by Parliament. Yes, we need a European search and arrest warrant. Yes, we need a European Public Prosecutor. Yes, Eurojust must become operational as soon as possible. Yes, judicial and police cooperation can be communitised. Yes, the fight against organised crime in all its forms can be communitised, particularly financial crime and arms trafficking. These are all immediate measures to which the European Parliament invites you this evening to give a decisive political impetus. They will be facilitated by the political agreement reached yesterday evening in the Council, provided that the obstacles to harmonising criminal law, which is after all provided for in the Amsterdam Treaty, are removed once and for all by the beginning of December. The people of Europe expect these measures. They are needed to reassure people, and they form an integral part of the area of security, freedom and justice you decided on at the European Council in Tampere. But they will not be truly effective without a change in thinking and behaviour. There must be unfailing cooperation between all the services responsible for security. In the face of the immense dangers which have recently been so shockingly brought home to us there will either be total security or there will be no security at all. In order to have an effective voice with its international partners, the European Union must be exemplary in the way it handles dealings between its own Member States.

Bulletin 21.09.2001

- EN -

PE 310.210


PRESIDENT'S SPEECH

7

You will undoubtedly be giving a great deal of attention to the economic impact of last week's attacks. Whole swathes of our economies could be affected, with serious consequences for employment. At a time when the euro is about to make its entry into the daily lives of over 300 million Europeans, the close cooperation required in security matters must also apply to the economy. What we need is not fifteen national responses, but one European response. The tragic events of last week demand an instant response from the European Union. The new challenges to be met require a major qualitative leap in the European project. The broad debate which has begun on the future of Europe in preparation for the 2004 deadline you set in Nice takes on an even greater importance as a result. Heads of State and Government, you can be certain that the European Parliament is behind you in the decisive action you are taking to meet the threat which Europe must face head on and overcome. ____________________

Bulletin 21.09.2001

- EN -

PE 310.210


