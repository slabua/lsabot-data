EUROPEAN PARLIAMENT
DIRECTORATE-GENERAL FOR RESEARCH DIRECTORATE A DIVISION FOR INTERNATIONAL AND CONSTITUTIONAL AFFAIRS

FACTSHEET
MALTA 1. The acquis 1 With the exception of veterinary and phytosanitary legislation, mainly in the form of directives, most of the regulations and legislation covered by this chapter will be directly applicable at the date of accession and do not therefore call for transposition on the part of the candidate countries. The emphasis in the preparations for accession will therefore be on a country's ability to implement and enforce the acquis. The following main fields are covered: � � � � � Horizontal issues: Agricultural Guarantee and Guidance Funds; trade mechanisms; quality policy; organic farming; Farm Accountancy Data Network; State Aids. Common Market Organisations: arable crops; cereals, oilseeds and protein crops; non-food, processed cereals, potato starch, cereal substitutes and rice; sugar; fibre crops. Specialised crops: fruit and vegetables; wine and alcohol; bananas; olive oil; tobacco. Animal products: milk and milk products; beefmeat; sheepmeat; pigmeat; rural development. Veterinary legislation: control system in the internal market; identification and registration of animals; control at the external borders; animal disease control measures; animal health trade in live animals and animal products; public health protection; animal welfare; zootechnical legislation. Phytosanitary legislation: harmful organisms; quality of seeds and propagating material; plant variety rights; plant protection products/pesticides; animal nutrition. Chapter 7 - Agriculture

�

1

Information largely drawn from the European Commission, DG Enlargement http://www.europa.eu.int/comm/enlargement/negotiations/chapters/index.htm

WIP/2003/01/0096

1


2. The negotiations The chapter has been closed with ten countries, including Malta, and remains open with Bulgaria and Romania. Chapter opened: December 2001 Status : Closed December 2002. Transitional arrangements: The key agreements reached in the negotiations with the ten countries which closed the chapter in December 2002 are: Financial and market related aspects of agriculture � The new member states will gradually phase in EU agricultural direct payments between 2004 and 2013. Direct payments will start at 25% in 2004, 30% in 2005 and 35% in 2006 of the present system and increase by 10 percentage steps to reach 100% of the then applicable EU level in 2013. � Within carefully defined limits, the new member states will have the option to "top-up" these EU direct payments with national subsidies. In 2004-2006, a new member state has the possibility to top up EU direct payments to � either 55% of EU level in the years 2004, 60% in 2005 and 65% in 2006. From 2007 the new member state may top-up EU direct payments by 30 percentage points above the applicable phasing-in level in the relevant year; � or to the total level of direct support the farmer would have been entitled to receive, on a product by product basis, in the new member state prior to accession under a like national scheme increased by 10 percentage points; In no case should the payment be higher than 100% of EU-15 level of direct payments. � Rather than applying the standard direct payment scheme applicable in the current EU, the new member states have the option, during a limited period, of granting their farmers CAP direct payments in the form of a decoupled area payment (a simplified payment scheme). An annual financial envelope will be calculated for the new member state according to agreed criteria and then divided between the utilised agricultural area. The new member states will have special additional financial aid for rural development for a limited period. This includes a higher proportion of EU co-financing in rural development projects. Certain rural development measures have been adapted or created in order to reflect better the requirements of the new member states in the first years of accession. This means that for a limited period, new member states will be able to use rural development funds for schemes specifically designed to help restructuring of the rural sector. For example, there is support for semi-subsistence farms undergoing restructuring as well as specific measures to assist farmers in meeting EU standards. Reference quantities (e.g. quotas, base areas) have been agreed for all the applicable products on the basis of recent production and taking into account country specific circumstances (e.g. drought). In a few specific instances, transitional periods were agreed for the adoption and implementation of certain parts of EU legislation. These transitional periods are exceptional and limited in time and in scope. 2

� �

� �

WIP/2003/01/0096


Veterinary and phytosanitary aspects of agriculture � Certain food establishments have been granted a transitional period in order to upgrade to fully meet EU requirements. These include 52 premises in the Czech Republic, 44 in Hungary, 117 in Latvia, 20 in Lithuania, 485 in Poland and 2 in Slovakia. Such transitional periods are limited in time and scope and do not involve any exemption from food hygiene legislation. During the transitional period, products which come from these establishments must be specially marked and cannot be marketed in any form in other EU countries. All establishments not subject to a transitional period will have to comply with the acquis on accession and their products will be able to be freely marketed within the EU. Certain establishments have been granted a transitional period in order to upgrade to fully meet structural requirements for hen cages (only the slope and height of the cages). Such transitional periods are limited in time and scope and are applicable in the Czech Republic, Hungary, Malta, Poland and Slovenia. There are also transitional periods in the field of phytosanitary legislation for Lithuania and Poland (potato ring rot and potato wart disease respectively) as well as transitional periods on certain parts of seed quality legislation for Malta, Cyprus, Latvia and Slovenia. Again, these transitional periods are limited in time and scope.

� �

�

3. Position of the European Parliament1 In its resolution of 4 October 20002, Parliament observes that Malta still needs to adopt further measures with regard to approximation in the field of agriculture, and in this context welcomes Malta's efforts to reduce tariffs in stages in this economically small but important sector, and accordingly endorses the priorities of the NPAA. In its resolution of 5 September 20013, Parliament: � Urges Malta to press on with agricultural reforms, in continuous consultation with farmer associations and, in particular, to put forward an analysis of the development opportunities for Maltese agriculture opened up by EU aid for rural areas; � Notes with concern that Malta has made little progress in establishing the administrative structures necessary for implementation of the common agricultural policy; � Draws attention to the fact that, in the phytosanitary area, the relevant provisions of the acquis still have to be incorporated and points out that the ability to deal with new phytosanitary aspects still has to be developed; welcomes the fact, however, that a veterinary medicine monitoring programme has been submitted and a number of testing standards brought into line with EU requirements; points out that arrangements for the disposal of waste material from slaughtered animals are not yet in line with the Community acquis; � Points out that, within quality policy, Community-compatible quality designations for the various agricultural products still have to be developed; � Hopes that Malta will be able to present its negotiating position on the agriculture chapter as soon as possible. In its resolution of 13 June 20021, Parliament welcomes the substantial improvement in administrative capacity the areas of statistics and financial control; calls on the Government to
1 2

For Parliament's position on enlargement and agriculture, see the resolution of 13 June 2002: A5-0200/2002 Resolution on the state of negotiations with Malta, � 12: A5-0243/2000 3 Resolution on the state of negotiations with Malta, � 13 - 16, 18: A5-0262/2001

WIP/2003/01/0096

3


extend these efforts to cover every administrative sphere, in particular by implementing restructuring and training measures, and to focus greater attention on.......veterinary and plant protection. In its resolution of 20 November 20022, Parliament: � reiterates its support for the objective of gradually phasing in direct payments for farmers in the new Member States; recalls that the estimated agricultural expenditure related to enlargement can be accommodated within the Berlin agreement, consisting of commitments in 2005 of some EUR 1.1 billion in total direct payments, some EUR 750 million for market expenditure and some EUR 1.6 billion for rural development; � recommends that the agreements to be reached under the Agriculture chapter should be such as to ensure the continuing vitality and viability of Maltese agriculture with the instruments available under the CAP; calls for the upgrade of Maltese administrative capacity for the implementation of rural and development schemes through the appropriate EU instruments; encourages Malta, in line with the concerns expressed by the Commission, to substantially expedite progress in setting up the physical and legal structures for the proper handling of the CAP in Malta after accession, in particular as regards common market organisations and phytosanitary legislation. 4. Latest Assessment by the European Commission
3

In the 1999 update of its Opinion, the Commission concluded that the integration of Maltese agriculture and agricultural policy into the CAP would still require efforts, and that for Malta, the adoption of the CAP could improve its capacity, in particular the food processing export industry, as well as its efficiency. The Commission added that the Maltese agricultural policy measures were still not in line with the acquis and the role of the State in the marketing of agricultural produce remained important. The implementation of the CAP also required a reinforcement of the institutional and administrative capacity, a scheme for the application of (international standards) for fruit and vegetables, as well as a control mechanism to enforce compliance. Since the 1999 update of the Opinion, Malta has made some progress in bringing its legislation in line in the veterinary and phytosanitary areas. It has taken important steps to set up horizontal measures needed for the Common Agricultural Policy as well as measures needed for the Common Market Organisations. It has started to strengthen its administrative capacity and defined a rural development policy providing a framework for the future development of Maltese agriculture. It has initiated the restructuring of its agriculture by starting to implement a process of dismantling of the levies on some agricultural products. Negotiations on this chapter continue. Malta is meeting the majority of the commitments it has made in the accession negotiations in this field. However, delays have occurred on the establishment of the Paying Agency. This needs to be urgently addressed. In order to be ready for membership, Malta needs to give urgent attention to the restructuring policies associated with the dismantling of levies, as well as the devolving of marketing
1 2

Resolution on the state of the enlargement negotiations, � 113: A5-0190/2002 Resolution on the progress of the candidate countries towards accession, � 30 & 97: A5-0371/2002 3 European Commission, Regular Report on Malta 2002: p. 57: http://www.europa.eu.int/comm/enlargement/report2002/ml_en.pdf

WIP/2003/01/0096

4


responsibilities from the state to the farmers. Malta needs to implement all remaining legislation in this area, whether it concerns the Common Market Organisations or the veterinary and phytosanitary areas and, as an urgent priority, to upgrade considerably its capacity to implement the acquis, particularly in the area of rural development. Ongoing efforts to strengthen administrative capacity and to continue the dismantling of levies should be vigorously pursued.

January 2003

WIP/2003/01/0096

5


