EUROPEAN PARLIAMENT
DIRECTORATE-GENERAL FOR RESEARCH DIRECTORATE A DIVISION FOR INTERNATIONAL AND CONSTITUTIONAL AFFAIRS

FACTSHEET
SLOVENIA 1. The acquis The acquis in this chapter relates to Article 43 EC on freedom of establishment and Article 49 EC on the freedom to provide services: � Financial services: Banking, Insurance, investment services and securities markets. The acquis lays down minimum requirements for the different types of institutions in order to create a uniform minimum standard based on the principles of minimal harmonisation of authorisation conditions and the prudential rules, home country control and single licence, mutual recognition of national supervisory standards. � A directive on the protection of personal data and the free movement of such data. � Directives on the freedom of establishment and freedom to provide services for craftsmen, traders and farmers. � A directive on self-employed commercial agents. � Information society directives on the provision of information in the field of technical standards and regulations and of rules, and on the legal protection of services on conditional access. This chapter is closely linked to others, particularly free movement of capital and of persons. In addition to alignment, the establishment of an effective regulatory infrastructure is particularly important in order to implement the acquis and ensure an efficient and well-supervised financial sector. There is close monitoring by the Commission in cooperation with the Member States. Most candidates began transposing the acquis some time ago, also in the framework of the Europe Agreements. However, a significant amount remains to be done, both in financial services and the primary treaty provisions on right of establishment and to provide services, those countries which made an early start on economic and financial reform and harmonisation being furthest advanced.
1

Chapter 3 - Freedom to provide services 1

Information largely drawn from the European Commission, DG Enlargement http://europa.eu.int/comm/enlargement/negotiations/index.htm

WIP/2002/11/0295

1


2. The negotiations The chapter has been closed with ten countries and provisionally closed with Bulgaria, while it remains open with Romania. For several countries, transition periods of up to 5 years have been agreed, and some have been accorded exemption from the banking acquis for very small institutions such as credit unions. Chapter opened: July 1999 Status: closed in December 2002 (provisionally closed in November 2000) Transitional arrangements: � Lower level of capital requirements for savings and loan undertakings until end-2004 3. Position of the European Parliament In its resolution of 4 October 20001, Parliament stresses the need to develop the privatisation process in the insurance sector and adopt legislation to reform the pensions system. In its resolution of 5 September 20012, Parliament: � calls on the Slovene Government to speed up the privatisation of the leading banks in Ljublana and Maribor and views the most recent developments as a useful step in this direction; � calls on the Slovene Government to revitalise reform of the financial sector by encouraging competition, to speed up company restructuring and to increase manpower flexibility, so as to foster business development and improve market efficiency. In its resolution of 13 June 20023, Parliament welcomes the progress of the Government concerning privatisation in the banking sector; calls on the Government to speed up privatisation in the insurance sector and to continue to reduce the overall prominence of State ownership in the economy through privatisations; calls on the Government to define in a clear and transparent way the real decision-making rights in privatised banks. 4. Latest Assessment of the European Commission
4

In its 1997 Opinion, the Commission concluded that the acquis in this sector had already been to a considerable extent transposed, but important provisions still remained to be adopted. Full free establishment in the areas of banking, securities and insurance, in particular with regard to branches of foreign institutions, still had to be adequately introduced in the sectors' legislative frameworks and duly implemented. As regards insurance, privatisation needed to be speeded up and the insurance and reinsurance sector opened up to EC operators and to foreign investment. The supervisory functions in the sector were expected to be strengthened by the new legislation under preparation. However, provided harmonisation proceeded as planned, no major
1 2

Resolution on the state of negotiations with Slovenia, � 6: A5-0242/2000 Resolution on the state of negotiations with Slovenia, � 2 & 3: A5-026/2001 3 Resolution on the state of the enlargement negotiations, � 145: A5-0190/2002 4 European Commission, Regular Report on Slovenia 2002, p. 53: http://www.europa.eu.int/comm/enlargement/report2002/si_en.pdf

WIP/2002/11/0295

2


difficulties were foreseen in achieving full approximation in relation to free movement of financial services in the medium term. Since the Opinion, Slovenia has made steady progress in most areas of the chapter, both in terms of legislation and in bolstering the administrative and regulatory infrastructure required to supervise the financial services sector. Slovenia's alignment process is now well advanced, but some further legislative alignment is still needed with regard to some aspects of the acquis. Institutional structures are largely in place, although those related to data protection do not yet entirely meet the requirements of the acquis. Administrative capacity needs to be strengthened in a number of areas. Negotiations on this chapter have been provisionally closed. Slovenia has been granted two transitional arrangements concerning savings and loans undertakings established before 20 February 1999 (until 31 December 2004), and concerning the deposit guarantee scheme (until 31 December 2005). Slovenia is generally meeting the commitments it has made in the accession negotiations in this field. In order to complete preparations for membership, Slovenia's efforts now need to focus on completing alignment in the areas of non-financial services, investment services and securities markets as well as protection of personal data.

January 2003

WIP/2002/11/0295

3


