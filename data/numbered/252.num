INT/081

Brussels, 24 January 2001

OPINION of the Economic and Social Committee on the Exhaustion of registered trademark rights (own-initiative opinion) ____________________

CES 42/2001 ES/PM/GW/vh


-1On 13 July 2000 the Economic and Social Committee, acting under the third paragraph of Rule 23 of its Rules of Procedure, decided to draw up an opinion on the Exhaustion of registered trademark rights. The Section for the Single Market, Production and Consumption, which was responsible for preparing the Committee's work on the subject, adopted its opinion on 10 January 2001. The rapporteur was Mrs S�nchez Miguel. At its 378th plenary session of 24 and 25 January 2001(meeting of 24 January), the Economic and Social Committee adopted the following opinion unanimously. 1. Introduction

1.1 The purpose of this opinion is to endorse the Commission's decision of May 2000 not to change the current Community exhaustion regime for registered trademarks, owing in part to the need to continue protecting European goods and services identified by trademarks. 1.2 Registered trademarks form part of the corpus of legislation governing industrial and intellectual property rights. At European level the discussion on registered trademarks has focused on the accession of the European Community to the Madrid Protocol and on the Community exhaustion regime. Existing Community law1 on industrial property rights (design, registered trademarks, 1.3 copyright and similar rights) provides for the principle of Community exhaustion of rights. The purpose of this regime is to guarantee free movement of goods within the EU; it allows the owner of a trademark to prevent imports of products bearing that trademark which were first brought on to the market outside the EU. 1.4 In November 1999 the Commission presented a working document that was intended to form the basis for future, more detailed, discussions in the group of experts appointed by the Member States at the request of the Council about a possible EU position on any change in the current Community exhaustion arrangements. 1.5 At the Internal Market Council of 25 May 2000, ministers exchanged contrasting views based on the outcome of recent discussions in the group of experts. At this Council,

1

Council Directive 89/104/EEC of 21 December 1988 to approximate the laws of the Member States relating to trade marks; Council Regulation (EC) No. 40/94 of 20 December 1993 on the Community trade mark; Council Directive 87/54/EEC of 16 December 1986 on the legal protection of topographies of semiconductor products; Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs; Council Directive 92/100/EEC of 19 November 1992 on rental right and lending right and on certain rights related to copyright in the field of intellectual property; Council Regulation (EC) No 2100/94 of 27 July 1994 on Community plant variety rights; Directive 96/9/EC of the European Parliament and of the Council of 11 March 1996 on the legal protection of databases.

CES 42/2001 ES/PM/GW/vh

.../...


-2Commissioner Frederik Bolkestein informed the ministers of the Member States that the Commission had decided not to propose any change to the current Community exhaustion regime. 1.6 The exhaustion regime was included at the request of the European Parliament, under Article 13 of Council Regulation 40/94 and Article 7 of Council Directive 89/104. 2. General comments: arguments for and against a change in the Community exhaustion regime The NERA study

2.1

2.1.1 In November 1998 the Commission asked for a study to be carried out ("The economic consequences of the choice of regime of exhaustion in the area of trademarks") by National Economic Research Associates (NERA), S.J. Berwin & Co. and IFF Research. 2.1.2 The main aim of the study was to look at the potential economic consequences for the European Union of any change in the exhaustion regime for trademarks. The study analysed the effects that the exhaustion regimes (Community and international) might have on prices and trade volumes, market and product structures, and consumers, and the impact of these regimes on macroeconomic indicators such as employment. 2.1.3 The study concluded that the only obvious beneficiaries of a switch from the Community exhaustion regime to an international exhaustion regime would be parallel importers and the transport sector. National importers and exporters, and manufacturers, would suffer most. A change in the regime would produce a barely perceptible reduction in consumer 2.1.4 prices (0-2%). The study also showed that the initial fall in prices would probably disappear over the long term. 2.1.5 The study did not quantify the potential job losses that a change in the regime would cause, although it indicated that jobs would probably be lost among "national" suppliers of a product, while new ones would be created among "external" suppliers. 2.1.6 Other conclusions of the study were that a change in the Community exhaustion regime would affect the quality of products, their availability to consumers and after-sales services. 2.2 Public hearing

2.2.1 On 28 April 1999 the Commission organised a public hearing attended by 180 representatives of various interest groups, including owners of registered trademarks from different industrial sectors, consumers, parallel traders and retailers.

CES 42/2001 ES/PM/GW/vh

.../...


-32.2.2 At the hearing arguments were advanced for change and for maintaining the current Community exhaustion system: - Those in favour of maintaining the current arrangements argued that an international regime would lower the economic value of trademark rights, which would have a negative impact on research and innovation, and reduce investment, causing higher unemployment. - A number of participants argued against adopting the international exhaustion system on the grounds that there was a close link between parallel trade and piracy. - Arguments supporting a change in the current regime focused exclusively on the fall in prices (02% according to the NERA study) that would benefit European consumers. - The potential broader range of products was another argument put forward by defenders of the international exhaustion regime. 2.3 Groups of experts in the Council

2.3.1 The Commission has held a number of meetings with the Member States and interest groups. Two meetings have been held with experts from the Member States on the basis of the working document prepared by the Commission in November 1999. 2.3.2 The ESC agrees in particular with the following arguments put forward by the national experts: - The introduction and use of new technologies such as electronic commerce may make a wider range of products available to consumers at lower prices, so there would be less reason to change the current exhaustion regime for price reasons. - In many cases products are protected not just by registered trademarks but by a number of intellectual property rights (industrial designs). Introducing an international exhaustion regime for registered trademarks would therefore have only a limited impact on a small number of sectors. - In Europe, registered trademarks are governed by Directive 89/104 (national trademarks) and Regulation 40/94 (Community trademarks). It is essential that exhaustion regimes should be the same for both types of trademark (national and Community). Different co-existing systems would create confusion in the market, and among consumers, especially in terms of whether or not a product with a specific trademark had been brought onto the market legally.

CES 42/2001 ES/PM/GW/vh

.../...


-42.4 2.4.1 Consequences of a possible change in regime Community legislation

2.4.1.1 First and foremost, the Committee feels it is essential that the exhaustion regime be the same for national and Community registered trademarks. However, it must be borne in mind that there can be no guarantee that the exhaustion regime would be changed in both the legal instruments regulating this area (the Directive for national registered trademarks and the Regulation for Community registered trademarks), since the Directive can be changed by qualified majority in the Council, whereas amendment of the Regulation requires unanimity. 2.4.1.2 It is likely that some Member States would resist a change in the Regulation, which might result in two different exhaustion regimes existing alongside each other, a situation that would only produce confusion in the market and among consumers. However, the Community exhaustion regime also furthers integration of the single market. An international exhaustion regime could put European companies at a disadvantage, since there has been no equivalent process of integration at global level as yet; SMEs would be particularly affected, as they are covered by national trademark arrangements, these being cheaper. 2.4.2 Other intellectual property rights

2.4.2.1 Registered trademarks are only a part of the corpus of legislation dealing with intellectual and industrial property. In practice, most products are covered by a complex of rights relating to industrial property, registered trademarks, patents, copyright and designs. It is rare for the trademark to be the only industrial property right covering a product. For example, in the case of an audio compact disc the music will be protected by copyright, the technology by patent and the trademark by registered trademark rights. 2.4.2.2 The legislative processes relating to intellectual and industrial property rights are likely to be complex and long-drawn-out. The European-level discussion about designs began in 1993 and has not yet finished. As the Commission recently noted2, changing the exhaustion regime for registered trademarks would not have much impact on the market because most products are covered by a number of intellectual property rights. The Commission does not consider it appropriate to introduce an international exhaustion regime for all intellectual property rights.

2

Commission Communication of 7 June 2000.

CES 42/2001 ES/PM/GW/vh

.../...


-52.4.3 Economic growth in Europe

2.4.3.1 A change in the Community exhaustion regime could in the long term inhibit investment in new products or even result in the withdrawal of products with registered trademarks that are already established on the market because they cannot compete with imported products. 2.4.3.2 Owners of registered trademarks might also decide to cut post-sales services or other features of their products that parallel importers do not provide to European consumers because they are not subject to a Community standard. The Community exhaustion regime meets the need to further integrate the single 2.4.3.3 market. An international exhaustion regime would put European companies at a competitive disadvantage because such an integration process has not taken place at global level. Conditions of access to markets for products from third countries vary more at global level than within the EU. 2.4.3.4 The single market has brought about economic integration and a levelling-out of prices across the EU. These conditions do not however exist on the global market. The countries coming under the WTO thus do not form a customs or economic union like the EU, or even a freetrade area. Numerous tariff and non-tariff barriers still exist between these countries, as do major differences in terms of their economies, legal systems, levels of wealth and development, controls and regulations. In addition to the possible consequences of a change of regime for European 2.4.3.5 manufacturers, consideration must be given product marketing/distribution channels - especially of specialist products - and franchising. At present, franchising arrangements give European consumers access to high-quality products with clear references. A change of regime would give rise to confusion among European consumers, who could come across products with well-known trademarks but failing to meet their expectations of quality. Another important single market issue is the risk of counterfeiting and piracy 2.4.3.6 concerning products coming from non-EU countries. As argued in the Green Paper on counterfeiting and piracy and its follow-up communication3, account must be taken of: - the negative impact of these products on the European economy; - the risk of parallel import channels being used for such products; - the need for action taken by the Commission in implementing the green paper to be consistent.

3

ESC opinion in OJ C 116 of 28.4.1999, rapporteur: Mr Malosse. Green Paper - Combating counterfeiting and piracy in the Single Market (COM(1998) 569 final); Communication - Follow-up to the Green Paper on combating counterfeiting and piracy in the Single Market (COM(2000) 789).

CES 42/2001 ES/PM/GW/vh

.../...


-62.4.3.7 Finally, differences in administrative requirements or labour costs may affect the costs of parallel trade. Community policy has prevented such differences occurring within the EU, which is not the case at international level. 3. 3.1 Reasons to support the current Community exhaustion regime Consumers

3.1.1 The Committee believes that European consumers currently demand a certain level of quality and post-sales services, as well as competitive prices, and this is recognised in European law. But above all they expect to pay for what they believe they are getting. Sometimes producers of trademarks use different designs or variations for different customers. For example, the most popular brands of toothpaste in Europe are mint-flavoured, whereas in Indonesia the most popular ones are clove-flavoured. Another example would be lubricating oils for engines, whose composition can vary depending on the climate where they are to be used. 3.1.2 Access to products that are not designed to meet the climatic and technical requirements of European consumers could pose a safety risk to them. It is important in this part of the opinion to point to the possible impact on health in Europe of parallel imports of pharmaceutical products, bearing in mind that the pharmaceuticals sector in Europe is subject to considerable surveillance to protect consumer health. 3.1.3 Availability: the current Community exhaustion regime ensures the availability of all types and ranges, not just those that are most in demand. Thus an official vendor of jeans, for example, will offer its customers all sizes, not just those of the largest section of the population. Post-sales services: European consumers expect a range of services to be provided by 3.1.4 producers, which are not available to them with parallel imports. A television bought from an unofficial supplier may not come with any installation services or repair guarantee. Moreover, the instructions accompanying imported products are usually in the language of the country in which the parallel importer acquired the product, not that of the consumer. 3.1.5 Counterfeited and pirated products: The routes used by parallel importers are often the same as those used for pirated products. An international exhaustion regime might trigger an increase in pirated products in the EU, the harmful effects of which for the proper working of the single market were confirmed in the replies to the green paper. As indicated in point 2.4.3.6 above, the harmful effects on European economic growth would also have repercussions for consumers. The importers of such products are responsible for proving that they meet Community standards. It is also important to highlight the serious consequences of counterfeiting and piracy for consumer protection and public health and safety. Given that some counterfeited/pirated products are in everyday use, the risks are frightening. Among the most significant cases uncovered by the European Commission in

CES 42/2001 ES/PM/GW/vh

.../...


-74 1999 were 530 000 counterfeit toothbrushes, 21 tonnes of counterfeit rice and energy drinks. The quality and origin of these products evade any form of control by the Community or Member State authorities.

3.1.6 Prices: Advocates of parallel trading cite reduced prices as almost the sole argument in its favour. In fact, the NERA study has now shown that price reductions are negligible (between 0 and 2%) and that they tend to disappear in the long run. The NERA study also found that producers would lose up to 35% of earnings, resulting in less investment in research and development for European products. But European producers must innovate constantly in order to compete and provide more added value to consumers. This added value is increasingly to be found in the "intellectual" content of the brand (whether this is a technological or image-related innovation), which means that it is increasingly important for this intellectual property to be protected. 3.2 Community legislation

3.2.1 Keeping the current Community exhaustion regime would not lead to any change in Community legislation governing registered trademarks or any other industrial or intellectual property rights, particularly regulations or directives. 3.3 Single market

3.3.1 The Community exhaustion regime is a natural part of the single market, in which obstacles to free circulation of goods and people are being removed and the economies of the Member States are converging. The purpose of EU competition law is to prevent any obstacles to integration of the 3.3.2 market, including obstacles to consumers' freedom to buy what they want wherever they want in the EU. Competition law also provides an appropriate framework in which companies that consider themselves threatened or discriminated against in the face of potential dominant market positions can lodge any complaints. 3.3.3 The current Community exhaustion regime provides assurance to producers and suppliers with respect to investment in research and development for new products. 3.4 Trade relations with third countries

The first point to bear in mind is that the Community exhaustion regime is a natural 3.4.1 part of the single market in which the Member State economies converge and which seeks to remove obstacles to the free movement of goods.

4

1999 annual report on the Community's customs operations concerning piracy and counterfeiting.

CES 42/2001 ES/PM/GW/vh

.../...


-83.4.2 On a global level, there can be no valid comparison between European integration and the attempt to remove barriers to the free movement of goods on the one hand, and the WTO process on the other. Neither is there a parallel political process working to reduce existing differences between the EU and third countries. 3.4.3 Trademarks are important at international level. European companies competing on the world market must face companies with considerably lower production costs. The Community exhaustion regime offers a degree of protection to these companies and to non-European companies working within the single market. 3.4.4 An international exhaustion regime would mean that a trademark established in the EU would be unable to penetrate the markets of developing countries using marginal prices, since these products would immediately return to the EU, destroying the base market. Companies pursuing a marginal price strategy would have to choose between not exporting, or removing production from the EU to lower-cost markets. 3.4.5 It must also be borne in mind that parallel imports from third countries can have a significant deterrent effect on production and investment in innovation within the EU. This would probably result in reduced European exports and greater incentives to shift production to lower-cost locations than the EU. 3.4.6 EU competition law5 represents the best way of dealing with possible abuses by certain companies. Brussels, 24 January 2001. The President of the Economic and Social Committee The Secretary-General of the Economic and Social Committee

G�ke Frerichs

Patrick Venturini

5

Especially the following articles: Treaty Article 82 on abuse of dominant positions.

CES 42/2001 ES/PM/GW/vh


