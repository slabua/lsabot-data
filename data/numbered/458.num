EUROPEAN PARLIAMENT
1999
Session document
2

004

C5-0024/1999 19/07/1999

***II COMMON POSITION
Subject : COMMON POSITION (EC) No /1999 ADOPTED BY THE COUNCIL ON 12 JULY 1999 WITH A VIEW TO THE ADOPTION OF DECISION No /1999/EC OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL AMENDING THE BASIC DECISION RELATING TO THE SOCRATES PROGRAMME SO AS TO INCLUDE TURKEY AMONG THE BENEFICIARY COUNTRIES (COD 1996/0130)

EN

EN





COUNCIL OF THE EUROPEAN UNION

Brussels, 16 July 1999 (OR. f) 8076/1/99 REV 1 LIMITE NT 12 EDUC 33 SOC 168 JEUN 26 CODEC 249

Interinstitutional File: 96/0130 (COD)

COMMON POSITION (EC) No

/1999

ADOPTED BY THE COUNCIL ON 12 JULY 1999 WITH A VIEW TO THE ADOPTION OF DECISION No /1999/EC OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL AMENDING THE BASIC DECISION RELATING TO THE SOCRATES PROGRAMME SO AS TO INCLUDE TURKEY AMONG THE BENEFICIARY COUNTRIES

8076/1/99 REV 1

MO/mmk

EN


DECISION .../1999/EC OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL of amending the basic Decision relating to the Socrates programme so as to include Turkey among the beneficiary countries

THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION, Having regard to the Treaty establishing the European Community, and in particular Articles 149 and 150 thereof, Having regard to the proposal from the Commission 1 , Having regard to the Opinion of the Economic and Social Committee 2 , Having regard to the opinion of the Committee of the Regions 3 , Acting in accordance with the procedure laid down in Article 251 of the Treaty 4 ,

1 2 3 4

OJ C 186, 26.6.1996, p. 8. OJ C 158, 26.5.1997, p. 74. Opinion delivered on 3 June 1999 (not yet published in the Official Journal). Opinion of the European Parliament of 25 February 1999 (OJ C 153, 1.6.1999, p. 19), Common Position of the Council of ... (not yet published in the Official Journal) and Decision of the European Parliament of ... (not yet published in the Official Journal). MO/mmk EN

8076/1/99 REV 1


Whereas: (1) Decision 819/95/EC of the European Parliament and of the Council of 14 March 1995 establishes the Community action programme Socrates 1 in which Turkey does not participate; (2) Turkey is an associated country whose links with the Community have been substantially bolstered with the entry into force of the final phase of Customs Union; (3) The economic and trade links instituted by the Customs Union should be strengthened by closer cooperation in the field of education, training and youth; (4) A considerable amount of time needs to be allowed, on the one hand, between the amendment of the Decision establishing the said programme, which is the subject of this Decision, enabling it to be opened up to Turkey and the end of the negotiations on the arrangements (particularly financial arrangements) for Turkey's participation and, on the other, between the end of those negotiations and its actual participation; (5) The principle of such an opening-up, however, apart from giving a tangible sign of the European Union's oft-repeated willingness to develop sectoral cooperation with Turkey, makes it possible to undertake preparatory measures and measures to increase awareness, with a view to full participation in the said programme or in the future framework programme which is currently being drawn up, HAVE DECIDED AS FOLLOWS:

1

OJ L 87, 20.4.1995, p. 10. MO/mmk EN

8076/1/99 REV 1


Article 1 The second sentence of Article 7(3) of Decision 819/95/EC shall be replaced by the following: "This programme shall be open to the participation of Cyprus, Malta and Turkey on the basis of additional appropriations in accordance with procedures to be agreed with the countries in question, taking as a starting-point the rules applied to the European Free Trade Association countries, and in compliance with the provisions of Article 3 of the current Financial Regulation.". Article 2 This Decision concerns full or partial participation at the earliest possible date by Turkey in the Socrates programme in its current form, to the extent permitted by negotiations, as well as the launch of preparatory measures or measures to increase awareness with a view to such participation or to that provided for under the future framework programme (2000-2004). Article 3 The purpose of the participation of Turkey in the Socrates programme is to enable genuine exchanges to take place between young people from both sides and the staff accompanying them, while respecting their linguistic, educational and cultural diversity, in accordance with Article 149(1) of the Treaty, and the rights of minorities.

8076/1/99 REV 1

MO/mmk

EN


Article 4 The European Parliament shall be kept informed of the various measures taken to implement this Decision. Article 5 This Decision shall enter into force on the day of its publication in the Official Journal of the European Communities. Done at Brussels,

For the European Parliament The President

For the Council The President

8076/1/99 REV 1

MO/mmk

EN



EUROPEAN UNION THE COUNCIL

Brussels, 16 July 1999 (12.07) (OR. f) 8076/1/99 REV 1 ADD 1 LIMITE

Interinstitutional File No 96/0130 (COD)

NT EDUC SOC JEUN CODEC

12 33 168 26 249

COMMON POSITION ADOPTED BY THE COUNCIL ON 12 JULY 1999 WITH A VIEW TO ADOPTING A DECISION OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL AMENDING THE BASIC DECISION RELATING TO THE SOCRATES PROGRAMME TO INCLUDE TURKEY AMONG THE BENEFICIARY COUNTRIES

STATEMENT OF THE COUNCIL'S REASONS

8076/1/99 REV 1 ADD 1 DG E II

fel/MM/ldb

EN 1


I.

INTRODUCTION 1. On 14 May 1996, the Commission forwarded to the Council a proposal for a Decision amending the basic Decision relating to the Socrates programme to include Turkey among the beneficiary countries 1. The proposal is based on Articles 149 and 150 of the Treaty. 2. The European Parliament delivered its Opinion on 25 February 1999 2. The Economic and Social Committee delivered its Opinion on 20 March 1997 3. The Committee of the Regions delivered its Opinion on 3 June 1999 4. 3. On 12 July 1999, the Council adopted its common position pursuant to Article 251 of the Treaty.

II.

AIM OF PROPOSAL 4. The purpose of the proposal is to include Turkey among the countries which may benefit from the Socrates programme.

III.

ANALYSIS OF THE COMMON POSITION 5. 6. The Council has accepted the Commission proposal. In its Opinion, the European Parliament proposed 6 amendments. The Council decided to accept amendments 1-4 and 6 in their entirety. The Council was able to accept amendment 5 in part, but agreed to change the reference to minorities. Its view, supported by the Commission, was that the wording proposed by the European Parliament could give the impression that specific quotas were a possibility.

1 2 3 4

OJ C 186, 26.6.1996, p. 8.

Not yet published in the Official Journal.
OJ C 158, 26.5.1997, p. 74.

Not yet published in the Official Journal. fel/MM/ldb EN 2

8076/1/99 REV 1 ADD 1 DG E II






