EUROPEAN PARLIAMENT
Scientific and Technological Options Assessment

NEWSLETTER
January 2004
Editorial Alongside the political dimension, technology issues were also highlighted. STOA was formally represented by displays of its work, both in the Summit exhibition and at a special conference on Science in the Information Society, organised by CERN as a pre-summit opening event. It was particularly appropriate that CERN was the venue, as their computer networking research in the 80s and 90s, paved the way for the universal operating systems that still power the net today. One of the highlights of the conference was a presentation by Sir Tim Berners-Smith, who led the CERN work and who now runs the World Wide Web consortium at MIT. He gave us an insight into the next stage of web software development now being pioneered in their laboratories.

World Summit on the Information Society
By Malcolm Harbour, STOA Vice-Chair In December 2003, over 10,000 people travelled to Geneva from across the world to participate in the first World Summit on the Information Society. The focus of their interest was the need to spread the benefits of new information technologies across the globe, and to consider ways of bridging the "digital divide" between developed and developing countries. The political significance of the Summit can be gauged by the official participation of over 300 countries at Head of State or ministerial level. For the European Union, the Presidency was represented by Lucio Stanca, the Italian Minister for Innovation and Technologies, and by Commissioner Erkki Liikanen. The European Parliament was formally included in the EU Delegation and was represented by Malcolm Harbour MEP (STOA vice-president), Marco Cappato, and Paul R�big MEP (STOA panel member). Mrs Karin Junker MEP, a member of the ACP, also joined the EP team. In any vast international undertaking, most of the key political work is done beforehand. The EU played a key role in brokering the Summit declaration which sets out important principles for the global information society. It clearly states that human rights should be respected and that, open access should be unconditional. It encourages the use of the Internet to empower disadvantaged groups. Legal frameworks that stimulate competition and technological diversity are strongly endorsed - an EU political priority.

RSIS plenary session

Photo: STOA

A key theme for the CERN conference was the need to stimulate more information technology research across the world, and to invest in high quality networks, so that researchers can share and exploit their discoveries. Researchers from developing countries told us about the success of projects designed to tackle local problems, and of the importance of building local research capabilities to generate more local solutions. But they spoke of their frustration in getting access to cheap plentiful high quality communications networks. "Bandwidth" costs can be 5 to 10 thousand times higher than European researchers pay!

PE 338.676 - STOA Newsletter - January 2004


PE 338.676

STOA Newsletter - January 2004

In his presentation to the conference, Malcolm Harbour MEP, emphasised the strong political support that the European Parliament had given to Information Society research and particularly to research networking. He advocated more developed world support for networking and research in developing countries, to give them the tools needed to deliver the benefits of IT to their local communities. STOA's participation in the first WSIS showed that the European Parliament has a key role to play in establishing global policies that will shape the Information Society. The next summit will be held in Tunis in November 2005. The Parliament should plan to have a stronger presence and to take a lead in promoting a stronger role for Parliamentarians in the Summit preparations.

Many people, including VIPs, visited the two stands, expressing their interest and meeting the STOA delegation on this occasion.

STOA stand at ICT4D "While technology shapes the future, ultimately it is people who shape technology". Kofi Annan, Secretary General of the United Nations.

Photo: STOA

STOA participation in WSIS/RSIS/ICT4D
STOA was present at these events and managed also two presentation stands: one on the RSIS (alone) and one (shared with the Commission) on the ICT4D exhibition.

For completeness, bear in mind that some details and also further information are available on the events own websites (documents, pictures and multimedia information): � WSIS - World Summit on the Information Society 10-12 Dec: main event, organised by the UN (http://www.itu.int/wsis/); � RSIS - Role of Science in the Information Society 8-9 Dec: "related conference", at CERN (http://rsis.web.cern.ch/); � ICT4D - ICT for Development platform 9-13 Dec: "side event" of WSIS (www.ict-4d.org). This was a very interesting experience for all staff and, even if details in the organisation can be improved, this participation was very successful. Roger-Marie Dubois STOA Secretariat Some of most relevant recent STOA studies "Evaluation of the use of new technologies in order to facilitate democracy in Europe" (STOA Project 2001/04/01) "The use of open source code in public administration" (STOA Project 2002/07/02) "Protection and implementation of intellectual property rights in security technologies for digital media" (STOA Project 2002/13/02)

STOA stand at RSIS Photo:STOA General information about EP and STOA was available, as well as relevant STOA studies. It is worth mentioning that the collaboration was excellent with other European institutions and with EPTA (TA-SWISS).

"Healthcare and ICT in Developing Countries" (STOA Project 2002/14/01) "Education and ICT in Developing Countries" (STOA Project 2002/14/02)

PE 338.676 - STOA Newsletter - October 2003

2


PE 338.676

STOA Newsletter - January 2004

STOA News

STOA Annual Lecture 4 November 2003
In November 2002, STOA organised its first "Annual Lecture". This event is designed to help bring science and policymaking closer together and involves the bringing of a world-famous scientist to the European Parliament to talk about what he or she is doing and to explain the value and meaning of the science to the layman and to policymakers. In 2002 STOA invited Professor Kary Mullis, who won the Nobel Prize for his discovery of the polymerase chain reaction - a discovery which ultimately enabled the test for the HIV retrovirus in the blood. This year's lecturer was Professor Wolfgang Ketterle, from the Massachusetts Institute of Technology, who won the Nobel Prize for his work in exploring the realms of the ultra-cold and the production of the "Bose-Einstein condensate". At first sight, this is not a subject which is guaranteed to grip the attention and fire the imagination of the layman, or indeed the policymaker. The subject appears abstruse and, like much pure scientific research, of apparently little relevance to normal life. Those who attended the lecture, held in Brussels in the main hemicycle on Tuesday 4th November, were held spellbound by Prof. Ketterle's clear and enthusiastic explanation of his work. We were first given an explanation of temperature - an apparently simple concept to most of us, except for its true meaning, namely a measure of the amount of kinetic energy in an object, in this case, a gas. We were given an excellent picture of what is meant by "Absolute zero" (minus 273� Celsius, or 0� Kelvin). It is an unreachable state in the same way that it is impossible to completely empty a swimming pool - there will always remain the minutes trace of moisture. It is likewise impossible to empty an object or a gas of all kinetic energy. Nonetheless, we can come very close to it and Professor Ketterle and his team have come closer than anyone. The mechanics of the process are complex, but the Professor led us through the technology in a very clear and concise way, likening the magnetic fields used to help the cooling process to the blowing onto the surface of a cup of tea or coffee.

Prof. Wolfgang Kettlerle, speaker of the 2nd STOA Annual Lecture
Photo: STOA

Lasers are also used to reduce the gas, in this case sodium, to within a whisker of absolute zero. Chandra Bose and Einstein predicted many years ago that at such low temperatures, a "condensate" would form at atomic level which would behave in a peculiar way because of the "waveparticle duality" displayed by fundamental particles at the quantum level. Professor Ketterle evoked the excitement of working with a young and highly talented team of scientists from all over the world and the thrill of attaining the Bose-Einstein condensate after many hundreds of hours (usually late at night) in the laboratory. He evoked the sheer thrill and excitement of pure science very effectively and in a question and answer session afterwards provided further details of his work and explained the use of pure science comprehensively and, more important, comprehensibly. The Lecture was followed by a reception given by Prof. Trakatellis, the Chair of STOA and, at a later dinner attended by members of Parliament, members of the European Association of Scientific Academies and EP staff, Prof. Ketterle spoke about pure research and the need for continued support for it. STOA hopes to continue to attract such excellent scientists and speakers in the future and hopes that the STOA Annual Lecture will contribute to a greater understanding between scientists and politicians of their work and respective roles in s o c ie t y . Graham Chambers DG2

PE 338.676 - STOA Newsletter - October 2003

3


PE 338.676

STOA Newsletter - January 2004

EPTA news

EPTA Conference 2003
This year's EPTA Conference and Council meeting (27th and 28th October 2003) was hosted by TA SWISS, who hold the 2003 EPTA Presidency, at the Swiss Federal Parliament in Bern. Participants were scientists, parliamentarians and officials of TA bodies from many European countries. The EPTA Conference dealt with research involving human beings. Presentations came from the Swiss Centre for Technology Assessment (TA SWISS), the German Office of Technology Assessment (TAB - Germany), the Comitato per la Valutazione delle Scelte Scientifiche e Tecnologiche (VAST - Italy), the Parliamentary Office of Science and Technology (POST - United Kingdom), the Norwegian Board of Technology (Norway), the Rathenau Institute (Netherlands) and STOA (European Parliament), for which Theo Karapiperis of the ITRE Committee gave an overview of the work of the EP special committee on the human genome.

After short statements from the researchers, a further part of the conference was devoted to an exchange of views with representatives of clinical research from the University of Bern.

Photo: TA-SWISS

In the EPTA Council meeting the parliamentarian representatives and directors of the TA bodies were welcomed by Dr. J. Randegger and Dr. K. Hug, member and Chair respectively of the TASwiss Steering Committee. There followed a tour de table report from EPTA members and associates about their current and future programmes. Examples of the topics being addressed are "The vulnerability of nuclear installations to terrorist attack" (POST, UK), Genetically-modified organisms (Flemish TA), and oil-depletion scenarios over 10-20 years (Danish Teknologiradet). The members heard that Spain is considering the setting up of a TA unit and affiliation with EPTA and that the Swedish Riksdag is also planning to establish a TA unit. Graham Chambers / Rolf Meyer DG2 / STOA Secretariat

Photo: TA-SWISS

A wide range of topics were addressed in the presentations such as genetic database ("Biobank"), preimplantation genetic diagnosis, organ donation and transplantation medicine, xenotransplantation, stem cells, protection of biotechnologies. In connection with this a broad spectrum of approaches was used: scientific overview, international comparison of legislations, public survey, ethical inquiry, consensus conference. In all projects main objectives were the analysis of possible benefits and risks, public concerns and ethical aspects, and options for legislation.

STOA administration Head of Division: Mr. Paul Engstfeld pengstfeld@europarl.eu.int Dr. Rolf Meyer romeyer@europarl.eu.int Mr. Roger-Marie Dubois rmdubois@europarl.eu.int Opinions expressed in this STOA Newsletter do not necessarily represent the official view of the European Parliament. STOA Unit
http://www.europarl.eu.int/stoa/default_en.htm

Edited by Malcolm Harbour MEP, STOA Vice-chair.

PE 338.676 - STOA Newsletter - October 2003

4


