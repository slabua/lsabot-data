DRAFT CHARTER OF FUNDAMENTAL RIGHTS OF THE EUROPEAN UNION
fundamental.rights@consilium.eu.int Brussels, 22 June 2000 (22.06)

CHARTE 4381/00

CONTRIB 241

COVER NOTE Subject : Draft Charter on fundamental rights of the European Union

Please find hereafter a contribution by The Danish Centre for Human Rights. 1 2

________________________

1

2

Det Danske Center for Mennekeretigheder/The Danish Centre for Human Rights Studiestraede 38 DK-1455 Copenhagen V T�l. 45 33 30 88 88 Fax 45 33 30 88 00 This text has been transmitted in English only. mh JUR

CHARTE 4381/00

EN

1


Dear Sir or Madam, The undersigned human rights institutes and centres welcome the formulation of an EU Charter on Fundamental Rights as an important dimension in the European development of human rights standards. In our capacity as experts in the field of human rights we believe, however, that it is imperative to raise the following issues for further consideration. The institutes and centres are primarily concerned with the fact that the rapid pace of the preparations for the drafting of the EU Charter preempts a much needed general debate on the implications of the Charter. Instead, discussions at the European level should be allowed to mature in a more steady pace, thereby creating a platform for a broad popular involvement in the issues at stake. The time frame should not be seen as an end in itself, nor as a valid justification for not securing an open and participatory public debate. The institutes recognize the use of the Internet as a means to create transparency. However, it should be borne in mind that it is still only a fragmented part of the European population who has access to this technology. The use of the Internet should therefore be complemented by other more widely accessible means of communication. It is decisive for the future legitimacy of the European Union that there is a broad awareness of and support to the Charter. The creation of an EU Charter seems to involve a risk of legal uncertainty for the individual. It should be kept in mind that the European Convention on Human Rights not only defines individual rights subject to the jurisdiction of the Court, but also forms part of national law in all EU Member States. It is thus crucial to avoid a situation where powers, if vested in the European Court of Justice, would compete with the European Court of Human Rights. The institutes and centres consider it to be of the utmost importance that the Charter reflects the interdependent and indivisible nature of all human rights. The current normative framework after the Amsterdam Treaty is not fully satisfactory in this respect. The importance attached to one regional human rights treaty, i.e. the European Convention on Human Rights, could lead to the unfounded interpretation that EU organs would not be bound by all human rights treaties ratified by its Member States. However, a promising sign of the interdependence and indivisibility not having been forgotten is the discussion of the horizontal article, tentatively referred to as article 49, which is intended to clarify that the EU Charter cannot be used to deviate from or restrict fundamental rights which have been guaranteed in international law, including existing human rights treaties. In the process of drafting the EU Charter, it has been noted that attention has been paid not only to civil and political rights but also to other human rights, including economic and social rights. However, upon studying the catalogue of proposed economic and social rights it is observed that, on the one side, the catalogue is expanded considerably in respect of the protection of rights related to the labour market, whereas, on the other side, it does not include all the traditional rights, e.g. the right to housing. In this way, the EU runs the risk of down-scaling fundamental economic and social

CHARTE 4381/00 JUR

mh

EN

2


rights and, at the same time, upgrading detailed regulations, i.e. primarily within the realms of ILO conventions. Furthermore, in relation to economic and social rights the institutes and centres express their concern regarding proposals which would reduce economic and social rights to mere goals or policies in the EU Charter. In relation to non-discrimination it is positive that the draft Charter includes a prohibition against discrimination in all areas and not only in relation to civil and political rights as opposed to article 14 of the European Convention on Human Rights. At the same time, however, it is problematic that the provision limits the scope of this protection - unlike Article 14 - by listing an exhaustive number of grounds on which discrimination should be prohibited. Thus, the draft does not offer a general protection against discrimination on grounds of nationality as does the ECHR. This should be changed as the charter should not restrict a fundamental right guaranteed by the ECHR, nor word it in more limited terms. Though, the draft Charter does offer a certain degree of protection against discrimination on grounds of nationality, in principle, this only relates to discrimination against citizens of other EU Member States - while human rights protection against (all) discrimination applies to "everyone". It seems unclear why a prohibition against discrimination on grounds of nationality in the EU Charter should be limited. The inalienable right of any person not to be discriminated against on any ground ought to be included in the EU Charter on Fundamental Rights. Finally, the institutes and centres would like to point to the fact that the provision on the right to asylum as currently drafted could lead to discrimination on the basis of nationality. In general terms as well as in relation to the above mentioned draft article 49, it is crucial that there is consistency between the Charter and the 1951 Convention relating to the Status of Refugees and other international instruments regulating the protection of refugees. The institutes and centres will follow the process carefully and will, at the same time, make ourselves available for any further discussions with relevant bodies working on the draft EU Charter. It is of the utmost importance that Europe when embarking upon such an ambitious project is truly considering all aspects of the matter. This should be done in order not to divert or downgrade the human rights protection as it has developed since the adoption of the international and regional instruments in the aftermath of the Second World War. The human rights development is at a critical junction these years, and an EU Charter can, if carefully thought out, constitute an important contribution to furthering the protection of the human dignity.

CHARTE 4381/00 JUR

mh

EN

3


On behalf of the Nordic Human Rights Institutes and Centres Yours sincerely Morten Kjaerum Director, The Danish Centre for Human Rights Gudmundur Alfredsson, Director, Raoul Wallenberg Institute Martin Scheinin, Director, �bo Akademi University Institute for Human Rights Nils A. Butenschon, Director, Norwegian Institute of Human Rights Bjarney Fri�riksd�ttir, Director, The Icelandic Human Rights Center. Identical letters are sent to: Denmark's Foreign Minister, Niels Helveg Petersen Denmark's Minister of Justice, Frank Jensen Erling Olsen, the Danish Parliament Irli Plambech, the Danish Parliament . Det Danske Center for Menneskerettigheder/ The Danish Centre for Human Rights Studiestraede 38 DK-1455 Copenhagen V Tel: (+45) 33 30 88 88 Fax: (+45) 33 30 88 00

________________________

CHARTE 4381/00 JUR

mh

EN

4


