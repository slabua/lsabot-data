EUROPEAN PARLIAMENT
DIRECTORATE-GENERAL FOR RESEARCH DIRECTORATE A DIVISION FOR INTERNATIONAL AND CONSTITUTIONAL AFFAIRS

FACTSHEET
POLAND 1. The acquis 1 The acquis in this chapter does not require transposition into national legislation or any particular implementation and enforcement measures. It consists of general industrial competitiveness policy guidelines at horizontal and sector-specific level. These are not legally binding on the Member States. Many of the ECSC-based provisions are no longer applicable and the Treaty has expired. In the negotiations, the candidates have been asked to present industrial policy and restructuring strategies in order to assess whether they are in line with the principles, particularly regarding privatisation and restructuring, set out in the Council decision on the implementation of a Community Action Programme to Strengthen the Competitiveness of European Industry (94/413/EC). Industrial policy is closely linked to the chapters on free movement of goods, competition policy and social policy and employment. 2. The negotiations The chapter has been closed with ten countries and provisionally closed with Bulgaria and Romania. No transitional arrangements have been requested. Chapter opened second half of 1998 Status closed in December 2002 (provisionally closed in first half of 1999) Transitional arrangements: none Chapter 15 - Industrial policy

1

Information largely drawn from the European Commission, DG Enlargement http://europa.eu.int/comm/enlargement/negotiations/index.htm

WIP/2002/11/0242

1


3. Position of the European Parliament In its resolution of 4 October 20001, Parliament encourages Poland to speed up privatisation and restructuring of the major State undertakings, above all in the steel, energy, chemical, armaments, sugar and distilleries sectors, and recommends that consideration be given to the possible effects of restructuring on the Polish and European labour markets. In its resolution of 5 September 20012, Parliament: � reminds the Polish authorities of the need to continue industrial restructuring (especially in the steel, chemical, and railways sectors), never underestimating its social impact; � considers that, although economic development in Poland has made progress towards restructuring its industrial base, further efforts are still required; notes that Poland is still benefiting from fairly balanced development based on several centres of development and is thus experiencing less severe regional disparities than some Member States and candidate countries; recommends that priority continue to be given to such a multicentric development approach. In its resolution of 13 June 20023, Parliament points to the importance of a stable, transparent legal framework to encourage foreign direct investment and calls on the Polish authorities to complete the related legislation by improving legal certainty. 4. Latest Assessment by the European Commission 4 In its 1997 Opinion the Commission concluded that the key challenge Poland faced under this chapter was to restructure its traditional industries. Since the Opinion, good progress has been made in developing industrial policy, and restructuring is underway. Poland has achieved a high degree of alignment with the acquis and administrative capacity is at a simpler level. Poland's policy towards industry generally complies with the concepts and principles of EC industrial policy, i.e. it is market-based, stable and predictable. Negotiations on this chapter have been provisionally closed. Poland has not requested any transitional arrangements under this chapter. Poland is generally meeting the commitments agreed in the negotiations in this field. In order to complete preparations for membership, Poland's efforts now need to focus on seeing through the restructuring process, notably with respect to steel and other traditional industries, completing alignment with the acquis and more broadly ensuring that a stable transparent environment, for businesses and particularly investors is maintained and enhanced. Great care will have to be taken that the policy of restructuring is implemented in a manner which conforms to the competition and state aid acquis so as to create efficient competitive firms. January 2003
1 2

Resolution on the state of negotiations with Poland, � 9: A5-0246/2000 Resolution on the state of negotiations with Poland, �14 & 20: A5-0254/2001 3 Resolution on the state of the enlargement negotiations, � 123: A5-0190/2002 4 European Commission, Regular Report on Poland 2002, pp. 94 & 95: http://www.europa.eu.int/comm/enlargement/report2002/pl_en.pdf

WIP/2002/11/0242

2


