EUROPEAN PARLIAMENT
1999
��� � � � � � � � ��

2004

Session document

C5-00181/2002
2001/0127(COD)

EN
24/04/2002

Common position
with a view to the adoption of a Regulation of the European Parliament and of the Council amending Council Regulation (EC) No 577/98 on the organisation of a labour force sample survey in the Community

Doc.6241/1/02 + ADD1 SEC(2002)0467

EN

EN



COUNCIL OF THE EUROPEAN UNION

Luxembourg, 15 April 2002 (OR. en) 6241/1/02 REV 1

Interinstitutional File: 2001/0127 (COD)

ECOFIN 54 SOC 62 CODEC 183

LEGISLATIVE ACTS AND OTHER INSTRUMENTS Subject : Common position adopted by the Council on 15 April 2002 with a view to the adoption of a Regulation of the European Parliament and of the Council amending Council Regulation (EC) No 577/98 on the organisation of a labour force sample survey in the Community

6241/1/02 REV 1 DG G

CR/mm

EN


REGULATION (EC) No

/2002 OF THE EUROPEAN PARLIAMENT of

AND OF THE COUNCIL

amending Council Regulation (EC) No 577/98 on the organisation of a labour force sample survey in the Community

THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION, Having regard to the Treaty establishing the European Community, and in particular Article 285(1) thereof, Having regard to the proposal from the Commission 1, Having regard to the Opinion of the Economic and Social Committee 2, Acting in accordance with the procedure laid down in Article 251 of the Treaty 3,

1 2 3

OJ C 270 E, 25.9.2001, p. 23. OJ C 48, 21.2.2002, p. 67. Opinion of the European Parliament of 11 December 2001 (not yet published in the Official Journal), Council Common Position of (not yet published in the Official Journal) and Decision of the European Parliament of (not yet published in the Official Journal). CR/mm DG G

6241/1/02 REV 1

EN

1


Whereas: (1) Council Regulation (EC) No 577/98
1

lays down the basic provisions for a labour force

sample survey, designed to provide comparable statistical information on the level and pattern of, and trends in, employment and unemployment in the Member States. (2) An expeditious implementation by all Member States of the continuous labour force sample survey required by Regulation (EC) No 577/98 was considered a priority action in the Action Plan on EMU Statistical Requirements endorsed by the Council on 19 January 2001. (3) Sufficient time has now passed since Regulation (EC) No 577/98 took effect to allow all Member States to make the arrangements and commitments needed fully to implement that Regulation. However not all Member States have made such arrangements and commitments. Therefore, the derogation that allows Member States to limit themselves to an annual survey should be subject to a time limit. (4) The measures necessary for the implementation of Regulation (EC) No 577/98 should be adopted in accordance with Council Decision 1999/468/EC of 28 June 1999 laying down the procedures for the exercise of implementing powers conferred on the Commission 2. (5) (6) Regulation (EC) No 577/98 should therefore be amended accordingly. The Statistical Programme Committee, established by Council Decision 89/382/EEC, Euratom 3 has been consulted in accordance with Article 3 of that Decision,

1 2 3

OJ L 77, 14.3.1998, p. 3. OJ L 184, 17.7.1999, p. 23. OJ L 181, 28.6.1989, p. 47. CR/mm DG G

6241/1/02 REV 1

EN

2


HAVE ADOPTED THIS REGULATION: Article 1 Council Regulation (EC) No 577/98 is hereby amended as follows: 1) in Article 1, the second paragraph shall be replaced by the following: "The survey shall be a continuous survey providing quarterly and annual results; however, during a transitional period not extending beyond 2002, Member States which are unable to implement a continuous survey shall instead carry out an annual survey, to take place in the spring. By way of derogation, the transitional period shall be extended (a) (b) until 2003 for Italy; until 2004 for Germany under the condition that Germany provide quarterly substitute estimates for the main labour force sample survey aggregates as well as annual average estimates for some specified labour force sample survey aggregates.";

6241/1/02 REV 1 DG G

CR/mm

EN

3


2)

Article 8 shall be replaced by the following: "Article 8 Procedure 1. The Commission shall be assisted by the Statistical Programme Committee instituted by Article 1 of Council Decision No 89/382/EEC, Euratom (*). 2. Where reference is made to this paragraph, Articles 5 and 7 of Decision 1999/468/EC (**) shall apply, having regard to the provisions of Article 8 thereof. The period laid down in Article 5(6) of Decision 1999/468/EC shall be set at three months. 3. The Committee shall adopt its rules of procedure. _______________ (*) OJ L 181, 28.6.1989, p. 47. (**) OJ L 184, 17.7.1999, p. 23.".

6241/1/02 REV 1 DG G

CR/mm

EN

4


Article 2 This Regulation shall enter into force on the day following that of its publication in the Official Journal of the European Communities.

This Regulation shall be binding in its entirety and directly applicable in all Member States. Done at Luxembourg, For the European Parliament The President For the Council The President

6241/1/02 REV 1 DG G

CR/mm

EN

5


COUNCIL OF THE EUROPEAN UNION

Luxembourg, 15 April 2002

Interinstitutional File: 2001/ 0127(COD)

6241/1/02 REV 1 ADD 1

ECOFIN 54 SOC 62 CODEC 183 STATEMENT OF THE COUNCIL'S REASONS Subject : Common Position adopted by the Council on 15 April 2002 with a view to the adoption of a Regulation of the European Parliament and of the Council amending Regulation (EC) No.577/98 on the organisation of a labour force sample survey in the Community

STATEMENT OF THE COUNCIL'S REASONS

6241/1/02 REV 1 ADD 1 DG G I

KG

EN

1


I. 1.

INTRODUCTION On 13 June 2001, the Commission forwarded to the Council a proposal for a European Parliament and Council Regulation amending Regulation (EC) No 577/98 on the organisation of a labour force sample survey in the Community.

2.

The above proposal is based on Article 285 of the Treaty according to which the procedure of co-decision with the European Parliament prescribed in Article 251 of the Treaty applies.

3.

The European Parliament approved the Commission proposal without amendment in first reading on 11 December 2001.

4. 5.

The Economic and Social Committee delivered its Opinion on 14 January 2002. On 15 April 2002 the Council adopted its common position pursuant to Article 251 of the Treaty.

II.

OBJECTIVE OF THE PROPOSAL The purpose of the proposal is to modify Regulation (EC) No. 577/98 to ensure that all Member States implement a continuous labour force sample survey. It will withdraw the possibility for Member States which have difficulty in implementing a continuous survey to carry out only an annual survey. The proposal also updates the comitology provisions in the Regulation in accordance with Council Decision 1999/468/EC of 28 June 1999.

6241/1/02 REV 1 ADD 1 DG G I

KG

EN

2


III. ANALYSIS OF THE COMMON POSITION The common position follows the Commission proposal, approved by the European Parliament, with the addition of derogations for Italy and Germany to enable them to make the technical preparations necessary to implement a continuous survey. In the case of Italy, the derogation is for one year, until the end of 2003. In the case of Germany, the derogation is for two years, until the end of 2004, on the condition that Germany provide quarterly substitute estimates for the main labour force sample survey aggregates as well as annual average estimates for some specified labour force sample survey aggregates. The provision of this information will protect the integrity of EU statistics during the transitional period in which Germany will not provide data from a continuous survey, by ensuring the availability of more frequent and more specific data than that currently provided under the annual survey.

IV.

CONCLUSION The Council considers that the modifications introduced in its common position are fully in line with the objectives of the proposed Regulation and provide for full implementation of the Regulation as soon as possible.

6241/1/02 REV 1 ADD 1 DG G I

KG

EN

3



COMMISSION OF THE EUROPEAN COMMUNITIES

Brussels, 23.04.2002 SEC(2002) 467 final 2001/0127 (COD)

COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT pursuant to the second subparagraph of Article 251 (2) of the EC-Treaty concerning the common position of the Council on the adoption of a European Parliament and Council Regulation amending Council Regulation (EC) N� 577/98 on the organisation of a labour force sample survey in the Community


2001/0127(COD) COMMUNICATION FROM THE COMMISSION TO THE EUROPEAN PARLIAMENT pursuant to the second subparagraph of Article 251 (2) of the EC-Treaty concerning the common position of the Council on the adoption of a European Parliament and Council Regulation amending Council Regulation (EC) N� 577/98 on the organisation of a labour force sample survey in the Community

1.

BACKGROUND Proposal submitted to the European Parliament and the Council (COM(2001)319, COD 2001/127) 13 June 2001 Economic and Social Committee opinion European Parliament's Opinion (first reading): Adoption of Council common position: 28 November 2001 11 December 2001 15 April 2002

2.

PURPOSE OF COMMISSION PROPOSAL Regulation n� 577/98 introduced the continuous labour force survey. Due to the difficulty to implement the continuous labour force survey at the same moment in all Member States, this Council Regulation allowed the Member States "...to carry out an annual survey, to take place in Spring" when they were not in a position to implement a continuous survey. Because of the considerable limitation in the usefulness of the labour force survey when individual Member States do not carry out a continuous survey, the proposed Regulation specifies that the transition period allowing Member States "...to carry out an annual survey, to take place in Spring" shall not extend beyond 2002. The Economic and Social Committee was in favour of the Commission's proposition and considered itself that the derogation allowing Member states to simply carry out an annual survey must be limited in time. The Committee accepted the proposed limit in time.

3. 3.1

COMMENTS ON THE COMMON POSITION General At its first reading the EP adopted the Commission's proposal without any amendment and asked to be consulted again when the Commission would change or replace its proposal. 2


3.2 3.2.1

Decisions on the European Parliament's amendments following the first reading. Accepted by the Commission and incorporated into the common position None

3.2.2

Accepted by the Commission but not incorporated into the common position (the Commission's position) None

3.3

New provisions introduced by the Council and the Commission's position. The Council asked for an extension of the transition period until 2003 for Italy and until 2004 for Germany. The Commission has accepted the request of the Council that "by way of derogation, � the transition period is extended until 2003 for Italy � the transition period is extended until 2004 for Germany under the condition that Germany provides quarterly substitute estimates for the main labour force sample survey aggregates as well as annual average estimates for some specified labour force sample survey aggregates." The Commission has accepted this request because during the transition period, Italy is able to provide quarterly estimates with reference to a single week each quarter on the basis of the labour force survey and Germany has confirmed the list of quarterly substitute estimates and annual average estimates to be provided.

3.4

Problems of comitology encountered during the adoption of the common position (and the position adopted by the Commission). None

4.

CONCLUSION In conclusion therefore the Commission agrees completely with the Council's common position.

3


