QUESTIONNAIRE TO THE ATTENTION OF

Mr Jan FIGEL
Commissioner designate attached to Commissioner Liikanen, responsible for Enterprise and Information Society

A. GENERAL QUESTIONS

B. SPECIFIC QUESTIONS


A. GENERAL QUESTIONS

I. Personal and professional 1. What aspects of your professional experience do you feel will be of particular relevance in your prospective role as Commissioner? II. Independence 2. How would you describe your obligation to be independent in the performance of your duties and how do you concretely envisage putting this principle into practice? 3. What importance do national interests have for you? 4. Do you hold an active position or a function in a political party? Are you prepared to give up this active position or function if you are nominated Commissioner? 5. Do you have, or have you recently had, any business, financial interests or any other commitment that might clash with your prospective duties? Are you prepared to disclose all your commitments? III. Future of the European Union 6. Please describe your vision of future European integration. Do you think that the draft Constitution drawn up by the Convention on the Future of Europe will allow the enlarged Union to function efficiently? 7. What is your opinion on the timetable to be applied for the adoption of the draft Constitution as presented by the Convention on the future of Europe? 8. What, in your view, will be the importance and role of the European Commission in a 25Member State European Union? 9. What are your views on enhancing the transparency of the legislative process at European level and, in particular, the level of public access to information? 10. In its last Monitoring Report on the accession countries, the Commission drew attention to delays in the transposition of the acquis communautaire in a number of areas. In this connection, what are your views on the application of the safeguard clauses included in the Accession Treaties? 11. What steps would you propose be taken to ensure closer compliance by Member States with Community rules, especially as regards implementation of Community legislation in national legal orders?


IV. Democratic accountability to the European Parliament 12. How do you envisage your accountability to the European Parliament? 13. From a strictly political standpoint, do you consider that, in case a hearing should lead to a negative conclusion, the concerned Commissioner designate should withdraw his/her candidacy? 14. How important do you consider interinstitutional cooperation (Commission-Parliament) to be, and what approach would you take to relations with Parliament? 15. In particular, how do you envisage relations between the Commissioner and the relevant parliamentary committees in a given area of competence? Do you consider the current practice of exchange of information between a Commissioner and the relevant parliamentary committees sufficient to ensure transparency? V. Commissioner's role and priorities during his/her term of office 16. What would your priorities be during your six-months term of office? 17. How do you envisage your role and responsibilities during this period, with particular reference to your relations with your co-Commissioner? 18. Are you a candidate for, or would you be interested in taking up, the post of Commissioner in the next Commission? If so, in what area? 19. What concrete measures do you consider necessary in order to ensure that the concept of gender mainstreaming is implemented in your specific policy area? Do you have a policy strategy, and what financial and human resources do you think are necessary to implement gender mainstreaming in your portfolio?


B. SPECIFIC QUESTIONS

Lisbon process 1. What action needs to be taken at both EU and Member State level for Europe to build on existing achievements and accelerate progress towards the Lisbon goals? Telecommunications 2. The EU's 1999 telecoms review grants the Commission a number of powers to ensure that Member States transpose and fully implement the EU legislation. Do you feel that these powers are adequate, and in what circumstances would you envisage using these powers in the future? Information Society 3. How can the development of an information society be used as a tool to combat social exclusion? What do you envisage the Commission could do to encourage the use of new communication technologies to help integrate disadvantaged groups into society?


