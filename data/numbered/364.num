ANNEXES

21

EUROPEAN COUNCIL BRUSSELS

CONCLUSIONS OF THE PRESIDENCY

24 and 25 October 2002

ANNEXES

Bulletin 28.10.2002

- EN -

PE 323.933



ANNEXES

23

ANNEX I RESULT OF WORK OF THE GENERAL AFFAIRS AND EXTERNAL RELATIONS COUNCIL Budgetary and financial issues:
(a) Overall level of allocations for Structural Operations

1.

The candidate States should intensify and speed up their preparations so as to ensure that they can provide the Commission with their application for assistance, programming documents and cohesion fund projects so that they can be adopted in early 2004. The Commission and the Member States will continue to provide all possible assistance to that effect. The Commission will ensure that the approval process for programming documents and requests for payments will be as swift as possible. In order to meet the considerable needs for new infrastructure in the fields of transport and environment identified in the candidate States, one third of the overall allocation for structural operations will be devoted to the cohesion fund. The payment on account foreseen under the acquis will be paid in the first year after accession at the rate of 16% of the total contribution of the structural funds over the period 2004-2006. The EU foresees payment appropriations in 2004 equivalent to 3% of average annual commitments under the Structural Funds and 3% of the Cohesion Fund commitment.2

2.

3.

(b) Overall level of allocations for Internal Policies

4.

In view of Lithuania's confirmation that Unit 1 of the Ignalina Nuclear Power Plant will be closed before 2005 and of its commitment that Unit 2 will be closed by 2009, a programme for supporting activities relating to the decommissioning of the INPP will be established. The commitment appropriations foreseen for this programme will be 70 million euros3 for each of the years 2004 to 2006. Recognising that the decommissioning of the Ignalina Nuclear Power Plant will have to continue beyond the current financial perspective and that this effort represents for Lithuania an exceptional financial burden not commensurate with its size and economic strength, the European Union in solidarity with Lithuania confirms its readiness to provide adequate additional Community assistance to the decommissioning effort beyond 2006.

2 3

Payments during 2004 for structural operations in the new Member States will not affect the payments that must be included in the 2004 budget for structural operations of the current member States. Estimated figures to be revised as appropriate on the basis of the spending profile for the decommissioning activities of the Ignalina and Bohunice decommissioning funds. Phare commitments are above expectations for Ignalina and below expectations for Bohunice.

Bulletin 28.10.2002

- EN -

PE 323.933


24

ANNEXES

5.

To continue the pre-accession aid planned under PHARE for the decommissioning of the Bohunice Nuclear Power Plant in Slovakia, 20 million euros3 in commitment appropriations are foreseen for each of the years 2004 to 2006. Action to support institution building in the new Member States started under PHARE will be continued until 2006. To this end, commitment appropriations amounting to 200 million euros in 2004, 120 million euros in 2005 and 60 million euros in 2006 are foreseen. The levels for heading 3 should be fixed so that the main items of priority expenditure under this heading are maintained and sufficient means are provided for the extension of existing programmes to the new Member States.
Cyprus: Programme for the northern part

6.

7.

(c)

8.

With a view to the implementation of a political settlement in Cyprus, a programme will be established by the Council, especially to enable the northern part of the island to catch up. The total commitment appropriations foreseen will be 39 million euros in 2004, 67 million euros in 2005 and 100 million euros in 2006.
European Development Fund

(d)

9.
( e)

New Member States will accede to the EDF as of the new Financial Protocol (10th EDF).
European Coal and Steel Community

10.

New Member States will participate in the Research Fund for Coal and Steel from the day of accession. New Member States will pay the corresponding contributions to the Fund. Contributions to the Fund by new Member States will be made in four instalments starting from 2006 (2006: 15%, 2007: 20%, 2008: 30%, 2009: 35%).

INSTITUTIONAL ISSUES
(a) Transitional measures

Council For the period between the date of accession and 31 December 2004, where the Council is required to act by a qualified majority, the votes of its members shall be weighted as set out in the table appearing at Annex I. Acts of the Council shall require for their adoption at least 88 votes in favour where the Treaty requires them to be adopted on a proposal from the Commission. In other cases, for their adoption, acts of the Council shall require at least 88 votes in favour, cast by at least two-thirds of the members.

Bulletin 28.10.2002

- EN -

PE 323.933


ANNEXES

25

In the event of fewer than ten new Member States acceding to the European Union under the forthcoming Treaty of Accession, the threshold for the qualified majority shall be fixed by Council decision so as to correspond as closely as possible to 71,26% of the total number of votes. European Parliament From the date of accession until the elections for the 2004-2009 term of the European Parliament, the total number of members of the European Parliament and the allocation of members for each Member State shall be fixed by applying the same method as the one used for the current seat allocation.
(b) Weighting of votes in the Council and qualified majority threshold

Where the Council is required to act by a qualified majority, the votes of its members shall, as from 1 January 2005, be weighted as set out in the table appearing in Annex II. As of the same date, acts of the Council shall require for their adoption at least 232 votes in favour, cast by a majority of the members, where the Treaty requires them to be adopted on a proposal from the Commission. In other cases, for their adoption, acts of the Council shall require at least 232 votes in favour, cast by at least two-thirds of the members. When a decision is to be adopted by the Council by a qualified majority, a member of the Council may request verification that the Member States constituting the qualified majority represent at least 62% of the total population of the Union. If that condition is shown not to have been met, the decision in question shall not be adopted. In the event of fewer than ten new Member States acceding to the European Union under the forthcoming Treaty of Accession, the threshold for the qualified majority shall be fixed by Council decision by applying a strictly linear, arithmetical interpolation, rounded up or down to the nearest vote, between 71% for a Council with 300 votes and the level foreseen above for an EU of 25 Member States (72,27%).
( c) European Parliament

From the start of the 2004-2009 term of the European Parliament, each Member State shall be allocated a number of seats representing the sum of: (i) and (ii) the seats resulting from the allocation of the 50 seats which will not be taken up by Bulgaria and Romania, which shall be distributed according to the provisions of the Nice Treaty. the seats allocated to it in Declaration No. 20 attached to the Final Act of the Nice Treaty

The total number of seats thus obtained shall be as close to 732 as possible and the allocations shall respect the balance between the current Member States established at Nice. The same proportional approach shall be applied to the allocation of seats to the new Member States. The approach shall also be equitable and shall respect the equilibrium between all Member States.

Bulletin 28.10.2002

- EN -

PE 323.933


26

ANNEXES

The application of this method shall not result in any of the current Member States receiving a higher allocation of seats than at present.
(d) Council Presidency

The EC treaty provides for the office of President to be held in turn by each Member State in the Council. In order to give new Member States the time to prepare for their Presidency the European Council confirms that the present rotation order will continue until the end of 2006. The Council will decide on the question of the order of Presidencies for 2007 and onwards as soon as possible and at the latest one year after accession of the first new Member States. o o o

Bulletin 28.10.2002

- EN -

PE 323.933


ANNEXES

27

Annex 1 to ANNEX I THE WEIGHTING OF VOTES IN THE COUNCIL FOR THE PERIOD BETWEEN ACCESSION AND 31 DECEMBER 2004

MEMBER STATES Germany United Kingdom France Italy Spain Poland Netherlands Greece Czech Republic Belgium Hungary Portugal Sweden Austria Slovakia Denmark Finland Ireland Lithuania Latvia Slovenia Estonia Cyprus Luxembourg Malta TOTAL

VOTES 10 10 10 10 8 8 5 5 5 5 5 5 4 4 3 3 3 3 3 3 3 3 2 2 2 124

________________________

Bulletin 28.10.2002

- EN -

PE 323.933


28

ANNEXES

Annex 2 to ANNEX I THE WEIGHTING OF VOTES IN THE COUNCIL AS FROM 1 JANUARY 2005

MEMBER STATES Germany United Kingdom France Italy Spain Poland Netherlands Greece Czech Republic Belgium Hungary Portugal Sweden Austria Slovakia Denmark Finland Ireland Lithuania Latvia Slovenia Estonia Cyprus Luxembourg Malta TOTAL

VOTES 29 29 29 29 27 27 13 12 12 12 12 12 10 10 7 7 7 7 7 4 4 4 4 4 3 321

________________________

Bulletin 28.10.2002

- EN -

PE 323.933


ANNEXES

29

ANNEX II ESDP: IMPLEMENTATION OF THE NICE PROVISIONS ON THE INVOLVEMENT OF THE NON-EU EUROPEAN ALLIES

Respect by certain EU Member States of their NATO obligations 1. The Treaty of the European Union states (Article 17.1): "The policy of the Union in accordance with this Article shall not prejudice the specific character of the security and defence policy of certain Member States and shall respect the obligations of certain Member States, which see their common defence realised in the North Atlantic Treaty Organisation (NATO), under the North Atlantic Treaty and be compatible with the common security and defence policy established within that framework." 2. For the Member States concerned, this means that the actions and decisions they undertake within the framework of EU military crisis management will respect at all times all their Treaty obligations as NATO allies. This also means that under no circumstances, nor in any crisis, will ESDP be used against an Ally, on the understanding, reciprocally, that NATO military crisis management will not undertake any action against the EU or its Member States. It is also understood that no action will be undertaken that would violate the principles of the Charter of the United Nations. Participation of the non-EU European Allies in peace-time ESDP consultations 3. As agreed at the Nice European Council, the EU will have permanent and continuing consultations with the non-EU European Allies, covering the full range of security, defence and crisis management issues. Additional 15 + 6 meetings will be arranged as required. In particular, consultations will, as appropriate, involve additional meetings in the format of EU + 6 in advance of PSC and EUMC meetings where decisions may be taken on matters affecting the security interests of the Non-EU European Allies. The objective of these consultations will be for the EU and the non-EU European Allies to exchange views, and to discuss any concerns and interests raised by these Allies, so as to enable the EU to take them into consideration. As with CFSP, these consultations will enable the non-EU European Allies to contribute to European Security and Defence Policy and to associate themselves with EU decisions, actions and declarations on ESDP. 4. The consultations between the EU and the non-EU European Allies will be carefully prepared, including by consultations involving the Presidency, Council Secretariat, and the representatives of the non-EU European Allies and through circulation of relevant documents. The meetings will be properly followed up, including through the circulation by the Council Secretariat of a record of the discussion. The objective of these arrangements is to ensure that the consultations are both comprehensive and intensive.

Bulletin 28.10.2002

- EN -

PE 323.933


30

ANNEXES

5. 15 + 6 meetings, as provided for in the Nice arrangements, will be facilitated through the appointment of permanent interlocutors with the PSC. To provide for dialogue with the EUMC and to help prepare the 15 + 6 meetings at Military Committee representative level, the non-EU European Allies may also designate interlocutors with the Military Committee. The designated interlocutors with the various EU bodies will be able, on a day-to-day basis, to pursue bilateral contacts, which will underpin the regular 15 + 6 consultations. Relations with the EUMS and national HQs involved in EU-led operations. 6. Arrangements in NATO for non-NATO EU members will be taken as a basis for developing appropriate arrangements for the non-EU European Allies in the EU military structures, bearing in mind the differences in military structures of the two organisations. If operational planning is conducted in NATO, the non-EU European Allies will be fully involved. If operational planning is conducted in one of the European strategic-level Headquarters, the non-EU European Allies as contributors will be invited to send officers to that Headquarters. Involvement in EU-led exercises 7. The EU does not intend to conduct military exercises below Force HQ (FHQ) level. Exercises below that level will remain the responsibility of member states. 8. The EU is committed to dialogue, consultation and cooperation with the non-EU European Allies and these arrangements will also need to be covered in relevant exercises. 9. Arrangements for the participation of those Allies in EU exercises will mirror those agreed for their participation in EU-led operations. Non-EU European Allies will be able to participate in EU exercises which envisage use of NATO assets and capabilities. Since there is also the possibility of their participation in EU-led operations which do not have recourse to NATO assets and capabilities, there will accordingly be a need for the non-EU European Allies to participate in relevant exercises and for the EU to provide for this. Non-EU European Allies should be invited to observe other relevant exercises in which they do not participate. Modalities for participation in EU-led operations 10. In considering the options for response to a crisis, including a possible EU-led operation, the EU would take account of the interests and concerns of non-EU European Allies and consultations between them would be sufficiently intensive to ensure this was the case. 11. In the case of an EU-led operation using NATO assets and capabilities, non-EU European Allies will, if they wish, participate in the operation, and will be involved in its planning and preparation in accordance with the procedures laid down within NATO.

Bulletin 28.10.2002

- EN -

PE 323.933


ANNEXES

31

12. In the case of any EU-led operation not requiring recourse to NATO assets and capabilities, non-EU European Allies will be invited, upon a decision of the Council, to participate. In taking decisions on participation, the Council will take account of the security concerns of the non-EU European Allies. In a specific case when any of the non-EU European Allies raises its concerns that an envisaged autonomous EU operation will be conducted in the geographic proximity of a non-EU European Ally or may affect its national security interests, the Council will consult with that Ally and, taking into consideration the outcome of those consultations, decide on the participation of that Ally, bearing in mind the relevant provisions of the Treaty on the European Union quoted above and the statement in paragraph 2 above. Involvement in preparation, planning and management of an EU-led operation 13. The 15 + 6 consultations would constitute a forum for the non-EU European Allies, as potential contributors to any EU-led military operation to be engaged from the earliest stages of a crisis in dialogue with the EU and to be consulted in the development of the EU's thinking. 14. Contacts at all levels with non-EU European Allies will be intensified as the pre-crisis stage unfolds through 15 + 6 consultations and other arrangements. This process will be important for discussing provisional military contributions from the non-EU European Allies during the pre-operational phase, and relevant military factors during the development of strategic military options, in order to inform the planning and preparation on which a Council decision to launch an EU-led operation will be based. This will enable the views of the non-EU European Allies, particularly their security concerns and their views on the nature of an EU response to the crisis, to be taken into account by the Council before decisions on a military option. 15. Subsequently, consultations would be carried forward together in the 15 + 6 forum, including at PSC and EUMC levels, to discuss the development of the Concept of Operations and related issues such as command and force structures. The non-EU European Allies would have the opportunity to make known their views about the CONOPS, and about their potential participation, before the Council took decisions to proceed to detailed planning of an operation and decisions formally to invite non-EU Member States to take part. Once decisions were taken on non-EU Member States' participation, non-EU European Allies, as contributors, would be invited to take part in operational planning. Consultations in the 15 + 6 forum would address the ongoing detailed planning of the operation, including the OPLAN. 16. Following a decision by the Council to undertake a military operation, and a force generation conference, the Committee of Contributors would be established and convened in order to discuss the finalisation of the initial operational plans and military preparations for the operation.

Bulletin 28.10.2002

- EN -

PE 323.933


32

ANNEXES

17. As foreseen in Nice, the Committee of Contributors will play a key role in the day-to-day management of the operation. It will be the main forum where contributing nations collectively address questions relating to the employment of their forces in an operation. The Committee will discuss reports from and issues raised by the Operation Commander and, as necessary, provide advice to the PSC. The Committee of Contributors takes decisions on the day-to-day management of the operation and makes recommendations on possible adjustments to operational planning, including possible adjustments to objectives, by consensus. The views expressed by the Committee of Contributors will be taken into account by the PSC as it considers issues of political control and strategic direction of the operation. The Council Secretariat will prepare a record of discussions at each Committee of Contributors meeting, which will be forwarded to PSC and EUMC representatives in time for the next meetings of the respective Committees. 18. The Operation Commander will report on the operation to the Committee of Contributors so that it can exercise its responsibilities and key role in the day to day management of the operation.

________________________

Bulletin 28.10.2002

- EN -

PE 323.933


ANNEXES

33

ANNEX III DECLARATION OF THE EUROPEAN COUNCIL The European Council is appalled and shocked by the ongoing hostage taking in the theatre on Melnikov Street in Moscow. Our thoughts and hearts are with the large number of civilians being held hostage, their relatives, the Russian People and the Russian Government. Taking innocent civilian people hostage is a cowardly and criminal act of terrorism, which cannot be defended or justified for any cause. The European Council strongly condemns this act of terrorism and fully supports the efforts by the Russian Government to solve the crisis and ensure the early and safe release of the hostages. The civilised world stands united in the fight against terrorism. The European Council is ready to further develop its strategic partnership with the Russian Federation, also in the fight against terrorism. We look forward to taking important decisions to this effect at the coming Russia-EU summit in Copenhagen.

________________________

Bulletin 28.10.2002

- EN -

PE 323.933


