SCHUFA HOLDING AG Bereich RRIE Abteilung Recht Dr. Wulf Kamlah Tel.: (0611) 92 78-320 Fax: (0611) 92 78-319 E-Mail: Wulf.Kamlah@schufa.de

Memorandum

24.10.2002 Comparison of provisions of the Consumer Credit Directive (draft) and the Data Protection Directive 95/46/EG

Consumer Credit Directive Art. 1 Purpose of the directive Harmonisation of laws regarding consumer credit agreements and credit surety agreements concluded by consumers.

Data Protection Directive Art. 1: Purpose of the directive Protection of privacy in the processing of personal data (Para. 1) No restrictions imposed on the free exchange of personal data on the basis of protection guaranteed by Para. 1

Remarks The processing of personal data is already sufficiently covered by the Data Protection Directive. Thus there is no need for provisions regarding the processing of personal data in the Consumer Credit Directive, and such provisions are not in keeping with its primary objective. Moreover, the restrictive provisions of the draft version of the Consumer Credit Directive conflict with the principle of the free exchange of data (Art. 1 Para. 2 of the Data Protection Directive), which is intended to foster development of the Single Market. The scope of the Data Protection Directive covers the processing of personal data in depth. Therefore, separate provisions regarding the processing of personal data � such as those proposed in the draft version of the Consumer Credit Directive � are superfluous. They also deviate in part from those of the Data Protection Directive (see below). This can lead to unjustified rivalries and discriminatory practices.

Art. 3 and Art. 2 Scope/definitions - Definition of credit agreement - Definition of creditor - Definition of consumer

Art. 3 Scope The DP Directive applies to partially or wholly automated processing of personal data and the non-automated processing of personal data stored or to be stored in files


Consumer Credit Directive

Data Protection Directive

Remarks W hereas Art. 6 of the Consumer Credit Directive cites � consistently and adequately � the provisions of the Data Protection Directive relating to the processing of data, Art. 7 of the Consumer Credit Directive restricts processing of to data collected pursuant to Art. 6 of the Consumer Credit Directive to a specific purpose (and thus conflicts with the system underlying the relationship between the two directives). Thus, for example, a change of purpose in accordance with Art. 6 Para. 1 lit. b) of the Data Protection Directive is entirely possible, while Art. 7 of the Consumer Credit Directive (unnecessarily) proscribes further processing for a different purpose. First of all, the term "personal" data is introduced in the Consumer Credit Directive in deviation from the terminology of the Data Protection Directive. Art. 6 of Data Protection Directive already dictates adherence to purpose in data processing and makes no distinction between consumers or commercial entities or the type of credit agreement in question. To grant consumers additional protection with respect to their individual rights with respect to credit agreements as provided for by the Consumer Credit Directive would seem inappropriate, especially in view of the fact that the distinction between consumers and persons acting in a commercial capacity is not always clear (e.g. small-scale traders and self-employed persons).

Art. 6 Para. 1 and Art. 7 Art. 6 contains detailed Art. 6 Para. 1 states that credit information provisions regarding purpose. can be demanded only insofar as it is required for the purpose of assessing the credit applicant's financial situation and ability to repay. Art. 7 defines the purpose for which information may be demanded in keeping with Art. 6.

Art. 8 Para. 3 Sentence 1, Art. 7: Purpose Art. 7 relates to the substance of Art. 6 regarding purpose (see above), but appears in the same section as Art. 8. This is misleading, since Art. 8 Para. 3 (specifically) defines the purpose for which information obtained in accordance with Art. 8 Para. 1 can be processed.

Art. 6 Para. 1 lit. b): Collection of personal data for clearly specified lawful purposes; no further processing in a manner incompatible with the purpose in question lit. c) Processing must correspond to the purpose for which data were collected and to Art. 8 Para. 3 Sentence 1: Personal data which the data are relevant. obtained from a central database may only be processed for the purpose of assessing the credit applicant's financial situation and ability to repay.

2/4


Consumer Credit Directive

Data Protection Directive

Remarks The Data Protection Directive recognises the necessity of storing personal data for specific purposes and processing such data during the life of a contract. This conflicts with the requirement that data collected for the purpose of assessing a person's financial situation and ability to repay be deleted, even if such data are required to meet contract obligations.

Art 8 Para. 3 Sentence 2: Obligation to Art. 6 Para.1 lit e) Personal data delete data immediately following may not be processed for a conclusion or denial of a credit agreement. period longer than that required to fulfil the purpose for which the data were collected and to which the data are relevant. Art. 7 lit. b) The processing of personal data is permitted, provided such processing is necessary in order to meet contractual obligations. Art. 8 Para. 1 Sentence 4: Information According to Art. 12 of the Data provided to concerned individuals at no Protection Directive, the charge individual concerned has the right to receive information as requested and without restriction at reasonable intervals, without significant delay and without incurring excessive costs.

The Data Protection Directive specifies the rights of individuals to obtain information regarding the use of their personal data. At the same time, it recognises in the interest of the responsible institution/agency that massive and repeated requests for information generate costs. Therefore, costs may be billed; the only requirement is that these costs not be excessive. This Consumer Credit Directive contradicts this reasonable distribution of interests in that it provides for the right to receive information at no cost.

3/4


Consumer Credit Directive

Data Protection Directive

Remarks

Art. 30: Maximum harmonisation and the Art. 1: Members States are Die Data Protection Directive obliges Member States imperative nature of the provisions of the obliged to guarantee the only to guarantee protection of privacy in accordance directive protection of privacy and free with the provisions of the Consumer Credit Directive. exchange of information. The Consumer Credit Directive does not provide for Art. 30 Para. 1: Member States may not such freedom in application of the relevant provisions. enact other provisions except with respect Moreover, the Consumer Credit Directive severely to Art. 8 Para. 4 and Art. 33 restricts the free exchange of data in favour of the protection of personal data. Art. 30 Para. 4: No surrender of rights specified in the Directive by the consumer Art. 7 lit. a): Personal data may be processed provided the individual concerned has given consent for such processing. The Data Protection Directive permits the processing of personal data provided the individual concerned has given consent for such processing. This is entirely in keeping with the principle of self-determination with regard to information. Hoever, if a consumer cannot surrender rights specified in the Consumer Credit Directive, his rights with respect to his personal data are no longer covered by the right of selfdetermination. In this sense, Art. 7 of Consumer Credit Directive is too restrictive, as the language of the restriction to purpose contained in Art. 7 does not relate to all data collected for the purpose of concluding or fulfilling a credit agreement.

Summary: A reading of the Directives clearly shows that Article 9 of the draft Consumer Credit Directive relates directly to Article 6. Articles 7 and 8 contain some provisions which deviate substantially from those of the Data Protection Directive pertaining to the processing of personal data.

4/4


