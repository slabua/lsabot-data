EUROPEAN PARLIAMENT DIRECTORATE GENERAL FOR RESEARCH DIVISION FOR INTERNATIONAL AND CONSTITUTIONAL AFFAIRS FACTSHEET ROMANIA Chapter 29 � Finance and Budgetary Provisions

1. The acquis1 The acquis in the field of financial and budgetary provisions effectively covers the rules concerning the organisation, the establishment and the implementation of the EU budget. The acquis under this chapter consists of regulations and decisions (e.g. the financial regulation, and the own resources decision) which will be directly applicable by candidate countries upon accession and, therefore, do not require transposition into their national legislation. However, the effective application of these regulations and decisions by the day of accession has to be ensured. This will to a large degree depend on measures covered by other chapters of the acquis. For example, the ability to adequately establish the traditional own resources (customs and agricultural duties) will mainly depend on progress under the customs and agriculture chapters; the basis for the correct calculation of the GNI-based contribution to the EU budget is covered by the statistics chapter; the VAT-based contribution will depend to a large degree on the chapter taxation, and measures needed to ensure adequate protection of the Community's financial interests are mainly covered by the financial control chapter. Effective administrative capacity for the co-ordination, calculation, collection, payment and control of own resources also has to be assured. This administrative function is usually carried out through a central co-ordinating 'Own Resource Unit' in the Finance Ministry and is the main area of preparatory action directly required of the candidate countries under this chapter. Though the acquis imposes no set model for the organisation and working of a country's public finances, every appropriate step must be taken to guarantee the sound financial management of Community budget resources. The specific national financial and budgetary measures needed to

1

Information largely drawn from the European Commission, DG Enlargement, http://europa.eu.int/comm/enlargement/negotiations/chapters/index.htm

1


guarantee the proper implementation of the various Community spending programmes must be examined and their application monitored in the context of negotiations on the relevant chapters. Following agreement on an overall financial package for the three year period 2007-2009 worth over 15 billion in favour of Bulgaria and Romania, both countries withdrew their requests for transitional arrangements for own resources contributions. With this financial package, both countries can expect to be significant net beneficiaries of the EU budget. 2. The Negotiations Chapter opened: December 2002 Status: closed in June 2004 3. Position of the European Parliament In its resolution of 4 December 2003 on the Council and Commission statements on the preparation of the European Council in Brussels on 12-13 December 20031, Parliament: � ...; is of the opinion that the financial framework for the accession of Bulgaria and Romania should be based on the same principles as apply to the ten countries acceding in 2004, and the principle that accession may not lead to a deterioration in the net budgetary position in comparison with the year preceding accession. 4. Latest Assessment of the European Commission2 Romania has made some progress since the last Regular Report. A sound framework for multiannual budgeting has been established and Romania should consolidate the progress made in the field of budget formulation and execution. At the same time, the structures and the administrative capacity to manage Romania's contribution to the EU budget do not yet exist. An adequately staffed and equipped co-ordinating body needs to be developed in order to prepare for application of the acquis on own resources.

June 2004

1 2

Minutes of the Plenary Session European Commission, 2003 Regular Report on Romania's progress towards accession: http://www.europa.eu.int/comm/enlargement/report_2003/pdf/rr_ro_final.pdf

2


