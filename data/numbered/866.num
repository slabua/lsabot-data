EUROPEAN PARLIAMENT
DIRECTORATE-GENERAL FOR RESEARCH DIRECTORATE A DIVISION FOR INTERNATIONAL AND CONSTITUTIONAL AFFAIRS

FACTSHEET
CYPRUS Chapter 21 - Regional policy and coordination of structural instruments 1. The acquis 1 This chapter covers mainly administrative and programming capacity and eligibility for funding. The acquis under this chapter does not define how the specific structures for the management of the structural and cohesion funds should be set up, but leaves it up to the individual Member States. The administrative structures required also differ depending on the size and degree of regionalisation of the country concerned. Candidate countries will need to put the necessary structures in place by the time of accession in order to benefit from the funds. A framework regulation (Council Regulation (EC) 1260/1999) lays down the general provisions concerning the structural funds. There is also a series of implementing regulations and decisions which do not have to be transposed into national law. However, on accession, the new Member States will have to comply with certain requirements which have been addressed in the accession negotiations: � Legislative framework: the candidate countries need to have the appropriate legal framework allowing for implementation of the specific provisions in this area; � territorial organisation: the candidate countries need to agree with the Commission on a provisional NUTS classification for the implementation of the structural funds; � programming capacity: the candidate countries need to design a development plan; put in place appropriate procedures for multi-annual programming of budgetary expenditure; ensure implementation of the principle of partnership at the different stages of provision of structural fund assistance; comply with the specific monitoring and evaluation requirements; � administrative capacity: the candidate countries have to define the tasks and responsibilities of all bodies and institutions involved and ensure effective inter-ministerial coordination;

1

Information largely drawn from the European Commission, DG Enlargement http://www.europa.eu.int/comm/enlargement/negotiations/chapters/index.htm

WIP/2002/11/0034

1


�

financial and budgetary management: the candidate countries need to comply with specific control provisions applicable to the funds and provide information on their co-financing capacity and on the level of public or equivalent expenditure for structural actions.

2. The negotiations The chapter has been closed with ten countries and remains open with Bulgaria and Romania. Chapter opened: April 2000 Status: Closed December 2002 (provisionally closed in April 2002) 3. Position of the European Parliament In its resolution of 5 September 20011, the European Parliament urges Cyprus to take the necessary steps to ensure the proper management of pre-accession funding and future structural funds. In its resolution of 13 June 20022, the European Parliament welcomes the proposal made in January 2001, as part of the Commission's common financial framework (2004-2006) for accession negotiations, to adjust the framework of funds to help the northern part of Cyprus catch up in its development after accession, in the context of a political settlement. 4. Latest Assessment by the European Commission3 In its 1998 Regular Report, the Commission concluded that the regional disparities between the northern and southern part of the island had further increased. The implementation of structural funds would have to address these disparities when a political solution was found. Since the 1998 Regular Report, due to the economic crisis in the northern part since 2000, the income gap with the southern part of the island has been aggravated. A political settlement on the basis of which these disparities could have been addressed has not yet been found. Meanwhile, the southern part of Cyprus has continued to make important progress in aligning with the acquis and preparing for participation in structural instruments, and few problems have been encountered. Cyprus has reached a good level of alignment with the acquis and is well advanced with respect to administrative structures. Negotiations on this chapter have been provisionally closed. Cyprus has not requested any transitional arrangements in this area. It is generally meeting the commitments it has made in the accession negotiations in this chapter. In order to complete preparations for membership, Cyprus's efforts should now focus on further preparing its Single Programming Documents. Therefore, it needs seriously to increase the resources devoted to this process. Furthermore, Cyprus needs to focus on improving the final
1 2

Resolution on the state of negotiations with Cyprus , � 24: A5-0261/2001 Resolution on the state of the enlargement negotiations 2002, � 41: A5-0190/2002 3 European Commission, Regular Report on Cyprus's progress towards accession 2002, pp. 96-97: http://www.europa.eu.int/comm/enlargement/report2002/cy_en.pdf

WIP/2002/11/0034

2


implementation structures and reinforcing their administrative capacity, and reinforcing systems and procedures for effective monitoring, financial management and control to enable the management of the Cohesion and Structural Funds.

January 2003

WIP/2002/11/0034

3


