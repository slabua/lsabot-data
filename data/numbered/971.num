EUROPEAN PARLIAMENT
DIRECTORATE-GENERAL FOR RESEARCH DIRECTORATE A DIVISION FOR INTERNATIONAL AND CONSTITUTIONAL AFFAIRS

FACTSHEET
ESTONIA 1. The acquis 1 With the exception of veterinary and phytosanitary legislation, mainly in the form of directives, most of the regulations and legislation covered by this chapter will be directly applicable at the date of accession and do not therefore call for transposition on the part of the candidate countries. The emphasis in the preparations for accession will therefore be on a country's ability to implement and enforce the acquis. The following main fields are covered: � � � � � Horizontal issues: Agricultural Guarantee and Guidance Funds; trade mechanisms; quality policy; organic farming; Farm Accountancy Data Network; State Aids. Common Market Organisations: arable crops; cereals, oilseeds and protein crops; non-food, processed cereals, potato starch, cereal substitutes and rice; sugar; fibre crops. Specialised crops: fruit and vegetables; wine and alcohol; bananas; olive oil; tobacco. Animal products: milk and milk products; beefmeat; sheepmeat; pigmeat; rural development. Veterinary legislation: control system in the internal market; identification and registration of animals; control at the external borders; animal disease control measures; animal health trade in live animals and animal products; public health protection; animal welfare; zootechnical legislation. Phytosanitary legislation: harmful organisms; quality of seeds and propagating material; plant variety rights; plant protection products/pesticides; animal nutrition. Chapter 7 - Agriculture

�

1

Information largely drawn from the European Commission, DG Enlargement http://www.europa.eu.int/comm/enlargement/negotiations/chapters/index.htm

WIP/2003/01/0078

1


2. The negotiations The chapter has been closed with ten countries, including Estonia, and remains open with Bulgaria and Romania. Chapter opened: June 2000 Status : Closed December 2002. Transitional arrangements: The key agreements reached in the negotiations with the ten countries which closed the chapter in December 2002 are: Financial and market related aspects of agriculture � The new member states will gradually phase in EU agricultural direct payments between 2004 and 2013. Direct payments will start at 25% in 2004, 30% in 2005 and 35% in 2006 of the present system and increase by 10 percentage steps to reach 100% of the then applicable EU level in 2013. � Within carefully defined limits, the new member states will have the option to "top-up" these EU direct payments with national subsidies. In 2004-2006, a new member state has the possibility to top up EU direct payments to � either 55% of EU level in the years 2004, 60% in 2005 and 65% in 2006. From 2007 the new member state may top-up EU direct payments by 30 percentage points above the applicable phasing-in level in the relevant year; � or to the total level of direct support the farmer would have been entitled to receive, on a product by product basis, in the new member state prior to accession under a like national scheme increased by 10 percentage points; In no case should the payment be higher than 100% of EU-15 level of direct payments. � Rather than applying the standard direct payment scheme applicable in the current EU, the new member states have the option, during a limited period, of granting their farmers CAP direct payments in the form of a decoupled area payment (a simplified payment scheme). An annual financial envelope will be calculated for the new member state according to agreed criteria and then divided between the utilised agricultural area. The new member states will have special additional financial aid for rural development for a limited period. This includes a higher proportion of EU co-financing in rural development projects. Certain rural development measures have been adapted or created in order to reflect better the requirements of the new member states in the first years of accession. This means that for a limited period, new member states will be able to use rural development funds for schemes specifically designed to help restructuring of the rural sector. For example, there is support for semi-subsistence farms undergoing restructuring as well as specific measures to assist farmers in meeting EU standards. Reference quantities (e.g. quotas, base areas) have been agreed for all the applicable products on the basis of recent production and taking into account country specific circumstances (e.g. drought). In a few specific instances, transitional periods were agreed for the adoption and implementation of certain parts of EU legislation. These transitional periods are exceptional and limited in time and in scope. 2

� �

� �

WIP/2003/01/0078


Veterinary and phytosanitary aspects of agriculture � Certain food establishments have been granted a transitional period in order to upgrade to fully meet EU requirements. These include 52 premises in the Czech Republic, 44 in Hungary, 117 in Latvia, 20 in Lithuania, 485 in Poland and 2 in Slovakia. Such transitional periods are limited in time and scope and do not involve any exemption from food hygiene legislation. During the transitional period, products which come from these establishments must be specially marked and cannot be marketed in any form in other EU countries. All establishments not subject to a transitional period will have to comply with the acquis on accession and their products will be able to be freely marketed within the EU. Certain establishments have been granted a transitional period in order to upgrade to fully meet structural requirements for hen cages (only the slope and height of the cages). Such transitional periods are limited in time and scope and are applicable in the Czech Republic, Hungary, Malta, Poland and Slovenia. There are also transitional periods in the field of phytosanitary legislation for Lithuania and Poland (potato ring rot and potato wart disease respectively) as well as transitional periods on certain parts of seed quality legislation for Malta, Cyprus, Latvia and Slovenia. Again, these transitional periods are limited in time and scope.

� �

�

3. Position of the European Parliament1 In its resolution of 4 October 20002, Parliament: � Notes that, in addition to having adverse effects on agriculture in developing countries, the EU's Common Agricultural Policy also distorts competition in candidate countries" markets for agricultural products; points out that this is particularly true in the case of Estonia, whose farmers receive minimal subsidies but have to compete against imports from the EU, sold at artificially low prices made possible only by massive subsidies; � Stresses that negotiations on liberalisation of trade, in particular in agricultural products, should duly take into account the development needs of Estonia's agricultural sectors; underlines that the restructuring of the agricultural sector and rural development measures must focus on the multiple functions of rural economies and the diversification of income in rural areas by appropriate use of local resources and specificities, while support for agricultural production has to integrate aspects of environmental protection, biodiversity and food quality. In its resolution of 5 September 20013, Parliament welcomes the fact that the Estonian SAPARD agency has finally been accredited; stresses the importance of integrated rural development for the enlargement process; notes with concern, however, the rural population's lack of information on, and growing dissatisfaction; calls on the Commission to help ensure that there is a considerable improvement in the information provided to the rural population on rural development plans and in its involvement in them.

1 2

For Parliament's position on enlargement and agriculture, see the resolution of 13 June 2002 : A5-0200/2002 Resolution on the state of negotiations with Estonia, � 9 & 10: A5-0238/2000 3 Resolution on the state of negotiations with Estonia, � 27: A5-0251/2001

WIP/2003/01/0078

3


In its resolution of 20 November 20021, Parliament: � reiterates its support for the objective of gradually phasing in direct payments for farmers in the new Member States; recalls that the estimated agricultural expenditure related to enlargement can be accommodated within the Berlin agreement, consisting of commitments in 2005 of some EUR 1.1 billion in total direct payments, some EUR 750 million for market expenditure and some EUR 1.6 billion for rural development; � congratulates Estonia on the success of its strongly market-oriented economic and commercial policies; notes that Estonian farmers have received virtually no subsidies and have been exposed to competition from imports from the EU sold at artificially low prices made possible only by the EU's export subsidies; notes, also, that a slump in demand in the important Russian export market caused a contraction of Estonian agricultural production in the years later chosen as reference years for the setting of production quotas in the framework of the CAP; considers that the EU must take full account of the above in the negotiations and agree to quotas at an appropriate level for Estonian farmers. 4. Latest Assessment by the European Commission
2

In its 1997 Opinion, the Commission concluded that substantial efforts to align with the acquis were still necessary, although progress had been made in adopting the measures mentioned in the Commission's White Paper of 1995 on the Internal Market. The Commission added that particular efforts were needed in the following areas: the implementation and enforcement of veterinary and phytosanitary requirements and upgrading establishments to meet EC standards; strengthening administrative structures to ensure the necessary capacity to implement and enforce CAP policy instruments, including the import arrangements; and further restructuring of the agri-food sector to improve its competitive capacity. The Commission further pointed out that since only a limited number of the mechanisms of the Common Agricultural Policy existed at that point in time, fundamental reform of policy would be needed, and a substantial effort would be necessary to prepare for accession in the medium term. Since the Opinion, Estonia has made considerable progress towards alignment with the EC agricultural acquis, in particular over the past two years, and, more recently, has progressed with developing the necessary administrative capacity to implement the acquis in this area. Overall, legislation in this area is now largely aligned and Estonia has made good preparations in developing the required administrative capacity, but further significant efforts will be required. Negotiations on this chapter continue, although all negotiation issues in the veterinary and phytosanitary fields have been clarified. Estonia is generally meeting the commitments it has made in the accession negotiations in this field. In order to complete preparations for membership, Estonia's efforts now need to focus on: ensuring close cross-departmental co-operation, availability of competent staff and efficient project management (within both the Ministry of Agriculture and beneficiary institutions, such as the Agricultural Registers and Information Board) in particular for the paying agency, the Integrated Administration and Control System, Common Market Organisations price support
1 2

Resolution on the progress of the candidate countries towards accession, � 30 & 62: A5-0371/2002 European Commission, Regular Report on Estonia 2002: pp. 64 & 65: http://www.europa.eu.int/comm/enlargement/report2002/ee_en.pdf

WIP/2003/01/0078

4


measures, trade mechanisms and the milk quota system; further reinforcing the administrative capacity to implement and enforce the Common Agricultural Policy acquis, in particular in the veterinary field and that of food safety; completing legislative alignment; and ensuring that establishments are duly upgraded to meet Community standards.

January 2003

WIP/2003/01/0078

5


